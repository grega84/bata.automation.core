package com.bata.authlogin;

import java.time.Duration;

import com.bata.pagepattern.DinamicData;

import bean.datatable.CredentialBean;
import common.CommonFields;

public class AuthLoginThread extends Thread 
{
	
	public AuthLoginThread(String baseImageToFindImage,CredentialBean bean,Duration d) 
	{
		super(new Runnable() {

			@Override
			public void run() 
			{
				AuthLogin page = new AuthLogin();
				
				DinamicData.getIstance().set(CommonFields.AUTHLOGIN_IMAGE_FOUND, false);
				
				try
				{
					long toTime=d.toMillis();
					
					while(toTime >= 0)
					{
						if(page.LoginFormIsVisible(baseImageToFindImage))
						{
							page.login(bean.getAuthUsername(), bean.getAuthPassword());
							DinamicData.getIstance().set(CommonFields.AUTHLOGIN_IMAGE_FOUND, true);
							break;
						}
						
						Thread.sleep(500);
						
						toTime -= 500;
					}
				}
				catch(Exception err)
				{
					err.printStackTrace();
					DinamicData.getIstance().set(CommonFields.AUTHLOGIN_IMAGE_FOUND, false);
				}
				
			}
			
		});
		
	}
}