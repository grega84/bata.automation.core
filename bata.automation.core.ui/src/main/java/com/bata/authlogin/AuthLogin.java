package com.bata.authlogin;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.junit.Assert;

import test.automation.core.ImageUtils;
import test.automation.core.SystemClipboard;

public class AuthLogin 
{
	private Robot robot;

	public AuthLogin()
	{
		try {
			robot=new Robot();
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean LoginFormIsVisible(String path) throws Exception
	{
		//System.out.println(System.getProperty("user.dir"));
		//String path=baseImageToFindPath+"/dev-cc.bata.cz_authLogin.png";
		
		String imageToFind=new File(path).getAbsolutePath();
		Dimension dimension=Toolkit.getDefaultToolkit().getScreenSize();
		java.awt.Rectangle rectangle=new java.awt.Rectangle(dimension);
		
		BufferedImage screen=robot.createScreenCapture(rectangle);
		File temp = File.createTempFile("temp-screen-captured", ".png");
		Assert.assertTrue(temp != null);
		
		ImageIO.write(screen, "PNG", temp);
		
		return ImageUtils.image().findImage(imageToFind, temp.getAbsolutePath(), 0.75);
	}
	
	public void login(String username,String password)
	{
		
		try {
			//Robot robot=new Robot();
			
			SystemClipboard.copyAndPaste(username);
			
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
			
			Thread.sleep(1000);
			
			SystemClipboard.copyAndPaste(password);
			
			Thread.sleep(1000);
			
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}