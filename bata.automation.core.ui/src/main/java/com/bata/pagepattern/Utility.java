package com.bata.pagepattern;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.function.Function;

import org.apache.commons.lang.text.StrSubstitutor;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import com.bata.authlogin.AuthLoginThread;

import bean.datatable.CredentialBean;
import common.CommonFields;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import test.automation.core.UIUtils;

public class Utility {
	
	private static final String SCROLL_TO_ELEMENT = "arguments[0].scrollIntoView({behavior: \"smooth\",block: \"end\",inline: \"end\"});";
	
	public static String getAlphaNumericString(int n) 
    { 
  
        // length is bounded by 256 Character 
        byte[] array = new byte[256]; 
        new Random().nextBytes(array); 
  
        String randomString 
            = new String(array, Charset.forName("UTF-8")); 
  
        // Create a StringBuffer to store the result 
        StringBuffer r = new StringBuffer(); 
  
        // remove all spacial char 
        String  AlphaNumericString 
            = randomString 
                  .replaceAll("[^A-Za-z0-9]", ""); 
  
        // Append first 20 alphanumeric characters 
        // from the generated random String into the result 
        for (int k = 0; k < AlphaNumericString.length(); k++) { 
  
            if (Character.isLetter(AlphaNumericString.charAt(k)) 
                    && (n > 0) 
                || Character.isDigit(AlphaNumericString.charAt(k)) 
                       && (n > 0)) { 
  
                r.append(AlphaNumericString.charAt(k)); 
                n--; 
            } 
        } 
  
        // return the resultant string 
        return r.toString(); 
    }
	
	public static String generateRandomDigits(int n) 
	{
		 // length is bounded by 256 Character 
        byte[] array = new byte[256]; 
        new Random().nextBytes(array); 
  
        String randomString 
            = new String(array, Charset.forName("UTF-8")); 
  
        // Create a StringBuffer to store the result 
        StringBuffer r = new StringBuffer(); 
  
        // remove all spacial char 
        String  AlphaNumericString 
            = randomString 
                  .replaceAll("[^A-Za-z0-9]", ""); 
  
        // Append first 20 alphanumeric characters 
        // from the generated random String into the result 
        for (int k = 0; k < AlphaNumericString.length(); k++) { 
  
            if (Character.isDigit(AlphaNumericString.charAt(k)) 
                       && (n > 0)) { 
  
                r.append(AlphaNumericString.charAt(k)); 
                n--; 
            } 
        } 
  
        // return the resultant string 
        return r.toString(); 
	}

	public static String replacePlaceHolders(String str, Entry ... placeholders)
	{
		HashMap<String,String> map=new HashMap<String,String>();
		
		for(Entry p : placeholders)
		{
			map.put(p.getKey(), p.getValue());
		}
		
		return StrSubstitutor.replace(str, map);
	}
	
	public static WebElement scrollToWebElement(WebDriver driver,WebElement element)
	{
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		
		jse.executeScript(SCROLL_TO_ELEMENT, element);
		return UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOf(element));
	}
	
	public static Properties getLanguageLocators(String country) 
	{
		InputStream authConfigStream = Utility.class.getClassLoader().getResourceAsStream("com/bata/language/locators-"+country+".properties");

	    if (authConfigStream != null) {
	      Properties prov = new Properties();
	      try 
	      {
	        prov.load(authConfigStream);
	        
	        return prov;
	        
	      } catch (Exception e) 
	      {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
	        try {
				authConfigStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	      }
	    }
	    
		return null;
	}
	
	public static double parseCurrency(final String amount, final String country)
	{  
	    try {
	    	final NumberFormat format = NumberFormat.getNumberInstance(getLocale(country));
		    if (format instanceof DecimalFormat) {
		        ((DecimalFormat) format).setParseBigDecimal(true);
		    }
			return format.parse(amount.replaceAll("[^\\d.,]","")).doubleValue();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    return -1;
	}
	
	private static Locale getLocale(String country) 
	{
		switch(country.toLowerCase())
		{
		case "ita":
			return Locale.ITALY;
			default:
				return Locale.ITALY;
		}
	}

	public static String toCurrency(double d, Locale locale) 
	{
		NumberFormat format = NumberFormat.getNumberInstance(locale);
		
		return format.format(d);
	}
	
	public static double round(double value, int places) 
	{
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	public static void closeEditPageMenu(WebDriver driver)
	{
		try
		{
			FluentWait wait=new FluentWait(driver);
			wait.withTimeout(Duration.ofSeconds(5));
			wait.pollingEvery(Duration.ofSeconds(1));
			
			wait.until(new Function<WebDriver,Boolean>(){

				@Override
				public Boolean apply(WebDriver arg0) 
				{
					return arg0.findElement(By.xpath("//iframe[@id='DW-SFToolkit']")) != null;
				}
				
			});
			
			driver.switchTo().frame("DW-SFToolkit");
			driver.findElement(By.id("ext-gen16")).click();
			driver.switchTo().defaultContent();
		}
		catch(Exception err)
		{
			err.printStackTrace();
			driver.switchTo().defaultContent();
		}
	}
	
	public static void IOSStagingAccess(IOSDriver<?> driver,String username,String password)
	{
		driver.context("NATIVE_APP");
		
		try
		{
			MobileElement e=(MobileElement) driver.findElement(By.xpath("//*[@placeholder='Nome utente' and ./parent::*[@placeholder='Nome utente']]"));
			
			e.sendKeys(username);
			
			Thread.sleep(1000);
			
			e=(MobileElement) driver.findElement(By.xpath("//*[@placeholder='Password' and ./parent::*[@placeholder='Password']]"));
			
			e.sendKeys(password);
			
			Thread.sleep(1000);
			
			driver.findElement(By.xpath("//*[@text='Accedi']")).click();
			
			
		}
		catch(Exception err)
		{
			
		}
		
		driver.context("WEBVIEW_1");
	}

	public static void IOSCloseSafariPanel(IOSDriver<?> driver) 
	{
		driver.context("NATIVE_APP");
		
		driver.findElement(By.xpath("//*[@text='Pannelli' or @text='Fine']")).click();
		
		try {Thread.sleep(2000);} catch (InterruptedException e) {}
		
		try
		{
			driver.findElement(By.xpath("//*[@name='closeTabButton']")).click();
		}catch(Exception err)
		{
			driver.findElement(By.xpath("//*[@text='Fine']")).click();
		}
		
		driver.context("WEBVIEW_1");
	}

	public static void desktopStagingAccess(WebDriver driver, List<CredentialBean> table,String baseImageToFindURL,String typeOfTest) throws Exception 
	{
		if(Locators.getDriverType(driver) == Locators.DESKTOP_IE)
	    {
	    	Duration maxTime=Duration.ofSeconds(30);
	    	
	    	AuthLoginThread thread=new AuthLoginThread(baseImageToFindURL, table.get(0), maxTime);
	    	thread.start();
	    	
	    	boolean loginOK=(DinamicData.getIstance().get(CommonFields.AUTHLOGIN_IMAGE_FOUND) != null) ? (boolean) DinamicData.getIstance().get(CommonFields.AUTHLOGIN_IMAGE_FOUND) : false;
	    	
	    	long time=maxTime.toMillis();
	    	
	    	while(time > 0)
	    	{
	    		if(loginOK)
	    		{
	    			thread.interrupt();
	    			break;
	    		}
	    		
	    		loginOK=(DinamicData.getIstance().get(CommonFields.AUTHLOGIN_IMAGE_FOUND) != null) ? (boolean) DinamicData.getIstance().get(CommonFields.AUTHLOGIN_IMAGE_FOUND) : false;
		    	
	    		Thread.sleep(Duration.ofSeconds(1).toMillis());
	    		
	    		time -= Duration.ofSeconds(1).toMillis();
	    	}
	    }
	    
    	if (typeOfTest.equals("webTest") && Locators.getDriverType(driver) != Locators.DESKTOP_IE)
	    	driver.manage().window().maximize();
	}

	public static void waitNewWindowAndGoToIt(WebDriver driver, String winHandler) 
	{
		//attendo apertura nuova finestra
		UIUtils.ui().waitForCondition(driver, new ExpectedCondition<Boolean>() {
			
			@Override
			public Boolean apply(WebDriver driver) 
			{
				Set<String> winId = driver.getWindowHandles();
                
				if(winId.size() > 1)
                {
                    return true;
                }
                
                return false;
			}
			
		}, 120);
		
		Set<String> winId = driver.getWindowHandles();
		System.out.println("windows:"+winId.toString());
		
		for(String s : winId)
		{
			driver.switchTo().window(s);
			
			if(!s.equals(winHandler))
			{
				break;
			}
		}
		
		System.out.println("prev:"+winHandler);
		System.out.println("next:"+driver.getWindowHandle());
	}
}
