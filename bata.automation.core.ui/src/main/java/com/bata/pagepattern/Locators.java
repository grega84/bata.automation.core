package com.bata.pagepattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

public class Locators {

	public interface ILocator
	{
		public String getAndroidLocator();
		public String getIOSLocator();
		public String getWebLocator();
	}
	
	public static final int DESKTOP_CHROME=0;
	public static final int DESKTOP_IE=1;
	public static final int DESKTOP_SAFARI=2;
	public static final int ANDROID_CHROME=3;
	public static final int IOS_SAFARI=4;
	public static final String IPAD_MINI_2 = "1705ec145391df37ccda2f761559b3318f54dca1";
	
	public enum GlobalHeader implements ILocator
	{
		CUSTOMER_SERVICE("//div[@class='b-customer-services__promo']","",""),
		SHOPS("//nav[@class='b-customer-services__menu']//a[@class='b-customer-services__link icon-locator-small']","",""),
		CONTACTS("//nav[@class='b-customer-services__menu']//a[@class='b-customer-services__link'][contains(text(),'${contacts}')]","",""),
		NEWSLETTER("//nav[@class='b-customer-services__menu']//a[@class='b-customer-services__link'][contains(text(),'${newsletters}')]","",""),
		BATACLUB("//nav[@class='b-customer-services__menu']//a[@class='b-customer-services__link'][contains(text(),'${bataclub}')]","","");

		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;

		GlobalHeader(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}

		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}
	}


	public enum Homepage implements ILocator
	{
		/*ciao*/
		CUSTOMER_SERVICE("//div[@class='b-customer-services__promo']","",""),
		SHOPS("//nav[@class='b-customer-services__menu']//a[@class='b-customer-services__link icon-locator-small']","//a[@class='b-utility-menu__link icon-locator-small']",""),
		CONTACTS("//nav[@class='b-customer-services__menu']//a[@class='b-customer-services__link'][contains(text(),'${contacts}')]","//li[@class='b-menu__item b-customer-services_hamburger h-visible-small']//a[@class='b-customer-services__link'][contains(text(),'${contacts}')]",""),
		NEWSLETTER("//nav[@class='b-customer-services__menu']//a[@class='b-customer-services__link'][contains(text(),'${newsletters}')]","",""),
		BATACLUB("//nav[@class='b-customer-services__menu']//a[@class='b-customer-services__link'][contains(text(),'${bataclub}')]","",""),
		LOGO("//img[@class='b-header__logo-image']","",""),
		MYACCOUNT("//ul[@class='b-utility-menu__list js-menu-utility-user-head']//a[contains(@class,'b-utility-menu__link icon-account')]","",""),
		WISHLIST("//ul[@class='b-utility-menu__list js-menu-utility-user-head']//a[@class='b-utility-menu__link icon-wishlist']","",""),
		ICONBAG("//a[contains(@class,'b-utility-menu__link icon-bag')]","",""),
		SEARCH_BAR("//input[@id='q']","",""),
		SEARCH_ICON("//button[@class='b-search__button-search']","//a[@class='b-utility-menu__link icon-search js-search-icon js-cmp-inited js-cmp-searchIcon']",""),
		ICON_SEARCH_BAR("//div[@class='b-search__buttons']","",""),
		CLOSE_COOKIE_BUTTON("//button[@class='b-btn_primary js-cookies-bar-close']","",""),
		TRACK_AN_ORDER_FOOTER_LINK("//a[contains(text(),'${trackAnOrder}')]","",""),
		SUGGESTION_PRODUCT("//a[@class='b-suggestions__link']","",""),
		NO_MORE_ELEMENT_TEXT("//span[@class='b-suggestions__message' and contains(text(),'${noMoreElementText}')]","",""),
		POPULAR_SEARCH_DIV("//div[@class='b-suggestions__hitgroup' and ./child::div[contains(text(),'${popularSearchHeader}')]]","",""),
		POPULAR_SEARCH_FIRST("//div[@class='b-suggestions__hitgroup' and ./child::div[contains(text(),'${popularSearchHeader}')]]//a[1]","",""),
		CATEGORY_SEARCH_DIV("//div[@class='b-suggestions__hitgroup' and ./child::div[contains(text(),'${categorySearchHeader}')]]","",""),
		CATEGORY_SEARCH_FIRST("//div[@class='b-suggestions__hitgroup' and ./child::div[contains(text(),'${categorySearchHeader}')]]//a[1]","",""),
		SUGGESTION_SEARCH_LINK("//a[./child::span[@class='corrected' or @class='completed']]","",""),
		MOBILE_LEFT_MENU("//button[@class='b-header__menu-button js-menu-toggle']","",""),
		MOBILE_LEFT_MENU_CLOSE("//button[@class='b-header__menu-button js-menu-toggle b-header__menu-button_active']","",""),
		MOBILE_WISHLIST_ICON("//ul[@class='b-utility-menu__list js-bottom-account']//li[@class='b-utility-menu__item b-utility-menu_wishlist']","","");

		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;

		Homepage(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}

		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}
	}

	public enum myAccountLogo implements ILocator
	{
		LOGO("//img[@class='b-header__logo-image']","",""),
		MYACCOUNT("(//a[@class='b-utility-menu__link icon-account js-user-account'])[1]","",""),
		HAMBURGER_MENU("","//button[@class='b-header__menu-button js-menu-toggle']",""),
		ACCOUNT("","(//a[@class='b-utility-menu__link icon-account js-user-account'])[2]",""),
		ESCI("(//ul[@class='b-utility-menu__list js-menu-utility-user-head']//a[@class='b-utility-menu__flyout-link null'])[4]","(//ul[@class='b-utility-menu__flyout-list'])[3]/li[4]/a","");
		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;

		myAccountLogo(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}

		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}
	}

	
	
	public enum Contacts implements ILocator
	{
		TITLE("//h1[@class='h-text-center']","",""),
		ADDRESS_CONTACT("//section[@class='b-content__contacts']//div[@class='b-content__contact'][1]","",""),
		PHONE_CONTACT("//section[@class='b-content__contacts']//div[@class='b-content__contact'][2]","",""),
		EMAIL_CONTACT("//section[@class='b-content__contacts']//div[@class='b-content__contact'][3]","",""),
		FAQ("//section[@class='b-content__contacts']//div[@class='b-content__contact'][4]","",""),
		TITLE_FORM("//div[@class='b-content__description'][2]","",""),
		EMAIL_INPUT("//input[@id='dwfrm_contactus_email']","",""),
		PHONE_INPUT("//input[@id='dwfrm_contactus_phone']","",""),
		NOME_INPUT("//input[@id='dwfrm_contactus_firstname']","",""),
		SURNAME_INPUT("//input[@id='dwfrm_contactus_lastname']","",""),
		RAGIONE_INPUT("//select[@id='dwfrm_contactus_reason']","",""),
		COMMENT_INPUT("//textarea[@id='dwfrm_contactus_comment']","",""),
		CHECKBOX("//label[@for='dwfrm_contactus_personaldata']","",""),
		INVIA_BUTTON("//button[@id='sendBtn']","",""),
		SEND_MAIL_POPUP_DIV("//div[@class='ui-dialog-content_wrapper']","",""),
		SEND_MAIL_POPUP_TITLE("//span[@id='ui-id-9']","",""),
		SEND_MAIL_POPUP_BODY("//div[@id='dialog-container']","",""),
		SEND_MAIL_POPUP_OK_BUTTON("//button[@class='b-btn_primary ui-button ui-corner-all ui-widget']","","");
		
		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;

		Contacts(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}

		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}
	}

	
	public enum ProductPage implements ILocator
	{
		TITLE_PRODUCT("//div[@class='b-pdp__product-label']","",""),
		BRAND_PRODUCT("//h1[@class='b-pdp__product-title']","",""),
		SHOPS("//nav[@class='b-customer-services__menu']//a[@class='b-customer-services__link icon-locator-small']","",""),
		ERROR_ADD_TO_WISHLIST("//div[@class='b-pdp__availability b-pdp__availability-select js-reminder-hint js-cmp-inited js-cmp-block']","",""),
		LIST_SIZE("//ul[@class='swatches b-size-selector__list b-size-selector_large']","",""),
		SIZE_PRODUCT("//ul[@class='swatches b-size-selector__list b-size-selector_large']//a[not(contains(@class, 'disable'))][1]","",""),
		SIZE_PRODUCT_BY_VALUE("//ul[@class='swatches b-size-selector__list b-size-selector_large']//a[not(contains(@class, 'disable')) and @title='${size}']","",""),
		ADD_TO_CART("//button[@data-cmp-id='addToCartButton']","",""),
		COLOR_PRODUCT("//div[@class='b-color-selector__value']","",""),
		ID_PRODUCT("//div[@class='b-pdp__description-article']//span","",""),
		BRAND_PRODUCT_INFO("//div[@class='b-pdp__description-brand']//span","",""),
		ADD_TO_WISHLIST("//a[@data-id='addToWishlist']","",""),
		REMOVE_TO_WISHLIST("//a[@class='b-wishlist js-cmp-inited js-cmp-addToWishlist b-wishlist_added']","",""),
		FIND_IN_STORE("//button[@class='b-pdp__findinstore b-btn_third js-cmp-inited js-cmp-findInStoreButton']","",""),
		MINI_CART(/*"//div[@class='js-minicart-wrapper js-cmp-inited js-cmp-minicartWrapper']"*/"//a[@class='b-utility-menu__link icon-bag js-dropdown-button']","",""),
		WISHLIST_ICON("//ul[@class='b-utility-menu__list js-menu-utility-user-head']//a[@class='b-utility-menu__link icon-wishlist']","",""),
		ARTICLE_NUMBER("//div[@class='b-pdp__description-article']","",""),
		IN_STORE_BUTTON("//button[@data-cmp='findInStoreButton']","","");
		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;

		ProductPage(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}

		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}
	}

	
	public enum MiniCartProductPage implements ILocator
	{
		TITLE_MINI_CART("//div[@class='b-minicart__title']","",""),
		SHOW_CART_BUTTON("//div[@class='b-minicart__content js-minicart-content']//a[contains(@class, 'minicart__view-bag')]","//a[@class='b-utility-menu__link icon-bag js-dropdown-button']",""), //lista di due pulsanti uguali, da prendere il primo.
		PROCEDE_TO_CHECKOUT("//form[@data-cmp-id='proceedToCheckoutForm']","",""),
		CLOSE_MINI_CART ("//a[@class='b-minicart__close js-minicart-close-btn']","",""),
		PRODUCT_NAME("//h1[@class='b-pdp__product-title']","",""),
		PRODUCT_PRICE("//div[@id='product-content']//span[@class='b-price__sale']","",""),
		PRODUCT_ELEMENT("//li[contains(@class,'b-minicart__item')]","",""),
		PRODUCT_ELEMENT_TITLE("//div[@class='b-minicart__product-title']//a","",""),
		PRODUCT_ELEMENT_SIZE("//div[@data-attribute='size']//span[@class='b-product__value b-product-list__value']","",""),
		PRODUCT_ELEMENT_PRICE("//span[@class='mini-cart-price']","",""),
		PRICE("//div[@class='b-minicart__subtotal-price']","","");
		
		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;
		
		MiniCartProductPage(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}
		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}
	}
	
	public enum Cart implements ILocator
	{
		TITLE_CART("//h1[@class='b-cart__title']","",""),
		IN_STOCK("//span[@class='b-product-list__message_in-stock is-in-stock' or @class='b-product-list__message is-in-stock h-color-red']","",""),
		QUANTITY("//table[@id='cart-table']//tr[#]//td[4]//span[@class='qty-value js-qty-value']","",""),
		DELETE_ITEM("//i[@class='icon-close-remove']","",""),
		TITLE_COUPON("//h4[@class='b-cart__coupon-title']","",""),
		INPUT_COUPON("//input[@id='dwfrm_cart_couponCode']","",""),
		APPLY_BUTTON("//button[@name='dwfrm_cart_addCoupon']","",""),
		TOTAL_PRICE("//td[@class='b-order-summary__total-value']","",""),
		CONTINUE_SHOPPING("//button[@name='dwfrm_cart_continueShopping']","",""),
		PROCEDE_TO_CHECKOUT("//form[@id='checkout-form']","",""),
		ERROR_COUPON("//div[@class='b-cart__actions-message b-cart__actions-error js-coupon-error']","",""),
	    CLOSE_ERROR_VOUCHER("//span[@class='b-cart__actions-link js-close-coupon-error js-coupon-error']","",""),
	    ARROW_UP("//table[@id='cart-table']//tr[#]//td[4]//span[@class='js-qty-incr b-stepper__plus']","",""),
	    ARROW_DOWN("//table[@id='cart-table']//tr[#]//td[4]//span[@class='b-stepper__minus js-qty-decr']","",""),
	    REMOVE_ITEM("//i[@class='icon-close-remove']","",""),
	    CART_TABLE("//table[@id='cart-table']","",""),
	    CART_ELEMENT("//table[@id='cart-table']//tr","",""),
	    CART_ELEMENT_TITLE("//table[@id='cart-table']//tr[#]//div[@class='b-product-list__name']","",""),
	    CART_ELEMENT_SIZE("//table[@id='cart-table']//tr[#]//td[@class='b-product-list__cell b-product-list__dscr']//div[@class='b-product-list__details product-list-item']//div[@class='b-product__attribute b-product-list__attribute'][3]//span[@class='b-product__value b-product-list__value']","",""),
		CART_ELEMENT_PRICE("//table[@id='cart-table']//tr[#]//span[@class='b-price__sale']","",""),
		CART_ELEMENT_ID("//table[@id='cart-table']//tr[#]//td[2]//div[2]//span[2]","",""),
		CART_ELEMENT_BRAND("//table[@id='cart-table']//tr[#]//td[2]//div[3]//span[2]","",""),
		CART_ELEMENT_COLOR("//table[@id='cart-table']//tr[#]//td[2]//div[5]//span[2]","",""),
		CART_ELEMENT_QUANTITY("//table[@id='cart-table']//tr[#]//td[4]//span[@class='qty-value js-qty-value']","",""),
	    EMPTY_CART("//div[@class='b-cart__header b-cart__header-empty']","",""),
		ADD_TO_WISHLIST("a[@name='dwfrm_cart_shipments_i0_items_i0_addToWishList']","",""),
		COUPON_ERROR_MESSAGE("//div[@class='b-checkout-billing__coupon-error js-coupon-error-message']","","");
		
		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;
		
		Cart(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}
		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}
	}
	
	public enum ShippingMethod implements ILocator
	{
		TITLE("//h1[@class='b-checkout-tabs__title']","",""),
		SHOPSHIPPING("//div[@class='b-checkout-tabs__tab b-checkout-tabs__tab_second']//span[@class='b-checkout-tabs__tab-input']","",""),
		HOMESHIPPING("//div[@class='b-checkout-tabs__tab b-checkout-tabs__tab_first']//span[@class='b-checkout-tabs__tab-input']","",""),
		LABEL_POSITION("//h3[@class='b-storelocator__results-text']","",""),
		INPUT_POSITION("//input[@class='b-storelocator__search js-autocomplete-input js-input_field ui-autocomplete-input']","",""),
		SEARCH_POSITION("//button[@class='b-storelocator__search-button']","",""),
		TITLE_DELIVERY("//legend[@class='b-register-form__title']","",""),
		EMAIL_LABEL("//div[@class='b-checkout-tabs__tab-content b-checkout-tabs__tab-content_first']//span[@class='b-label__value'][contains(text(),'E-mail')]","",""),
		INPUT_EMAIL("//div[@class='b-checkout-tabs__tab-content b-checkout-tabs__tab-content_first']//input[@id='dwfrm_billing_billingAddress_email_emailAddress']","",""),
		ERROR_EMAIL("//div[contains(text(),'Inserisci un indirizzo e-mail')]","",""),
		ERROR_EMAIL_2("//div[@class='b-form__row b-form__row_half required b-form__row_required js-cmp-inited js-cmp-inputEmail state-error']//input[@id='dwfrm_billing_billingAddress_email_emailAddress']//following::div[@class='form-row_error b-error-msg'][1]","",""),
		INPUT_CELL("//div[@class='b-checkout-tabs__tab-content b-checkout-tabs__tab-content_first']//input[@id='dwfrm_singleshipping_shippingAddress_addressFields_phone']","",""),
		INPUT_NAME("//div[@class='js-address-fields b-checkout__address-form_active']//input[@id='dwfrm_singleshipping_shippingAddress_addressFields_firstName']","",""),
		INPUT_SURNAME("//div[@class='js-address-fields b-checkout__address-form_active']//input[@id='dwfrm_singleshipping_shippingAddress_addressFields_lastName']","",""),
		INPUT_CAP("//input[@id='dwfrm_singleshipping_shippingAddress_addressFields_postal']","",""),
		INPUT_ADDRESS("//input[@id='dwfrm_singleshipping_shippingAddress_addressFields_address1']","",""),
		INPUT_CITY("//input[@id='dwfrm_singleshipping_shippingAddress_addressFields_city']","",""),
		INPUT_PROVINCE("//select[@id='dwfrm_singleshipping_shippingAddress_addressFields_states_state']","",""),		
		INPUT_COUNTRY("//div[@class='b-form__row b-form__row_half js-cmp-inited js-cmp-inputHidden']//div[@class='b-form__field js-form-field']","",""),
		ERROR_CELL("//div[contains(text(),'Il numero di telefono è troppo corto')]","",""),
		ERROR_CELL_2("//div[@class='b-form__row b-form__row_half required b-form__row_required js-cmp-inited js-cmp-inputEmail state-error']//input[@id='dwfrm_billing_billingAddress_email_emailAddress']//following::div[@class='form-row_error b-error-msg'][2]","",""),
		ERROR_TITLE("//div[contains(text(),'Campo obbligatorio')]","",""),
		ERROR_TITLE_2("//div[@class='b-form__row b-form__row_half required b-form__row_required js-cmp-inited js-cmp-inputEmail state-error']//input[@id='dwfrm_billing_billingAddress_email_emailAddress']//following::div[@class='form-row_error b-error-msg'][3]","",""),
		ERROR_NOME("/div[contains(text(),'Inserisci il nome')]","",""),
		ERROR_NOME_2("//div[@class='b-form__row b-form__row_half required b-form__row_required js-cmp-inited js-cmp-inputEmail state-error']//input[@id='dwfrm_billing_billingAddress_email_emailAddress']//following::div[@class='form-row_error b-error-msg'][4]","",""),
		ERROR_COGNOME("//div[contains(text(),'Inserisci il cognome')]","",""),
		ERROR_COGNOME_2("//div[@class='b-form__row b-form__row_half required b-form__row_required js-cmp-inited js-cmp-inputEmail state-error']//input[@id='dwfrm_billing_billingAddress_email_emailAddress']//following::div[@class='form-row_error b-error-msg'][5]","",""),
		ERROR_INDIRIZZO("//div[contains(text(),\"Inserisci l'indirizzo\")]","",""),
		ERROR_INDIRIZZO_2("//div[@class='b-form__row b-form__row_half required b-form__row_required js-cmp-inited js-cmp-inputEmail state-error']//input[@id='dwfrm_billing_billingAddress_email_emailAddress']//following::div[@class='form-row_error b-error-msg'][6]","",""),
		ERROR_CAP("//div[contains(text(),'Please enter postcode')]","",""),
		ERROR_CAP_2("//div[@class='b-form__row b-form__row_half required b-form__row_required js-cmp-inited js-cmp-inputEmail state-error']//input[@id='dwfrm_billing_billingAddress_email_emailAddress']//following::div[@class='form-row_error b-error-msg'][7]","",""),
		ERROR_CITTA("//div[contains(text(),'Inserisci la città')]","",""),
		ERROR_CITTA_2("//div[@class='b-form__row b-form__row_half required b-form__row_required js-cmp-inited js-cmp-inputEmail state-error']//input[@id='dwfrm_billing_billingAddress_email_emailAddress']//following::div[@class='form-row_error b-error-msg'][8]","",""),
		ERROR_PROVINCIA("//div[contains(text(),'Scegli la provincia')]","",""),
		MRSRADIOBUTTON("//label[@for='dwfrm_singleshipping_shippingAddress_addressFields_title_Mrs']","",""),
		CHECKBOX_BILLING_ADDRESS("//label[@for='dwfrm_singleshipping_shippingAddress_useAsBillingAddress']","",""),
		CHECKBOX_SMS("//label[@for='dwfrm_singleshipping_shippingAddress_smsNotification']","",""),
		OPZIONI_CONSEGNA("//legend[@class='b-checkout-delivery__title']","",""),
		PRICE("//tr[@class='b-order-summary__total js-order-total']//td[@class='b-order-summary__total-value']","",""),
		MRRADIOBUTTON("//label[@for='dwfrm_singleshipping_shippingAddress_addressFields_title_Mr']","",""),
		SHOW_CART("//div[@class='b-checkout-tabs__tab-content b-checkout-tabs__tab-content_first']//button[@class='b-checkout-delivery__open-bag js-cmp-inited js-cmp-button']","",""),
		PAYMENT_BUTTON("//button[@name='dwfrm_singleshipping_shippingAddress_save']","",""),
		ERROR_PROVINCIA_2("//div[@class='b-form__row b-form__row_half required b-form__row_required js-cmp-inited js-cmp-inputEmail state-error']//input[@id='dwfrm_billing_billingAddress_email_emailAddress']//following::div[@class='form-row_error b-error-msg'][9]","",""),
		CONFERMA_INDIRIZZO_MODAL("//div[@class='ui-dialog-content_wrapper']","",""),
		SELECT_FIRST_ADDRESS_ITEM("(//label[@class='b-checkout-delivery__address-label b-label'])[1]","",""),
		CLOSE_ADDRESS_MODAL("//button[@class='b-btn_primary js-dialog-button ui-button ui-corner-all ui-widget ui-button-disabled ui-state-disabled']","","");
		
		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;

		ShippingMethod(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}
		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}
	}
	
	public enum ShippingMethodPL implements ILocator
	{
		TITLE("//h1[@class='b-checkout-tabs__title']","",""),
		SHOPSHIPPING("//div[@class='b-checkout-tabs__tab b-checkout-tabs__tab_second']//span[@class='b-checkout-tabs__tab-input']","",""),
		HOMESHIPPING("//div[@class='b-checkout-tabs__tab b-checkout-tabs__tab_first']//span[@class='b-checkout-tabs__tab-input']","",""),
		LABEL_POSITION("//h3[@class='b-storelocator__results-text']","",""),
		INPUT_POSITION("//input[@class='b-storelocator__search js-autocomplete-input js-input_field ui-autocomplete-input']","",""),
		SEARCH_POSITION("//button[@class='b-storelocator__search-button']","",""),
		TITLE_DELIVERY("//legend[@class='b-register-form__title']","",""),
		EMAIL_LABEL("//span[@class='b-label__value'][contains(text(),'E-mail')]","",""),
		INPUT_EMAIL("//input[@id='dwfrm_billing_billingAddress_email_emailAddress']","",""),
		ERROR_EMAIL("//div[contains(text(),'Inserisci un indirizzo e-mail')]","",""),
		ERROR_EMAIL_2("//div[@class='b-form__row b-form__row_half required b-form__row_required js-cmp-inited js-cmp-inputEmail state-error']//input[@id='dwfrm_billing_billingAddress_email_emailAddress']//following::div[@class='form-row_error b-error-msg'][1]","",""),
		INPUT_CELL("//input[@id='dwfrm_singleshipping_shippingAddress_addressFields_phone']","",""),
		INPUT_NAME("//div[@class='js-address-fields b-checkout__address-form_active']//input[@id='dwfrm_singleshipping_shippingAddress_addressFields_firstName']","",""),
		INPUT_SURNAME("//div[@class='js-address-fields b-checkout__address-form_active']//input[@id='dwfrm_singleshipping_shippingAddress_addressFields_lastName']","",""),
		INPUT_CAP("//input[@id='dwfrm_singleshipping_shippingAddress_addressFields_postal']","",""),
		INPUT_ADDRESS("//input[@id='dwfrm_singleshipping_shippingAddress_addressFields_address1']","",""),
		INPUT_CITY("//input[@id='dwfrm_singleshipping_shippingAddress_addressFields_city']","",""),
		INPUT_PROVINCE("//select[@id='dwfrm_singleshipping_shippingAddress_addressFields_states_state']","",""),		
		INPUT_COUNTRY("//div[@class='b-form__row b-form__row_half js-cmp-inited js-cmp-inputHidden']//div[@class='b-form__field js-form-field']","",""),
		ERROR_CELL("//div[contains(text(),'Il numero di telefono è troppo corto')]","",""),
		ERROR_CELL_2("//div[@class='b-form__row b-form__row_half required b-form__row_required js-cmp-inited js-cmp-inputEmail state-error']//input[@id='dwfrm_billing_billingAddress_email_emailAddress']//following::div[@class='form-row_error b-error-msg'][2]","",""),
		ERROR_TITLE("//div[contains(text(),'Campo obbligatorio')]","",""),
		ERROR_TITLE_2("//div[@class='b-form__row b-form__row_half required b-form__row_required js-cmp-inited js-cmp-inputEmail state-error']//input[@id='dwfrm_billing_billingAddress_email_emailAddress']//following::div[@class='form-row_error b-error-msg'][3]","",""),
		ERROR_NOME("/div[contains(text(),'Inserisci il nome')]","",""),
		ERROR_NOME_2("//div[@class='b-form__row b-form__row_half required b-form__row_required js-cmp-inited js-cmp-inputEmail state-error']//input[@id='dwfrm_billing_billingAddress_email_emailAddress']//following::div[@class='form-row_error b-error-msg'][4]","",""),
		ERROR_COGNOME("//div[contains(text(),'Inserisci il cognome')]","",""),
		ERROR_COGNOME_2("//div[@class='b-form__row b-form__row_half required b-form__row_required js-cmp-inited js-cmp-inputEmail state-error']//input[@id='dwfrm_billing_billingAddress_email_emailAddress']//following::div[@class='form-row_error b-error-msg'][5]","",""),
		ERROR_INDIRIZZO("//div[contains(text(),\"Inserisci l'indirizzo\")]","",""),
		ERROR_INDIRIZZO_2("//div[@class='b-form__row b-form__row_half required b-form__row_required js-cmp-inited js-cmp-inputEmail state-error']//input[@id='dwfrm_billing_billingAddress_email_emailAddress']//following::div[@class='form-row_error b-error-msg'][6]","",""),
		ERROR_CAP("//div[contains(text(),'Please enter postcode')]","",""),
		ERROR_CAP_2("//div[@class='b-form__row b-form__row_half required b-form__row_required js-cmp-inited js-cmp-inputEmail state-error']//input[@id='dwfrm_billing_billingAddress_email_emailAddress']//following::div[@class='form-row_error b-error-msg'][7]","",""),
		ERROR_CITTA("//div[contains(text(),'Inserisci la città')]","",""),
		ERROR_CITTA_2("//div[@class='b-form__row b-form__row_half required b-form__row_required js-cmp-inited js-cmp-inputEmail state-error']//input[@id='dwfrm_billing_billingAddress_email_emailAddress']//following::div[@class='form-row_error b-error-msg'][8]","",""),
		ERROR_PROVINCIA("//div[contains(text(),'Scegli la provincia')]","",""),
		MRSRADIOBUTTON("//label[@for='dwfrm_singleshipping_shippingAddress_addressFields_title_Mrs']","",""),
		CHECKBOX_BILLING_ADDRESS("//label[@for='dwfrm_singleshipping_shippingAddress_useAsBillingAddress']","",""),
		CHECKBOX_SMS("//label[@for='dwfrm_singleshipping_shippingAddress_smsNotification']","",""),
		OPZIONI_CONSEGNA("//legend[@class='b-checkout-delivery__title']","",""),
		PRICE("//tr[@class='b-order-summary__total js-order-total']//td[@class='b-order-summary__total-value']","",""),
		MRRADIOBUTTON("//label[@for='dwfrm_singleshipping_shippingAddress_addressFields_title_Mr']","",""),
		SHOW_CART("//button[@class='b-checkout-delivery__open-bag js-cmp-inited js-cmp-button']","",""),
		PAYMENT_BUTTON("//button[@name='dwfrm_singleshipping_shippingAddress_save']","",""),
		ERROR_PROVINCIA_2("//div[@class='b-form__row b-form__row_half required b-form__row_required js-cmp-inited js-cmp-inputEmail state-error']//input[@id='dwfrm_billing_billingAddress_email_emailAddress']//following::div[@class='form-row_error b-error-msg'][9]","",""),
		CONFERMA_INDIRIZZO_MODAL("//div[@class='ui-dialog-content_wrapper']","",""),
		SELECT_FIRST_ADDRESS_ITEM("(//label[@class='b-checkout-delivery__address-label b-label'])[1]","",""),
		CLOSE_ADDRESS_MODAL("//button[@class='b-btn_primary js-dialog-button ui-button ui-corner-all ui-widget ui-button-disabled ui-state-disabled']","","");
		
		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;

		ShippingMethodPL(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}
		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}
	}
	
	public enum collectFromStore implements ILocator
	{
		LOGO("//img[@class='b-header__logo-image']" ,"",""),
		SHOPSHIPPING("//div[@class='b-checkout-tabs__tab b-checkout-tabs__tab_second']//span[@class='b-checkout-tabs__tab-input']","",""),
		HOMESHIPPING("//div[@class='b-checkout-tabs__tab b-checkout-tabs__tab_first']//span[@class='b-checkout-tabs__tab-input']","",""),
		LABEL_POSITION("//h3[@class='b-storelocator__results-text']","",""),
		INPUT_POSITION("//input[@class='b-storelocator__search js-autocomplete-input js-input_field ui-autocomplete-input']","",""),
		SEARCH_POSITION("//button[@class='b-storelocator__search-button']","",""),
		SHOW_LIST("//label[@class='b-storelocator__tabs-label1']","",""),
		SELECT_STORE("(//button[@class='b-checkout-collect__button-select b-btn_fourth js-select-store'])[1]","",""),
		CHANGE_STORE("//button[@class='b-checkout-collect__store-button b-btn_fourth js-change-store']","",""),
		INPUT_EMAIL("//input[@id='dwfrm_billing_billingAddress_email_emailAddress']","",""),
		INPUT_EMAIL_STORE("(//input[@id='dwfrm_billing_billingAddress_email_emailAddress'])[2]","","//*[@id='dwfrm_billing_billingAddress_email_emailAddress']"),
		INPUT_CELL("//div[@class='b-checkout-collect__delivery-wrapper js-contact-details']//input[@id='dwfrm_singleshipping_shippingAddress_addressFields_phone']","",""),
		INPUT_NAME("//div[@class='b-checkout-collect__delivery-wrapper js-contact-details']//input[@id='dwfrm_singleshipping_shippingAddress_addressFields_firstName']","",""),
		INPUT_SURNAME("//div[@class='b-checkout-collect__delivery-wrapper js-contact-details']//input[@id='dwfrm_singleshipping_shippingAddress_addressFields_lastName']","",""),
		MRRADIOBUTTON("(//div[@class='b-radiobutton__item']//label)[4]","","(//*[@class='b-radiobutton__item']//label)[2]"),
		MRSRADIOBUTTON("(//div[@class='b-radiobutton__item']//label)[3]","",""),
		PRICE("(//td[@class='b-order-summary__total-value'])[2]","",""),
		STORE_SELECTED_NAME("//a[@class='b-checkout-collect__store-name']","",""),
		STORE_SELECTED_DETAILS("//div[@class='b-checkout-collect__store-address']//p","",""),
		STORE_SELECTED_HOUR("//div[@class='b-checkout-collect__store-hours icon-clock']","",""),
		STORE_SELECTED_PHONE("//*[@class='b-checkout-collect__store-phone icon-phone']","",""),
		PROCEDE_TO_PAYMENT("(//button[@data-cmp-id='checkoutButton'])[2] | (//button[@name='dwfrm_singleshipping_shippingAddress_shipToStore'])[2]","","//*[@class='b-btn_primary js-collect-button js-cmp-inited js-cmp-button']"),
		PROCEDE_TO_PAYMENT_CZ_STORE("(//button[@data-cmp-id='checkoutButton'])[3]","",""),
		STORE_NUMBER_FOUND("//div[@class='b-storelocator__results-text js-results-text']","",""),
		STORE_NO_RESULT_FOUND("//div[@class='js-no-results b-storelocator__no-results']","","");
		
		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;

		collectFromStore(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}

		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}
	}
	
	
	public enum LoginPage implements ILocator
	{
		//HEADER("","",""),
		LOGO("//img[@class='b-header__logo-image']" ,"",""),
		LABELACCESS("//h1[@class='b-login__title']","",""),
		LABELLOGACCOUNT("//h2[@class='b-login__sub-title h-hidden-small']","",""),
		LABELWELCOMEBACK("//p[@class='h-hidden-small']","",""),
		LABELEMAIL("//div[@class='b-form__row b-login__row username required b-form__row_required js-cmp-inited js-cmp-inputEmail']","",""),
		INPUTEMAIL("//input[@id='dwfrm_login_username']","",""),
		ERROR_CREDENTIAL("//div[@class='b-error']","",""),
		ERRORMESSAGEMAIL("//input[@id='dwfrm_login_username_d0ufkqvudcix']/parent::div/following-sibling::div[@class='form-row_error b-error-msg']","",""),
		LABELPASSWORD("//div[@class='b-form__row b-login__row password required b-form__row_required js-cmp-inited js-cmp-inputPassword']","",""),
		INPUTPASSWORD("//input[@type='password']","",""),
		ERRORMESSAGEPASSWORD("//input[@id='dwfrm_login_password_d0impayduzij']/parent::div/following-sibling::div[@class='form-row_error b-error-msg']","",""),
		CHECKBOXREMEMBERME("//input[@id='dwfrm_login_rememberme']","",""),
		TEXTLINKFORGOTPASS("//a[@class='js-cmp-inited js-cmp-dialogLink']","",""),
		ACCESSBUTTON("//button[@name='dwfrm_login_login']","",""),
		TEXTLINKFINDMORE("//a[@class='b-link_ninth b-login__link']","",""),
		CHECKOUTGUEST("//button[@name='dwfrm_login_unregistered']","",""),
		REGISTERBUTTON("//*[@name=\"dwfrm_login_register\"]","","");
		
		
		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;

		LoginPage(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}

		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}
	}
	
	public enum BenvenutoCheckoutPage implements ILocator
	{
		LABELACCESS("//h1[@class='b-login__title']","",""),
		LABELLOGACCOUNT("//h2[@class='b-login__sub-title h-hidden-small']","",""),
		LABELWELCOMEBACK("//p[@class='h-hidden-small']","",""),
		LABELEMAIL("//div[@class='b-form__row b-login__row username required b-form__row_required js-cmp-inited js-cmp-inputEmail']","",""),
		INPUTEMAIL("//input[@id='dwfrm_login_username']","",""),
		ERRORMESSAGEMAIL("//input[@id='dwfrm_login_username_d0ufkqvudcix']/parent::div/following-sibling::div[@class='form-row_error b-error-msg']","",""),
		LABELPASSWORD("//div[@class='b-form__row b-login__row password required b-form__row_required js-cmp-inited js-cmp-inputPassword']","",""),
		INPUTPASSWORD("//input[@type='password']","",""),
		ERRORMESSAGEPASSWORD("//input[@id='dwfrm_login_password_d0impayduzij']/parent::div/following-sibling::div[@class='form-row_error b-error-msg']","",""),
		CHECKBOXREMEMBERME("//input[@id='dwfrm_login_rememberme']","",""),
		TEXTLINKFORGOTPASS("//a[@class='js-cmp-inited js-cmp-dialogLink']","",""),
		ACCESSBUTTON("//button[@name='dwfrm_login_login']","",""),
		CHECKOUTBUTTON("//button[@name='dwfrm_login_unregistered']","","");
		
		
		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;

		BenvenutoCheckoutPage(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}

		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}
	}

	public enum Registration implements ILocator
	{
		//HEADER("","",""),
		LOGO("//div[@class='b-account__registration-message']//p//img" ,"",""),
		TITLE("//h1[@class='b-account__title']","",""),
		LABELEMAIL("//div[@class='b-form__row required b-form__row_required js-cmp-inited js-cmp-inputEmail']","",""),
		INPUTEMAIL("//input[@id='dwfrm_profile_customer_email']","",""),
		ERRORMESSAGEEMAIL("//input[@id='dwfrm_profile_customer_email']/parent::div/following-sibling::div[@class='form-row_error b-error-msg']","",""),
		LABELPASSWORD("//span[contains(text(),'Password')]","",""),
		INPUTPASSWORD("(//input[@type='password'])[1]","",""), 
		ERRORMESSAGEPSSWORD("//input[@id='dwfrm_profile_login_password_d0ysxcizhchb']/parent::div/following-sibling::div[@class='form-row_error b-error-msg']","",""),
		INPUTCONFIRMPASSWORD("(//input[@type='password'])[2]","",""),
		ERRORMESSAGECONFIRMPASSWORD("//input[@id='dwfrm_profile_login_passwordconfirm_d0mtcjrdwuhl']/parent::div/following-sibling::div[@class='form-row_error b-error-msg'])","",""),
		LABELTITLE("//div[@class='b-form__row required b-form__row_required js-cmp-inited js-cmp-inputRadio']","",""),
		RADIOBUTTONMRS("//label[@for='dwfrm_profile_address_title_Mrs']","",""),
		RADIOBUTTONMR("//label[@for='dwfrm_profile_address_title_Mr']","",""),
		LABELNAME("//div[@class='b-form__row b-form__row_half required b-form__row_required js-cmp-inited js-cmp-inputText state-durty state-error']","",""),
		INPUTNAME("//input[@id='dwfrm_profile_customer_firstName']","",""),
		ERRORMESSAGENAME("//input[@id='dwfrm_profile_customer_firstName']/parent::div/following-sibling::div[@class='form-row_error b-error-msg']","",""),
		INPUTSURNAME("//input[@id='dwfrm_profile_customer_lastName']","",""),
		ERRORMESSAGESURNAME("//input[@id='dwfrm_profile_customer_lastName']/parent::div/following-sibling::div[@class='form-row_error b-error-msg']","",""),
		LABELADDRESS1("//div[@class='b-form__row required b-form__row_required js-cmp-inited js-cmp-inputText']//label[@class='b-label js-input-label']","",""),
		INPUTADDRESS1("//input[@id='dwfrm_profile_address_address1']","",""),
		ERRORMESSAGEADDRESS1("//input[@id='dwfrm_profile_address_address1']/parent::div/following-sibling::div[@class='form-row_error b-error-msg']","",""),
		LABELADDRESS2("//div[@class='b-form__row js-cmp-inited js-cmp-inputText']//label[@class='b-label js-input-label']","",""),
		//INPUTADDRESS2("","",""),
		INPUTCAP("//input[@id='dwfrm_profile_address_postal']","",""),
		ERRORMESSAGEPOSTALCODE("//input[@id='dwfrm_profile_address_postal']/parent::div/following-sibling::div[@class='form-row_error b-error-msg']","",""),
		INPUTCITY("//input[@id='dwfrm_profile_address_city']","",""),
		ERRORMESSAGECITY("//input[@id='dwfrm_profile_address_city']/parent::div/following-sibling::div[@class='form-row_error b-error-msg']","",""),
		LABELPROVINCE("//div[@class='b-form__row b-form__row_half required b-form__row_required js-cmp-inited js-cmp-inputSelect']","",""),
		SELECTPROVINCE("//select[@id='dwfrm_profile_address_states_state']","",""),
		ERRORMESSAGEPROVINCE("//select[@id='dwfrm_profile_address_states_state']/parent::div/following-sibling::div[@class='form-row_error b-error-msg']","",""),
		LABELPHONE("//div[@class='b-form__row b-form__row_half required b-form__row_required js-cmp-inited js-cmp-inputPhone']","",""),
		INPUTPHONE("//input[@id='dwfrm_profile_customer_phone']","",""),
		ERRORMESSAGEPHONE("//input[@id='dwfrm_profile_customer_phone']/parent::div/following-sibling::div[@class='form-row_error b-error-msg']","",""),
		LABELDATEOFBIRTH("//body[@class='js-cmp-inited js-cmp-bodyTrigger scrolling-active']/div[@id='wrapper']/div[@id='main']/div[@id='primary']/div[@class='b-account']/div[@class='grid']/div[@class='col-12']/div[@class='b-account__registration']/form[@id='RegistrationForm']/fieldset/div[@class='b-form__row b-form__row_required js-cmp-inited js-cmp-dateSelect']/label[1]","",""),
		SELECTDAY("//select[@id='dwfrm_profile_customer_birthdayfields_day']","",""),
		SELECTMONHT("//select[@id='dwfrm_profile_customer_birthdayfields_month']","",""),
		SELECTYEAR("//select[@id='dwfrm_profile_customer_birthdayfields_year']","",""),
		ERRORMESSAGEDATEOFBITTH("//select[@id='dwfrm_profile_customer_birthdayfields_day']/parent::div/parent::div/parent::div/following-sibling::div[@class='form-row_error b-error-msg']","",""),
		LABELTERMSCONDITIONS("//body[@class='js-cmp-inited js-cmp-bodyTrigger scrolling-active']/div[@id='wrapper']/div[@id='main']/div[@id='primary']/div[@class='b-account']/div[@class='grid']/div[@class='col-12']/div[@class='b-account__registration']/form[@id='RegistrationForm']/fieldset/div[14]/label[1]","",""),
		RADIOBUTTONTERMSCONDITIONSAGREE("//div[@class='b-radiobutton__item']//label[@for='dwfrm_profile_customer_termsandcondition_true']","",""),
		ERRORMESAAGETERMSCONDITIONS("//input[@id='dwfrm_profile_customer_termsandcondition_true']/parent::div/parent::div/following-sibling::div[@class='form-row_error b-error-msg']","",""),
		LABELMARKETING("//body[@class='js-cmp-inited js-cmp-bodyTrigger scrolling-active']/div[@id='wrapper']/div[@id='main']/div[@id='primary']/div[@class='b-account']/div[@class='grid']/div[@class='col-12']/div[@class='b-account__registration']/form[@id='RegistrationForm']/fieldset/div[15]/label[1]","",""),
		RADIOBUTTONMARKETINGAGREE("//div[@class='b-radiobutton__item']//label[@for='dwfrm_profile_customer_addtoemaillist_true']","",""),
		ERRORMESSAGEMARKETING("//input[@id='dwfrm_profile_customer_addtoemaillist_true']/parent::div/parent::div/following-sibling::div[@class='form-row_error b-error-msg']","",""),
		LABELPROFILING("//body[@class='js-cmp-inited js-cmp-bodyTrigger scrolling-active']/div[@id='wrapper']/div[@id='main']/div[@id='primary']/div[@class='b-account']/div[@class='grid']/div[@class='col-12']/div[@class='b-account__registration']/form[@id='RegistrationForm']/fieldset/div[16]/label[1]","",""),
		RADIOBUTTONPROFILINGAGREE("//div[@class='b-radiobutton__item']//label[@for='dwfrm_profile_customer_profilation_true']","",""),
		ERRORMESSAGEPROFILING("//input[@id='dwfrm_profile_customer_profilation_true']/parent::div/parent::div/following-sibling::div[@class='form-row_error b-error-msg']","",""),
		BUTTONSIGNIN("//button[@name='dwfrm_profile_confirm']","",""),
		EMAIL_ALREADY_EXISTS_ERROR("(//div[@class='form-row_error b-error-msg'])[1]","",""),
		INVALID_PHONE_NUMBER_ERROR("(//div[@class='form-row_error b-error-msg'])[10]","",""),
		PRIVACY_ERROR("(//div[@class='form-row_error b-error-msg'])[11 or 7]","",""),
		MARKETING_ERROR("(//div[@class='form-row_error b-error-msg'])[12]","",""),
		PROFIL_ERROR("(//div[@class='form-row_error b-error-msg'])[13]","",""),;
		
		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;

		Registration(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}
		

		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}
	}
	

	public enum ProfilePage implements ILocator
	{
		//HEADER("","",""),
		LOGO("//img[@class='b-header__logo-image']","",""),
		TITLE("//h1[@class='b-account__title']" ,"",""),
		POINTSANDCOUPONSBUTTON("//nav[@class='b-account__nav']//li[1]","",""),
		PURCHASESANDORDERSBUTTON("//nav[@class='b-account__nav']//li[2]","",""),
		MYPROFILEBUTTON("//nav[@class='b-account__nav']//li[3]","","");
		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;

		ProfilePage(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}
		

		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}

	}
	
	public enum PreviewItemsSearched implements ILocator
	{
		//HEADER("","",""),
		PRODUCT1("//div[@class='b-suggestions__products b-suggestions__products_right-border']//div[@class='b-suggestions__product js-product-impression'][1]","",""),
		PRODUCT2("//div[@class='b-suggestions__products b-suggestions__products_right-border']//div[@class='b-suggestions__product js-product-impression'][2]","",""),
		PRODUCT3("//div[@class='b-suggestions__products b-suggestions__products_right-border']//div[@class='b-suggestions__product js-product-impression'][3]","",""),
		PRODUCT4("//div[@class='b-suggestions__products b-suggestions__products_right-border']//div[@class='b-suggestions__product js-product-impression'][4]","",""),
		PRODUCT5("//div[@class='b-suggestions__products b-suggestions__products_right-border']//div[@class='b-suggestions__product js-product-impression'][5]","",""),
		PRODUCT6("//div[@class='b-suggestions__products b-suggestions__products_right-border']//div[@class='b-suggestions__product js-product-impression'][6]","",""),
		SHOWALL("//a[@class='b-suggestions__link-all js-category-suggestions']","",""),
		PRODUCT_PRICE("//span[@class='b-price__sale']","",""),
		SEARCH_BUTTON("//button[@class='b-search__button-search']","",""),
		PRODUCT_NAME("//div[@class='b-suggestions__name']","",""),
		CATEGORY_FILTER1("(//span[@class='b-suggestions__category'])[1]","",""),
		CATEGORY_FILTER_CONTAINS("//span[contains(text(),'in ${category} / Brand')]","",""),
		CATEGORY_FILTER2("(//span[@class='b-suggestions__category'])[2]","",""),
		CATEGORY_FILTER_RESULT("(//a[@class='b-breadcrumbs__link'])[1]","",""),
		CATEGORY_FILTER_RESULT_2("(//a[@class='b-breadcrumbs__link'])[2]","",""),
		CATEGORY_FILTER3("(//span[@class='b-suggestions__category'])[3]","","");
		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;

		PreviewItemsSearched(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}
		

		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}

	}
	
	public enum PaymentPage implements ILocator
	{
		//HEADER("","",""),
		LOGO("//img[@class='b-header__logo-image']","",""),
		//TITLE("//fieldset[@class='b-checkout-billing__address-list']//legend[@class='b-register-form__title']","",""),
		BILLING_ADDRESS("//legend[contains(text(),'${addressFatt}')]","",""),
		PAYMENT_LABEL("//fieldset[@class='b-checkout-billing__discount-wrapper']//legend[@class='b-register-form__title']","",""),
		INPUT_COUPON("//input[@id='dwfrm_billing_couponCode']","",""),
		APPLY_BUTTON("//button[@name='dwfrm_billing_applyCoupon']","",""),
		CREDIT_CARD("//label[@for='CREDIT_CARD' or @for='WEBPAY']","",""),
		OWNER_CARD("//input[@id='cardOwner']","",""),
		NUMBER_CARD("//input[@id='cardNumber']","",""),
		MONTH_EXPIRED("//select[@id='cardExpireMonth']","",""),
		YEAR_EXPIRED("//select[@id='cardExpireYear']","",""),
		CVV("//input[@id='cardCvn']","",""),
		PAYPAL("//label[@for='PayPal']","",""),
		COD("//label[@for='CASH_ON_DELIVERY']","",""),
		PRICE("//td[@class='b-order-summary__total-value']","",""),
		PAYPAL_BUTTON("//div[contains(@class, 'paypal')]//div[contains(@id, 'paypal')]","",""),
		CHECKBOX_INFORMATIVA("//label[@for='dwfrm_billing_billingAddress_personalData']","",""),
		CHECKBOX_CONDITIONS_2("//label[@for='dwfrm_billing_billingAddress_tersmsOfSale']","",""),
		CHECKBOX_CONDITIONS("//fieldset[@class='b-checkout-billing__notes-wrapper']//div[2]//div[1]//label[1]","",""),
		MRSRADIOBUTTON("//label[@for='dwfrm_billing_billingAddress_addressFields_title_Mrs']","",""),
		MRRADIOBUTTON("//label[@for='dwfrm_billing_billingAddress_addressFields_title_Mr']","",""),
		INPUT_NAME("//input[@id='dwfrm_billing_billingAddress_addressFields_firstName']","",""),
		INPUT_SURNAME("//input[@id='dwfrm_billing_billingAddress_addressFields_lastName']","",""),
		INPUT_ADDRESS("//input[@id='dwfrm_billing_billingAddress_addressFields_address1']","",""),
		INPUT_CAP("//input[@id='dwfrm_billing_billingAddress_addressFields_postal']","",""),
		INPUT_CITY("//input[@id='dwfrm_billing_billingAddress_addressFields_city']","",""),
		INPUT_PROVINCE("//select[@id='dwfrm_billing_billingAddress_addressFields_states_state']","",""),
		CONFIRM_BUTTON("//button[@data-cmp-id='checkoutButton']","",""),
		PERSONAL_DATA_ERROR_MESSAGE("//div[@data-cmp-id='dwfrm_billing_billingAddress_personalData']//div[@class='form-row_error b-error-msg']","",""),
		TERMS_OF_SALE_ERROR_MESSAGE("//div[@data-cmp-id='dwfrm_billing_billingAddress_tersmsOfSale']//div[@class='form-row_error b-error-msg']","",""),
		CREDIT_CARD_OWNER_ERROR("//div[@data-cmp-id='cardOwner']//div[@class='form-row_error b-error-msg']","",""),
		CREDIT_CARD_NUMBER_ERROR("//div[@data-cmp-id='cardNumber']//div[@class='form-row_error b-error-msg']","",""),
		CREDIT_CARD_CVN_ERROR("//div[@data-cmp-id='cardCvn']//div[@class='form-row_error b-error-msg']","","");
		
		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;

		PaymentPage(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}
		

		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}

	}
	
	public enum RiepilogoPage implements ILocator
	{
		LOGO("//img[@class='b-header__logo-image']","",""),
		CONFIRM_TITLE_MESSAGE("//h1[@class='b-checkout-confirmation__title']","",""),
		ORDER_NUMBER("//h3[@class='b-checkout-confirmation__order-title']","",""),
		MINICART_ELEMENTS("//li[@class='b-minicart__item']","",""),
		MINICART_ELEMENT_TITLE("//li[@class='b-minicart__item'][#]//div[@class='b-minicart__product-title']","",""),
		MINICART_ELEMENT_PRICE("(//li[@class='b-minicart__item'][#]//span[@class='b-minicart__product-value'])[3]","",""),
		MINICART_ELEMENT_ID("(//li[@class='b-minicart__item'][#]//span[@class='b-minicart__product-value'])[1]","",""),
		MINICART_ELEMENT_BRAND("(//li[@class='b-minicart__item'][#]//span[@class='b-minicart__product-value'])[2]","",""),
		MINICART_ELEMENT_COLOR("(//li[@class='b-minicart__item'][#]//span[@class='b-product__value b-product-list__value'])[2]","",""),
		MINICART_ELEMENT_QUANTITY("(//li[@class='b-minicart__item'][#]//span[@class='b-minicart__product-value'])[4]","",""),
		MINICART_ELEMENT_TOTAL_PRICE("//li[@class='b-minicart__item'][#]//span[@class='mini-cart-price']","",""),
		ORDER_DETAIL_HEADER("//h3[@class='b-checkout-confirmation__details-title']","",""),
		ORDER_SHIPPING("(//p[@class='b-checkout-confirmation__detail-description'])[1]","",""),
		ORDER_PAYMENT("(//p[@class='b-checkout-confirmation__detail-description'])[2]","",""),
		ORDER_OWNER("(//p[@class='b-checkout-confirmation__detail-description'])[3]","",""),
		ORDER_ADDRESS_BILLING("(//p[@class='b-checkout-confirmation__detail-description'])[4]","",""),
		ORDER_ADDRESS_DELIVERY("(//p[@class='b-checkout-confirmation__detail-description'])[5]","",""),
		ORDER_SUB_TOTAL("//td[@class='b-order-summary__value' and ./preceding-sibling::*[text()='${subtotale}:']]","",""),
		ORDER_STORE_PRICE("//td[@class='b-order-summary__value' and ./preceding-sibling::*[text()='${ritiroNegozio}:']]","",""),
		ORDER_DETAILS_DELIVERY_HEADER("(//span[@class='b-checkout-confirmation__detail-title'])[1]","",""),
		ORDER_DETAILS_PAYMENTS_HEADER("(//span[@class='b-checkout-confirmation__detail-title'])[2]","",""),
		ORDER_DETAILS_OWNER_OR_STORE_HEADER("(//span[@class='b-checkout-confirmation__detail-title'])[3]","",""),
		ORDER_DETAILS_ADDRESS_INVOICE_HEADER("(//span[@class='b-checkout-confirmation__detail-title'])[4]","",""),
		ORDER_DETAILS_ADDRESS_OR_DELIVERY_HEADER("(//span[@class='b-checkout-confirmation__detail-title'])[5]","",""),
		ORDER_FROM_STORE_CONFIRM_MAIL("//p[@class='b-checkout-confirmation__detail-description b-checkout-confirmation__detail-description_italic']","",""),
		PRICE("//td[@class='b-order-summary__total-value']","","");
		
		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;

		RiepilogoPage(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}
		

		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}

	}
	public enum PaypalPaymentPage implements ILocator
	{
		LOGO("//p[contains(@class,'paypal-logo')]","",""),
		BILLING_ADDRESS("//fieldset[@class='b-checkout-billing__address-list']//legend[@class='b-register-form__title']","",""),
		INPUT_EMAIL("//input[@id='email']","",""),
		INPUT_PASSWORD("//input[@id='password']","",""),
		ACCEDI_BUTTON("//button[@id='btnLogin']","",""),
		ERROR_MESSAGE("//p[@class='notification notification-critical']","","");
		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;

		PaypalPaymentPage(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}
		

		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}

	}
	
	public enum PaypalPaymentRiepilogoPage implements ILocator
	{
		LOGO("//*[@data-testid=\"header-logo\"]","",""),
		CONTINUE_BUTTON("//*[@id='payment-submit-btn']","",""),
		ACCEPT_COOKIE("//*[@id=\"acceptAllButton\"]","","");
		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;

		PaypalPaymentRiepilogoPage(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}
		

		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}

	}
	
	public enum GuerillaMailPage implements ILocator
	{
		LOGO("//div[@id='logo']//a//img","",""),
		MAIL("//span[@id='inbox-id']","",""),
		INPUT_NEW_MAIL("//span[@id='inbox-id']//input","",""),
		REGOLA_BUTTON("//button[@class='save button small']","",""),
		ALIAS("//input[@id='use-alias']","",""),
		SUBJECT_MAIL("(//td[@class='td3'])[1]","",""),
		SUBJECT_MAIL_2("//td[contains(text(),'${subject}')]","",""),
		CONTINUE_BUTTON("//input[@id='confirmButtonTop']","","");
		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;

		GuerillaMailPage(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}
		

		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}

	}
	
	public enum EmailTemplate implements ILocator
	{
		EMAIL_WELCOME("//body/center[1]/table[2]/tbody[1]/tr[1]/td[1]/center[1]/span[1]","",""),
		EMAIL_RIEPILOGO_ORDINE("//body/center[1]/table[2]/tbody[1]/tr[1]/td[1]/center[1]/span//center","",""),
		EMAIL_RIEPILOGO_ORDINE_LINK("//body/center[1]/table[2]/tbody[1]/tr[1]/td[1]/center[1]/span//center//a","",""),
		EMAIL_HELP_SUBSECTION("//body/center[1]/table[2]/tbody[1]/tr[1]/td[1]/center[1]/span[1]","",""),
		EMAIL_PRODUCT_LIST_HEADER("//body/center[1]/table[2]/tbody[1]/tr[1]/td[1]/center[1]/span[1]//div[1]/b","",""),
		EMAIL_PRODUCT_LIST("//body/center[1]/table[2]/tbody[1]/tr[1]/td[1]/center[1]/span[1]//div/table","",""),
		EMAIL_PRODUCT_LIST_ELEMENTS("(//body/center[1]/table[2]/tbody[1]/tr[1]/td[1]/center[1]/span[1]//div/table)[#]","",""),
		EMAIL_PRODUCT_ELEMENT_DETAILS("//body/center[1]/table[2]/tbody[1]/tr[1]/td[1]/center[1]/span[1]/div[3]/table[__index__]/tbody/tr/td[2]","",""),
		EMAIL_PRODUCT_ELEMENT_TOTAL("//body/center[1]/table[2]/tbody[1]/tr[1]/td[1]/center[1]/span[1]/div[3]/table[__index__]/tbody/tr/td[3]","",""),
//		EMAIL_PRODUCT_SUMMARY("//div[@class='email_body']/table[1]","",""),
//		EMAIL_PRODUCT_SUMMARY_SUBTOTAL_TEXT("//div[@class='email_body']/table[1]/tbody/tr[1]/td[1]","",""),
//		EMAIL_PRODUCT_SUMMARY_SUBTOTAL_VALUE("//div[@class='email_body']/table[1]/tbody/tr[1]/td[2]/em","",""),
//		EMAIL_PRODUCT_SUMMARY_SALE_TEXT("//div[@class='email_body']/table[1]/tbody/tr[2]/td[1]","",""),
//		EMAIL_PRODUCT_SUMMARY_SALE_VALUE("//div[@class='email_body']/table[1]/tbody/tr[2]/td[2]/em","",""),
//		EMAIL_PRODUCT_SUMMARY_DELIVERY_TEXT("//div[@class='email_body']/table[1]/tbody/tr[3]/td[1]","",""),
//		EMAIL_PRODUCT_SUMMARY_DELIVERY_VALUE("//div[@class='email_body']/table[1]/tbody/tr[3]/td[2]/em","",""),
//		EMAIL_PRODUCT_SUMMARY_PAYMENT_TEXT("//div[@class='email_body']/table[1]/tbody/tr[4]/td[1]","",""),
//		EMAIL_PRODUCT_SUMMARY_PAYMENT_VALUE("//div[@class='email_body']/table[1]/tbody/tr[4]/td[2]/em","",""),
		EMAIL_PRODUCT_SUMMARY_TOTAL_PRICE_TEXT("//body/center[1]/table[2]/tbody/tr/td/center/span/table[2]/tbody/tr/td/b","",""),
		EMAIL_PRODUCT_SUMMARY_TOTAL_PRICE_VALUE("//body/center[1]/table[2]/tbody/tr/td/center/span/table[2]/tbody/tr/td[2]/em","",""),
		EMAIL_PRODUCT_BILLING_SUMMARY("//div[@class='email_body']/table[3]","",""),
		EMAIL_PRODUCT_BILLING_SUMMARY_HEADER("(//body/center[1]/table[2]/tbody/tr/td/center/span/div/b)[2]","",""),
		EMAIL_PRODUCT_BILLING_SUMMARY_ORDERNUMBER("//body/center[1]/table[2]/tbody/tr/td/center/span/table[3]/tbody/tr[1]//table[1]//td[1]","",""),
		EMAIL_PRODUCT_BILLING_SUMMARY_DATE("//body/center[1]/table[2]/tbody/tr/td/center/span/table[3]/tbody/tr[1]//table[2]//td[1]","",""),
		EMAIL_PRODUCT_BILLING_SUMMARY_PAYMENT("//body/center[1]/table[2]/tbody/tr/td/center/span/table[3]/tbody/tr[1]//table[3]//td[1]","",""),
		EMAIL_PRODUCT_BILLING_SUMMARY_SHIPPING("//body/center[1]/table[2]/tbody/tr/td/center/span/table[3]/tbody/tr[1]//table[4]//td[1]","",""),
		EMAIL_PRODUCT_BILLING_SUMMARY_BILLINGADDRESS("//body/center[1]/table[2]/tbody/tr/td/center/span/table[3]/tbody/tr[2]//table[1]/tbody/tr[1]/td","",""),
		EMAIL_PRODUCT_BILLING_SUMMARY_BILLINGADDRESS_STORE("//body/center[1]/table[2]/tbody/tr/td/center/span/table[3]/tbody/tr[2]//table[1]/tbody/tr[2]/td","",""),
		EMAIL_PRODUCT_BILLING_SUMMARY_DELIVERYADDRESS("//body/center[1]/table[2]/tbody/tr/td/center/span/table[3]/tbody/tr[2]//table[2]/tbody/tr[1]/td","","")
//		EMAIL_PRODUCT_FOOTER("//div[@class='email_body']/i","",""),
//		EMAIL_BODY("//div[@class='email_body']","","");
		;
		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;
		
		EmailTemplate(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}
		
		
		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}
	}
	
	public enum SearchingPageFiltered implements ILocator
	{
		//HEADER("","",""),
		LOGO("//img[@class='b-header__logo-image']","",""),
		TITLE("//h1[@class='b-plp__title']" ,"",""),
		TAGLIA_FILTER("(//div[@class='b-filter__title js-dropdown-button'])[1]","(//div[contains(@class,'b-filter__title')])[1]",""),
		PREZZO_FILTER("(//div[@class='b-filter__title js-dropdown-button'])[2]","(//div[contains(@class,'b-filter__title')])[2]",""),
		COLORE_FILTER("(//div[@class='b-filter__title js-dropdown-button'])[3]","(//div[contains(@class,'b-filter__title')])[3]",""),
		MATERIALE_FILTER("(//div[@class='b-filter__title js-dropdown-button'])[4]","(//div[contains(@class,'b-filter__title')])[4]",""),
		BRAND_FILTER("(//div[@class='b-filter__title js-dropdown-button'])[5]","(//div[contains(@class,'b-filter__title')])[5]",""),
		NEWS("//div[@class='ui-dialog-buttonset']//button[@class='b-marketing-popup__btn ui-button ui-corner-all ui-widget']","",""),
		CATEGORY_FILTER_RESULT_TEXT("//a[@class='b-breadcrumbs__link'][contains(text(),'${category}')]","",""),
		//FILTER_ELEMENT("//a[@class='b-filter__dropdown-link js-cmp-inited js-cmp-refinementButton' and @data-filter-name='${filterCategory}']","(//a[@class='b-filter__dropdown-link js-cmp-inited js-cmp-refinementButton' and @data-cmp='${filterCategory}'])[1]",""),
		FILTER_ELEMENT("//li[@data-cmp='refinementButtonContainer' and child::a[@data-filter-name='${filterCategory}']]//a","(//a[@class='b-filter__dropdown-link js-cmp-inited js-cmp-refinementButton' and @data-cmp='${filterCategory}'])[1]",""),
		FILTER_ELEMENT_BY_VALUE("//a[@data-filter-value='${filterValue}' and @data-filter-name='${filterCategory}']","",""),
		FILTER_APPLIED_ELEMENTS("//a[@data-cmp=\"refinementButton\"]","",""),
		FILTER_APPLIED_ELEMENT("(//a[@data-cmp=\"refinementButton\"])[#]","",""),
		TAGLIA_SUBMENU("//ul[contains(@class,'b-filter__dropdown size')]","",""),
		PREZZO_SUBMENU("//ul[contains(@class,'b-filter__dropdown price')]","",""),
		COLORE_SUBMENU("//ul[contains(@class,'b-filter__dropdown color')]","",""),
		MATERIALE_SUBMENU("//ul[contains(@class,'b-filter__dropdown refinementMaterial')]","",""),
		BRAND_SUBMENU("//ul[contains(@class,'b-filter__dropdown brand')]","",""),
		FILTER_BY_SIZE_QUERY("//a[@class='b-product-tile__size-link  js-variations-link' and contains(text(),'${size}')]","",""),
		FILTER_BY_COLOR_QUERY("//a[contains(@class,'b-product-tile__color-link') and @title='${color}']","",""),
		FILTER_BY_BRAND_QUERY("//a[@class='name-link' and contains(text(),'${brand}')]","",""),
		FILTER_BY_PRICE_QUERY_ALL("//span[@class='b-price__sale']","",""),
		FILTER_BY_PRICE_QUERY("(//span[@class='b-price__sale'])[#]","",""),
		TOTAL_ELEMENT_FILTERED("//span[@class='b-plp__title-quantity' and contains(text(),'${total}')]","",""),
		FILTERED_LIST("//li[contains(@class,'b-plp__product-item js-grid-tile')]","",""),
		ORDER_BY_BUTTON("(//*[@class='b-sortby__title js-dropdown-button'])[2]","//div[@class='b-plp__sort-by_mobile']//div[@class='b-sortby js-cmp-inited js-cmp-dropdown']",""),
		ORDER_BY_FILTER("(//li[contains(@class,'b-sortby__item js-sort') and contains(@data-sorting,'${filter}')])[2]","(//li[contains(@class,'b-sortby__item js-sort') and contains(@data-sorting,'${filter}')])[1]",""), 
		FILTRI_MENU("//div[@class='b-filter__open-button js-open-filter-btn']","",""), 
		APPLY_FILTER_MOBILE("//*[contains(@class,'b-btn_third js-button-back')]","",""),
		APPLY_FILTER_OK_MOBILE("//div[@class='b-plp__filter-apply b-btn_third js-close-filter-btn']","",""), 
		CLOSE_FILTER_MENU_MOBILE("//div[@class='b-filter__flyout-title js-close-filter-btn']","","");
		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;

		SearchingPageFiltered(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}
		

		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}

	}
	
	public enum SearchingPage implements ILocator
	{
		//HEADER("","",""),
		LOGO("//img[@class='b-header__logo-image']","",""),
		TITLE("//h1[@class='b-plp__title']" ,"",""),
		TAGLIA_FILTER("(//div[@class='b-filter__title js-dropdown-button'])[1]","",""),
		PREZZO_FILTER("(//div[@class='b-filter__title js-dropdown-button'])[2]","",""),
		COLORE_FILTER("(//div[@class='b-filter__title js-dropdown-button'])[3]","",""),
		MATERIALE_FILTER("(//div[@class='b-filter__title js-dropdown-button'])[4]","",""),
		BRAND_FILTER("(//div[@class='b-filter__title js-dropdown-button'])[5]","",""),
		FIRST_ITEM("(//a[@class='b-product-tile__image-link js-cmp-inited js-cmp-tileImage'])[1]","",""),
		FIRST_SIZE("(//ul[@class='swatches b-product-tile__size-list']//li)[1]","",""),
		ADD_TO_CART_FIRST_ELEMENT("(//button[@data-cmp-id='addToCartButton'])[1]","",""),
		REMOVE_TO_WISHLIST("//a[@class='b-wishlist js-cmp-inited js-cmp-addToWishlist b-wishlist_added']","",""),
		FIRST_ITEM_IMAGE("(//div[@class='b-product-tile__image-simple js-simple'])[1]//img","",""),
		FIRST_ITEM_PRICE("(//div[@class='b-price'])[1]","",""),
		ADD_TO_WISHLIST("//a[@data-id='addToWishlist']","","");
		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;

		SearchingPage(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}
		

		public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}

	}
	
		public enum Shops implements ILocator
	{
		//HEADER("","",""),
		TITLESHOP("//h1[@class='b-storelocator__title']" ,"",""),
		INPUT("//input[@class='b-storelocator__search js-autocomplete-input js-input_field ui-autocomplete-input']","",""),
		LISTAUTOCOMPLETE("//div[@class='js-cmp-inited js-cmp-googleAutocomplete']","",""),
		BUTTONLOCATEME("//button[@class='b-storelocator__search-icon js-my-location js-cmp-inited js-cmp-button']","",""),
		BUTTONSEARCH("//button[@class='b-storelocator__search-button']","",""),
		LABELNEARESTSTORE("//div[@class='b-storelocator__results-text js-results-text']","",""),
		FILTER_DIRETTO("//label[@class='b-storelocator__types-original b-label']","",""),
		FILTER_FRANCHISING("//label[@class='b-storelocator__types-franchising b-label']","",""),
		BUTTONSHOWONTHEMAP("//label[@class='b-storelocator__tabs-label1']","",""),
		MAP("//div[@class='b-storelocator__tab-container-1']","",""),
		BUTTONSHOWLIST("//label[@class='b-storelocator__tabs-label2']","",""),
		LABELTYPESOFSHOPS("//span[@class='b-storelocator__types-title']","",""),
		LOGO_DIRETTO("(//div[@data-cmp='googleMap']//img[@src='/on/demandware.static/Sites-bata-it-Site/-/default/dw4339ea5f/images/marker-original.svg'])[1]","",""),
		LOGO_DIRETTO_ALL("//div[@data-cmp='googleMap']//img[@src='/on/demandware.static/Sites-bata-it-Site/-/default/dw4339ea5f/images/marker-original.svg']","",""),
		LOGO_FRANCHISING("(//div[@data-cmp='googleMap']//img[@src='/on/demandware.static/Sites-bata-it-Site/-/default/dw2e5b8388/images/marker-franchising.svg'])[1]","",""),
		BUTTONSHOWMORE("//button[@class='b-find-store__load-more b-btn_fifth js-show-more js-cmp-inited js-cmp-loadMoreButton']","",""),
		TITLE_DETAILS_STORE("//div[@class='b-storelocator__info-block js-info-window-wrapper']//div[@class='b-find-store__address-title js-title-wrapper']","",""),
		KM_DETAILS_STORE("//div[@class='b-storelocator__info-block js-info-window-wrapper']//div[@class='b-find-store__distance js-distance-wrapper']","",""),
		ADDRESS_DETAILS_1("(//div[@class='si-frame si-content-wrapper']//p)[1]","",""),
		ADDRESS_DETAILS_2("(//div[@class='si-frame si-content-wrapper']//p)[2]","",""),
		ADDRESS_DETAILS_3("//div[@class='b-storelocator__info-block js-info-window-wrapper']//div[@class='b-find-store__address-description']","",""),
		VISUALIZZA_NEGOZIO("(//a[@href='/negozio?storeID=170R_S_72727'])[2]","",""),
		
		
		TITLE_DETAILS_STORE_LIST("(//div[@class='b-find-store__address-title js-title-wrapper'])[1]","",""),
	//	KM_DETAILS_STORE_LIST("","",""),
		ADDRESS_DETAILS_1_LIST("(//div[@class='b-find-store__type js-type-wrapper'])[1]","",""),
		ADDRESS_DETAILS_2_LIST("(//div[@class='b-find-store__address'])[1]","","");
		//ADDRESS_DETAILS_3_LIST("(//div[@class='b-find-store__address'])[2]","","");
		
		
		private String locatorAndroid;
		private String locatorIOS;
		private String locatorWeb;

		Shops(String locatorWeb, String locatorAndroid, String locatorIOS) {
			this.locatorAndroid = locatorAndroid;
			this.locatorIOS=locatorIOS;
			this.locatorWeb=locatorWeb;
		}
				public String getAndroidLocator()
		{
			return this.locatorAndroid;
		}

		public String getIOSLocator()
		{
			return this.locatorIOS;
		}
		public String getWebLocator()
		{
			return this.locatorWeb;
		}

	}
	
		public enum MyWishlistPage implements ILocator
		{
			//HEADER("","",""),
			LOGO("//img[@class='b-header__logo-image']","",""),
			TITLE("//h1[@class='b-account__title js-wihslist-header' or @class='b-account__title']" ,"",""),
			DETAILS_ITEM("//div[@class='b-product-tile__bottom js-product-impression js-cmp-inited js-cmp-productVariant']","",""),
			HOMEPAGE_BUTTON("//a[@title='homepage']","",""),
			ADD_TO_CART_FIRST_ITEM("(//button[@data-cmp-id='addToCartButton'])[1]","",""),
			NUOVI_ARRIVI_BUTTON("//a[@class='b-btn_fifth'][1]","",""),
			REMOVE_BUTTON("//a[@class='b-wishlist b-wishlist_remove b-wishlist_added js-cmp-inited js-cmp-addToWishlist']","",""),
			URL_COPY("//a[@class='js-copy-link']","","");
			private String locatorAndroid;
			private String locatorIOS;
			private String locatorWeb;

			MyWishlistPage(String locatorWeb, String locatorAndroid, String locatorIOS) {
				this.locatorAndroid = locatorAndroid;
				this.locatorIOS=locatorIOS;
				this.locatorWeb=locatorWeb;
			}
			

			public String getAndroidLocator()
			{
				return this.locatorAndroid;
			}

			public String getIOSLocator()
			{
				return this.locatorIOS;
			}
			public String getWebLocator()
			{
				return this.locatorWeb;
			}

		}
		
		public enum TrackAnOderPage implements ILocator
		{
			//HEADER("","",""),
			TITLE("//h1[@class='b-login__title']","",""),
			ORDER_NUMBER("//input[contains(@id,'dwfrm_orderstatus_orderno')]","",""),
			EMAIL("//input[@id='dwfrm_orderstatus_email']","",""),
			FIND_BUTTON("//button[@name='dwfrm_orderstatus_search']","",""),
			ERROR_MESSAGE("//div[@class='b-error']","","");
			
			private String locatorAndroid;
			private String locatorIOS;
			private String locatorWeb;

			TrackAnOderPage(String locatorWeb, String locatorAndroid, String locatorIOS) {
				this.locatorAndroid = locatorAndroid;
				this.locatorIOS=locatorIOS;
				this.locatorWeb=locatorWeb;
			}
			

			public String getAndroidLocator()
			{
				return this.locatorAndroid;
			}

			public String getIOSLocator()
			{
				return this.locatorIOS;
			}
			public String getWebLocator()
			{
				return this.locatorWeb;
			}

		}
		
		public enum TrackAnOderDetailsPage implements ILocator
		{
			//HEADER("","",""),
			HEADER("//h2[@class='b-account-orders__title']","",""),
			ORDER_TITLE("//h2[@class='b-account-order__title']","",""),
			ORDER_STATUS("//div[@class='b-account-order__attribute-value' and ./preceding-sibling::*[contains(text(),'${orderStatusText}')]]","",""),
			ORDER_NUMBER("//div[@class='b-account-order__attribute-value' and ./preceding-sibling::*[contains(text(),'${orderNumberText}')]]","",""),
			OWNER_DATA("//div[@class='b-account-order__attribute-value' and ./preceding-sibling::*[contains(text(),'${ownerDataText}')]]","",""),
			DELIVERY_ADDRESS("//div[@class='b-account-order__attribute-value' and ./preceding-sibling::*[contains(text(),'${deliveryAddress}') or contains(text(),'${shipDelivery}')]]","",""),
			PAYMENT("//div[@class='b-account-order__attribute-value' and ./preceding-sibling::*[contains(text(),'${paymentsText}')]]","",""),
			BILLING_ADDRESS("//div[@class='b-account-order__attribute-value' and ./preceding-sibling::*[contains(text(),'${billingAddress}')]]","",""),
			TOTAL_PRICE("//td[@class='b-order-summary__total-value']","",""),
			ORDER_ITEM("(//div[@class='b-account-order__item'])[#]","",""),
			ORDER_ITEM_TITLE("(//div[@class='b-account-order__item'])[#]//div[@class='b-product-list__name']","",""),
			ORDER_ITEM_ID("((//div[@class='b-account-order__item'])[#]//span[@class='b-product__value'])[1]","",""),
			ORDER_ITEM_BRAND("((//div[@class='b-account-order__item'])[#]//span[@class='b-product__value'])[2]","",""),
			ORDER_ITEM_SIZE("((//div[@class='b-account-order__item'])[#]//span[@class='b-product__value'])[3]","",""),
			ORDER_ITEM_COLOR("((//div[@class='b-account-order__item'])[#]//span[@class='b-product__value'])[4]","",""),
			ORDER_ITEM_QUANTITY("((//div[@class='b-account-order__item'])[#]//span[@class='b-product__value'])[5]","",""),
			ORDER_ITEM_PRICE("(//div[@class='b-account-order__item'])[#]//span[@class='b-minicart__product-value']","","");
			
			private String locatorAndroid;
			private String locatorIOS;
			private String locatorWeb;

			TrackAnOderDetailsPage(String locatorWeb, String locatorAndroid, String locatorIOS) {
				this.locatorAndroid = locatorAndroid;
				this.locatorIOS=locatorIOS;
				this.locatorWeb=locatorWeb;
			}
			

			public String getAndroidLocator()
			{
				return this.locatorAndroid;
			}

			public String getIOSLocator()
			{
				return this.locatorIOS;
			}
			public String getWebLocator()
			{
				return this.locatorWeb;
			}

		}
		
		public enum PurchasesAndOrdersPage implements ILocator
		{
			//HEADER("","",""),
			ORDER_LIST("//ul[@class='b-account-orders__list']","",""),
			ORDER_LIST_ELEMENT("//li[@class='b-account-order b-account-orders__item']","",""),
			ORDER_LIST_ELEMENT_HEADER("(//li[@class='b-account-order b-account-orders__item'])[#]//h2[@class='b-account-order__title']","",""),
			ORDER_LIST_ELEMENT_STATUS("((//li[@class='b-account-order b-account-orders__item'])[#]//div[@class='b-account-order__attribute-value'])[1]","",""),
			ORDER_LIST_ELEMENT_ID("((//li[@class='b-account-order b-account-orders__item'])[#]//div[@class='b-account-order__attribute-value'])[2]","",""),
			ORDER_LIST_ELEMENT_DETAILS_BUTTON("(//li[@class='b-account-order b-account-orders__item'])[#]//a[@class='b-btn_third']","","");
			
			private String locatorAndroid;
			private String locatorIOS;
			private String locatorWeb;

			PurchasesAndOrdersPage(String locatorWeb, String locatorAndroid, String locatorIOS) {
				this.locatorAndroid = locatorAndroid;
				this.locatorIOS=locatorIOS;
				this.locatorWeb=locatorWeb;
			}
			

			public String getAndroidLocator()
			{
				return this.locatorAndroid;
			}

			public String getIOSLocator()
			{
				return this.locatorIOS;
			}
			public String getWebLocator()
			{
				return this.locatorWeb;
			}

		}
		
		public enum SendRequestMailTemplate implements ILocator
		{
			//HEADER("","",""),
			MAIL_BODY("//center/table[1]","",""),
			MAIL_WELCOME("//center/table[1]/tbody[1]/tr[2]/td[1]/h2","",""),
			MAIL_DESCRIPTION("//center/table[1]/tbody[1]/tr[3]/td[1]/p[1]","",""),
			MAIL_DESCRIPTION_TNKS("//center/table[1]/tbody[1]/tr[3]/td[1]/p[2]","",""),
			MAIL_I_TUOI_DATI_HEADER("//center/table[1]/tbody/tr[4]/td[1]/table/caption/em","",""),
			MAIL_NAME_FIELD_TEXT("//center/table[1]/tbody[1]/tr[4]/td[1]/table/tbody/tr[1]/td[1]","",""),
			MAIL_NAME_FIELD_VALUE("//center/table[1]/tbody[1]/tr[4]/td[1]/table/tbody/tr[1]/td[2]","",""),
			MAIL_EMAIL_FIELD_TEXT("//center/table[1]/tbody[1]/tr[4]/td[1]/table/tbody/tr[2]/td[1]","",""),
			MAIL_EMAIL_FIELD_VALUE("//center/table[1]/tbody[1]/tr[4]/td[1]/table/tbody/tr[2]/td[2]","",""),
			MAIL_PHONE_FIELD_TEXT("//center/table[1]/tbody[1]/tr[4]/td[1]/table/tbody/tr[3]/td[1]","",""),
			MAIL_PHONE_FIELD_VALUE("//center/table[1]/tbody[1]/tr[4]/td[1]/table/tbody/tr[3]/td[2]","",""),
			MAIL_REASON_FIELD_TEXT("//center/table[1]/tbody[1]/tr[4]/td[1]/table/tbody/tr[4]/td[1]","",""),
			MAIL_REASON_FIELD_VALUE("//center/table[1]/tbody[1]/tr[4]/td[1]/table/tbody/tr[4]/td[2]","",""),
			MAIL_MESSAGE_FIELD_TEXT("//center/table[1]/tbody[1]/tr[4]/td[1]/table/tbody/tr[5]/td[1]","",""),
			MAIL_MESSAGE_FIELD_VALUE("//center/table[1]/tbody[1]/tr[4]/td[1]/table/tbody/tr[5]/td[2]","","");
			
			private String locatorAndroid;
			private String locatorIOS;
			private String locatorWeb;

			SendRequestMailTemplate(String locatorWeb, String locatorAndroid, String locatorIOS) {
				this.locatorAndroid = locatorAndroid;
				this.locatorIOS=locatorIOS;
				this.locatorWeb=locatorWeb;
			}
			

			public String getAndroidLocator()
			{
				return this.locatorAndroid;
			}

			public String getIOSLocator()
			{
				return this.locatorIOS;
			}
			public String getWebLocator()
			{
				return this.locatorWeb;
			}

		}
		
		public enum FindInStoreProductPage implements ILocator
		{
			HEADER("//span[@id='ui-id-15']","",""),
			GEOIP_BUTTON("//button[@data-id='current-location']","",""),
			SEARCH_BAR("//input[@class='b-find-store__search js-autocomplete-input js-input_field ui-autocomplete-input']","",""),
			FIND_BUTTON("//button[@class='b-find-store__search-button']","",""),
			CLOSE_BUTTON("//button[@class='ui-button ui-corner-all ui-widget ui-button-icon-only ui-dialog-titlebar-close']","",""),
			STORE_LIST("//div[@data-id='store']","",""),
			LOAD_MORE_BUTTON("//button[@data-id='loadMoreButton']","",""),
			STORE_ELEMENT("(//div[@data-id='store'])[#]","",""),
			STORE_ELEMENT_TITLE("(//div[@data-id='store'])[#]//div[@class='b-find-store__address-title']","",""),
			STORE_ELEMENT_ADDRESS("(//div[@data-id='store'])[#]//div[@class='b-find-store__address']","",""),
			NOT_FOUND_MESSAGE("//div[@class='b-find-store__error']","","");
			
			private String locatorAndroid;
			private String locatorIOS;
			private String locatorWeb;

			FindInStoreProductPage(String locatorWeb, String locatorAndroid, String locatorIOS) {
				this.locatorAndroid = locatorAndroid;
				this.locatorIOS=locatorIOS;
				this.locatorWeb=locatorWeb;
			}
			

			public String getAndroidLocator()
			{
				return this.locatorAndroid;
			}

			public String getIOSLocator()
			{
				return this.locatorIOS;
			}
			public String getWebLocator()
			{
				return this.locatorWeb;
			}

		}
		
		public enum CRMPage implements ILocator
		{
			USERNAME("//input[@id='username']","",""),
			PASSWORD("//input[@id='password']","",""),
			LOGIN("//button[@class='btn btn-block btn-primary mt-lg']","",""),
			CLIENTI_BUTTON("//aside[@class='aside']//li[3]//a[1]","",""),
			NOME_FIELD_SEARCH("//input[@id='app_bundle_customer_search_type_name']","",""),
			COGNOME_FIELD_SEARCH("//input[@id='app_bundle_customer_search_type_surname']","",""),
			EMAIL_FIELD_SEARCH("//input[@id='app_bundle_customer_search_type_email']","",""),
			CELLULARE_FIELD_SEARCH("//input[@id='app_bundle_customer_search_type_phone']","",""),
			NUMEROCARTA_FIELD_SEARCH("//input[@id='app_bundle_customer_search_type_cardid']","",""),
			CODCLIENTE_FIELD_SEARCH("//input[@id='app_bundle_customer_search_type_customerid']","",""),
			SEARCH_FIELD_SEARCH("//button[@id='app_bundle_customer_search_type_submit']","",""),
			CLIENTE_LINK("//table[@class='table table-hover']/tbody/tr[1]/td[1]/a","",""),
			MODIFICA_CLIENTE("//button[@class='btn btn-primary btn-lg']","",""),
			MODIFICA_CLIENTE_NOME("//input[@id='app_bundle_customer_add_type_name']","",""),
			MODIFICA_CLIENTE_COGNOME("//input[@id='app_bundle_customer_add_type_surname']","",""),
			MODIFICA_CLIENTE_OPERATORE("//input[@id='app_bundle_customer_add_type_operator']","",""),
			MODIFICA_CLIENTE_ANNO_NASCITA("//select[@id='app_bundle_customer_add_type_dateofbirth_year']","",""),
			MODIFICA_CLIENTE_SAVE("//button[@id='app_bundle_customer_add_type_submit']","",""),
			CONFIRM_OK_POPUP("//button[@class='confirm']","",""),
			OPERATION_OK_MESSAGE("//p[contains(text(),'Cliente modificato correttamente!')]","",""),
			LOGOUT("//a[@class='hidden-xs']//em[@class='fa fa-power-off']","",""),
			INFO_CLIENTE_TAB("//a[contains(text(),'Info Cliente')]","",""),
			INDIRIZZO_CLIENTE("//td[@id='rowInfo-addresses']//ul/li","",""),
			DATA_DI_NASCITA_CLIENTE("//td[@id='rowInfo-dateofbirth']","",""),
			NUCLEO_CLIENTE("//td[@id='rowInfo-nofamilycomponents']","","");
			
			private String locatorAndroid;
			private String locatorIOS;
			private String locatorWeb;

			CRMPage(String locatorWeb, String locatorAndroid, String locatorIOS) {
				this.locatorAndroid = locatorAndroid;
				this.locatorIOS=locatorIOS;
				this.locatorWeb=locatorWeb;
			}
			

			public String getAndroidLocator()
			{
				return this.locatorAndroid;
			}

			public String getIOSLocator()
			{
				return this.locatorIOS;
			}
			public String getWebLocator()
			{
				return this.locatorWeb;
			}

		}
		
		public enum MyProfilePage implements ILocator
		{
			DATI_CONTATTO_HEADER("//h2[contains(text(),'${datiContattoHeader}')]","",""),
			DATI_CONTATTO_EMAIL("//div[@class='b-account-data__value js-email-field']","",""),
			DATI_CONTATTO_PHONE("//div[@class='b-account-data__value js-phone-field']","",""),
			MODIFICA_DATI_CONTATTO("//div[@class='b-account-data__value js-phone-field']//a","",""),
			MODIFICA_PASSWORD("//a[contains(text(),'${modificaPassword}')]","",""),
			DATI_PERSONALI_HEADER("//h2[contains(text(),'${datiPersonaliHeader}')]","",""),
			NOME_TEXT("//div[@class='b-account-data__value' and ./preceding-sibling::*[contains(text(),'${nameField}')]]","",""),
			INDIRIZZO_TEXT("//div[@class='b-account-data__value' and ./preceding-sibling::*[contains(text(),'${addressField}')]]","",""),
			DATA_NASCITA_TEXT("//div[@class='b-account-data__value' and ./preceding-sibling::*[contains(text(),'${dataNascitaField}')]]","",""),
			NAZIONALITA_TEXT("//div[@class='b-account-data__value' and ./preceding-sibling::*[contains(text(),'${nazionalitaField}')]]","",""),
			NUCLEO_FAMILIARE_TEXT("//div[@class='b-account-data__value' and ./preceding-sibling::*[contains(text(),'${nucleoFamiliareField}')]]","",""),
			MINORI_14_TEXT("//div[@class='b-account-data__value' and ./preceding-sibling::*[contains(text(),'${minori14Field}')]]","",""),
			MODIFICA_I_TUOI_DATI("//div[@class='b-account-data__attribute b-account-data__summary-actions']//a","",""),
			ALTRI_INDIRIZZI_HEADER("//h2[contains(text(),'${altriIndirizziHeader}')]","",""),
			ALTRI_INDIRIZZI_ELEMENT("//li[@data-cmp='addressBookItem']","",""),
			ALTRI_INDIRIZZI_ELEMENT_NAME("//li[@data-cmp='addressBookItem'][#]//div[1]","",""),
			ALTRI_INDIRIZZI_ELEMENT_MODIFICA("//li[@data-cmp='addressBookItem'][#]//a[@class='address-edit js-edit-address b-link_tenth']","",""),
			ALTRI_INDIRIZZI_ELEMENT_ELIMINA("//li[@data-cmp='addressBookItem'][#]//a[@data-cmp-id='ConfirmDialogAddressDelete']","",""),
			ALTRI_INDIRIZZI_CONFIRM_DELETE("//button[@class='b-btn_primary ui-button ui-corner-all ui-widget']","",""),
			AGGIUNGI_INDIRIZZO("//a[@class='js-add-new-address b-btn_tenth']","",""),
			MODIFICA_DATI_CONTATTO_EMAIL_TXTFIELD("//input[@id='dwfrm_profile_customer_email']","",""),
			MODIFICA_DATI_CONTATTO_PHONE_TXTFIELD("//input[@id='dwfrm_profile_customer_phone']","",""),
			MODIFICA_DATI_CONTATTO_PASSWORD_TXTFIELD("//input[contains(@id,'dwfrm_profile_login_password')]","",""),
			MODIFICA_DATI_CONTATTO_SUBMIT_BUTTON("//button[@name='dwfrm_profile_changecontact']","",""),
			MODIFICA_PSWD_PASSWORD_ATTUALE("//input[contains(@id,'dwfrm_profile_login_currentpassword')]","",""),
			MODIFICA_PSWD_PASSWORD_NUOVA("(//input[contains(@id,'dwfrm_profile_login_newpassword')])[1]","",""),
			MODIFICA_PSWD_PASSWORD_CONFERMA("(//input[contains(@id,'dwfrm_profile_login_newpassword')])[2]","",""),
			MODIFICA_PSWD_SUBMIT_BUTTON("//button[@name='dwfrm_profile_changepassword']","",""),
			PROFILE_LINK("//ul[@class='b-utility-menu__list js-menu-utility-user-head']//a[@class='b-utility-menu__link icon-account js-user-account']","",""),
			LOGOUT_BUTTON("(//ul[@class='b-utility-menu__list js-menu-utility-user-head']//a[@class='b-utility-menu__flyout-link null'])[4]","",""),
			NAME_FIELD("//input[@id='dwfrm_profile_customer_firstName']","",""),
			SURNAME_FIELD("//input[@id='dwfrm_profile_customer_lastName']","",""),
			ADDRESS_FIELD("//input[@id='dwfrm_profile_address_address1']","",""),
			CAP_FIELD("//input[@id='dwfrm_profile_address_postal']","",""),
			CITY_FIELD("//input[@id='dwfrm_profile_address_city']","",""),
			PROVINCE_FIELD("//select[@id='dwfrm_profile_address_states_state']","",""),
			DAY_OF_BORN("//select[@id='dwfrm_profile_customer_birthdayfields_day']","",""),
			MONTH_OF_BORN("//select[@id='dwfrm_profile_customer_birthdayfields_month']","",""),
			YEAR_OF_BORN("//select[@id='dwfrm_profile_customer_birthdayfields_year']","",""),
			CONSENSI("//label[@for='dwfrm_profile_customer_termsandcondition_true']","",""),
			NAZIONALITY_FIELD("//select[@id='dwfrm_profile_nationality_nationality']","",""),
			NUMBERS_FAMILY_2("//label[@for='dwfrm_profile_custom_numberoffamily_2']","",""),
			NUMBERS_FAILY_1("//label[@for='dwfrm_profile_custom_numberoffamily_1']","",""),
			MODIFICA_DATI_PERSONALI_SUBMIT("//button[@name='dwfrm_profile_editprofile']","",""),
			MR_RADIO("(//div[@class='b-radiobutton__item']//label)[2]","",""),
			NAME_NEW_ADDRESS_FIELD("//input[contains(@id,'dwfrm_profile_address_firstName')]","",""),
			SURNAME_NEW_ADDRESS_FIELD("//input[contains(@id,'dwfrm_profile_address_lastName')]","",""),
			ADDRESS_NEW_ADDRESS_FIELD("//input[contains(@id,'dwfrm_profile_address_address1')]","",""),
			CAP_NEW_ADDRESS_FIELD("//input[contains(@id,'dwfrm_profile_address_postal')]","",""),
			CITY_NEW_ADDRESS_FIELD("//input[contains(@id,'dwfrm_profile_address_city')]","",""),
			PROVINCE_NEW_ADDRESS_FIELD("//select[contains(@id,'dwfrm_profile_address_states')]","",""),
			SUBMIT_NEW_ADDRESS_BUTTON("//button[@name='dwfrm_profile_address_create']","",""),
			NUMERO_BAMBINI("//label[@for='dwfrm_profile_custom_numberofchildren_0']","",""),
			CATEGORIA_BAMBINI("//label[@for='dwfrm_profile_custom_prefferedcategory_children']","",""),
			ACQUISTI_ONLINE_SI("//label[@for='dwfrm_profile_custom_onlinepurchases_yes']","",""),
			HEADER_POPUP("//span[@id='ui-id-7']","",""),
			CONGRATULAZIONI_MESSAGE("//div[@class='b-popup__congrats-message']//p","",""),
			CLOSE_CONGRAT_POPUP("//button[@class='b-btn_primary ui-button ui-corner-all ui-widget']","","");
			
			private String locatorAndroid;
			private String locatorIOS;
			private String locatorWeb;

			MyProfilePage(String locatorWeb, String locatorAndroid, String locatorIOS) {
				this.locatorAndroid = locatorAndroid;
				this.locatorIOS=locatorIOS;
				this.locatorWeb=locatorWeb;
			}
			

			public String getAndroidLocator()
			{
				return this.locatorAndroid;
			}

			public String getIOSLocator()
			{
				return this.locatorIOS;
			}
			public String getWebLocator()
			{
				return this.locatorWeb;
			}

		}
		
		public enum GPWebPayPage implements ILocator
		{
			CARD("//input[@id='inputPan']","",""),
			MONTH("(//span[contains(@class,'sod_select')])[2]","",""),
			MONTH_VALUE("(//span[contains(@class,'sod_select')])[2]//span[@data-value='${value}']","",""),
			YEAR("(//span[contains(@class,'sod_select')])[3]","",""),
			YEAR_VALUE("(//span[contains(@class,'sod_select')])[3]//span[@data-value='${value}']","",""),
			CVV("//input[@id='inputCvc']","",""),
			SUBMIT("//button[@id='send']","",""),
			SMS_SUBMIT("//input[@id='smsCodeSubmit']","","");
			
			private String locatorAndroid;
			private String locatorIOS;
			private String locatorWeb;

			GPWebPayPage(String locatorWeb, String locatorAndroid, String locatorIOS) {
				this.locatorAndroid = locatorAndroid;
				this.locatorIOS=locatorIOS;
				this.locatorWeb=locatorWeb;
			}
			

			public String getAndroidLocator()
			{
				return this.locatorAndroid;
			}

			public String getIOSLocator()
			{
				return this.locatorIOS;
			}
			public String getWebLocator()
			{
				return this.locatorWeb;
			}

		}
		
		public enum IOSNative implements ILocator
		{
			PICKER_WELL_LIST("","","//*[@class='UIAView' and ./ancestor::*[@class='UIAPicker']]"), 
			PICKER_SELECTED_ELEMENT("","","//*[@XCElementType='XCUIElementTypePicker']//*[@XCElementType='XCUIElementTypeOther' and ./child::*[@class='UIAView']]");
			
			private String locatorAndroid;
			private String locatorIOS;
			private String locatorWeb;

			IOSNative(String locatorWeb, String locatorAndroid, String locatorIOS) {
				this.locatorAndroid = locatorAndroid;
				this.locatorIOS=locatorIOS;
				this.locatorWeb=locatorWeb;
			}
			

			public String getAndroidLocator()
			{
				return this.locatorAndroid;
			}

			public String getIOSLocator()
			{
				return this.locatorIOS;
			}
			public String getWebLocator()
			{
				return this.locatorWeb;
			}

		}
		
		public enum YahooMailPage implements ILocator
		{
			USERNAME("//*[@id='login-username']","",""), 
			GO_NEXT("//*[@id='login-signin']","",""),
			PASSWORD("//*[@id='login-passwd']","",""),
			LOGIN("//*[@id='login-signin']","",""),
			MAIL_LIST("//*[@data-test-id=\"message-subject\"]","",""),
			MAIL("//*[contains(text(),'${subject}')]","","");
			
			private String locatorAndroid;
			private String locatorIOS;
			private String locatorWeb;

			YahooMailPage(String locatorWeb, String locatorAndroid, String locatorIOS) {
				this.locatorAndroid = locatorAndroid;
				this.locatorIOS=locatorIOS;
				this.locatorWeb=locatorWeb;
			}
			

			public String getAndroidLocator()
			{
				return this.locatorAndroid;
			}

			public String getIOSLocator()
			{
				return this.locatorIOS;
			}
			public String getWebLocator()
			{
				return this.locatorWeb;
			}

		}
		
		public enum GmailMailPage implements ILocator
		{
			USERNAME("//input[@id='identifierId']","",""), 
			GO_NEXT("//div[@id='identifierNext']","",""),
			PASSWORD("//input[@type='password']","",""),
			LOGIN("//div[@id='passwordNext']","",""),
			MAIL_LIST("//table[@class='F cf zt']/tbody/tr/td[6]//span[1]","",""),
			MAIL("//table[@class='F cf zt']/tbody/tr/td[6]//span[contains(text(),'${subject}')]","","");
			
			private String locatorAndroid;
			private String locatorIOS;
			private String locatorWeb;

			GmailMailPage(String locatorWeb, String locatorAndroid, String locatorIOS) {
				this.locatorAndroid = locatorAndroid;
				this.locatorIOS=locatorIOS;
				this.locatorWeb=locatorWeb;
			}
			

			public String getAndroidLocator()
			{
				return this.locatorAndroid;
			}

			public String getIOSLocator()
			{
				return this.locatorIOS;
			}
			public String getWebLocator()
			{
				return this.locatorWeb;
			}

		}
		
		public enum WelcomeUserPage implements ILocator
		{
			HEADER("//h1[@class='h-text-center']","",""), 
			GO_NEXT("//a[@class='b-btn_secondary h-margin-top-10']","","");
			
			private String locatorAndroid;
			private String locatorIOS;
			private String locatorWeb;

			WelcomeUserPage(String locatorWeb, String locatorAndroid, String locatorIOS) {
				this.locatorAndroid = locatorAndroid;
				this.locatorIOS=locatorIOS;
				this.locatorWeb=locatorWeb;
			}
			

			public String getAndroidLocator()
			{
				return this.locatorAndroid;
			}

			public String getIOSLocator()
			{
				return this.locatorIOS;
			}
			public String getWebLocator()
			{
				return this.locatorWeb;
			}

		}

		public static int getDriverType(WebDriver driver) 
		{
			if(driver instanceof ChromeDriver)
			{
				return DESKTOP_CHROME;
			}
			else if(driver instanceof InternetExplorerDriver)
			{
				return DESKTOP_IE;
			}
			else if(driver instanceof SafariDriver)
			{
				return DESKTOP_SAFARI;
			}
			else if(driver instanceof AndroidDriver)
			{
				return ANDROID_CHROME;
			}
			else if(driver instanceof IOSDriver)
			{
				return IOS_SAFARI;
			}
			return -1;
		}
		
}