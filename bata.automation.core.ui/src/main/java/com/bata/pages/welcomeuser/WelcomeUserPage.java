package com.bata.pages.welcomeuser;

import org.openqa.selenium.WebDriver;

import java.util.Properties;

import org.openqa.selenium.support.PageFactory;

import org.openqa.selenium.support.ui.LoadableComponent;
import com.bata.pagepattern.Locators;


public class WelcomeUserPage extends LoadableComponent<WelcomeUserPage> {
	
	
	private int DRIVER;
	private WelcomePageManager gesture;
	
	protected WelcomeUserPage(WebDriver driver,Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		default:
			gesture=WelcomePageManager.with(driver, languages);
		}
	}
	
	public static WelcomeUserPage from(WebDriver driver, Properties languages)
	{
		return new WelcomeUserPage(driver, languages).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}
}
