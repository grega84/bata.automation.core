package com.bata.pages.welcomeuser;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Locators;
import com.bata.pages.AbstractPageManager;

import common.CommonFields;
import test.automation.core.UIUtils;

public class WelcomePageManager extends AbstractPageManager {

	private Object logo;
	private WebElement header;
	private WebElement goNext;

	private WelcomePageManager(WebDriver driver, Properties languages) 
	{
		super(driver,languages);
	}

	public static WelcomePageManager with(WebDriver driver, Properties languages) 
	{
		return new WelcomePageManager(driver, languages);
	}

	public WelcomePageManager isLoaded() 
	{
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("ita"))
			return this;
		
		logo=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Homepage.LOGO.getWebLocator())),40);
		Assert.assertTrue(logo!=null);
		
		header=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.WelcomeUserPage.HEADER.getWebLocator())),40);
		Assert.assertTrue(header!=null);
		
		goNext=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.WelcomeUserPage.GO_NEXT.getWebLocator())),40);
		Assert.assertTrue(goNext!=null);
		
		
		return this;
	}

}
