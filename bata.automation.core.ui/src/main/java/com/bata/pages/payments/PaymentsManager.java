package com.bata.pages.payments;

import java.util.Properties;
import java.util.Set;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.AbstractPageManager;

import bean.datatable.CreditCardErrorCheckBean;
import bean.datatable.PaymentBean;
import common.CommonFields;
import common.WaitManager;
import test.automation.core.UIUtils;

public class PaymentsManager extends AbstractPageManager 
{
	protected WebElement logo=null;
	protected WebElement billingAddress=null;
	protected WebElement inputCoupon=null;
	protected WebElement applyButton=null;
	protected WebElement creditCard=null;
	protected WebElement cardOwner=null;
	protected WebElement cardNumber=null;
	protected WebElement monthExpired=null;
	protected String monthExpiredDate=null;
	protected WebElement yearExpired=null;
	protected String yearExpiredDate=null;
	protected WebElement cvv=null;
	protected WebElement checkboxInformativa=null;
	protected WebElement checkboxConditions=null;
	protected WebElement confirmButton=null;
	protected WebElement paypalLabel=null;
	protected WebElement price=null;
	protected WebElement paypalButton=null;
	protected WebElement cod=null;
	
	public PaymentsManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public PaymentsManager applyCupon(PaymentBean paymentBean)
	{
		inputCoupon=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.INPUT_COUPON.getWebLocator()));
		inputCoupon.sendKeys(paymentBean.getCoupon());
		applyButton=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PaymentPage.APPLY_BUTTON.getWebLocator())),40);
		applyButton.click();
		
		return this;
	}
	
	public PaymentsManager insertCreditCardData(PaymentBean paymentBean)
	{
		if(!DinamicData.getIstance().get(CommonFields.COUNTRY).equals("cz"))
		{
			cardOwner=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.OWNER_CARD.getWebLocator()));
			cardOwner.clear();
			cardOwner.sendKeys(paymentBean.getOwnerCard());
			cardNumber=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.NUMBER_CARD.getWebLocator()));
			cardNumber.clear();
			cardNumber.sendKeys(paymentBean.getCardNumber());
			
			monthExpired=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.MONTH_EXPIRED.getWebLocator()));
			monthExpiredDate=paymentBean.getMonthExpiredDate();
			Select monthExpiredSelect = new Select(monthExpired);
			monthExpiredSelect.selectByVisibleText(monthExpiredDate);
			
			yearExpired=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.YEAR_EXPIRED.getWebLocator()));
			yearExpiredDate=paymentBean.getYearExpiredDate();
			Select yearExpiredSelect = new Select(yearExpired);
			yearExpiredSelect.selectByVisibleText(yearExpiredDate);
			cvv=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.CVV.getWebLocator()));
			cvv.clear();
			cvv.sendKeys(paymentBean.getCvv());
			price=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.PRICE.getWebLocator()));
			
			
		}
		else if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("cz"))
		{
			try {Thread.sleep(5000);} catch (InterruptedException e) {}
		}
		
		return this;
	}
	
	public PaymentsManager selectTc()
	{
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("sk"))
		{
			checkboxInformativa=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.CHECKBOX_INFORMATIVA.getWebLocator()));
			try {Thread.sleep(1000);} catch (InterruptedException e) {}
			
			checkboxInformativa=UIUtils.ui().waitForCondition(driver, ExpectedConditions.elementToBeClickable(By.xpath(Locators.PaymentPage.CHECKBOX_INFORMATIVA.getWebLocator())));
			
			Dimension d=checkboxInformativa.getSize();
			float dx,dy;
			dx=d.width * 0.3f;
			
			Actions act=new Actions(driver);
			
			act.moveToElement(checkboxInformativa, (int) -dx, 0).click().build().perform();
			
			checkboxConditions=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.CHECKBOX_CONDITIONS_2.getWebLocator()));
			try {Thread.sleep(1000);} catch (InterruptedException e) {}
			
			
			d=checkboxConditions.getSize();
			dx=d.width * 0.3f;
			
			act=new Actions(driver);
			
			act.moveToElement(checkboxConditions, (int) -dx, 0).click().build().perform();
			
		}
		else
		{
			UIUtils.ui().safari().scrollToElement(driver, Locators.PaymentPage.CHECKBOX_INFORMATIVA.getWebLocator());
			WaitManager.get().waitShortTime();
			UIUtils.ui().safari().click(driver, Locators.PaymentPage.CHECKBOX_INFORMATIVA.getWebLocator());
			WaitManager.get().waitShortTime();
			UIUtils.ui().safari().scrollToElement(driver, Locators.PaymentPage.CHECKBOX_CONDITIONS_2.getWebLocator());
			WaitManager.get().waitShortTime();
			UIUtils.ui().safari().click(driver, Locators.PaymentPage.CHECKBOX_CONDITIONS_2.getWebLocator());
			WaitManager.get().waitShortTime();
		}
		
		return this;
	}

	public PaymentsManager selectCreditCard() 
	{
		//TODO: pezza a colori su i need invoice clicco senza un domani cosi da chiudere il brokenart
		try
		{
			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//fieldset[@class='b-checkout-billing__invoice-wrapper js-invoice-fields']")), 5);
			WebElement needInvoice=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath("//label[@for='dwfrm_billing_billingAddress_invoice_invoiceRequested']"));
			Thread.sleep(500);
			needInvoice.click();
		}
		catch(Exception err)
		{
			
		}
		
		creditCard=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.CREDIT_CARD.getWebLocator()));
		creditCard.click();
		
		return this;
	}
	
	public PaymentsManager submitCreditCardPayment()
	{
		confirmButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.CONFIRM_BUTTON.getWebLocator()));
		confirmButton.click();
		return this;
	}
	
	public PaymentsManager selectPayPal()
	{
		//TODO: pezza a colori su i need invoice clicco senza un domani cosi da chiudere il brokenart
		try
		{
			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//fieldset[@class='b-checkout-billing__invoice-wrapper js-invoice-fields']")), 5);
			WebElement needInvoice=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath("//label[@for='dwfrm_billing_billingAddress_invoice_invoiceRequested']"));
			Thread.sleep(500);
			needInvoice.click();
		}
		catch(Exception err)
		{
			
		}
		
		paypalLabel=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.PAYPAL.getWebLocator()));
		paypalLabel.click();
		
		return this;
	}
	
	public PaymentsManager submitPayPalPayment()
	{
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String winHandler=driver.getWindowHandle();
		
		paypalButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.PAYPAL_BUTTON.getWebLocator()));
		paypalButton.click();
		
		Utility.waitNewWindowAndGoToIt(driver,winHandler);
		
		//System.out.println("page source:"+driver.getPageSource());
	

		return this;
	}
	
	public PaymentsManager selectCashOnDelivery()
	{
		//TODO: pezza a colori su i need invoice clicco senza un domani cosi da chiudere il brokenart
		try
		{
			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//fieldset[@class='b-checkout-billing__invoice-wrapper js-invoice-fields']")), 5);
			WebElement needInvoice=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath("//label[@for='dwfrm_billing_billingAddress_invoice_invoiceRequested']"));
			Thread.sleep(500);
			needInvoice.click();
		}
		catch(Exception err)
		{
			
		}
		
		cod=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.COD.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		cod.click();
		
		return this;
	}
	
	public PaymentsManager submitCashOnDeliveryPayment()
	{
		confirmButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.CONFIRM_BUTTON.getWebLocator()));
		try {Thread.sleep(1000);}catch(Exception err) {}
		confirmButton.click();
		
		return this;
	}
	
	public PaymentsManager checkTsErrors()
	{
		WebElement error=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PaymentPage.PERSONAL_DATA_ERROR_MESSAGE.getWebLocator())));
		Assert.assertTrue(error != null);
		
		String actual=error.getText().trim();
		String expected=language.getProperty("personalDataError");
		
		Assert.assertTrue("controllo comparsa messaggio di errore per personal data; attuale:"+actual+", atteso:"+expected,actual.equals(expected));
		
		error=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PaymentPage.TERMS_OF_SALE_ERROR_MESSAGE.getWebLocator())));
		Assert.assertTrue(error != null);
		
		actual=error.getText().trim();
		expected=language.getProperty("termsOfSaleError");
		
		Assert.assertTrue("controllo comparsa messaggio di errore per termini del servizio; attuale:"+actual+", atteso:"+expected,actual.equals(expected));

		return this;
	}
	
	public PaymentsManager checkCreditCardErrors(CreditCardErrorCheckBean error)
	{
		WebElement errorMessage=null;
		String actual=null;
		String expected=null;
		
		switch(error.getErrorCheck())
		{
		case CreditCardErrorCheckBean.ALL_FIELD_EMPTY:
			//controllo credit card owner
			errorMessage=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.CREDIT_CARD_OWNER_ERROR.getWebLocator()));
			try {Thread.sleep(500);}catch(Exception err) {}
			
			actual=errorMessage.getText().trim();
			expected=language.getProperty("creditCardOwnerEmptyError");
			
			Assert.assertTrue("controllo presenza messaggio di errore requiredtext per cardOwner; attuale:"+actual+",expected:"+expected, actual.equals(expected));
			
			//controllo credit card number
			errorMessage=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.CREDIT_CARD_NUMBER_ERROR.getWebLocator()));
			try {Thread.sleep(500);}catch(Exception err) {}
			
			actual=errorMessage.getText().trim();
			expected=language.getProperty("creditCardOwnerEmptyError");
			
			Assert.assertTrue("controllo presenza messaggio di errore requiredtext per cardNumber; attuale:"+actual+",expected:"+expected, actual.equals(expected));
			
			//controllo credit card cvn
			errorMessage=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.CREDIT_CARD_CVN_ERROR.getWebLocator()));
			try {Thread.sleep(500);}catch(Exception err) {}
			
			actual=errorMessage.getText().trim();
			expected=language.getProperty("creditCardOwnerEmptyError");
			
			Assert.assertTrue("controllo presenza messaggio di errore requiredtext per cardCvn; attuale:"+actual+",expected:"+expected, actual.equals(expected));
			
			break;
		case CreditCardErrorCheckBean.CARD_NUMBER_LESS_THAN_16:
			//controllo credit card number
			errorMessage=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.CREDIT_CARD_NUMBER_ERROR.getWebLocator()));
			try {Thread.sleep(500);}catch(Exception err) {}
			
			actual=errorMessage.getText().trim();
			expected=language.getProperty("cardNumberLessThan16");
			
			Assert.assertTrue("controllo presenza messaggio di errore numero inferiore a 16 per cardNumber; attuale:"+actual+",expected:"+expected, actual.equals(expected));
	
			break;
			
		case CreditCardErrorCheckBean.CARD_NUMBER_GREATER_THAN_16:
			//controllo credit card number
			errorMessage=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.CREDIT_CARD_NUMBER_ERROR.getWebLocator()));
			try {Thread.sleep(500);}catch(Exception err) {}
			
			actual=errorMessage.getText().trim();
			expected=language.getProperty("cardNumberLessThan16");
			
			Assert.assertTrue("controllo presenza messaggio di errore numero maggiore a 16 per cardNumber; attuale:"+actual+",expected:"+expected, actual.equals(expected));
	
			break;
			
		case CreditCardErrorCheckBean.CARD_NUMBER_WITH_ALFANUMERIC:
			//controllo credit card number
			errorMessage=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.CREDIT_CARD_NUMBER_ERROR.getWebLocator()));
			try {Thread.sleep(500);}catch(Exception err) {}
			
			actual=errorMessage.getText().trim();
			expected=language.getProperty("cardNumberWithAlpha");
			
			Assert.assertTrue("controllo presenza messaggio di errore numero contenente valori alfanumerici per cardNumber; attuale:"+actual+",expected:"+expected, actual.equals(expected));
	
			break;
		case CreditCardErrorCheckBean.CARD_OWNER_WITH_ONE_CHAR:
			//controllo credit card number
			errorMessage=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.CREDIT_CARD_OWNER_ERROR.getWebLocator()));
			try {Thread.sleep(500);}catch(Exception err) {}
			
			actual=errorMessage.getText().trim();
			expected=language.getProperty("cardOwnerInvalid");
			
			Assert.assertTrue("controllo presenza messaggio di errore owner con un solo carattere per cardOwner; attuale:"+actual+",expected:"+expected, actual.equals(expected));
	
			break;
			
		case CreditCardErrorCheckBean.CARD_OWNER_WITH_ONLY_DIGITS:
			//controllo credit card number
			errorMessage=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.CREDIT_CARD_OWNER_ERROR.getWebLocator()));
			try {Thread.sleep(500);}catch(Exception err) {}
			
			actual=errorMessage.getText().trim();
			expected=language.getProperty("cardOwnerInvalid");
			
			Assert.assertTrue("controllo presenza messaggio di errore owner con un solo carattere per cardOwner; attuale:"+actual+",expected:"+expected, actual.equals(expected));
	
			break;
			
		case CreditCardErrorCheckBean.CARD_CVV_LESS_THAN_3:
			//controllo credit card number
			errorMessage=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.CREDIT_CARD_CVN_ERROR.getWebLocator()));
			try {Thread.sleep(500);}catch(Exception err) {}
			
			actual=errorMessage.getText().trim();
			expected=language.getProperty("cvvLessThan3");
			
			Assert.assertTrue("controllo presenza messaggio di errore cvv con un solo carattere per cardCvn; attuale:"+actual+",expected:"+expected, actual.equals(expected));
	
			break;
		case CreditCardErrorCheckBean.CARD_CVV_ALPHANUMERIC:
			//controllo credit card number
			errorMessage=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.CREDIT_CARD_CVN_ERROR.getWebLocator()));
			try {Thread.sleep(500);}catch(Exception err) {}
			
			actual=errorMessage.getText().trim();
			expected=language.getProperty("cvvInvalid");
			
			Assert.assertTrue("controllo presenza messaggio di errore cvv solo caratteri alfanumerici per cardCvn; attuale:"+actual+",expected:"+expected, actual.equals(expected));
	
			break;
		}
		
		return this;
	}
	
	public PaymentsManager checkCodIsntVisible()
	{
		try
		{
			this.cod=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.COD.getWebLocator()));
			Assert.assertTrue("cod ancora presente dopo ordine maggiore di 500€",false);
		}
		catch(Exception err)
		{
			
		}
		
		return this;
	}
}
