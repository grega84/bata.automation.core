package com.bata.pages.contactpage;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.Locators;
import com.bata.pages.AbstractPageManager;

import bean.datatable.ContactBean;
import io.appium.java_client.AppiumDriver;
import test.automation.core.UIUtils;

public class ContactPageManager extends AbstractPageManager 
{
	protected WebElement title;
	protected WebElement titleForm;
	protected WebElement contactAddress;
	protected WebElement contactPhone;
	protected WebElement contactEmail;
	protected WebElement inputEmail;
	protected WebElement inputPhone;
	protected WebElement inputName;
	protected WebElement inputSurname;
	protected WebElement inputRagione;
	protected WebElement inputCommento;
	protected WebElement checkboxInformativa;
	protected WebElement buttonInvia;
	protected WebElement faq;
	protected WebElement modal;
	protected WebElement modalTitle;
	protected WebElement modalBody;
	protected WebElement modalButton;
	
	protected ContactPageManager(WebDriver driver, Properties languages) 
	{
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static ContactPageManager with(WebDriver driver, Properties languages)
	{
		return new ContactPageManager(driver, languages);
	}

	public ContactPageManager isLoaded()
	{
		title=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Contacts.TITLE.getWebLocator())),40);
		contactAddress=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Contacts.ADDRESS_CONTACT.getWebLocator())),40);
		contactPhone=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Contacts.PHONE_CONTACT.getWebLocator())),40);
		contactEmail=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Contacts.EMAIL_CONTACT.getWebLocator())),40);
		faq=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Contacts.FAQ.getWebLocator())),40);
		Assert.assertTrue(title != null);
		Assert.assertTrue(contactAddress != null);
		Assert.assertTrue(contactPhone != null);
		Assert.assertTrue(contactEmail != null);
		Assert.assertTrue(faq != null);
		return this;
	}
	
	public ContactPageManager compileForm(ContactBean contactBean, Properties language)
	{
		inputEmail=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Contacts.EMAIL_INPUT.getWebLocator()));
		try {Thread.sleep(500);} catch (InterruptedException e) {}
		inputEmail.sendKeys(contactBean.getEmail());
		try {((AppiumDriver)driver).hideKeyboard();} catch(Exception err) {}
		
		inputPhone=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Contacts.PHONE_INPUT.getWebLocator()));
		try {Thread.sleep(500);} catch (InterruptedException e) {}
		inputPhone.sendKeys(contactBean.getPhone());
		try {((AppiumDriver)driver).hideKeyboard();} catch(Exception err) {}
		
		inputName=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Contacts.NOME_INPUT.getWebLocator()));
		try {Thread.sleep(500);} catch (InterruptedException e) {}
		inputName.sendKeys(contactBean.getName());
		try {((AppiumDriver)driver).hideKeyboard();} catch(Exception err) {}
		
		inputSurname=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Contacts.SURNAME_INPUT.getWebLocator()));
		try {Thread.sleep(500);} catch (InterruptedException e) {}
		inputSurname.sendKeys(contactBean.getSurname());
		try {((AppiumDriver)driver).hideKeyboard();} catch(Exception err) {}
		
		inputRagione=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Contacts.RAGIONE_INPUT.getWebLocator()));
		
		Select ragioneSelect = new Select(inputRagione);
		ragioneSelect.selectByVisibleText(language.getProperty(contactBean.getReason()));
		
		inputCommento=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Contacts.COMMENT_INPUT.getWebLocator()));
		inputCommento.sendKeys(contactBean.getComment());
		try {((AppiumDriver)driver).hideKeyboard();} catch(Exception err) {}
		try {Thread.sleep(500);} catch(Exception err) {}
		
		
		checkboxInformativa=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Contacts.CHECKBOX.getWebLocator()));
		checkboxInformativa.click();
		try {Thread.sleep(500);} catch(Exception err) {}
		
		return this;
	}
	
	public ContactPageManager checkSendRequestPopup() 
	{
		modal=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Contacts.SEND_MAIL_POPUP_DIV.getWebLocator())), 60);
		Assert.assertTrue(modal != null);
		
		modalTitle=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Contacts.SEND_MAIL_POPUP_TITLE.getWebLocator())), 60);
		Assert.assertTrue(modalTitle != null);
		
		modalBody=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Contacts.SEND_MAIL_POPUP_BODY.getWebLocator())), 60);
		Assert.assertTrue(modalBody != null);
		
		//controllo popup
		String actual=modalTitle.getText().trim().toLowerCase();
		String expected=language.getProperty("modalTitle").toLowerCase();
		
		Assert.assertTrue("controllo titolo popup di send contact;attuale:"+actual+", atteso:"+expected,actual.equals(expected));
		
		actual=modalBody.getText().trim().toLowerCase();
		expected=language.getProperty("modalBody").toLowerCase();
		
		Assert.assertTrue("controllo body popup di send contact;attuale:"+actual+", atteso:"+expected,actual.equals(expected));

		return this;
	}

	public ContactPageManager closePopup() 
	{
		modalButton=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Contacts.SEND_MAIL_POPUP_OK_BUTTON.getWebLocator())), 60);
		Assert.assertTrue(modalButton != null);
		
		modalButton.click();
		return this;
	}

	public ContactPageManager sendRequest() 
	{
		buttonInvia=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Contacts.INVIA_BUTTON.getWebLocator()));
		buttonInvia.click();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			
		}

		return this;
	}
}
