package com.bata.pages.contactpage;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;

import com.bata.pagepattern.Locators;

import bean.datatable.ContactBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;

public class ContactPageManagerIOS extends ContactPageManager 
{

	public ContactPageManagerIOS(WebDriver driver, Properties languages) 
	{
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static ContactPageManagerIOS with(WebDriver driver, Properties languages)
	{
		return new ContactPageManagerIOS(driver, languages);
	}
	
	public ContactPageManagerIOS compileForm(ContactBean contactBean, Properties language)
	{
		inputEmail=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Contacts.EMAIL_INPUT.getWebLocator()));
		try {Thread.sleep(500);} catch (InterruptedException e) {}
		inputEmail.sendKeys(contactBean.getEmail());
		try {((AppiumDriver)driver).hideKeyboard();} catch(Exception err) {}
		
		inputPhone=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Contacts.PHONE_INPUT.getWebLocator()));
		try {Thread.sleep(500);} catch (InterruptedException e) {}
		inputPhone.sendKeys(contactBean.getPhone());
		try {((AppiumDriver)driver).hideKeyboard();} catch(Exception err) {}
		
		inputName=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Contacts.NOME_INPUT.getWebLocator()));
		try {Thread.sleep(500);} catch (InterruptedException e) {}
		inputName.sendKeys(contactBean.getName());
		try {((AppiumDriver)driver).hideKeyboard();} catch(Exception err) {}
		
		inputSurname=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Contacts.SURNAME_INPUT.getWebLocator()));
		try {Thread.sleep(500);} catch (InterruptedException e) {}
		inputSurname.sendKeys(contactBean.getSurname());
		try {((AppiumDriver)driver).hideKeyboard();} catch(Exception err) {}
		
		inputRagione=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Contacts.RAGIONE_INPUT.getWebLocator()));
		
		inputRagione.click();
		try {Thread.sleep(1000);} catch(Exception err) {}
		((AppiumDriver)driver).context("NATIVE_APP");
		
		UIUtils.ui().mobile().selectElementFromIOSPickerWeel((MobileDriver) driver, Locators.IOSNative.PICKER_WELL_LIST.getIOSLocator(), Locators.IOSNative.PICKER_SELECTED_ELEMENT.getIOSLocator(), language.getProperty(contactBean.getReason()));
		
		((AppiumDriver)driver).context("WEBVIEW_1");
		
		inputCommento=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Contacts.COMMENT_INPUT.getWebLocator()));
		inputCommento.sendKeys(contactBean.getComment());
		try {((AppiumDriver)driver).hideKeyboard();} catch(Exception err) {}
		try {Thread.sleep(1000);} catch(Exception err) {}
		
		checkboxInformativa=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Contacts.CHECKBOX.getWebLocator()));
		checkboxInformativa=driver.findElement(By.xpath(Locators.Contacts.CHECKBOX.getWebLocator()));
		
		Point p=checkboxInformativa.getLocation();
		Dimension l=checkboxInformativa.getSize();
		
		UIUtils.ui().safari().click(driver, Locators.Contacts.CHECKBOX.getWebLocator());
		
		//((AppiumDriver)driver).context("WEBVIEW_1");
		
		try {Thread.sleep(1000);} catch(Exception err) {}
		
		return this;
	}
}
