package com.bata.pages.contactpage;

import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;

import bean.datatable.ContactBean;
import bean.datatable.PaymentBean;
import bean.datatable.SearchItemBean;
import bean.datatable.ShippingBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import kafka.utils.threadsafe;
import test.automation.core.UIUtils;

public class ContactPage extends LoadableComponent<ContactPage> {
	
	private ContactPageManager gesture;
	
	private int DRIVER;
	
	protected ContactPage(WebDriver driver,Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		case Locators.IOS_SAFARI:
			gesture=ContactPageManagerIOS.with(driver, languages);
			break;
		case Locators.DESKTOP_IE:
			gesture=ConcactPageManagerIE.with(driver, languages);
			break;
		default:
			gesture=ContactPageManager.with(driver, languages);
		}
	}
	
	public static ContactPage from(WebDriver driver,Properties language)
	{
		return new ContactPage(driver, language).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}
	
	public ContactPage compileForm(ContactBean contactBean, Properties language) throws InterruptedException 
	{
		gesture.compileForm(contactBean, language);
		return this;
	}

	public ContactPage checkSendRequestPopup(Properties language) 
	{
		gesture.checkSendRequestPopup();
		return this;
	}

	public ContactPage closePopup() 
	{
		gesture.closePopup();
		return this;
	}

	public ContactPage sendRequest() 
	{
		gesture.sendRequest();
		return this;
	}
	
}
