package com.bata.pages.contactpage;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.Locators;

import bean.datatable.ContactBean;
import io.appium.java_client.AppiumDriver;
import test.automation.core.SystemClipboard;
import test.automation.core.UIUtils;

public class ConcactPageManagerIE extends ContactPageManager {

	public ConcactPageManagerIE(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static ConcactPageManagerIE with(WebDriver driver, Properties languages)
	{
		return new ConcactPageManagerIE(driver, languages);
	}
	
	public ContactPageManager compileForm(ContactBean contactBean, Properties language)
	{
		inputEmail=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Contacts.EMAIL_INPUT.getWebLocator()));
		try {Thread.sleep(500);} catch (InterruptedException e) {}
		inputEmail.click();
		try {SystemClipboard.copyAndPaste(contactBean.getEmail());} catch(Exception err) {}
		
		inputPhone=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Contacts.PHONE_INPUT.getWebLocator()));
		try {Thread.sleep(500);} catch (InterruptedException e) {}
		inputPhone.click();
		try {SystemClipboard.copyAndPaste(contactBean.getPhone());} catch(Exception err) {}
		
		
		inputName=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Contacts.NOME_INPUT.getWebLocator()));
		try {Thread.sleep(500);} catch (InterruptedException e) {}
		inputName.click();
		try {SystemClipboard.copyAndPaste(contactBean.getName());} catch(Exception err) {}
		
		
		inputSurname=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Contacts.SURNAME_INPUT.getWebLocator()));
		try {Thread.sleep(500);} catch (InterruptedException e) {}
		inputSurname.click();
		try {SystemClipboard.copyAndPaste(contactBean.getSurname());} catch(Exception err) {}
		
		
		inputRagione=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Contacts.RAGIONE_INPUT.getWebLocator()));
		
		Select ragioneSelect = new Select(inputRagione);
		ragioneSelect.selectByVisibleText(language.getProperty(contactBean.getReason()));
		
		inputCommento=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Contacts.COMMENT_INPUT.getWebLocator()));
		inputCommento.click();
		try {SystemClipboard.copyAndPaste(contactBean.getComment());} catch(Exception err) {}
		
		checkboxInformativa=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Contacts.CHECKBOX.getWebLocator()));
		checkboxInformativa.click();
		try {Thread.sleep(500);} catch(Exception err) {}
		
		return this;
	}
}
