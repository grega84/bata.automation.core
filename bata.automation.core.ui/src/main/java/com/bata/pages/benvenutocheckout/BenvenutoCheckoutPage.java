package com.bata.pages.benvenutocheckout;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import com.bata.pagepattern.Locators;
import com.bata.pages.profile.ProfilePage;
import com.bata.pages.shipping.ShippingPage;

import bean.datatable.CredentialBean;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class BenvenutoCheckoutPage extends LoadableComponent<BenvenutoCheckoutPage> {
	
	private BenvenutoCheckoutPageManager gesture;
	private int DRIVER=-1;
	

	protected BenvenutoCheckoutPage(WebDriver driver,Properties language) 
	{
		DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		default:
			gesture=BenvenutoCheckoutPageManager.with(driver, language);
		}
	}
	
	public static BenvenutoCheckoutPage from(WebDriver driver,Properties language)
	{
		return new BenvenutoCheckoutPage(driver, language).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}
	
	public ProfilePage Login(CredentialBean credentialBean) 
	{
		gesture.login(credentialBean);
		return ProfilePage.from(gesture.getDriver(),gesture.getLanguages());
	}
	
	public ShippingPage checkoutOspite() 
	{
		gesture.checkOutComeOspite();
		return ShippingPage.from(gesture.getDriver(),gesture.getLanguages());
	}
	

}
