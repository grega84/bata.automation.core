package com.bata.pages.benvenutocheckout;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.Locators;
import com.bata.pages.AbstractPageManager;

import bean.datatable.CredentialBean;
import test.automation.core.UIUtils;

public class BenvenutoCheckoutPageManager extends AbstractPageManager 
{
	protected WebElement accedilabel=null;
	protected WebElement emailLabel=null;
	protected WebElement emailInput=null;
	protected WebElement passwordLabel=null;
	protected WebElement passwordInput=null;
	protected WebElement accessButton=null;
	protected WebElement checkoutGuest=null;
	
	protected BenvenutoCheckoutPageManager(WebDriver driver, Properties languages) 
	{
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static BenvenutoCheckoutPageManager with(WebDriver driver, Properties languages)
	{
		return new BenvenutoCheckoutPageManager(driver, languages);
	}
	
	public BenvenutoCheckoutPageManager isLoaded()
	{
		accedilabel=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.BenvenutoCheckoutPage.LABELACCESS.getWebLocator())),120);
		emailLabel=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.BenvenutoCheckoutPage.LABELEMAIL.getWebLocator())),40);
		emailInput=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.BenvenutoCheckoutPage.INPUTEMAIL.getWebLocator())),40);
		passwordLabel=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.BenvenutoCheckoutPage.LABELPASSWORD.getWebLocator())),40);
		passwordInput=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.BenvenutoCheckoutPage.INPUTPASSWORD.getWebLocator())),40);
		accessButton=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.BenvenutoCheckoutPage.ACCESSBUTTON.getWebLocator())),40);
		checkoutGuest=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.BenvenutoCheckoutPage.CHECKOUTBUTTON.getWebLocator())),40);
		
		Assert.assertTrue(accedilabel!=null);
		Assert.assertTrue(emailLabel!=null);
		Assert.assertTrue(emailInput!=null);
		Assert.assertTrue(passwordLabel!=null);
		Assert.assertTrue(passwordInput!=null);
		Assert.assertTrue(accessButton!=null);
		
		return this;
	}
	
	public BenvenutoCheckoutPageManager login(CredentialBean credentialBean)
	{
		emailInput.sendKeys(credentialBean.getEmail());
		passwordInput.sendKeys(credentialBean.getPassword());
		accessButton.click();
		
		return this;
	}
	
	public BenvenutoCheckoutPageManager checkOutComeOspite()
	{
		checkoutGuest=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LoginPage.CHECKOUTGUEST.getWebLocator())),40);
		Assert.assertTrue(checkoutGuest!=null);
		checkoutGuest.click();
		
		return this;
	}
}
