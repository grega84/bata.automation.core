package com.bata.pages.yahoomail;

import java.time.Duration;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.email_template.EmailTemplate;
import com.bata.email_template.EmailTemplateFactory;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.AbstractPageManager;

import bean.datatable.EmailTemplateBean;
import test.automation.core.UIUtils;

public class YahooMailPageManager extends AbstractPageManager 
{
	private WebElement email;
	private WebElement goNext;
	private WebElement password;
	private WebElement login;
	private WebElement mail;
	private WebElement riepilogoOrdineLink;

	protected YahooMailPageManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static YahooMailPageManager with(WebDriver driver, Properties languages)
	{
		return new YahooMailPageManager(driver, languages);
	}

	public YahooMailPageManager isLoaded()
	{
		email=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.YahooMailPage.USERNAME.getWebLocator())),40);
		Assert.assertTrue(email!=null);
		
		return this;
	}
	
	public YahooMailPageManager checkMail(String tmpMail,String subject)
	{
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.YahooMailPage.MAIL_LIST.getWebLocator())), 40);
		
		List<WebElement> mails=driver.findElements(By.xpath(Locators.YahooMailPage.MAIL_LIST.getWebLocator()));
		
		long maxTime=Duration.ofMinutes(5).toMillis();
		
		while(maxTime > 0)
		{
			for(WebElement e : mails)
			{
				String s=e.getText();
				
				if(s.contains(subject))
					return this;
			}
			
			try {Thread.sleep(Duration.ofSeconds(20).toMillis());} catch(Exception err) {}
			maxTime -= Duration.ofSeconds(20).toMillis();
			
			driver.navigate().refresh();
		}
		
		Assert.assertTrue("Email non arrivata!",false);
		
		return this;
	}
	
	public YahooMailPageManager login(String user,String passwd)
	{
		email=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.YahooMailPage.USERNAME.getWebLocator()));
		email.clear();
		email.sendKeys(StringUtils.isBlank(user) ? "tester_prisma001@yahoo.com" : user);
		
		goNext=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.YahooMailPage.GO_NEXT.getWebLocator()));
		goNext.click();
		
		try {Thread.sleep(3000);} catch(Exception err) {}
		
		password=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.YahooMailPage.PASSWORD.getWebLocator()));
		password.clear();
		password.sendKeys(StringUtils.isBlank(passwd) ? "Alice.it0$" : passwd);
		
		login=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.YahooMailPage.LOGIN.getWebLocator()));
		login.click();
		
		return this;
	}
	
	public YahooMailPageManager checkSummaryMail(String subjectMail, EmailTemplateBean bean, String country)
	{
		//apro la mail
		this.mail=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.YahooMailPage.MAIL.getWebLocator(), new Entry("subject",subjectMail))));
		Assert.assertTrue(mail != null);
		
		this.mail.click();
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		LoadableComponent<?> template=((LoadableComponent<?>) EmailTemplateFactory.getEmailTemplate(driver, country)).get();
		
		((EmailTemplate)template).checkMail(bean);
		
		return this;
	}
	
	public YahooMailPageManager clickRiepilogoOrdine()
	{
		String winHandle=driver.getWindowHandle();
		
		this.riepilogoOrdineLink=UIUtils.ui().scrollAndReturnElementLocatedBy(driver,By.xpath(Locators.EmailTemplate.EMAIL_RIEPILOGO_ORDINE_LINK.getWebLocator()));
		this.riepilogoOrdineLink.click();
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//TODO:pezza a ipercolori multi jet come la television
		/*
		 * ricavo il link puntato dall'ancor e lo modifico per farlo puntare a dev in
		 * quanto è presente un bug sul link di riepilogo ordine
		 */
		Set<String> wins=driver.getWindowHandles();
		
		for(String s : wins)
		{
			driver.switchTo().window(s);
			
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(driver.getCurrentUrl().contains("https://www.bata."))
			{
				break;
			}
		}
		
		try {
			Thread.sleep(300);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String url=driver.getCurrentUrl().replace("https://www.bata.", "https://dev-cc.bata.");
		driver.get(url);
		//### FINE PEZZA ###
		
		return this;
	}
	
	public YahooMailPageManager checkOrderIsTrackedRegistered(String orderId,long maxTimeOut)
	{
		//attendo max 5 min
		long max=maxTimeOut;
		
		while(max >= 0)
		{
			try 
			{
				UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PurchasesAndOrdersPage.ORDER_LIST_ELEMENT_ID.getWebLocator().replace("#", "1"))), 1);
				String order=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PurchasesAndOrdersPage.ORDER_LIST_ELEMENT_ID.getWebLocator().replace("#", "1"))).getText().trim();
				Assert.assertTrue(order.equals(orderId));
				
				UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PurchasesAndOrdersPage.ORDER_LIST_ELEMENT_DETAILS_BUTTON.getWebLocator().replace("#", "1"))).click();
				
				break;
			}
			catch(Exception err) 
			{
				
				driver.navigate().refresh();
				
				try {
					Thread.sleep(Duration.ofSeconds(10).toMillis());
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				max -= Duration.ofSeconds(10).toMillis();
			}
		}
		
		return this;
	}
	
	public YahooMailPageManager checkOrderIsTrackedGuest(long maxTimeOut)
	{
		//attendo max 5 min
		long max=maxTimeOut;
		
		while(max >= 0)
		{
			try 
			{
				UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.TrackAnOderDetailsPage.HEADER.getWebLocator())), 1);
				break;
			}
			catch(Exception err) 
			{
				
				driver.navigate().refresh();
				
				try {
					Thread.sleep(TimeUnit.SECONDS.toMillis(10));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				max -=TimeUnit.SECONDS.toMillis(10);
			}
		}
		
		return this;
	}
}
