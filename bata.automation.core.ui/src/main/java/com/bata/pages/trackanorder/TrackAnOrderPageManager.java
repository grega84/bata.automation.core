package com.bata.pages.trackanorder;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.Locators;
import com.bata.pages.AbstractPageManager;

import bean.datatable.TrackOrderBean;
import io.appium.java_client.AppiumDriver;
import test.automation.core.UIUtils;

public class TrackAnOrderPageManager extends AbstractPageManager 
{
	protected WebElement title;
	protected WebElement orderNumber;
	protected WebElement email;
	protected WebElement findButton;
	protected WebElement errorMessage;

	protected TrackAnOrderPageManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static TrackAnOrderPageManager with(WebDriver driver, Properties languages)
	{
		return new TrackAnOrderPageManager(driver, languages);
	}
	
	public TrackAnOrderPageManager isLoaded()
	{
		this.title=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.TrackAnOderPage.TITLE.getWebLocator())), 60);
		this.orderNumber=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.TrackAnOderPage.ORDER_NUMBER.getWebLocator())), 60);
		this.email=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.TrackAnOderPage.EMAIL.getWebLocator())), 60);
		this.findButton=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.TrackAnOderPage.FIND_BUTTON.getWebLocator())), 60);
		
		Assert.assertTrue(this.title != null);
		Assert.assertTrue(this.orderNumber != null);
		Assert.assertTrue(this.email != null);
		Assert.assertTrue(this.findButton != null);
		 return this;
	}
	
	public TrackAnOrderPageManager gotoDetails(TrackOrderBean bean)
	{
		this.orderNumber=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.TrackAnOderPage.ORDER_NUMBER.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		this.orderNumber.sendKeys(bean.getOrderId());
		try {((AppiumDriver)driver).hideKeyboard();}catch(Exception err) {}
		
		this.email=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.TrackAnOderPage.EMAIL.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		this.email.sendKeys(bean.getEmailAddress());
		try {((AppiumDriver)driver).hideKeyboard();}catch(Exception err) {}
		
		this.findButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.TrackAnOderPage.FIND_BUTTON.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		this.findButton.click();
		
		return this;
	}
	
	public TrackAnOrderPageManager checkErrorMessage(String trackErrorMessage)
	{
		try {((AppiumDriver)driver).hideKeyboard();}catch(Exception err) {}
		this.errorMessage=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.TrackAnOderPage.ERROR_MESSAGE.getWebLocator()));
		String actual=this.errorMessage.getText().trim();
		
		Assert.assertTrue("controllo messaggio di errore:=attuale:"+actual+"\natteso:"+trackErrorMessage,actual.equals(trackErrorMessage));
		
		return this;
	}

}
