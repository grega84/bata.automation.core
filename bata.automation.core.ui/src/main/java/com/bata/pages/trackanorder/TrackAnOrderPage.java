package com.bata.pages.trackanorder;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import com.bata.pagepattern.Locators;

import bean.datatable.CredentialBean;
import bean.datatable.TrackOrderBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class TrackAnOrderPage extends LoadableComponent<TrackAnOrderPage> {
	
	
	private int DRIVER;
	private TrackAnOrderPageManager gesture;
	
	protected TrackAnOrderPage(WebDriver driver,Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		default:
			gesture=TrackAnOrderPageManager.with(driver, languages);
		}
	}
	
	public static TrackAnOrderPage from(WebDriver driver, Properties languages)
	{
		return new TrackAnOrderPage(driver, languages).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}
	
	public TrackAnOrderDetailsPage gotoDetails(TrackOrderBean bean,Properties language)
	{
		gesture.gotoDetails(bean);
		
		if(bean.getWithError().equals("no"))
			return TrackAnOrderDetailsPage.from(gesture.getDriver(), gesture.getLanguages());
		else
			return null;
	}

	public TrackAnOrderPage checkErrorMessage(String trackErrorMessage) 
	{
		gesture.checkErrorMessage(trackErrorMessage);
		return this;
	}
}
