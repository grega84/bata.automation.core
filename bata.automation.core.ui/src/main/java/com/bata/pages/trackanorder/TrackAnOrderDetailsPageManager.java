package com.bata.pages.trackanorder;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.AbstractPageManager;

import bean.datatable.TrackOrderBean;
import test.automation.core.UIUtils;

public class TrackAnOrderDetailsPageManager extends AbstractPageManager 
{
	protected WebElement header;
	protected WebElement orderTitle;
	protected WebElement orderStatus;
	protected WebElement orderNumber;
	protected WebElement owner;
	protected WebElement deliveryAddress;
	protected WebElement payment;
	protected WebElement billingAddress;
	protected WebElement totalPrice;

	protected TrackAnOrderDetailsPageManager(WebDriver driver, Properties languages) {
		super(driver, languages);
	}
	
	public static TrackAnOrderDetailsPageManager with(WebDriver driver, Properties languages)
	{
		return new TrackAnOrderDetailsPageManager(driver, languages);
	}
	
	public TrackAnOrderDetailsPageManager isLoaded()
	{
		this.header=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.TrackAnOderDetailsPage.HEADER.getWebLocator())), 60);
		this.orderTitle=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.TrackAnOderDetailsPage.ORDER_TITLE.getWebLocator())), 60);
		this.orderStatus=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.TrackAnOderDetailsPage.ORDER_STATUS.getWebLocator(), new Entry("orderStatusText",language.getProperty("orderStatusText"))))), 60);
		this.orderNumber=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.TrackAnOderDetailsPage.ORDER_NUMBER.getWebLocator(), new Entry("orderNumberText",language.getProperty("orderNumberText"))))), 60);
		this.owner=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.TrackAnOderDetailsPage.OWNER_DATA.getWebLocator(), new Entry("ownerDataText",language.getProperty("ownerDataText"))))), 60);
		this.deliveryAddress=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.TrackAnOderDetailsPage.DELIVERY_ADDRESS.getWebLocator(), new Entry("deliveryAddress",language.getProperty("deliveryAddress")),new Entry("shipDelivery",language.getProperty("shipDelivery"))))), 60);
		this.payment=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.TrackAnOderDetailsPage.PAYMENT.getWebLocator(), new Entry("paymentsText",language.getProperty("paymentsText"))))), 60);
		this.billingAddress=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.TrackAnOderDetailsPage.BILLING_ADDRESS.getWebLocator(), new Entry("billingAddress",language.getProperty("billingAddress"))))), 60);
		this.totalPrice=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.TrackAnOderDetailsPage.TOTAL_PRICE.getWebLocator())), 60);
		
		Assert.assertTrue(this.header != null);
		Assert.assertTrue(this.orderTitle != null);
		Assert.assertTrue(this.orderStatus != null);
		Assert.assertTrue(this.orderNumber != null);
		Assert.assertTrue(this.owner != null);
		Assert.assertTrue(this.deliveryAddress != null);
		Assert.assertTrue(this.payment != null);
		Assert.assertTrue(this.billingAddress != null);
		Assert.assertTrue(this.totalPrice != null);
		
		return this;
	}
	
	public TrackAnOrderDetailsPageManager checkOrderStatus(TrackOrderBean bean) 
	{
		this.orderTitle=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.TrackAnOderDetailsPage.ORDER_TITLE.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		String actual=this.orderTitle.getText().trim();
		String expected=bean.getOrderDetailHeader();
		
		Assert.assertTrue("controllo header ordine:=attuale:"+actual+"\natteso:"+expected,actual.equals(expected));
		
		this.orderStatus=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.TrackAnOderDetailsPage.ORDER_STATUS.getWebLocator(), new Entry("orderStatusText",language.getProperty("orderStatusText")))));
		
		try {Thread.sleep(500);}catch(Exception err) {}
		
		actual=this.orderStatus.getText().trim();
		expected=bean.getOrderStatus();
		
		Assert.assertTrue("controllo stato ordine:=attuale:"+actual+"\natteso:"+expected,actual.equals(expected));
		
		return this;
	}

	public TrackAnOrderDetailsPageManager checkOrder(TrackOrderBean bean) 
	{
		//this.checkOrderStatus(bean);
		
		//controllo numero ordine
		this.orderNumber=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.TrackAnOderDetailsPage.ORDER_NUMBER.getWebLocator(), new Entry("orderNumberText",language.getProperty("orderNumberText")))));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		String actual=this.orderNumber.getText().trim();
		String expected=bean.getOrderNumber();
		
		Assert.assertTrue("controllo numero ordine:=attuale:"+actual+"\natteso:"+expected,actual.equals(expected));
		
		//controllo dati di contatto
		this.owner=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.TrackAnOderDetailsPage.OWNER_DATA.getWebLocator(), new Entry("ownerDataText",language.getProperty("ownerDataText")))));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		actual=this.owner.getText().trim();
		expected=bean.getContactData();
		
		Assert.assertTrue("controllo dati di contatto:=attuale:"+actual+"\natteso:"+expected,actual.equals(expected));
		
		//controllo indirizzo di consegna
		this.deliveryAddress=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.TrackAnOderDetailsPage.DELIVERY_ADDRESS.getWebLocator(), new Entry("deliveryAddress",language.getProperty("deliveryAddress")),new Entry("shipDelivery",language.getProperty("shipDelivery")))));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		actual=this.deliveryAddress.getText().trim();
		expected=bean.getDeliveryAddress();
		
		Assert.assertTrue("controllo indirizzo di spedizione:=attuale:"+actual+"\natteso:"+expected,actual.equals(expected));

		//controllo metodo di pagamento
		this.payment=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.TrackAnOderDetailsPage.PAYMENT.getWebLocator(), new Entry("paymentsText",language.getProperty("paymentsText")))));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		actual=this.payment.getText().trim();
		expected=bean.getPayment();
		
		Assert.assertTrue("controllo metodo di pagamento:=attuale:"+actual+"\natteso:"+expected,actual.equals(expected));

		//controllo indirizzo di fatturazione
		this.billingAddress=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.TrackAnOderDetailsPage.BILLING_ADDRESS.getWebLocator(), new Entry("billingAddress",language.getProperty("billingAddress")))));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		actual=this.billingAddress.getText().trim();
		expected=bean.getBillingAddress();
		
		Assert.assertTrue("controllo indirizzo di fatturazione:=attuale:"+actual+"\natteso:"+expected,actual.equals(expected));

		//controllo prezzo totale
		this.totalPrice=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.TrackAnOderDetailsPage.TOTAL_PRICE.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		actual=this.totalPrice.getText().trim();
		expected=bean.getTotalPrice();
		
		Assert.assertTrue("controllo totale ordine:=attuale:"+actual+"\natteso:"+expected,actual.equals(expected));

		//controllo items
		String[] productName,productBrand,productId,productSize,productColor,productQuantity,productPrice;
		
		productName=bean.getProductName().split(";");
		productBrand=bean.getProductBrand().split(";");
		productId=bean.getProductId().split(";");
		productPrice=bean.getProductPrice().split(";");
		productQuantity=bean.getProductQuantity().split(";");
		productSize=bean.getProductSize().split(";");
		productColor=bean.getProductColor().split(";");
		
		for(int i=0;i<productName.length;i++)
		{
			//controllo titolo articolo
			actual=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.TrackAnOderDetailsPage.ORDER_ITEM_TITLE.getWebLocator().replace("#", ""+(i+1)))).getText().trim();
			expected=productName[i];
			
			Assert.assertTrue("controllo titolo item:=attuale:"+actual+"\natteso:"+expected,actual.equals(expected));

			//controllo id articolo
			actual=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.TrackAnOderDetailsPage.ORDER_ITEM_ID.getWebLocator().replace("#", ""+(i+1)))).getText().trim();
			expected=productId[i];
			
			Assert.assertTrue("controllo id item:=attuale:"+actual+"\natteso:"+expected,actual.equals(expected));
			
			//controllo brand articolo
			actual=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.TrackAnOderDetailsPage.ORDER_ITEM_BRAND.getWebLocator().replace("#", ""+(i+1)))).getText().trim();
			expected=productBrand[i];
			
			Assert.assertTrue("controllo brand item:=attuale:"+actual+"\natteso:"+expected,actual.equals(expected));

			//controllo taglia articolo
			actual=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.TrackAnOderDetailsPage.ORDER_ITEM_SIZE.getWebLocator().replace("#", ""+(i+1)))).getText().trim();
			expected=productSize[i];
			
			Assert.assertTrue("controllo taglia item:=attuale:"+actual+"\natteso:"+expected,actual.equals(expected));

			//controllo colore articolo
			actual=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.TrackAnOderDetailsPage.ORDER_ITEM_COLOR.getWebLocator().replace("#", ""+(i+1)))).getText().trim();
			expected=productColor[i];
			
			Assert.assertTrue("controllo colore item:=attuale:"+actual+"\natteso:"+expected,actual.equals(expected));

			//controllo quantità articolo
			actual=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.TrackAnOderDetailsPage.ORDER_ITEM_QUANTITY.getWebLocator().replace("#", ""+(i+1)))).getText().trim();
			expected=productQuantity[i];
			
			Assert.assertTrue("controllo quantitativo item:=attuale:"+actual+"\natteso:"+expected,actual.equals(expected));

			//controllo prezzo unitario articolo
			actual=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.TrackAnOderDetailsPage.ORDER_ITEM_PRICE.getWebLocator().replace("#", ""+(i+1)))).getText().trim();
			expected=productPrice[i];
			
			Assert.assertTrue("controllo prezzo unitario item:=attuale:"+actual+"\natteso:"+expected,actual.equals(expected));

		}
		
		return this;
	}

}
