package com.bata.pages.trackanorder;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;

import bean.datatable.CredentialBean;
import bean.datatable.TrackOrderBean;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class TrackAnOrderDetailsPage extends LoadableComponent<TrackAnOrderDetailsPage> {
	
	private WebDriver driver;
	private Properties language;
	private int DRIVER;
	private TrackAnOrderDetailsPageManager gesture;
	
	protected TrackAnOrderDetailsPage(WebDriver driver,Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		default:
			gesture=TrackAnOrderDetailsPageManager.with(driver, languages);
		}
		
		this.driver=driver;
		this.language=languages;
	}
	
	public static TrackAnOrderDetailsPage from(WebDriver driver, Properties languages)
	{
		return new TrackAnOrderDetailsPage(driver, languages).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}

	public TrackAnOrderDetailsPage checkOrderStatus(TrackOrderBean bean) 
	{
		gesture.checkOrderStatus(bean);
		return this;
	}

	public TrackAnOrderDetailsPage checkOrder(TrackOrderBean bean) 
	{
		gesture.checkOrder(bean);
		return this;
	}
}
