package com.bata.pages.paymenthome;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.bata.pagepattern.Locators;
import com.bata.pages.payments.PaymentsManager;

import io.appium.java_client.android.AndroidDriver;
import test.automation.core.UIUtils;

public class PaymentShipToHomeManagerAndroid extends PaymentShipToHomeManager {

	public PaymentShipToHomeManagerAndroid(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static PaymentShipToHomeManagerAndroid with(WebDriver driver, Properties languages)
	{
		return new PaymentShipToHomeManagerAndroid(driver, languages);
	}
	
	public PaymentShipToHomeManagerAndroid submitCreditCardPayment()
	{
		confirmButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.CONFIRM_BUTTON.getWebLocator()));
		confirmButton.click();
		
		((AndroidDriver) driver).context("NATIVE_APP");
		
		try
		{
			Thread.sleep(3000);
			((AndroidDriver) driver).findElementById("com.android.chrome:id/infobar_close_button").click();
		}
		catch(Exception err)
		{
			err.printStackTrace();
		}
		
		((AndroidDriver) driver).context("CHROMIUM");
		
		return this;
	}
}
