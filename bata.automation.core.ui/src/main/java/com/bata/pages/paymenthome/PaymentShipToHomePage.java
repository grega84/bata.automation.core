package com.bata.pages.paymenthome;

import java.util.Properties;
import java.util.Set;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.PayWith;
import com.bata.pages.gpwebpay.GPWebPayPage;
import com.bata.pages.paypalpayment.PaypalPaymentPage;
import com.bata.pages.riepilogo.RiepilogoPage;

import bean.datatable.CreditCardErrorCheckBean;
import bean.datatable.PaymentBean;
import bean.datatable.ShippingBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.PointOption;
import test.automation.core.UIUtils;

public class PaymentShipToHomePage extends LoadableComponent<PaymentShipToHomePage> implements PayWith {
	
	
	private int DRIVER;
	private PaymentShipToHomeManager gesture;

	protected PaymentShipToHomePage(WebDriver driver, Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		case Locators.ANDROID_CHROME:
			gesture=PaymentShipToHomeManagerAndroid.with(driver, languages);
			break;
		case Locators.IOS_SAFARI:
			gesture=PaymentShipToHomeManagerIOS.with(driver, languages);
			break;
		default:
			gesture=PaymentShipToHomeManager.with(driver, languages);
		}
	}
	
	public static PaymentShipToHomePage from(WebDriver driver, Properties languages)
	{
		return new PaymentShipToHomePage(driver, languages).get();
	}

	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}
	
	public PaymentShipToHomePage applyCoupon(PaymentBean paymentBean) 
	{
		gesture.applyCupon(paymentBean);
		return this;
	}
	
	public RiepilogoPage payWithCreditCard(PaymentBean paymentBean) 
	{
		gesture.selectCreditCard()
		.insertCreditCardData(paymentBean)
		.selectTc()
		.submitCreditCardPayment();
		
		return RiepilogoPage.from(gesture.getDriver(),gesture.getLanguages());
	}

	public PaypalPaymentPage payWithPayPal(PaymentBean paymentBean)  
	{
		gesture.selectPayPal()
		.selectTc()
		.submitPayPalPayment();
		
		return PaypalPaymentPage.from(gesture.getDriver(),gesture.getLanguages());
	}
	
	public RiepilogoPage payWithCod(PaymentBean paymentBean) 
	{
		gesture.selectCashOnDelivery()
		.selectTc()
		.submitCashOnDeliveryPayment();
		
		return RiepilogoPage.from(gesture.getDriver(),gesture.getLanguages());
	}

	@Override
	public PaymentShipToHomePage insertBillingDataGuestUser(ShippingBean shippingBean) {
		return this;
	}

	@Override
	public PaymentShipToHomePage payWithCreditCardWithoutTS(PaymentBean paymentBean) 
	{
		gesture.selectCreditCard()
		.insertCreditCardData(paymentBean)
		.submitCreditCardPayment();
		return this;
	}

	@Override
	public PaymentShipToHomePage payWithPayPalWithoutTS(PaymentBean b) 
	{
		gesture.selectPayPal()
		.submitPayPalPayment();
		return this;
	}

	@Override
	public PaymentShipToHomePage checkTsErrorMessages(Properties language) 
	{
		gesture.checkTsErrors();
		return this;
	}

	@Override
	public PaymentShipToHomePage selectCreditCardPaymentMethod() 
	{
		gesture.selectCreditCard();
		return this;
	}

	@Override
	public PaymentShipToHomePage selectPaypalPaymentMethod() 
	{
		gesture.selectPayPal();
		return this;
	}

	@Override
	public PaymentShipToHomePage checkCreditCardErrorMessage(CreditCardErrorCheckBean error) 
	{
		gesture.selectCreditCard()
		.insertCreditCardData(error)
		.selectTc()
		.submitCreditCardPayment()
		.checkCreditCardErrors(error);
		return this;
	}

	@Override
	public PaymentShipToHomePage checkCODIsntDisplayed() 
	{
		gesture.checkCodIsntVisible();
		return this;
	}

	@Override
	public GPWebPayPage payWithWebPay(PaymentBean paymentBean) 
	{
		gesture.selectCreditCard()
		.insertCreditCardData(paymentBean)
		.selectTc()
		.submitCreditCardPayment();
		
		return GPWebPayPage.from(gesture.getDriver(),gesture.getLanguages());
	}
	
	
}
