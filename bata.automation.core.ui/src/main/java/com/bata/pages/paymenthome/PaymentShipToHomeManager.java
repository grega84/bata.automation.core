package com.bata.pages.paymenthome;

import java.util.Properties;
import java.util.Set;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.AbstractPageManager;
import com.bata.pages.payments.PaymentsManager;

import bean.datatable.CreditCardErrorCheckBean;
import bean.datatable.PaymentBean;
import test.automation.core.UIUtils;

public class PaymentShipToHomeManager extends PaymentsManager 
{
	
	protected PaymentShipToHomeManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static PaymentShipToHomeManager with(WebDriver driver, Properties languages)
	{
		return new PaymentShipToHomeManager(driver, languages);
	}
	
	public PaymentShipToHomeManager isLoaded()
	{
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.PaymentPage.BILLING_ADDRESS.getWebLocator(), new Entry("addressFatt",language.getProperty("addressFatt"))))),120);
		billingAddress=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.PaymentPage.BILLING_ADDRESS.getWebLocator(), new Entry("addressFatt",language.getProperty("addressFatt")))));
		
		return this;
	}
	
	
}
