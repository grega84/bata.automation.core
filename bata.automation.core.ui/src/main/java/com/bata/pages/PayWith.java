package com.bata.pages;

import java.util.Properties;

import com.bata.pages.gpwebpay.GPWebPayPage;
import com.bata.pages.paypalpayment.PaypalPaymentPage;
import com.bata.pages.riepilogo.RiepilogoPage;

import bean.datatable.CreditCardErrorCheckBean;
import bean.datatable.PaymentBean;
import bean.datatable.ShippingBean;

public interface PayWith 
{
	public PayWith applyCoupon(PaymentBean paymentBean);
	
	public RiepilogoPage payWithCreditCard(PaymentBean paymentBean);
	
	public GPWebPayPage payWithWebPay(PaymentBean paymentBean);
	
	public PaypalPaymentPage payWithPayPal(PaymentBean paymentBean);
	
	public RiepilogoPage payWithCod(PaymentBean paymentBean);
	
	public PayWith insertBillingDataGuestUser(ShippingBean shippingBean);

	public PayWith payWithCreditCardWithoutTS(PaymentBean b);

	public PayWith payWithPayPalWithoutTS(PaymentBean b);

	public PayWith checkTsErrorMessages(Properties language);

	public PayWith selectCreditCardPaymentMethod();
	
	public PayWith selectPaypalPaymentMethod();

	public PayWith checkCreditCardErrorMessage(CreditCardErrorCheckBean error);

	public PayWith checkCODIsntDisplayed();
}
