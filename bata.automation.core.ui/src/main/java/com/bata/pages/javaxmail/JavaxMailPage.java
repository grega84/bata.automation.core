package com.bata.pages.javaxmail;

import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.email_template.EmailTemplate;
import com.bata.email_template.EmailTemplateFactory;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.IEmailProvider;
import com.bata.pages.gmail.GmailMailPage;
import com.bata.pages.purchaseandoders.PurchasesAndOrdersPage;
import com.bata.pages.trackanorder.TrackAnOrderDetailsPage;
import com.bata.pages.welcomeuser.WelcomeUserPage;

import bean.datatable.ContactBean;
import bean.datatable.EmailTemplateBean;
import bean.datatable.RiepilogoBean;
import test.automation.core.UIUtils;

public class JavaxMailPage extends LoadableComponent<JavaxMailPage> implements IEmailProvider<JavaxMailPage>
{
	public static final String DEFAULT_MAIL = "automation.test.prisma";
	private int DRIVER;
	private JavaxMailPageManager gesture;

	protected JavaxMailPage(WebDriver driver,Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		default:
			gesture=JavaxMailPageManager.with(driver, languages);
		}
	}
	
	public static JavaxMailPage from(WebDriver driver, Properties languages)
	{
		return new JavaxMailPage(driver, languages).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}
	
	public JavaxMailPage checkEmail(String tmpMail,String subject) 
	{
		gesture.checkMail(tmpMail, subject);
		return this;
	}
	
	public WelcomeUserPage clickOnRegisterLink() 
	{
		gesture.clickOnRegisterLink();
		return WelcomeUserPage.from(gesture.getDriver(), gesture.getLanguages());
	}
	
	public JavaxMailPage login(String user,String passwd)
	{
		gesture.login(user, passwd);
		return this;
	}

	public JavaxMailPage checkSummaryMail(String subjectMail, EmailTemplateBean bean, String country) 
	{
		gesture.checkSummaryMail(subjectMail, bean, country);
		return this;
	}
	
	public PurchasesAndOrdersPage clickRiepilogoOrdineRegisteredUser(Properties language, String orderId,long maxTimeOut)
	{
		gesture.clickRiepilogoOrdine()
		.checkOrderIsTrackedRegistered(orderId, maxTimeOut);
		
		return PurchasesAndOrdersPage.from(gesture.getDriver(),gesture.getLanguages());
	}

	public TrackAnOrderDetailsPage clickRiepilogoOrdine(Properties language,long maxTimeOut) 
	{
		gesture.clickRiepilogoOrdine()
		.checkOrderIsTrackedGuest(maxTimeOut);

		return TrackAnOrderDetailsPage.from(gesture.getDriver(),gesture.getLanguages());
	}

	public JavaxMailPage openMail(String subject) 
	{
		gesture.openMail(subject);
		return this;
	}

	public JavaxMailPage init(String driverMailConfig) 
	{
		gesture.init(driverMailConfig);
		return this;
	}
}
