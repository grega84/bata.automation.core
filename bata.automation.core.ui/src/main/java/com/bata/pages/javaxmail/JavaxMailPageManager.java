package com.bata.pages.javaxmail;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.StringReader;
import java.time.Duration;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.jms.Message;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.xml.sax.InputSource;

import com.bata.email_template.EmailTemplate;
import com.bata.email_template.EmailTemplateFactory;
import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.AbstractPageManager;
import com.bata.pages.guerillamail.GuerillaMailPageManager;
import com.bata.pages.trackanorder.TrackAnOrderDetailsPage;

import bean.datatable.EmailTemplateBean;
import common.CommonFields;
import common.WaitManager;
import test.automation.core.SystemClipboard;
import test.automation.core.UIUtils;

public class JavaxMailPageManager extends AbstractPageManager 
{

	private Properties config;
	private Document mail;
	private String mailTxt;
	private org.w3c.dom.Document xmlMail;
	private javax.mail.Message messageEmail;
	private WebElement riepilogoOrdineLink;

	protected JavaxMailPageManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static JavaxMailPageManager with(WebDriver driver, Properties languages)
	{
		return new JavaxMailPageManager(driver, languages);
	}

	public JavaxMailPageManager isLoaded()
	{
		//email=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.GmailMailPage.USERNAME.getWebLocator())),40);
		//Assert.assertTrue(email!=null);

		return this;
	}

	public JavaxMailPageManager checkMail(String tmpMail,String subject)
	{
		//aspetto max 3 minuti
		for(int i=0;i<6;i++)
		{
			try {
				Properties properties = new Properties();

				properties.put("mail.imap.host", config.getProperty("mail.host"));
				properties.put("mail.imap.port", "993");
				properties.put("mail.imap.ssl.enable", "true");
				Session emailSession = Session.getDefaultInstance(properties);

				//create the POP3 store object and connect with the pop server
				Store store = emailSession.getStore("imap");

				store.connect(
						config.getProperty("mail.host"), 
						config.getProperty("mail.user"), 
						config.getProperty("mail.pswd"));

				//create the folder object and open it
				Folder emailFolder = store.getFolder("INBOX");
				emailFolder.open(Folder.READ_WRITE);

				// retrieve the messages from the folder in an array and print it
				javax.mail.Message[] messages = emailFolder.getMessages();

				Arrays.sort(messages, new Comparator<javax.mail.Message>() 
				{

					@Override
					public int compare(javax.mail.Message m1, javax.mail.Message m2) {
						try {
							return m2.getReceivedDate().compareTo(m1.getReceivedDate());
						} catch (MessagingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return 0;
					}
				});
				System.out.println("messages.length---" + messages.length);

				for (int j = 0, n = messages.length; j < n; j++) {
					javax.mail.Message message = messages[j];
					System.out.println("---------------------------------");
					System.out.println("Email Number " + (j + 1));
					System.out.println("Subject: " + message.getSubject());

					if(message.getSubject().contains(subject))
					{
						System.out.println("FIND");
						String result="";
						if (message.isMimeType("multipart/*")) {
							MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
							result = getTextFromMimeMultipart(mimeMultipart);
						}

						System.out.println(result);
						mail=Jsoup.parse(result);
						mailTxt=result;

						messageEmail=message;

						message.setFlag(Flags.Flag.DELETED, true);

						break;
					}
					//System.out.println("From: " + message.getFrom()[0]);
					//System.out.println("Text: " + message.getContent().toString());

				}

				//close the store and folder objects
				emailFolder.close(true);
				store.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			if(mail != null)
				break;

			try {
				Thread.sleep(TimeUnit.SECONDS.toMillis(30));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return this;
	}

	private String getTextFromMimeMultipart(
			MimeMultipart mimeMultipart)  throws Exception{
		String result = "";
		int count = mimeMultipart.getCount();
		for (int i = 0; i < count; i++) {
			BodyPart bodyPart = mimeMultipart.getBodyPart(i);
			if (bodyPart.isMimeType("text/html")) {
				String html = (String) bodyPart.getContent();
				result = html;
			} else if (bodyPart.getContent() instanceof MimeMultipart){
				result = result + getTextFromMimeMultipart((MimeMultipart)bodyPart.getContent());
			}
		}
		return result;
	}

	public JavaxMailPageManager login(String mail,String passwd) 
	{


		return this;
	}

	public JavaxMailPageManager checkSummaryMail(String subjectMail, EmailTemplateBean bean, String country)
	{
		WaitManager.get().waitMediumTime();

		LoadableComponent<?> template=((LoadableComponent<?>) EmailTemplateFactory.getEmailTemplate(driver, country)).get();

		((EmailTemplate)template).checkMail(bean);

		return this;
	}

	public JavaxMailPageManager clickRiepilogoOrdine()
	{
		String winHandle=driver.getWindowHandle();

		this.riepilogoOrdineLink=UIUtils.ui().scrollAndReturnElementLocatedBy(driver,By.xpath(Locators.EmailTemplate.EMAIL_RIEPILOGO_ORDINE_LINK.getWebLocator()));
		this.riepilogoOrdineLink.click();

		WaitManager.get().waitLongTime();

		//TODO:pezza a ipercolori multi jet come la television
		/*
		 * ricavo il link puntato dall'ancor e lo modifico per farlo puntare a dev in
		 * quanto è presente un bug sul link di riepilogo ordine
		 */
		Set<String> wins=driver.getWindowHandles();

		for(String s : wins)
		{
			driver.switchTo().window(s);

			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if(driver.getCurrentUrl().contains("https://www.bata."))
			{
				break;
			}
		}

		try {
			Thread.sleep(300);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String url=driver.getCurrentUrl().replace("https://www.bata.", "https://dev-cc.bata.");
		//String[] auth=((String) DinamicData.getIstance().get(CommonFields.AUTHLOGIN)).split(":");
		driver.get(url.replace("//","//"+DinamicData.getIstance().get(CommonFields.AUTHLOGIN)+"@"));
		//### FINE PEZZA ###

		return this;
	}

	public JavaxMailPageManager checkOrderIsTrackedRegistered(String orderId,long maxTimeOut)
	{
		//attendo max 5 min
		long max=maxTimeOut;
		
		while(max >= 0)
		{
			try 
			{
				UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PurchasesAndOrdersPage.ORDER_LIST_ELEMENT_ID.getWebLocator().replace("#", "1"))), 1);
				String order=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PurchasesAndOrdersPage.ORDER_LIST_ELEMENT_ID.getWebLocator().replace("#", "1"))).getText().trim();
				Assert.assertTrue(order.equals(orderId));
				
				UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PurchasesAndOrdersPage.ORDER_LIST_ELEMENT_DETAILS_BUTTON.getWebLocator().replace("#", "1"))).click();
				
				break;
			}
			catch(Exception err) 
			{
				
				driver.navigate().refresh();
				
				try {
					Thread.sleep(Duration.ofSeconds(10).toMillis());
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				max -= Duration.ofSeconds(10).toMillis();
			}
		}
		
		return this;
	}

	public JavaxMailPageManager checkOrderIsTrackedGuest(long maxTimeOut)
	{
		//attendo max 5 min
		long max=maxTimeOut;
		
		while(max >= 0)
		{
			try 
			{
				UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.TrackAnOderDetailsPage.HEADER.getWebLocator())), 1);
				TrackAnOrderDetailsPage.from(getDriver(),getLanguages());
				break;
			}
			catch(Exception err) 
			{
				
				driver.navigate().refresh();
				
				try {
					Thread.sleep(TimeUnit.SECONDS.toMillis(10));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				max -=TimeUnit.SECONDS.toMillis(10);
			}
		}
		
		return this;
	}

	private org.w3c.dom.Document convertStringToXMLDocument(String xmlString) 
	{
		//Parser that produces DOM object trees from XML content
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		//API to obtain DOM Document instance
		DocumentBuilder builder = null;
		try
		{
			//Create DocumentBuilder with default configuration
			builder = factory.newDocumentBuilder();

			//Parse the content to Document object
			org.w3c.dom.Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
			return doc;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}

	public JavaxMailPageManager clickOnRegisterLink() 
	{
		String win=driver.getWindowHandle();

		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("ita"))
		{
			//la registrazione è avvenuta con successo.
			Element l=getLink("la registrazione è avvenuta con successo");
			String url=l.attr("href");
			driver.get(url);
		}
		else if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("cz"))
		{
			Element l=getLink("LENSTVÍ V NOVÉM BA");
			String url=l.attr("href");
			driver.get(url);
		}
		else if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("sk"))
		{
			Element l=getLink("LENSTVO V NOVOM BA");
			String url=l.attr("href");
			driver.get(url);

		}
		else if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("pl"))
		{
			Element l=getLink("POTWIERDZI");
			String url=l.attr("href");
			driver.get(url);
		}

		String[] auth=((String) DinamicData.getIstance().get(CommonFields.AUTHLOGIN)).split(":");

		try
		{
			WaitManager.get().waitLongTime();

			SystemClipboard.copyAndPaste(auth[0]);
			Robot robot = new Robot();

			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);

			WaitManager.get().waitMediumTime();

			SystemClipboard.copyAndPaste(auth[1]);

			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);

			WaitManager.get().waitMediumTime();

			//			robot.keyPress(KeyEvent.VK_F5);
			//			robot.keyRelease(KeyEvent.VK_F5);
			//
			//			Utility.waitNewWindowAndGoToIt(driver, win);
			//
			//			Thread.sleep(1000);
			//
			//			robot.keyPress(KeyEvent.VK_F5);
			//			robot.keyRelease(KeyEvent.VK_F5);
		}
		catch(Exception err) {}

		WaitManager.get().waitLongTime();

		return this;
	}

	private Element getLink(String text) 
	{
		Elements rows = mail.getElementsByTag("a");
		for (Element element : rows) {
			System.out.println("element = " + element.text());
			if(element.hasText() && element.wholeText().contains(text))
			{
				return element;
			}
		}

		return null;
	}

	public JavaxMailPageManager openMail(String subjectMail) 
	{
		//Se non visualizzi correttamente questo messaggio clicca qui.
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("ita"))
		{
			Element l=getLink("Se non visualizzi correttamente questo messaggio clicca qui");
			String url=l.attr("href");
			driver.get(url);
		}

		WaitManager.get().waitHighTime();

		return this;
	}

	public JavaxMailPageManager init(String driverMailConfig) 
	{
		this.config=UIUtils.ui().getProperties(driverMailConfig);
		System.out.println(JavaxMailPageManager.class.getClassLoader().getResource("waitMail.html").toExternalForm());
		driver.get(JavaxMailPageManager.class.getClassLoader().getResource("waitMail.html").toExternalForm());
		return this;
	}
}
