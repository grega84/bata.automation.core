package com.bata.pages.login;

import java.awt.AWTException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.Locators;

import bean.datatable.CredentialBean;
import test.automation.core.SystemClipboard;
import test.automation.core.UIUtils;

public class LoginPageManagerIE extends LoginPageManager {

	protected LoginPageManagerIE(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static LoginPageManagerIE with(WebDriver driver, Properties languages)
	{
		return new LoginPageManagerIE(driver, languages);
	}
	
	public LoginPageManagerIE insertCredential(CredentialBean credentialBean)
	{
		emailInput=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LoginPage.INPUTEMAIL.getWebLocator())),40);
		emailInput.click();
		try {SystemClipboard.copyAndPaste(credentialBean.getEmail());} catch (AWTException e) {}
		
		passwordInput=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LoginPage.INPUTPASSWORD.getWebLocator())),40);
		passwordInput.click();
		try {SystemClipboard.copyAndPaste(credentialBean.getPassword());} catch (AWTException e) {}
		return this;
	}
}
