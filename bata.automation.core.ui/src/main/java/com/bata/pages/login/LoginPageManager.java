package com.bata.pages.login;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.Locators;
import com.bata.pages.AbstractPageManager;

import bean.datatable.CredentialBean;
import common.WaitManager;
import test.automation.core.UIUtils;

public class LoginPageManager extends AbstractPageManager 
{
	protected WebElement accedilabel=null;
	protected WebElement emailLabel=null;
	protected WebElement emailInput=null;
	protected WebElement passwordLabel=null;
	protected WebElement passwordInput=null;
	protected WebElement accessButton=null;
	protected WebElement registrationButton=null;
	protected WebElement errorMessage=null;
	protected WebElement logo=null;
	protected WebElement myProfile=null;
	protected WebElement logout=null;
	protected WebElement hamburger;
	protected WebElement account;
	
	protected LoginPageManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static LoginPageManager with(WebDriver driver, Properties languages)
	{
		return new LoginPageManager(driver, languages);
	}
	
	public LoginPageManager isLoaded()
	{
		accedilabel=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LoginPage.LABELACCESS.getWebLocator())),40);
		emailLabel=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LoginPage.LABELEMAIL.getWebLocator())),40);
		emailInput=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LoginPage.INPUTEMAIL.getWebLocator())),40);
		passwordLabel=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LoginPage.LABELPASSWORD.getWebLocator())),40);
		passwordInput=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LoginPage.INPUTPASSWORD.getWebLocator())),40);
		accessButton=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LoginPage.ACCESSBUTTON.getWebLocator())),40);
		registrationButton=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LoginPage.REGISTERBUTTON.getWebLocator())),40);
		
		Assert.assertTrue(accedilabel!=null);
		Assert.assertTrue(emailLabel!=null);
		Assert.assertTrue(emailInput!=null);
		Assert.assertTrue(passwordLabel!=null);
		Assert.assertTrue(passwordInput!=null);
		Assert.assertTrue(accessButton!=null);
		Assert.assertTrue(registrationButton!=null);
		
		return this;
	}
	
	public LoginPageManager insertCredential(CredentialBean credentialBean)
	{
		WaitManager.get().waitLongTime();
		emailInput=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LoginPage.INPUTEMAIL.getWebLocator())),40);
		emailInput.sendKeys(credentialBean.getEmail());
		passwordInput=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LoginPage.INPUTPASSWORD.getWebLocator())),40);
		passwordInput.sendKeys(credentialBean.getPassword());
		return this;
	}
	
	public LoginPageManager submitLogin()
	{
		WaitManager.get().waitShortTime();
		accessButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.LoginPage.ACCESSBUTTON.getWebLocator()));
		accessButton.click();
		return this;
	}
	
	public LoginPageManager checkLoginErrorMessage()
	{
		errorMessage=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LoginPage.ERROR_CREDENTIAL.getWebLocator())),40);
		return this;
	}
	
	public LoginPageManager gotoHomePage()
	{
		logo=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LoginPage.LOGO.getWebLocator())),50);
		logo.click();
		
		return this;
	}
	
	public LoginPageManager logout()
	{
		myProfile=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Homepage.MYACCOUNT.getWebLocator())),40);
		myProfile.click();
		logout=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.myAccountLogo.ESCI.getWebLocator())),40);
		logout.click();
		
		return this;
	}
	
	public LoginPageManager gotoRegistrationPage()
	{
		WaitManager.get().waitMediumTime();
		driver.getPageSource();
		registrationButton=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LoginPage.REGISTERBUTTON.getWebLocator())),40);
		registrationButton.click();
		return this;
	}
}
