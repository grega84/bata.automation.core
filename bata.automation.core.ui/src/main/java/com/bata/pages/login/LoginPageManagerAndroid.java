package com.bata.pages.login;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.Locators;

import test.automation.core.UIUtils;

public class LoginPageManagerAndroid extends LoginPageManager {

	public LoginPageManagerAndroid(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static LoginPageManagerAndroid with(WebDriver driver, Properties languages)
	{
		return new LoginPageManagerAndroid(driver, languages);
	}
	
	public LoginPageManagerAndroid logout()
	{
		myProfile=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Homepage.MYACCOUNT.getWebLocator())),40);
		myProfile.click();
		
		try {Thread.sleep(1000);}catch(Exception err) {}
		hamburger=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.myAccountLogo.HAMBURGER_MENU.getAndroidLocator()));
		hamburger.click();
		try {Thread.sleep(1000);}catch(Exception err) {}
		account=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.myAccountLogo.ACCOUNT.getAndroidLocator()));
		account.click();
		try {Thread.sleep(1000);}catch(Exception err) {}
		logout=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.myAccountLogo.ESCI.getAndroidLocator()));
		logout.click();
		
		
		return this;
	}
}
