package com.bata.pages.login;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.pagepattern.Locators;
import com.bata.pages.homepage.HomePage;
import com.bata.pages.profile.ProfilePage;
import com.bata.pages.registration.RegistrationPage;

import bean.datatable.CredentialBean;
import common.WaitManager;
import test.automation.core.UIUtils;

public class LoginPage extends LoadableComponent<LoginPage> {
	

	private int DRIVER;
	private LoginPageManager gesture;

	protected LoginPage(WebDriver driver, Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		case Locators.ANDROID_CHROME:
			gesture=LoginPageManagerAndroid.with(driver, languages);
			break;
		case Locators.IOS_SAFARI:
			gesture=LoginPageManagerIOS.with(driver, languages);
			break;
		case Locators.DESKTOP_IE:
			gesture=LoginPageManagerIE.with(driver, languages);
			break;
		default:
			gesture=LoginPageManager.with(driver, languages);
		}
	}
	
	public static LoginPage from(WebDriver driver, Properties languages)
	{
		return new LoginPage(driver, languages).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}
	
	public ProfilePage loginOK(CredentialBean credentialBean) 
	{
		gesture.insertCredential(credentialBean)
		.submitLogin();
		
		return ProfilePage.from(gesture.getDriver(),gesture.getLanguages());
	}
	
	public LoginPage loginKO(CredentialBean credentialBean) 
	{
		gesture.insertCredential(credentialBean)
		.submitLogin()
		.checkLoginErrorMessage();
		return this;
	}
	
	public LoginPage checkMessageWrongCredential() 
	{
		gesture.checkLoginErrorMessage();
		return this;
	}
	
	public HomePage goToHomePage() 
	{
		gesture.gotoHomePage();
		return HomePage.from(gesture.getDriver(),gesture.getLanguages());
	}
	
	public LoginPage logout(String typeOfTest) throws Exception 
	{
		gesture.logout();
		
		return this.get();

	}
	
	public RegistrationPage goToRegistrationPage() 
	{
		gesture.gotoRegistrationPage();
		WaitManager.get().waitLongTime();
		return RegistrationPage.from(gesture.getDriver(),gesture.getLanguages());
	}
}
