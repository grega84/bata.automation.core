package com.bata.pages.login;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.Locators;

import test.automation.core.UIUtils;

public class LoginPageManagerIOS extends LoginPageManagerAndroid {

	public LoginPageManagerIOS(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static LoginPageManagerIOS with(WebDriver driver, Properties languages)
	{
		return new LoginPageManagerIOS(driver, languages);
	}
	
	public LoginPageManagerIOS isLoaded()
	{
		accedilabel=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LoginPage.LABELACCESS.getWebLocator())),40);
		emailLabel=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LoginPage.LABELEMAIL.getWebLocator())),40);
		emailInput=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LoginPage.INPUTEMAIL.getWebLocator())),40);
		passwordLabel=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LoginPage.LABELPASSWORD.getWebLocator())),40);
		passwordInput=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LoginPage.INPUTPASSWORD.getWebLocator())),40);
		accessButton=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LoginPage.ACCESSBUTTON.getWebLocator())),40);
		registrationButton=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LoginPage.REGISTERBUTTON.getWebLocator())),40);
		
		Assert.assertTrue(accedilabel!=null);
		Assert.assertTrue(emailLabel!=null);
		Assert.assertTrue(emailInput!=null);
		Assert.assertTrue(passwordLabel!=null);
		Assert.assertTrue(passwordInput!=null);
		Assert.assertTrue(accessButton!=null);
		Assert.assertTrue(registrationButton!=null);
		
		UIUtils.ui().safari().scrollToElement(driver, Locators.LoginPage.LABELACCESS.getWebLocator());
		
		return this;
	}
	
	public LoginPageManagerAndroid logout()
	{
		myProfile=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Homepage.MYACCOUNT.getWebLocator())),40);
		myProfile.click();
		
		try {Thread.sleep(1000);}catch(Exception err) {}
		hamburger=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.myAccountLogo.HAMBURGER_MENU.getAndroidLocator()));
		hamburger.click();
		try {Thread.sleep(1000);}catch(Exception err) {}
		account=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.myAccountLogo.ACCOUNT.getAndroidLocator()));
		account.click();
		try {Thread.sleep(1000);}catch(Exception err) {}
		
		UIUtils.ui().safari().scrollToElement(driver, Locators.myAccountLogo.ESCI.getAndroidLocator());
		
		UIUtils.ui().safari().click(driver, Locators.myAccountLogo.ESCI.getAndroidLocator());
		
		return this;
	}
}
