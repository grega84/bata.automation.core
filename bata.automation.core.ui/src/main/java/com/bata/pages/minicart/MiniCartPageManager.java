package com.bata.pages.minicart;

import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Locators;
import com.bata.pages.AbstractPageManager;

import bean.datatable.SearchItemBean;
import common.CommonFields;
import test.automation.core.UIUtils;

public class MiniCartPageManager extends AbstractPageManager 
{
	protected WebElement title_mini_cart;
	protected WebElement showcart;
	protected WebElement procede_checkout;
	protected WebElement price;

	protected MiniCartPageManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static MiniCartPageManager with(WebDriver driver, Properties languages)
	{
		return new MiniCartPageManager(driver, languages);
	}
	
	public MiniCartPageManager isLoaded()
	{
		title_mini_cart=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MiniCartProductPage.TITLE_MINI_CART.getWebLocator())),20);
		showcart=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MiniCartProductPage.SHOW_CART_BUTTON.getWebLocator())),20);
		procede_checkout=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MiniCartProductPage.PROCEDE_TO_CHECKOUT.getWebLocator())),20);
		price=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MiniCartProductPage.PRICE.getWebLocator())),20);
		Assert.assertTrue(title_mini_cart!=null);
		Assert.assertTrue(showcart!=null);
		Assert.assertTrue(procede_checkout!=null);
		Assert.assertTrue(price!=null);
		
		return this;
	}
	
	public MiniCartPageManager checkPresenceOfAddedElement() 
	{
		//controllo presenza elemento nella lista
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MiniCartProductPage.PRODUCT_ELEMENT.getWebLocator())));
		List<WebElement> elements=driver.findElements(By.xpath(Locators.MiniCartProductPage.PRODUCT_ELEMENT.getWebLocator()));
		
		boolean find=false;
		
		SearchItemBean bean=(SearchItemBean) DinamicData.getIstance().get(CommonFields.SELECTED_ITEM);
		
		for(WebElement e : elements)
		{
			//controllo nome e prezzo
			String name=e.findElement(By.xpath(Locators.MiniCartProductPage.PRODUCT_ELEMENT_TITLE.getWebLocator())).getText().trim().toLowerCase();
			String size=e.findElement(By.xpath(Locators.MiniCartProductPage.PRODUCT_ELEMENT_SIZE.getWebLocator())).getText().trim().toLowerCase();
			//String price=e.findElement(By.xpath(Locators.MiniCartProductPage.PRODUCT_ELEMENT_PRICE.getWebLocator())).getText().trim().toLowerCase();
			
			if(name.equals(bean.getName().toLowerCase()) && size.equals(bean.getSize().toLowerCase()) /*&& price.equals(bean.getPrice().toLowerCase())*/)
			{
				find=true;
				break;
			}
		}
		
		Assert.assertTrue("controllo presenza item nel carrello KO "+bean.getName()+" - "+bean.getPrice()+" - "+bean.getSize(),find == true);
		
		return this;
	}
	
	public MiniCartPageManager gotoCartPage()
	{
		this.showcart=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MiniCartProductPage.SHOW_CART_BUTTON.getWebLocator())));
		Assert.assertTrue(this.showcart != null);
		this.showcart.click();
		
		return this;
	}
}
