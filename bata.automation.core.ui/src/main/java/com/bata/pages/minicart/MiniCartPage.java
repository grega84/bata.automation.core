package com.bata.pages.minicart;

import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Locators;
import com.bata.pages.cartpage.CartPage;

import bean.datatable.PaymentBean;
import bean.datatable.SearchItemBean;
import common.WaitManager;
import test.automation.core.UIUtils;

public class MiniCartPage extends LoadableComponent<MiniCartPage> {
	
	
	private int DRIVER;
	private MiniCartPageManager gesture;
	
	protected MiniCartPage(WebDriver driver,Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		default:
			gesture=MiniCartPageManager.with(driver, languages);
		}
	}
	
	public static MiniCartPage from(WebDriver driver, Properties languages)
	{
		return new MiniCartPage(driver, languages).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}

	public MiniCartPage checkPresenceOfAddedElement() 
	{
		gesture.checkPresenceOfAddedElement();
		return this;
	}

	public CartPage clickShowCart() 
	{
		WaitManager.get().waitMediumTime();
		gesture.gotoCartPage();
		return CartPage.from(gesture.getDriver(),gesture.getLanguages());
	}
	

	
	
}
