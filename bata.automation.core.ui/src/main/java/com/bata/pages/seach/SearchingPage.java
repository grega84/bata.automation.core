package com.bata.pages.seach;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Locators;

import bean.datatable.SearchItemBean;
import bean.datatable.ShippingBean;
import bean.datatable.StoreSearchBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class SearchingPage extends LoadableComponent<SearchingPage> {
	
	
	private int DRIVER;
	private SearchingPageManager gesture;
	
	protected SearchingPage(WebDriver driver,Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		case Locators.ANDROID_CHROME:
			gesture=SearchingPageManagerAndroid.with(driver, languages);
		default:
			gesture=SearchingPageManager.with(driver, languages);
		}
	}
	
	public static SearchingPage from(WebDriver driver, Properties languages)
	{
		return new SearchingPage(driver, languages).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}
	
	public SearchingPage focusOnItem() 
	{
		gesture.focusOnItem();
		return this;
	}	
	public SearchingPage addToWishlistItemFocusedGuest() throws InterruptedException 
	{
		gesture.selectFirstSize()
		.addToWishList();
		return this;
		//removeToWishList=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SearchingPage.REMOVE_TO_WISHLIST.getWebLocator())),60);
	}
	
	public SearchingPage addToWishlistItemFocusedRegistered() throws InterruptedException 
	{
		gesture.selectFirstSize()
		.addToWishList()
		.checkRemoveFromWishListIsPresent();
		return this;
	}
	
	public SearchingPage removeToWishlistItemFocused() throws InterruptedException 
	{
		gesture.selectFirstSize()
		.removeFromWishList()
		.checkAddToWishListIsPresent();
		return this;
	}

	public SearchingPage addItemToCart() 
	{
		gesture.addItemToCart();
		return this;
	}
}
