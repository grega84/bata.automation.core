package com.bata.pages.seach;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.AbstractPageManager;

import bean.datatable.ProductBean;
import bean.datatable.SearchFilterBean;
import common.CommonFields;
import common.WaitManager;
import io.appium.java_client.AppiumDriver;
import test.automation.core.UIUtils;

public class SearchPageFilteredManager extends AbstractPageManager 
{
	protected static final String SIZE = "size";
	protected static final String PRICE = "price";
	protected static final String COLOR = "color";
	protected static final String MATERIAL = "material";
	protected static final String BRAND = "brand";
	protected static final String SELECTED_STORE= "SELECTED_SEARCH_FILTER";
	protected static final String RACCOMANDATI = "recommended";
	protected static final String NOVITA = "new-in";
	protected static final String PIUVENDUTI = "top-sellers";
	protected static final String PREZZO_CRESCENTE = "price-low-to-high";
	protected static final String PREZZO_DECRESCENTE = "price-high-to-low";
	
	protected WebElement logo=null;
	protected WebElement title=null;
	protected WebElement tagliaFilter=null;
	protected WebElement prezzoFilter=null;
	protected WebElement coloreFilter=null;
	protected WebElement materialeFilter=null;
	protected WebElement brandFilter=null;
	protected WebElement categoryName=null;
	protected WebElement news;
	protected WebElement filterElement;
	protected WebElement orderBy;
	protected WebElement orderByFilter;
	protected WebElement filtriMenu;
	protected WebElement applyFilterMobile;
	protected WebElement applyFilterMobileOK;
	protected WebElement closeFilterMobile;

	public SearchPageFilteredManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static SearchPageFilteredManager with(WebDriver driver, Properties languages)
	{
		return new SearchPageFilteredManager(driver, languages);
	}

	public SearchPageFilteredManager isLaded()
	{
		logo=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SearchingPageFiltered.LOGO.getWebLocator())),80);
		
		try {
			news=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SearchingPageFiltered.NEWS.getWebLocator())),10);
			news.click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		WaitManager.get().waitHighTime();
		
		title=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.TITLE.getWebLocator()));
		
		tagliaFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.TAGLIA_FILTER.getWebLocator()));
		prezzoFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.PREZZO_FILTER.getWebLocator()));
		//coloreFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.COLORE_FILTER.getWebLocator()));
		//materialeFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.MATERIALE_FILTER.getWebLocator()));
		//brandFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.BRAND_FILTER.getWebLocator()));
	
		
		Assert.assertTrue(logo!=null);
		Assert.assertTrue(title!=null);
		Assert.assertTrue(tagliaFilter!=null);
		Assert.assertTrue(prezzoFilter!=null);
		//Assert.assertTrue(coloreFilter!=null);
		//Assert.assertTrue(materialeFilter!=null);
		//Assert.assertTrue(brandFilter!=null);
		
		return this;
	}
	
	public SearchPageFilteredManager checkCategoryName(ProductBean productBean)
	{
		categoryName=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.SearchingPageFiltered.CATEGORY_FILTER_RESULT_TEXT.getWebLocator(), new Entry("category",productBean.getCategoryName()))));
		Assert.assertTrue(categoryName!=null);
		
		return this;
	}
	
	public SearchPageFilteredManager applyFilter(SearchFilterBean filter, String country)
	{
		switch(filter.getFilterKey())
		{
		case SIZE:
			filterBySize(filter);
			break;
		case PRICE:
			filterByPrice(filter,country);
			break;
		case COLOR:
			filterByColor(filter);
			break;
		case MATERIAL:
			filterByMaterial(filter);
			break;
		case BRAND:
			filterByBrand(filter);
			break;
		}
		
		return this;
	}

	public SearchPageFilteredManager filterByBrand(SearchFilterBean filter) 
	{
		brandFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.BRAND_FILTER.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		Assert.assertTrue(brandFilter != null);
		
		brandFilter.click();
		
		selectFilter(brandFilter,filter,"brand", null);
		
//		brandFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.BRAND_FILTER.getWebLocator()));
//		try {Thread.sleep(500);}catch(Exception err) {}
//		Assert.assertTrue(tagliaFilter != null);
//		
//		brandFilter.click();
		
		return this;
	}

	public SearchPageFilteredManager filterByMaterial(SearchFilterBean filter) 
	{
		materialeFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.MATERIALE_FILTER.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		Assert.assertTrue(materialeFilter != null);
		
		materialeFilter.click();
		
		selectFilter(materialeFilter,filter,"upperCode", null);
		
//		materialeFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.MATERIALE_FILTER.getWebLocator()));
//		try {Thread.sleep(500);}catch(Exception err) {}
//		Assert.assertTrue(tagliaFilter != null);
//		
//		materialeFilter.click();
		
		return this;
		
	}

	public SearchPageFilteredManager filterByColor(SearchFilterBean filter) 
	{
		coloreFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.COLORE_FILTER.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		Assert.assertTrue(coloreFilter != null);
		
		coloreFilter.click();
		
		selectFilter(coloreFilter,filter,"color", null);
		
//		coloreFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.COLORE_FILTER.getWebLocator()));
//		try {Thread.sleep(500);}catch(Exception err) {}
//		Assert.assertTrue(tagliaFilter != null);
//		
//		coloreFilter.click();
		
		return this;
	}

	public SearchPageFilteredManager filterByPrice(SearchFilterBean filter, String country) 
	{
		prezzoFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.PREZZO_FILTER.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		Assert.assertTrue(prezzoFilter != null);
		
		prezzoFilter.click();
		
		selectFilter(prezzoFilter,filter,"price",country);
		
//		prezzoFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.PREZZO_FILTER.getWebLocator()));
//		try {Thread.sleep(500);}catch(Exception err) {}
//		Assert.assertTrue(tagliaFilter != null);
//		
//		prezzoFilter.click();
		
		return this;

	}

	public SearchPageFilteredManager filterBySize(SearchFilterBean filter) 
	{
		tagliaFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.TAGLIA_FILTER.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		Assert.assertTrue(tagliaFilter != null);
		
		tagliaFilter.click();
		
		selectFilter(tagliaFilter,filter,"size", null);
		
//		tagliaFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.TAGLIA_FILTER.getWebLocator()));
//		try {Thread.sleep(500);}catch(Exception err) {}
//		Assert.assertTrue(tagliaFilter != null);
//		
//		tagliaFilter.click();
		
		return this;

	}

	protected void selectFilter(WebElement filterMacro, SearchFilterBean filter, String filterCategory, String country) 
	{
		String nomeDiv=filterMacro.getText().trim();
		
		WaitManager.get().waitShortTime();
		
		//se è vuoto filter value clicco sul primo item altrimenti sul desiderato
		if(StringUtils.isBlank(filter.getFilterValue()))
		{
			if((driver instanceof AppiumDriver) && filterCategory.equals("upperCode"))
			{
				filterElement=filterMacro.findElement(By.xpath(Utility.replacePlaceHolders(Locators.SearchingPageFiltered.FILTER_ELEMENT.getAndroidLocator(), new Entry("filterCategory",filterCategory))));
			}
			else
				filterElement=filterMacro.findElement(By.xpath(Utility.replacePlaceHolders(Locators.SearchingPageFiltered.FILTER_ELEMENT.getWebLocator(), new Entry("filterCategory",filterCategory))));
		}
		else
		{
			filterElement=filterMacro.findElement(By.xpath(Utility.replacePlaceHolders(Locators.SearchingPageFiltered.FILTER_ELEMENT_BY_VALUE.getWebLocator(), new Entry("filterValue",filter.getFilterValue()),new Entry("filterCategory",filterCategory))));
		}
		
		String filterValue=filterElement.getText().trim();
		
		//ricavo gli elementi che mi aspetto dal filtro
		int expectedResults=0;
		
		switch(filterCategory)
		{
		case "size":
			expectedResults=driver.findElements(By.xpath(Utility.replacePlaceHolders(Locators.SearchingPageFiltered.FILTER_BY_SIZE_QUERY.getWebLocator(), new Entry("size",filterValue)))).size();
			break;
		case "color":
			expectedResults=driver.findElements(By.xpath(Utility.replacePlaceHolders(Locators.SearchingPageFiltered.FILTER_BY_COLOR_QUERY.getWebLocator(), new Entry("color",filterValue)))).size();
			break;
		case "brand":
			expectedResults=driver.findElements(By.xpath(Utility.replacePlaceHolders(Locators.SearchingPageFiltered.FILTER_BY_BRAND_QUERY.getWebLocator(), new Entry("brand",filterValue)))).size();
			break;
		case "price":
			double[] allPrices=getAllPrices(country);
			String values=filterValue.replaceAll("\u20AC", "");
			String[] v=values.split("-");
			DinamicData.getIstance().set(CommonFields.ALL_PRICES, allPrices);
			DinamicData.getIstance().set(CommonFields.RANGE, v);
			break;
		}
		
		DinamicData.getIstance().set(CommonFields.EXPECTED_RESULTS, expectedResults);
		
		SearchFilterBean selectedFilter=new SearchFilterBean();
		
		selectedFilter.setFilterKey(nomeDiv);
		selectedFilter.setFilterValue(filterValue);
		
		//aggiungo l'elemento alla lista di filtri corrispondente
		ArrayList<SearchFilterBean> list=(ArrayList<SearchFilterBean>) DinamicData.getIstance().get(CommonFields.SELECTED_FILTER+"-"+filter.getFilterKey());
		
		if(list == null)
		{
			list=new ArrayList<SearchFilterBean>();
		}
		
		list.add(selectedFilter);
		DinamicData.getIstance().set(CommonFields.SELECTED_FILTER+"-"+filter.getFilterKey(), list);
		
//		filterElement.click();
//		try {Thread.sleep(3000);}catch(Exception err) {}
		
		String filterUrl=filterElement.getAttribute("href");
		
		System.out.println("href:"+filterUrl);
		
		driver.get(filterUrl);
		
		WaitManager.get().waitLongTime();
		
	}

	protected double[] getAllPrices(String country) 
	{
		//ricavo tutti i prezzi della lista mostrata
		double[] L=null;
		
		List<WebElement> l=driver.findElements(By.xpath(Locators.SearchingPageFiltered.FILTER_BY_PRICE_QUERY_ALL.getWebLocator()));
		
		L=new double[l.size()];
		
		for(int i=0;i<l.size();i++)
		{
			String prices=driver.findElement(By.xpath(Locators.SearchingPageFiltered.FILTER_BY_PRICE_QUERY.getWebLocator().replace("#", ""+(i+1)))).getText().trim().replace("\u20AC", "").replace("Kč", "").replace(" ", "");
			System.out.println(prices);
			double value=Double.parseDouble(StringUtils.isBlank(prices) ? "0" : prices);
			L[i]=value;
		}
		
		return L;
	}

	public SearchPageFilteredManager checkAppliedFilter(SearchFilterBean filter) 
	{
		switch(filter.getFilterKey())
		{
		case SIZE:
			checkAppliedFilterBySize(filter);
			break;
		case PRICE:
			checkAppliedFilterByPrice(filter);
			break;
		case COLOR:
			checkAppliedFilterByColor(filter);
			break;
		case MATERIAL:
			checkAppliedFilterByMaterial(filter);
			break;
		case BRAND:
			checkAppliedFilterByBrand(filter);
			break;
		}
		
		return this;
	}

	public SearchPageFilteredManager checkAppliedFilterByBrand(SearchFilterBean filter) 
	{
		/*
		brandFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.BRAND_FILTER.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		Assert.assertTrue(brandFilter != null);
		*/
		
		WebElement find=findAppliedFilter(brandFilter,filter);
		
		Assert.assertTrue("nessun filtro applicato",find != null);
		
		return this;
	}

	public SearchPageFilteredManager checkAppliedFilterByMaterial(SearchFilterBean filter) 
	{
		/*
		materialeFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.MATERIALE_FILTER.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		Assert.assertTrue(materialeFilter != null);
		*/
		
		WebElement find=findAppliedFilter(materialeFilter,filter);
		
		Assert.assertTrue("nessun filtro applicato",find != null);
		
		return this;
	}

	public SearchPageFilteredManager checkAppliedFilterByColor(SearchFilterBean filter) 
	{
		/*
		coloreFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.COLORE_FILTER.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		Assert.assertTrue(coloreFilter != null);
		*/
		
		WebElement find=findAppliedFilter(coloreFilter,filter);
		
		Assert.assertTrue("nessun filtro applicato",find != null);
		
		return this;
	}

	public SearchPageFilteredManager checkAppliedFilterByPrice(SearchFilterBean filter) 
	{
		/*
		if(!(driver instanceof AppiumDriver))
			prezzoFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.PREZZO_FILTER.getWebLocator()));
		
		try {Thread.sleep(500);}catch(Exception err) {}
		Assert.assertTrue(prezzoFilter != null);
		*/
		
		WebElement find=findAppliedFilter(prezzoFilter,filter);
		
		Assert.assertTrue("nessun filtro applicato",find != null);
		
		return this;
	}

	public SearchPageFilteredManager checkAppliedFilterBySize(SearchFilterBean filter) 
	{
		/*
		if(!(driver instanceof AppiumDriver))
			tagliaFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.TAGLIA_FILTER.getWebLocator()));
		
		try {Thread.sleep(500);}catch(Exception err) {}
		Assert.assertTrue(tagliaFilter != null);
		*/
		
		WebElement find=findAppliedFilter(tagliaFilter,filter);
		
		Assert.assertTrue("nessun filtro applicato",find != null);
		
		return this;
	}

	protected WebElement findAppliedFilter(WebElement filterMacro, SearchFilterBean filter) 
	{
		ArrayList<SearchFilterBean> list=(ArrayList<SearchFilterBean>) DinamicData.getIstance().get(CommonFields.SELECTED_FILTER+"-"+filter.getFilterKey());
		Assert.assertTrue(list != null);
		
		boolean find=false;
		
		for(SearchFilterBean f : list)
		{
			int size=driver.findElements(By.xpath(Locators.SearchingPageFiltered.FILTER_APPLIED_ELEMENTS.getWebLocator())).size();
			
			for(int i=0;i<size;i++)
			{
				WebElement e=driver.findElement(By.xpath(Locators.SearchingPageFiltered.FILTER_APPLIED_ELEMENT.getWebLocator().replace("#", ""+(i+1))));
				
				String value=e.getText().trim();
				
				if(value.contains(f.getFilterKey()) && value.contains(f.getFilterValue()))
				{
					return e;
				}
			}
		}
		
		return null;
	}

	public SearchPageFilteredManager checkFilteredResults(SearchFilterBean filter) 
	{
		switch(filter.getFilterKey())
		{
		case SIZE:
			checkReturnedElementsBySize(filter);
			break;
		case PRICE:
			checkReturnedElementsByPrice(filter);
			break;
		case COLOR:
			checkReturnedElementsByColor(filter);
			break;
		case MATERIAL:
			checkReturnedElementsByMaterial(filter);
			break;
		case BRAND:
			checkReturnedElementsByBrand(filter);
			break;
		}
		
		return this;
	}

	public SearchPageFilteredManager checkReturnedElementsByBrand(SearchFilterBean filter) 
	{
		//ricavo numero atteso
		int expectedElements=(int) DinamicData.getIstance().get(CommonFields.EXPECTED_RESULTS);
		int actual=driver.findElements(By.xpath(Locators.SearchingPageFiltered.FILTERED_LIST.getWebLocator())).size();
		
		Assert.assertTrue("controllo risultati attesi dopo applicazione filtro brand:",actual>0);
		return this;
	}

	public SearchPageFilteredManager checkReturnedElementsByMaterial(SearchFilterBean filter) 
	{
		return this;
	}

	public SearchPageFilteredManager checkReturnedElementsByColor(SearchFilterBean filter) 
	{
		//ricavo numero atteso
		int expectedElements=(int) DinamicData.getIstance().get(CommonFields.EXPECTED_RESULTS);
		int actual=driver.findElements(By.xpath(Locators.SearchingPageFiltered.FILTERED_LIST.getWebLocator())).size();
		
		Assert.assertTrue("controllo risultati attesi dopo applicazione filtro colore: ",actual>0);
		
		WebElement total=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.SearchingPageFiltered.TOTAL_ELEMENT_FILTERED.getWebLocator(), new Entry("total",""+actual)))), 5);
		
		Assert.assertTrue("controllo presenza header prodotti trovati per xxx ("+actual+")",total != null);
	
		return this;
	}

	public SearchPageFilteredManager checkReturnedElementsByPrice(SearchFilterBean filter) 
	{
		double[] allPrices=(double[]) DinamicData.getIstance().get(CommonFields.ALL_PRICES);
		String[] range=(String[]) DinamicData.getIstance().get(CommonFields.RANGE);
		
		double min=Double.parseDouble(range[0].trim());
		double max=Double.parseDouble(range[1].trim());
		
		int expected=0;
		
		//mi ricavo tutti i valori compresi nel range min,max
		for(int i=0;i<allPrices.length;i++)
		{
			double value=allPrices[i];
			
			if(value >= min && value <= max)
			{
				expected++;
			}
		}
		
		int actual=driver.findElements(By.xpath(Locators.SearchingPageFiltered.FILTERED_LIST.getWebLocator())).size();
		
		Assert.assertTrue("controllo risultati attesi dopo applicazione filtro prezzo:",actual==expected);
		
		WebElement total=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.SearchingPageFiltered.TOTAL_ELEMENT_FILTERED.getWebLocator(), new Entry("total",""+expected)))), 5);
		
		Assert.assertTrue("controllo presenza header prodotti trovati per xxx ("+expected+")",total != null);
		
		return this;
	}

	public SearchPageFilteredManager checkReturnedElementsBySize(SearchFilterBean filter) 
	{
		//ricavo numero atteso
		int expectedElements=(int) DinamicData.getIstance().get(CommonFields.EXPECTED_RESULTS);
		int actual=driver.findElements(By.xpath(Locators.SearchingPageFiltered.FILTERED_LIST.getWebLocator())).size();
		
		Assert.assertTrue("controllo risultati attesi dopo applicazione filtro taglia: attuale:"+actual+", atteso:"+expectedElements,actual==expectedElements);
		
		WebElement total=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.SearchingPageFiltered.TOTAL_ELEMENT_FILTERED.getWebLocator(), new Entry("total",""+expectedElements)))), 5);
		
		Assert.assertTrue("controllo presenza header prodotti trovati per xxx ("+expectedElements+")",total != null);
	
		return this;
	}

	public SearchPageFilteredManager removeFilter(SearchFilterBean filter) 
	{
		switch(filter.getFilterKey())
		{
		case SIZE:
			removeFilterBySize(filter);
			break;
		case PRICE:
			removeFilterByPrice(filter);
			break;
		case COLOR:
			removeFilterByColor(filter);
			break;
		case MATERIAL:
			removeFilterByMaterial(filter);
			break;
		case BRAND:
			removeFilterByBrand(filter);
			break;
		}
		
		return this;
	}

	public SearchPageFilteredManager removeFilterByBrand(SearchFilterBean filter) 
	{
		brandFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.TAGLIA_FILTER.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		Assert.assertTrue(brandFilter != null);
		
		WebElement find=findAppliedFilter(brandFilter,filter);
		
		Assert.assertTrue(find != null);
		
		find.click();
		
		try {Thread.sleep(3000);}catch(Exception err) {}
		
		return this;
	}

	public SearchPageFilteredManager removeFilterByMaterial(SearchFilterBean filter) 
	{
		materialeFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.TAGLIA_FILTER.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		Assert.assertTrue(materialeFilter != null);
		
		WebElement find=findAppliedFilter(materialeFilter,filter);
		
		Assert.assertTrue(find != null);
		
		find.click();
		
		try {Thread.sleep(3000);}catch(Exception err) {}
		
		return this;
	}

	public SearchPageFilteredManager removeFilterByColor(SearchFilterBean filter) 
	{
		coloreFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.TAGLIA_FILTER.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		Assert.assertTrue(coloreFilter != null);
		
		WebElement find=findAppliedFilter(coloreFilter,filter);
		
		Assert.assertTrue(find != null);
		
		find.click();
		
		try {Thread.sleep(3000);}catch(Exception err) {}
		
		return this;
	}

	public SearchPageFilteredManager removeFilterByPrice(SearchFilterBean filter) 
	{
		prezzoFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.TAGLIA_FILTER.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		Assert.assertTrue(prezzoFilter != null);
		
		WebElement find=findAppliedFilter(prezzoFilter,filter);
		
		Assert.assertTrue(find != null);
		
		find.click();
		
		try {Thread.sleep(3000);}catch(Exception err) {}
		
		return this;
	}

	public SearchPageFilteredManager removeFilterBySize(SearchFilterBean filter) 
	{
		tagliaFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.TAGLIA_FILTER.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		Assert.assertTrue(tagliaFilter != null);
		
		WebElement find=findAppliedFilter(tagliaFilter,filter);
		
		Assert.assertTrue(find != null);
		
		find.click();
		
		try {Thread.sleep(3000);}catch(Exception err) {}
		
		return this;
	}

	public SearchPageFilteredManager checkListAfterRemovingFilter() 
	{
		int actual=driver.findElements(By.xpath(Locators.SearchingPageFiltered.FILTERED_LIST.getWebLocator())).size();
		
		Assert.assertTrue("controllo lista dopo rimozione filtri",actual > 0);
		
		return this;
	}

	public SearchPageFilteredManager applyOrderFilter(SearchFilterBean filter) 
	{
//		orderBy=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.ORDER_BY_BUTTON.getWebLocator()));
//		
//		WaitManager.get().waitShortTime();
//		orderBy.click();
//		WaitManager.get().waitMediumTime();
		
		driver.navigate().refresh();
		
		WaitManager.get().waitLongTime();
		UIUtils.safari().scrollToElement(driver, Utility.replacePlaceHolders(Locators.SearchingPageFiltered.ORDER_BY_BUTTON.getWebLocator(), new Entry("filter",filter.getOrderFilter())));
		WaitManager.get().waitShortTime();
		UIUtils.safari().click(driver, Utility.replacePlaceHolders(Locators.SearchingPageFiltered.ORDER_BY_BUTTON.getWebLocator(), new Entry("filter",filter.getOrderFilter())));
		WaitManager.get().waitMediumTime();
		
		//clicco su filtro ordinamento
		//UIUtils.safari().click(driver, Utility.replacePlaceHolders(Locators.SearchingPageFiltered.ORDER_BY_FILTER.getWebLocator(), new Entry("filter",filter.getOrderFilter())));
		orderByFilter=driver.findElement(By.xpath(Utility.replacePlaceHolders(Locators.SearchingPageFiltered.ORDER_BY_FILTER.getWebLocator(), new Entry("filter",filter.getOrderFilter()))));
		//orderByFilter.click();
		
		String dataUrl=orderByFilter.getAttribute("data-url");
		
		System.out.println("dataurl:"+dataUrl);
		driver.get(dataUrl);
		
		WaitManager.get().waitLongTime();
		
		for(int i=0;i<3;i++)
		{
			UIUtils.ui().scrollToElementLocatedBy(driver, By.xpath("//div[@class='b-footer__top']//li[1]//a[1]"));
			try {Thread.sleep(1000);}catch(Exception err) {}
		}
		
		WaitManager.get().waitMediumTime();
		
		int actual=driver.findElements(By.xpath(Locators.SearchingPageFiltered.FILTERED_LIST.getWebLocator())).size();
		
		DinamicData.getIstance().set(CommonFields.BEFORE_ORDERING, actual);
		
		return this;

	}

	protected void checkListAfterApplyOrdering(String filter) 
	{
		int actual=(int) DinamicData.getIstance().get(CommonFields.BEFORE_ORDERING);
		
		int actualAfterFilter=driver.findElements(By.xpath(Locators.SearchingPageFiltered.FILTERED_LIST.getWebLocator())).size();
		
		Assert.assertTrue("controllo risultati attesi dopo applicazione filtro prezzo:",actual==actualAfterFilter);
		
		WebElement total=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.SearchingPageFiltered.TOTAL_ELEMENT_FILTERED.getWebLocator(), new Entry("total",""+actual)))), 5);
		
		Assert.assertTrue("controllo presenza header prodotti trovati per xxx ("+actual+")",total != null);

	}

	public SearchPageFilteredManager checkAfterApplingFilter(SearchFilterBean filter) 
	{
		switch(filter.getOrderFilter())
		{
		case RACCOMANDATI:
		case PIUVENDUTI:
		case NOVITA:
			checkListAfterApplyOrdering(filter.getOrderFilter());
			break;
		case PREZZO_CRESCENTE:
			checkListAfterApplyPrezzoCrescente();
			break;
		case PREZZO_DECRESCENTE:
			checkListAfterApplyPrezzoDecrescente();
			break;
		}
		
		return this;
	}

	protected void checkListAfterApplyPrezzoDecrescente() 
	{
		double[] list=this.getAllPrices(null);
		
		Assert.assertTrue("controllo lista ",list.length > 0);
		
		double first=list[0];
		
		for(int i=1;i<list.length;i++)
		{
			if(first >= list[i])
			{
				first=list[i];
			}
			else
			{
				Assert.assertTrue("controllo ordinamento decrescente fallito:"+Arrays.toString(list),false);
			}
		}
	}

	protected void checkListAfterApplyPrezzoCrescente() 
	{
		double[] list=this.getAllPrices(null);
		
		Assert.assertTrue("controllo lista ",list.length > 0);
		
		double first=list[0];
		
		for(int i=1;i<list.length;i++)
		{
			if(first <= list[i])
			{
				first=list[i];
			}
			else
			{
				Assert.assertTrue("controllo ordinamento crescente fallito:"+Arrays.toString(list),false);
			}
		}
	}
}
