package com.bata.pages.seach;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;

import bean.datatable.SearchFilterBean;
import io.appium.java_client.AppiumDriver;
import test.automation.core.UIUtils;

public class SearchPageFilteredManagerAndroid extends SearchPageFilteredManager {

	public SearchPageFilteredManagerAndroid(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static SearchPageFilteredManagerAndroid with(WebDriver driver, Properties languages)
	{
		return new SearchPageFilteredManagerAndroid(driver, languages);
	}
	
	public SearchPageFilteredManagerAndroid isLaded()
	{
		logo=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SearchingPageFiltered.LOGO.getWebLocator())),80);
		
		try {
			news=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SearchingPageFiltered.NEWS.getWebLocator())),10);
			news.click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		title=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.TITLE.getWebLocator()));
		
		filtriMenu=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.FILTRI_MENU.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		filtriMenu.click();
		
		tagliaFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.TAGLIA_FILTER.getAndroidLocator()));
		prezzoFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.PREZZO_FILTER.getAndroidLocator()));
		coloreFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.COLORE_FILTER.getAndroidLocator()));
		materialeFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.MATERIALE_FILTER.getAndroidLocator()));
		brandFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.BRAND_FILTER.getAndroidLocator()));
		
		closeFilterMobile=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.CLOSE_FILTER_MENU_MOBILE.getWebLocator()));
		closeFilterMobile.click();
		
		Assert.assertTrue(logo!=null);
		Assert.assertTrue(title!=null);
		Assert.assertTrue(tagliaFilter!=null);
		Assert.assertTrue(prezzoFilter!=null);
		Assert.assertTrue(coloreFilter!=null);
		Assert.assertTrue(materialeFilter!=null);
		Assert.assertTrue(brandFilter!=null);
		
		return this;
	}
	
	public SearchPageFilteredManagerAndroid filterByBrand(SearchFilterBean filter) 
	{
		clickFiltriMenuMobile();
		
		brandFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.BRAND_FILTER.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		Assert.assertTrue(brandFilter != null);
		
		brandFilter.click();
		
		selectFilter(brandFilter,filter,"brand", null);
		
		applyFilterMobile();
		
		return this;
	}
	
	public SearchPageFilteredManagerAndroid filterByMaterial(SearchFilterBean filter) 
	{
		clickFiltriMenuMobile();
		
		materialeFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.MATERIALE_FILTER.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		Assert.assertTrue(materialeFilter != null);
		
		materialeFilter.click();
		
		selectFilter(materialeFilter,filter,"refinementMaterial", null);
		
		applyFilterMobile();
		
		return this;
		
	}
	
	public SearchPageFilteredManagerAndroid filterByColor(SearchFilterBean filter) 
	{
		clickFiltriMenuMobile();
		
		coloreFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.COLORE_FILTER.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		Assert.assertTrue(coloreFilter != null);
		
		coloreFilter.click();
		
		selectFilter(coloreFilter,filter,"color", null);
		
		applyFilterMobile();
		
		return this;
	}
	
	public SearchPageFilteredManagerAndroid filterByPrice(SearchFilterBean filter, String country) 
	{
		clickFiltriMenuMobile();
		
		prezzoFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.PREZZO_FILTER.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		Assert.assertTrue(prezzoFilter != null);
		
		prezzoFilter.click();
		
		selectFilter(prezzoFilter,filter,"price",country);
		
		applyFilterMobile();
		
		return this;

	}

	public SearchPageFilteredManagerAndroid filterBySize(SearchFilterBean filter) 
	{
		clickFiltriMenuMobile();
		
		tagliaFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.TAGLIA_FILTER.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		Assert.assertTrue(tagliaFilter != null);
		
		tagliaFilter.click();
		
		selectFilter(tagliaFilter,filter,"size", null);
		
		applyFilterMobile();
		
		return this;

	}
	
	protected void applyFilterMobile() 
	{
		applyFilterMobile=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.BRAND_SUBMENU.getWebLocator()+Locators.SearchingPageFiltered.APPLY_FILTER_MOBILE.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		applyFilterMobile.click();
		
		applyFilterMobileOK=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.APPLY_FILTER_OK_MOBILE.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		applyFilterMobileOK.click();
	}

	protected void clickFiltriMenuMobile() 
	{
		filtriMenu=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.FILTRI_MENU.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		filtriMenu.click();
		try {Thread.sleep(1000);} catch(Exception err) {}
		
	}
	
	public SearchPageFilteredManagerAndroid applyOrderFilter(SearchFilterBean filter) 
	{
		orderBy=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPageFiltered.ORDER_BY_BUTTON.getAndroidLocator()));
		
		
		try {Thread.sleep(500);}catch(Exception err) {}
		orderBy.click();
		
		
		//clicco su filtro ordinamento
		orderByFilter=driver.findElement(By.xpath(Utility.replacePlaceHolders(Locators.SearchingPageFiltered.ORDER_BY_FILTER.getAndroidLocator(), new Entry("filter",filter.getOrderFilter()))));
		System.out.println("locator:"+Utility.replacePlaceHolders(Locators.SearchingPageFiltered.ORDER_BY_FILTER.getAndroidLocator(), new Entry("filter",filter.getOrderFilter())));
		
		Dimension size=orderByFilter.getSize();
		Point loc=orderByFilter.getLocation();
		
		UIUtils.ui().mobile().tapWebViewElement((AppiumDriver) driver, orderByFilter);
		
		try {Thread.sleep(5000);}catch(Exception err) {}
		
		for(int i=0;i<3;i++)
		{
			UIUtils.ui().scrollToElementLocatedBy(driver, By.xpath("//div[@class='b-footer__top']//li[1]//a[1]"));
			try {Thread.sleep(1000);}catch(Exception err) {}
		}
		
		try {Thread.sleep(3000);}catch(Exception err) {}
		
		int actual=driver.findElements(By.xpath(Locators.SearchingPageFiltered.FILTERED_LIST.getWebLocator())).size();
		
		DinamicData.getIstance().set("BEFORE_ORDERING", actual);
		
		return this;

	}
}
