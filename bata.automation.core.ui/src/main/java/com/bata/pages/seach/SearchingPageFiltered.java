package com.bata.pages.seach;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;

import bean.datatable.ProductBean;
import bean.datatable.SearchFilterBean;
import bean.datatable.ShippingBean;
import bean.datatable.StoreSearchBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import test.automation.core.UIUtils;

public class SearchingPageFiltered extends LoadableComponent<SearchingPageFiltered> 
{
	private int DRIVER;
	private SearchPageFilteredManager gesture;
	
	protected SearchingPageFiltered(WebDriver driver,Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		case Locators.ANDROID_CHROME:
			gesture=SearchPageFilteredManagerAndroid.with(driver, languages);
		default:
			gesture=SearchPageFilteredManager.with(driver, languages);
		}
	}
	
	public static SearchingPageFiltered from(WebDriver driver, Properties languages)
	{
		return new SearchingPageFiltered(driver, languages).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLaded();
	}
	
	public SearchingPageFiltered checkCategoryName(ProductBean productBean) 
	{
		gesture.checkCategoryName(productBean);
		return this;
	}

	public SearchingPageFiltered applyFilter(SearchFilterBean filter, String country) 
	{
		gesture.applyFilter(filter, country);
		return this;
	}
	public SearchingPageFiltered checkAppliedFilter(SearchFilterBean filter) 
	{
		gesture.checkAppliedFilter(filter);
		return this;
	}

	public SearchingPageFiltered checkFilteredResults(SearchFilterBean filter) 
	{
		gesture.checkFilteredResults(filter);
		return this;
	}

	public SearchingPageFiltered removeFilter(SearchFilterBean filter) 
	{
		gesture.removeFilter(filter);
		return this;
	}

	public SearchingPageFiltered checkListAfterRemovingFilter() 
	{
		gesture.checkListAfterRemovingFilter();
		return this;
	}

	public SearchingPageFiltered applyOrderFilter(SearchFilterBean filter) 
	{
		gesture.applyOrderFilter(filter);
		return this;

	}

	public SearchingPageFiltered checkAfterApplingFilter(SearchFilterBean filter) 
	{
		gesture.checkAfterApplingFilter(filter);
		return this;
	}
	
}
