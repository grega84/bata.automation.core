package com.bata.pages.seach;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Locators;

import test.automation.core.UIUtils;

public class SearchingPageManagerAndroid extends SearchingPageManager {

	public SearchingPageManagerAndroid(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static SearchingPageManagerAndroid with(WebDriver driver, Properties languages)
	{
		return new SearchingPageManagerAndroid(driver, languages);
	}
	
	public SearchingPageManagerAndroid isLoaded()
	{
		
		logo=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SearchingPage.LOGO.getWebLocator())),60);
		title=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SearchingPage.TITLE.getWebLocator())), 60);
	
		Assert.assertTrue(logo!=null);
		Assert.assertTrue(title!=null);
		
		return this;
	}

}
