package com.bata.pages.seach;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Locators;
import com.bata.pages.AbstractPageManager;

import bean.datatable.SearchItemBean;
import test.automation.core.UIUtils;

public class SearchingPageManager extends AbstractPageManager 
{
	protected WebElement logo=null;
	protected WebElement title=null;
	protected WebElement tagliaFilter=null;
	protected WebElement prezzoFilter=null;
	protected WebElement coloreFilter=null;
	protected WebElement materialeFilter=null;
	protected WebElement brandFilter=null;
	protected WebElement firstSize=null;
	protected WebElement firstProduct=null;
	protected WebElement addToWishlist=null;
	protected WebElement removeToWishList=null;
	protected WebElement addToCartFirstElement;
	protected WebElement carticon;
	protected WebElement firstImage;
	protected WebElement firstPrice;

	protected SearchingPageManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static SearchingPageManager with(WebDriver driver, Properties languages)
	{
		return new SearchingPageManager(driver, languages);
	}
	
	public SearchingPageManager isLoaded()
	{
		logo=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SearchingPage.LOGO.getWebLocator())),60);
		title=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SearchingPage.TITLE.getWebLocator())), 60);
	
		//pezza a colori
		try
		{
			tagliaFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPage.TAGLIA_FILTER.getWebLocator()));
			prezzoFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPage.PREZZO_FILTER.getWebLocator()));
			coloreFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPage.COLORE_FILTER.getWebLocator()));
			materialeFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPage.MATERIALE_FILTER.getWebLocator()));
			brandFilter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPage.BRAND_FILTER.getWebLocator()));
			
			
			Assert.assertTrue(tagliaFilter!=null);
			Assert.assertTrue(prezzoFilter!=null);
			Assert.assertTrue(coloreFilter!=null);
			Assert.assertTrue(materialeFilter!=null);
			Assert.assertTrue(brandFilter!=null);
		}
		catch(Exception err)
		{
			
		}
		
		Assert.assertTrue(logo!=null);
		Assert.assertTrue(title!=null);
		
		return this;
	}
	
	public SearchingPageManager focusOnItem()
	{
		firstProduct=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPage.FIRST_ITEM.getWebLocator()));
		new Actions(driver).moveToElement(firstProduct).perform();
		
		return this;
	}
	
	public SearchingPageManager selectFirstSize()
	{
		firstSize=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPage.FIRST_SIZE.getWebLocator()));
		firstSize.click();
		try {Thread.sleep(1500);} catch(Exception err) {}
		
		return this;
	}
	
	public SearchingPageManager addToWishList()
	{
		addToCartFirstElement=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPage.ADD_TO_CART_FIRST_ELEMENT.getWebLocator()));
		UIUtils.ui().scroll(driver, UIUtils.SCROLL_DIRECTION.DOWN, 50);
		try {Thread.sleep(1500);} catch(Exception err) {}
		addToWishlist=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPage.ADD_TO_WISHLIST.getWebLocator()));
		addToWishlist.click();
		try {Thread.sleep(2000);} catch(Exception err) {}
		
		return this;
	}
	
	public SearchingPageManager checkRemoveFromWishListIsPresent()
	{
		removeToWishList=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SearchingPage.REMOVE_TO_WISHLIST.getWebLocator())),60);
		return this;
	}
	
	public SearchingPageManager removeFromWishList()
	{
		addToCartFirstElement=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPage.ADD_TO_CART_FIRST_ELEMENT.getWebLocator()));
		UIUtils.ui().scroll(driver, UIUtils.SCROLL_DIRECTION.DOWN, 50);
		try {Thread.sleep(1500);} catch(Exception err) {}
		removeToWishList=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPage.REMOVE_TO_WISHLIST.getWebLocator()));
		removeToWishList.click();
		try {Thread.sleep(2000);} catch(Exception err) {}
		
		return this;
	}
	
	public SearchingPageManager checkAddToWishListIsPresent()
	{
		addToWishlist=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SearchingPage.ADD_TO_WISHLIST.getWebLocator())),60);
		return this;
	}
	
	public SearchingPageManager addItemToCart()
	{
		firstImage=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPage.FIRST_ITEM_IMAGE.getWebLocator()));
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String name=this.firstImage.getAttribute("title").split(",")[0].trim();
		
		firstPrice=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPage.FIRST_ITEM_PRICE.getWebLocator()));
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String price=this.firstPrice.getText().trim();
		
		firstSize=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPage.FIRST_SIZE.getWebLocator()));
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String size=this.firstSize.getText().trim();
		
		firstSize.click();
		
		addToCartFirstElement=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SearchingPage.ADD_TO_CART_FIRST_ELEMENT.getWebLocator()));
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		addToCartFirstElement=driver.findElement(By.xpath(Locators.SearchingPage.ADD_TO_CART_FIRST_ELEMENT.getWebLocator()));
		
		addToCartFirstElement.click();
		
		UIUtils.ui().scroll(driver, UIUtils.SCROLL_DIRECTION.UP, 50);
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		carticon=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ProductPage.MINI_CART.getWebLocator())),10);	
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.elementToBeClickable(By.xpath(Locators.ProductPage.MINI_CART.getWebLocator())));
		carticon.click();
		
		SearchItemBean bean=new SearchItemBean();
		bean.setName(name.toLowerCase());
		bean.setPrice(price.toLowerCase());
		bean.setSize(size.toLowerCase());
		
		DinamicData.getIstance().set("SELECTED_ITEM", bean);
		
		return this;
	}
}
