package com.bata.pages.shipping;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Locators;

import bean.datatable.ShippingBean;
import common.CommonFields;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;

public class ShippingPageManagerIOS extends ShippingPageManager {

	protected ShippingPageManagerIOS(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static ShippingPageManagerIOS with(WebDriver driver, Properties languages)
	{
		return new ShippingPageManagerIOS(driver, languages);
	}
	
	public ShippingPageManagerIOS selectShippingToHome()
	{
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("pl"))
			return this;
		
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ShippingMethod.HOMESHIPPING.getWebLocator())),60);
		
		UIUtils.ui().safari().scrollToElement(driver, Locators.ShippingMethod.HOMESHIPPING.getWebLocator());
		try {Thread.sleep(1000);} catch(Exception err) {}
		UIUtils.ui().safari().click(driver, Locators.ShippingMethod.HOMESHIPPING.getWebLocator());
		
		return this;
	}
	
	public ShippingPageManagerIOS chooseShippingHomeGuest(ShippingBean shippingBean)
	{
		inputEmail=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.INPUT_EMAIL.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		inputEmail.sendKeys(shippingBean.getEmailAddress());
		
		inputPhone=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.INPUT_CELL.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		inputPhone.sendKeys(shippingBean.getPhone());
		
		mrRadio=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.MRRADIOBUTTON.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		mrRadio.click();
		name=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.INPUT_NAME.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		name.sendKeys(shippingBean.getName());
		
		surname=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.INPUT_SURNAME.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		surname.sendKeys(shippingBean.getSurname());
		
		address=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.INPUT_ADDRESS.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		address.sendKeys(shippingBean.getAddress());
		
		cap=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.INPUT_CAP.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		cap.sendKeys(shippingBean.getCap());
		
		
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("ita") || !DinamicData.getIstance().get(CommonFields.COUNTRY).equals("sk"))
		{
			city=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.INPUT_CITY.getWebLocator()));
			try {Thread.sleep(200);}catch(Exception err) {}
			city.sendKeys(shippingBean.getCity());
			
			
			province=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.INPUT_PROVINCE.getWebLocator()));
			province.click();
			
			System.out.println(((AppiumDriver)driver).getContextHandles());
			
			((AppiumDriver)driver).context("NATIVE_APP");
			
			UIUtils.ui().mobile().selectElementFromIOSPickerWeel((MobileDriver) driver, "//*[@class='UIAPicker']//*[@class='UIAView']", "//*[@class='UIAPicker']/*[@class='UIAPickerWheel']", shippingBean.getProvince());
			
			((AppiumDriver)driver).context("WEBVIEW_1");
			
			checkboxBillingAddress=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.CHECKBOX_BILLING_ADDRESS.getWebLocator()));
			try {Thread.sleep(200);}catch(Exception err) {}
		}
		
		//checkboxBillingAddress.click();
		
		/*spunta check sms
		checkboxSms=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.CHECKBOX_SMS.getWebLocator()));
		checkboxSms.click();
		*/
		try {Thread.sleep(200);}catch(Exception err) {}
		/*
		scegliOpzioneConsegna=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.OPZIONI_CONSEGNA.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		showCart=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.SHOW_CART.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}*/
		paymentButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.PAYMENT_BUTTON.getWebLocator()));
		//paymentButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.PAYMENT_BUTTON.getWebLocator()));
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		paymentButton.click();
		
		return this;
	}
	
	public ShippingPageManagerIOS selectShippingToStore(ShippingBean shippingBean)
	{
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("pl"))
			return this;
		
		UIUtils.ui().safari().scrollToElement(driver, Locators.collectFromStore.SHOPSHIPPING.getWebLocator());
		try {Thread.sleep(500);}catch(Exception err) {}
		UIUtils.ui().safari().click(driver, Locators.collectFromStore.SHOPSHIPPING.getWebLocator());
		
		try {Thread.sleep(500);}catch(Exception err) {}
		storeShipping.click();
		try {Thread.sleep(200);}catch(Exception err) {}
		//position=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.LABEL_POSITION.getWebLocator()));
		//try {Thread.sleep(200);}catch(Exception err) {}
		searchStore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.SEARCH_POSITION.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		
		if(shippingBean.getDataStore().equals(GEOIP))
		{
			UIUtils.ui().safari().scrollToElement(driver, Locators.collectFromStore.LABEL_POSITION.getWebLocator());
			try {Thread.sleep(1000);}catch(Exception err) {}
			UIUtils.ui().safari().click(driver, Locators.collectFromStore.LABEL_POSITION.getWebLocator());
			
		}
		else
		{
			dataStore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.INPUT_POSITION.getWebLocator()));
			try {Thread.sleep(200);}catch(Exception err) {}
			dataStore.clear();
			dataStore.sendKeys(shippingBean.getDataStore());
			
			try {Thread.sleep(1000);}catch(Exception err) {}
			searchStore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.SEARCH_POSITION.getWebLocator()));
			try {Thread.sleep(1000);}catch(Exception err) {}
			searchStore.click();
		}
		
		
		showList=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.SHOW_LIST.getWebLocator()));
		showList.click();
		try {Thread.sleep(200);}catch(Exception err) {}
		selectStore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.SELECT_STORE.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		selectStore=driver.findElement(By.xpath(Locators.collectFromStore.SELECT_STORE.getWebLocator()));
		selectStore.click();
		changeStore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.CHANGE_STORE.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		//saveStoreData();
		
		return this;
	}
	
	
	public ShippingPageManagerIOS chooseShippingStoreGuest(ShippingBean shippingBean)
	{
		try {Thread.sleep(1000);}catch(Exception err) {}
		
		inputEmail=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.INPUT_EMAIL_STORE.getIOSLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		inputEmail.sendKeys(shippingBean.getEmailAddress());
		inputPhone=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.INPUT_CELL.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		inputPhone.sendKeys(shippingBean.getPhone());
		
		mrRadio=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.MRRADIOBUTTON.getIOSLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		mrRadio.click();
		
		name=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.INPUT_NAME.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		name.sendKeys(shippingBean.getName());
		
		surname=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.INPUT_SURNAME.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		surname.sendKeys(shippingBean.getSurname());
		
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("ita"))
			confirmButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.PROCEDE_TO_PAYMENT.getIOSLocator()));
		else
			confirmButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.PROCEDE_TO_PAYMENT_CZ_STORE.getWebLocator()));
		
		try {Thread.sleep(200);}catch(Exception err) {}
		confirmButton.click();
		
		return this;
	}

}
