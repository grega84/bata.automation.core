package com.bata.pages.shipping;

import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.AbstractPageManager;

import bean.datatable.ShippingBean;
import bean.datatable.StoreBean;
import common.CommonFields;
import test.automation.core.UIUtils;

public class ShippingPageManager extends AbstractPageManager 
{
	protected static final String GEOIP = "!geoip!";
	protected WebElement title=null;
	protected WebElement storeShipping=null;
	protected WebElement homeShipping=null;
	protected WebElement infoConsegna=null;
	protected WebElement inputEmail=null;
	protected WebElement inputPhone=null;
	protected WebElement mrsRadio=null;
	protected WebElement mrRadio=null;
	protected WebElement name=null;
	protected WebElement surname=null;
	protected WebElement address=null;
	protected WebElement cap=null;
	protected WebElement city=null;
	protected WebElement province=null;
	protected WebElement country=null;
	protected WebElement checkboxBillingAddress=null;
	protected WebElement checkboxSms=null;
	protected WebElement scegliOpzioneConsegna=null;
	protected WebElement price=null;
	protected WebElement paymentButton=null;
	protected WebElement showCart=null;
	protected WebElement position=null;
	protected WebElement dataStore=null;
	protected WebElement searchStore=null;
	protected WebElement showList=null;
	protected WebElement selectStore=null;
	protected WebElement changeStore=null;
	protected WebElement confirmButton=null;
	protected Properties language;
	protected WebElement storeSelectedName;
	protected WebElement storeSelectedHours;
	protected WebElement storeSelectedPhone;
	protected WebElement storeSelectedDetails;
	protected WebElement storeFoundText;
	protected WebElement notFoundError;

	protected ShippingPageManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static ShippingPageManager with(WebDriver driver, Properties languages)
	{
		return new ShippingPageManager(driver, languages);
	}
	
	public ShippingPageManager isLoaded()
	{
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("pl"))
			return this;
		
		title=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ShippingMethod.TITLE.getWebLocator())),40);
		storeShipping=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ShippingMethod.SHOPSHIPPING.getWebLocator())),40);
		homeShipping=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ShippingMethod.HOMESHIPPING.getWebLocator())),40);
		infoConsegna=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ShippingMethod.TITLE_DELIVERY.getWebLocator())),40);
		Assert.assertTrue(title!=null);
		Assert.assertTrue(storeShipping!=null);
		Assert.assertTrue(homeShipping!=null);
		Assert.assertTrue(infoConsegna!=null);
		
		return this;
	}
	
	public ShippingPageManager chooseShippingHomeGuest(ShippingBean shippingBean)
	{
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("pl"))
			return chooseShippingHomeGuestPoland(shippingBean);
		
		inputEmail=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.INPUT_EMAIL.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		inputEmail.sendKeys(shippingBean.getEmailAddress());
		
		inputPhone=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.INPUT_CELL.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		inputPhone.sendKeys(shippingBean.getPhone());
		
		mrRadio=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.MRRADIOBUTTON.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		mrRadio.click();
		name=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.INPUT_NAME.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		name.sendKeys(shippingBean.getName());
		
		surname=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.INPUT_SURNAME.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		surname.sendKeys(shippingBean.getSurname());
		
		address=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.INPUT_ADDRESS.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		address.sendKeys(shippingBean.getAddress());
		
		cap=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.INPUT_CAP.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		cap.sendKeys(shippingBean.getCap());
		
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("ita"))
		{
			city=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.INPUT_CITY.getWebLocator()));
			try {Thread.sleep(200);}catch(Exception err) {}
			city.sendKeys(shippingBean.getCity());
			
			province=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.INPUT_PROVINCE.getWebLocator()));
			try {Thread.sleep(200);}catch(Exception err) {}
			Select provinceSelect = new Select(province);
			provinceSelect.selectByVisibleText(shippingBean.getProvince());
			checkboxBillingAddress=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.CHECKBOX_BILLING_ADDRESS.getWebLocator()));
			try {Thread.sleep(200);}catch(Exception err) {}
		}
		else
		{
			city=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.INPUT_CITY.getWebLocator()));
			try {Thread.sleep(200);}catch(Exception err) {}
			city.clear();
			city.sendKeys(shippingBean.getCity());
			city=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.INPUT_CITY.getWebLocator()));
			try {Thread.sleep(200);}catch(Exception err) {}
			city.clear();
			city.sendKeys(shippingBean.getCity());
			
		}
		
		//caso numero di delivery superiore a 1, in questo caso seleziono quello che 
		//sul quale non si deve selezionare l'indirizzo in quanto porta problemi sul click alla
		//selezione di uno qualsiasi degli indirizzi
		List<WebElement> deliveries=driver.findElements(By.xpath("//label[@class='b-checkout-delivery__option-label b-label']"));
		
		if(deliveries.size() > 1)
		{
			for(int i=0;i<deliveries.size();i++)
			{
				WebElement e=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath("(//label[@class='b-checkout-delivery__option-label b-label'])["+(i+1)+"]"));
				try {Thread.sleep(1000);}catch(Exception err) {}
				e.click();
				
				try
				{
					UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='ui-dialog-content_wrapper']")),5);
					driver.findElement(By.xpath("//button[@class='ui-button ui-corner-all ui-widget ui-button-icon-only ui-dialog-titlebar-close']")).click();
				}catch(Exception err)
				{
					break;
				}
			}
		}
		
		//checkboxBillingAddress.click();
		
		try {Thread.sleep(200);}catch(Exception err) {}
		
		paymentButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.PAYMENT_BUTTON.getWebLocator()));
		try {Thread.sleep(1000);} catch (InterruptedException e) {}
		paymentButton.click();
		
		try {Thread.sleep(2000);} catch (InterruptedException e) {}
		try
		{
			paymentButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.PAYMENT_BUTTON.getWebLocator()));
			try {Thread.sleep(1000);} catch (InterruptedException e) {}
			paymentButton.click();
		}
		catch(Exception err)
		{
			
		}
		
		return this;
	}
	
	protected ShippingPageManager chooseShippingHomeGuestPoland(ShippingBean shippingBean) 
	{
		inputEmail=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethodPL.INPUT_EMAIL.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		inputEmail.sendKeys(shippingBean.getEmailAddress());
		
		inputPhone=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethodPL.INPUT_CELL.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		inputPhone.sendKeys(shippingBean.getPhone());
		
		mrRadio=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethodPL.MRRADIOBUTTON.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		mrRadio.click();
		name=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethodPL.INPUT_NAME.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		name.sendKeys(shippingBean.getName());
		
		surname=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethodPL.INPUT_SURNAME.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		surname.sendKeys(shippingBean.getSurname());
		
		address=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethodPL.INPUT_ADDRESS.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		address.sendKeys(shippingBean.getAddress());
		
		cap=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethodPL.INPUT_CAP.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		cap.sendKeys(shippingBean.getCap());
		
		city=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethodPL.INPUT_CITY.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		city.clear();
		city.sendKeys(shippingBean.getCity());
		city=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethodPL.INPUT_CITY.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		city.clear();
		city.sendKeys(shippingBean.getCity());
		
		
		//checkboxBillingAddress.click();
		
		/*spunta check sms
		checkboxSms=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethodPL.CHECKBOX_SMS.getWebLocator()));
		checkboxSms.click();
		*/
		try {Thread.sleep(200);}catch(Exception err) {}
		/*
		scegliOpzioneConsegna=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethodPL.OPZIONI_CONSEGNA.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		showCart=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethodPL.SHOW_CART.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}*/
		paymentButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethodPL.PAYMENT_BUTTON.getWebLocator()));
		//paymentButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.PAYMENT_BUTTON.getWebLocator()));
		try {Thread.sleep(1000);} catch (InterruptedException e) {}
		paymentButton.click();
		
		try
		{
			try {Thread.sleep(2000);} catch (InterruptedException e) {}
			paymentButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethodPL.PAYMENT_BUTTON.getWebLocator()));
			try {Thread.sleep(1000);} catch (InterruptedException e) {}
			paymentButton.click();
		}
		catch(Exception err) {}
		
		return this;
	}

	public ShippingPageManager checkAddressModalAndClickOnFirstItem() 
	{
		try
		{
			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ShippingMethod.CONFERMA_INDIRIZZO_MODAL.getWebLocator())), 10);
			
			driver.findElement(By.xpath(Locators.ShippingMethod.SELECT_FIRST_ADDRESS_ITEM.getWebLocator()))
			.click();
			
			UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.CLOSE_ADDRESS_MODAL.getWebLocator()))
			.click();
		}
		catch(Exception err)
		{
			
		}
		
		return this;
	}

	public ShippingPageManager chooseShippingStoreGuest(ShippingBean shippingBean)
	{
		try {Thread.sleep(1000);}catch(Exception err) {}
		
		inputEmail=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.INPUT_EMAIL_STORE.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		inputEmail.sendKeys(shippingBean.getEmailAddress());
		inputPhone=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.INPUT_CELL.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		inputPhone.sendKeys(shippingBean.getPhone());
		
		mrRadio=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.MRRADIOBUTTON.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		mrRadio.click();
		
		name=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.INPUT_NAME.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		name.sendKeys(shippingBean.getName());
		
		surname=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.INPUT_SURNAME.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		surname.sendKeys(shippingBean.getSurname());
		
		if(!DinamicData.getIstance().get(CommonFields.COUNTRY).equals("cz"))
			confirmButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.PROCEDE_TO_PAYMENT.getWebLocator()));
		else
			confirmButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.PROCEDE_TO_PAYMENT_CZ_STORE.getWebLocator()));
		
		try {Thread.sleep(200);}catch(Exception err) {}
		confirmButton.click();
		
		return this;
	}
	
	public ShippingPageManager chooseShippingHomeRegistered()
	{
		checkboxBillingAddress=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.CHECKBOX_BILLING_ADDRESS.getWebLocator()));
		//checkboxBillingAddress.click();
		
		/*spunta check sms
		checkboxSms=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.CHECKBOX_SMS.getWebLocator()));
		checkboxSms.click();
		*/
		
		scegliOpzioneConsegna=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.OPZIONI_CONSEGNA.getWebLocator()));
		showCart=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.SHOW_CART.getWebLocator()));
		price=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.PRICE.getWebLocator()));
		paymentButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.PAYMENT_BUTTON.getWebLocator()));
		paymentButton.click();
		
		return this;
	}
	
	public ShippingPageManager chooseShippingStoreRegistered()
	{
		try
		{
			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//fieldset[@class='b-checkout-billing__invoice-wrapper js-invoice-fields']")));
			WebElement needInvoice=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath("//label[@for='dwfrm_billing_billingAddress_invoice_invoiceRequested']"));
			Thread.sleep(500);
			needInvoice.click();
		}
		catch(Exception err)
		{
			
		}
		
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("ita"))
			confirmButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.PROCEDE_TO_PAYMENT.getWebLocator()));
		else
			confirmButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.PROCEDE_TO_PAYMENT_CZ_STORE.getWebLocator()));
		
		try {Thread.sleep(500);}catch(Exception err) {}
		confirmButton.click();
		
		return this;
	}
	
	public ShippingPageManager selectShippingToHome()
	{
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("pl"))
			return this;
		
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ShippingMethod.HOMESHIPPING.getWebLocator())),60);
		
		homeShipping=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.HOMESHIPPING.getWebLocator()));
		try {Thread.sleep(1000);}catch(Exception err) {}
		homeShipping.click();
		
		return this;
	}
	
	public ShippingPageManager selectShippingToStore(ShippingBean shippingBean)
	{
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("pl"))
			return this;
		
		storeShipping=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.SHOPSHIPPING.getWebLocator()));
		Assert.assertTrue(storeShipping!=null);
		try {Thread.sleep(500);}catch(Exception err) {}
		storeShipping.click();
		try {Thread.sleep(200);}catch(Exception err) {}
		position=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.LABEL_POSITION.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		searchStore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.SEARCH_POSITION.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		
		if(shippingBean.getDataStore().equals(GEOIP))
		{
			position.click();
			try {Thread.sleep(1000);}catch(Exception err) {}
			
		}
		else
		{
			dataStore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.INPUT_POSITION.getWebLocator()));
			try {Thread.sleep(200);}catch(Exception err) {}
			dataStore.clear();
			dataStore.sendKeys(shippingBean.getDataStore());
			
			try {Thread.sleep(1000);}catch(Exception err) {}
			searchStore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.SEARCH_POSITION.getWebLocator()));
			try {Thread.sleep(1000);}catch(Exception err) {}
			searchStore.click();
		}
		
		
		showList=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.SHOW_LIST.getWebLocator()));
		showList.click();
		try {Thread.sleep(200);}catch(Exception err) {}
		selectStore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.SELECT_STORE.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		selectStore=driver.findElement(By.xpath(Locators.collectFromStore.SELECT_STORE.getWebLocator()));
		selectStore.click();
		changeStore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.CHANGE_STORE.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		//saveStoreData();
		
		return this;
	}
	
	protected void saveStoreData() 
	{
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("pl"))
			return;
		
		//salvo i dati sullo store
		StoreBean bean=new StoreBean();
		
		storeSelectedName=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.STORE_SELECTED_NAME.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		
		bean.setName(storeSelectedName.getText().trim().toLowerCase());
		
		storeSelectedDetails=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.STORE_SELECTED_DETAILS.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		
		String[] details=storeSelectedDetails.getText().trim().toLowerCase().split("\\n");
		bean.setDetails(details[0].substring(0, details[0].lastIndexOf(",")));
		bean.setStoreCity(details[1]);
		bean.setStoreCap(details[2]);
		
		storeSelectedHours=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.STORE_SELECTED_HOUR.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		
		bean.setStoreHours(storeSelectedHours.getText().trim().toLowerCase());
		
		storeSelectedPhone=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.STORE_SELECTED_PHONE.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		
		bean.setPhone(storeSelectedPhone.getText().trim().toLowerCase());
		
		DinamicData.getIstance().set(CommonFields.SELECTED_STORE, bean);
	}
	
	public ShippingPageManager changeStore(ShippingBean b)
	{
		//ricavo il precedente store selezionato
		StoreBean store=(StoreBean) DinamicData.getIstance().get(CommonFields.SELECTED_STORE);
		Assert.assertTrue(store != null);
		
		//clicco cambia store
		changeStore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.CHANGE_STORE.getWebLocator()));
		Assert.assertTrue(changeStore != null);
		try {Thread.sleep(200);}catch(Exception err) {}
		
		changeStore.click();
		
		b.setDataStore(b.getNewDataStore());
		
		//seleziono un nuovo store
		selectShippingToStore(b);
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//controllo che il negozio sia effettivamente cambiato
		StoreBean newStore=(StoreBean) DinamicData.getIstance().get(CommonFields.SELECTED_STORE);
		Assert.assertTrue(newStore != null);
		
		Assert.assertTrue("controllo cambiamento store",!newStore.equals(store));
		
		return this;
	}
	
	public ShippingPageManager searchAStore(ShippingBean shippingBean)
	{
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("pl"))
			return this;
		
		storeShipping=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.SHOPSHIPPING.getWebLocator()));
		Assert.assertTrue(storeShipping!=null);
		try {Thread.sleep(500);}catch(Exception err) {}
		storeShipping.click();
		try {Thread.sleep(200);}catch(Exception err) {}
		position=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.LABEL_POSITION.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		searchStore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.SEARCH_POSITION.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		dataStore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.INPUT_POSITION.getWebLocator()));
		dataStore.clear();
		dataStore.sendKeys(shippingBean.getDataStore());
		try {Thread.sleep(1000);}catch(Exception err) {}
		searchStore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.SEARCH_POSITION.getWebLocator()));
		try {Thread.sleep(1000);}catch(Exception err) {}
		searchStore.click();
		
		return this;
	}
	
	public ShippingPageManager checkStoreAfterSearch(ShippingBean shippingBean)
	{
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("pl"))
			return this;
		
		try {Thread.sleep(2000);} catch (InterruptedException e) {}
		
		storeFoundText=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.STORE_NUMBER_FOUND.getWebLocator()));
		try {Thread.sleep(2000);} catch (InterruptedException e) {}
		
		String actual=storeFoundText.getText().trim().toLowerCase();
		String expected=Utility.replacePlaceHolders(language.getProperty("storeFoundText"), new Entry("numberFound","0"), new Entry("storeName",shippingBean.getDataStore())).toLowerCase();
		
		Assert.assertTrue("controllo risultati dopo ricerca senza esiti: attuale:"+actual+", expected:"+expected,actual.equals(expected));
		
		notFoundError=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.collectFromStore.STORE_NO_RESULT_FOUND.getWebLocator()));
		try {Thread.sleep(2000);} catch (InterruptedException e) {}
		
		actual=notFoundError.getText().trim().toLowerCase();
		expected=Utility.replacePlaceHolders(language.getProperty("storeNotFoundError"), new Entry("storeName",shippingBean.getDataStore())).toLowerCase();
		Assert.assertTrue("controllo messaggio di errore dopo ricerca senza esiti: attuale:"+actual+", expected:"+expected,actual.equals(expected));
		
		
		return this;
	}
}
