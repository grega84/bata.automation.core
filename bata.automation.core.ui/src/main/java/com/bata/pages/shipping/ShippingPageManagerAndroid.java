package com.bata.pages.shipping;

import java.time.Duration;
import java.util.Properties;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.FluentWait;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Locators;

import bean.datatable.ShippingBean;
import common.CommonFields;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import test.automation.core.UIUtils;

public class ShippingPageManagerAndroid extends ShippingPageManager {

	public ShippingPageManagerAndroid(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static ShippingPageManagerAndroid with(WebDriver driver, Properties languages)
	{
		return new ShippingPageManagerAndroid(driver, languages);
	}
	
	public ShippingPageManagerAndroid chooseShippingHomeGuest(ShippingBean shippingBean)
	{
		((AndroidDriver) driver).context("NATIVE_APP");
		
		try
		{
			Thread.sleep(3000);
			
			((AndroidDriver) driver).findElementById("com.android.chrome:id/positive_button").click();
			
			Thread.sleep(200);
			((AndroidDriver) driver).findElementById("com.android.packageinstaller:id/permission_allow_button").click();
		}
		catch(Exception err)
		{
			err.printStackTrace();
		}
		
		((AndroidDriver) driver).context("CHROMIUM");
		
		super.chooseShippingHomeGuest(shippingBean);
		return this;
	}
	
	public ShippingPageManagerAndroid chooseShippingStoreGuest(ShippingBean shippingBean)
	{
		((AndroidDriver) driver).context("NATIVE_APP");
		
		try
		{
			Thread.sleep(3000);
			
			((AndroidDriver) driver).findElementById("com.android.chrome:id/positive_button").click();
			
			Thread.sleep(200);
			((AndroidDriver) driver).findElementById("com.android.packageinstaller:id/permission_allow_button").click();
		}
		catch(Exception err)
		{
			err.printStackTrace();
		}
		
		((AndroidDriver) driver).context("CHROMIUM");
		
		try {Thread.sleep(1000);}catch(Exception err) {}
		
		super.chooseShippingStoreGuest(shippingBean);
		
		return this;
	}
	
	public ShippingPageManagerAndroid chooseShippingHomeRegistered()
	{
		((AndroidDriver) driver).context("NATIVE_APP");
		
		try
		{
			Thread.sleep(3000);
			
			((AndroidDriver) driver).findElementById("com.android.chrome:id/positive_button").click();
			
			Thread.sleep(200);
			((AndroidDriver) driver).findElementById("com.android.packageinstaller:id/permission_allow_button").click();
		}
		catch(Exception err)
		{
			err.printStackTrace();
		}
		
		((AndroidDriver) driver).context("CHROMIUM");
		
		super.chooseShippingHomeRegistered();
		
		return this;
	}
	
	public ShippingPageManagerAndroid chooseShippingStoreRegistered()
	{
		((AndroidDriver) driver).context("NATIVE_APP");
		
		try
		{
			Thread.sleep(3000);
			
			((AndroidDriver) driver).findElementById("com.android.chrome:id/positive_button").click();
			
			Thread.sleep(200);
			((AndroidDriver) driver).findElementById("com.android.packageinstaller:id/permission_allow_button").click();
		}
		catch(Exception err)
		{
			err.printStackTrace();
		}
		
		((AndroidDriver) driver).context("CHROMIUM");
		
		super.chooseShippingStoreRegistered();
		
		return this;
		
	}
	
	public ShippingPageManagerAndroid selectShippingToHome()
	{
		((AndroidDriver) driver).context("NATIVE_APP");
		
		try
		{
			Thread.sleep(3000);
			
			((AndroidDriver) driver).findElementById("com.android.chrome:id/positive_button").click();
			
			Thread.sleep(200);
			((AndroidDriver) driver).findElementById("com.android.packageinstaller:id/permission_allow_button").click();
		}
		catch(Exception err)
		{
			err.printStackTrace();
		}
		
		((AndroidDriver) driver).context("CHROMIUM");
		
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("pl"))
			return this;
		
		
		FluentWait wait=new FluentWait(driver);
		
		wait.pollingEvery(Duration.ofMillis(200));
		wait.withTimeout(Duration.ofSeconds(60));
		wait.until(new Function<AppiumDriver,Boolean>(){

			@Override
			public Boolean apply(AppiumDriver t) 
			{
				return t.findElement(By.xpath(Locators.ShippingMethod.HOMESHIPPING.getWebLocator())) != null;
			}
			
		});
		
		
		homeShipping=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ShippingMethod.HOMESHIPPING.getWebLocator()));
		try {Thread.sleep(1000);}catch(Exception err) {}
		homeShipping.click();
		
		return this;
	}
}
