package com.bata.pages.shipping;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.time.Duration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Set;
import java.util.function.Function;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import bean.datatable.PaymentBean;
import bean.datatable.ShippingBean;
import bean.datatable.StoreBean;

import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.PayWith;
import com.bata.pages.paymenthome.PaymentShipToHomePage;
import com.bata.pages.paymentstore.PaymentShipToStorePage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import test.automation.core.UIUtils;

public class ShippingPage extends LoadableComponent<ShippingPage> {
	
	
	private int DRIVER;
	private ShippingPageManager gesture;
	
	protected ShippingPage(WebDriver driver,Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		case Locators.ANDROID_CHROME:
			gesture=ShippingPageManagerAndroid.with(driver, languages);
			break;
		case Locators.IOS_SAFARI:
			gesture=ShippingPageManagerIOS.with(driver, languages);
			break;
		default:
			gesture=ShippingPageManager.with(driver, languages);
		}
	}
	
	public static ShippingPage from(WebDriver driver, Properties languages)
	{
		return new ShippingPage(driver, languages).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}
	
	public PayWith chooseHomeShippingGuestUser(ShippingBean shippingBean) 
	{
		gesture.chooseShippingHomeGuest(shippingBean)
		.checkAddressModalAndClickOnFirstItem();
		return PaymentShipToHomePage.from(gesture.getDriver(),gesture.getLanguages());	
	}

	public PayWith chooseStoreShippingGuestUser(ShippingBean shippingBean) 
	{
		gesture.chooseShippingStoreGuest(shippingBean);
		return PaymentShipToStorePage.from(gesture.getDriver(),gesture.getLanguages());
	}
	
	public PaymentShipToHomePage chooseHomeShippingRegisteredUser() 
	{
		gesture.chooseShippingHomeRegistered()
		.checkAddressModalAndClickOnFirstItem();
		return PaymentShipToHomePage.from(gesture.getDriver(),gesture.getLanguages());
	}

	public PaymentShipToStorePage chooseStoreShippingRegisteredUser(ShippingBean shippingBean) 
	{
		gesture.chooseShippingStoreRegistered();
		return PaymentShipToStorePage.from(gesture.getDriver(),gesture.getLanguages());
	}

	public ShippingPage selectShippingToHome() 
	{
		gesture.selectShippingToHome();
		return this;
	}

	public ShippingPage selectShippingStore(ShippingBean shippingBean) 
	{
		gesture.selectShippingToStore(shippingBean).saveStoreData();
		return this;
	}

	public ShippingPage changeStore(ShippingBean b) 
	{
		gesture.changeStore(b);
		return this;
	}

	public ShippingPage searchStore(ShippingBean shippingBean) 
	{
		gesture.searchAStore(shippingBean);
		return this;
	}

	public ShippingPage checkStoreNotFound(Properties language, ShippingBean shippingBean) 
	{
		gesture.checkStoreAfterSearch(shippingBean);
		return this;
	}

	
	
}
