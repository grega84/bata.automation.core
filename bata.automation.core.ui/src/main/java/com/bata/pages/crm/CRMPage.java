package com.bata.pages.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Properties;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;

import bean.datatable.CRMCustomerInfoSearchBean;
import bean.datatable.CredentialBean;
import bean.datatable.DatiPersonaliBean;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class CRMPage extends LoadableComponent<CRMPage> 
{
	private CRMPageManager gesture;
	private int DRIVER;

	protected CRMPage(WebDriver driver,Properties language) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		default:
			gesture=CRMPageManager.with(driver, language);
		}
	}
	
	public static CRMPage from(WebDriver driver,Properties language)
	{
		return new CRMPage(driver, language).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}
	
	public CRMPage login(CredentialBean bean)
	{
		gesture.login(bean);
		return this;
	}

	public CRMPage gotoCustomerSection() 
	{
		gesture.gotoCustomerSection();
		return this;
	}

	public CRMPage searchAndSelectUser(CRMCustomerInfoSearchBean b) 
	{
		gesture.searchAndSelectUser(b);
		return this;
	}

	public CRMPage updateSelectedCustomerInfo() 
	{
		gesture.updateSelectedCustomerInfo();
		return this;
	}

	public CRMPage saveUpdateCustomerInfo() 
	{
		gesture.saveUpdateCustomerInfo();
		return this;
	}
	
}
