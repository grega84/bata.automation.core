package com.bata.pages.crm;

import java.util.Properties;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.AbstractPageManager;

import bean.datatable.CRMCustomerInfoSearchBean;
import bean.datatable.CredentialBean;
import bean.datatable.DatiPersonaliBean;
import test.automation.core.UIUtils;

public class CRMPageManager extends AbstractPageManager 
{
	protected WebDriver driver=null;
	protected WebElement username;
	protected WebElement password;
	protected WebElement loginButton;
	protected WebElement customerLink;
	protected WebElement nomeSearch;
	protected WebElement cognomeSearch;
	protected WebElement emailSearch;
	protected WebElement cellSearch;
	protected WebElement cartaSearch;
	protected WebElement codSearch;
	protected WebElement searchButton;
	protected WebElement clienteLink;
	protected WebElement modificaCliente;
	protected WebElement modNome;
	protected WebElement modCognome;
	protected WebElement operatore;
	protected WebElement modificaClienteSalva;
	protected WebElement confirmPopup;
	protected WebElement modAnno;
	
	protected String[] years=new String[] {
			"1999",
			"1987",
			"1954",
			"1985",
			"1990",
			"1997",
			"1984",
		};
	
	protected CRMPageManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static CRMPageManager with(WebDriver driver, Properties languages)
	{
		return new CRMPageManager(driver, languages);
	}
	
	public CRMPageManager isLoaded()
	{
		username=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.CRMPage.USERNAME.getWebLocator())), 60);
		password=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.CRMPage.PASSWORD.getWebLocator())));
		loginButton=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.CRMPage.LOGIN.getWebLocator())));
		
		
		Assert.assertTrue(username != null);
		Assert.assertTrue(password != null);
		Assert.assertTrue(loginButton != null);
		
		return this;
	}
	
	public CRMPageManager login(CredentialBean bean)
	{
		username=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.USERNAME.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		username.clear();
		username.sendKeys(bean.getEmail());
		
		password=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.PASSWORD.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		password.clear();
		password.sendKeys(bean.getPassword());
		
		loginButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.LOGIN.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		loginButton.click();
		
		return this;
	}

	public CRMPageManager gotoCustomerSection() 
	{
		customerLink=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.CLIENTI_BUTTON.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		customerLink.click();
		
		try {Thread.sleep(5000);} catch(Exception err) {}
		
		return this;
	}

	public CRMPageManager searchAndSelectUser(CRMCustomerInfoSearchBean b) 
	{
		if(!StringUtils.isBlank(b.getNome()))
		{
			nomeSearch=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.NOME_FIELD_SEARCH.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			nomeSearch.clear();
			nomeSearch.sendKeys(b.getNome());
		}
		
		if(!StringUtils.isBlank(b.getCognome()))
		{
			cognomeSearch=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.COGNOME_FIELD_SEARCH.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			cognomeSearch.clear();
			cognomeSearch.sendKeys(b.getCognome());
		}
		
		if(!StringUtils.isBlank(b.getEmail()))
		{
			emailSearch=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.EMAIL_FIELD_SEARCH.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			emailSearch.clear();
			emailSearch.sendKeys(b.getEmail());
		}
		
		if(!StringUtils.isBlank(b.getCellulare()))
		{
			cellSearch=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.CELLULARE_FIELD_SEARCH.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			cellSearch.clear();
			cellSearch.sendKeys(b.getCellulare());
		}
		
		if(!StringUtils.isBlank(b.getNumeroCarta()))
		{
			cartaSearch=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.NUMEROCARTA_FIELD_SEARCH.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			cartaSearch.clear();
			cartaSearch.sendKeys(b.getNumeroCarta());
		}
		
		if(!StringUtils.isBlank(b.getCodiceCliente()))
		{
			codSearch=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.CODCLIENTE_FIELD_SEARCH.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			codSearch.clear();
			codSearch.sendKeys(b.getCodiceCliente());
		}
		
		searchButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.SEARCH_FIELD_SEARCH.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		searchButton.click();
		
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.CRMPage.CLIENTE_LINK.getWebLocator())));
		
		clienteLink=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.CLIENTE_LINK.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		clienteLink.click();
		
		return this;
	}

	public CRMPageManager updateSelectedCustomerInfo() 
	{
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.CRMPage.INFO_CLIENTE_TAB.getWebLocator())),20);
		
		modificaCliente=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.MODIFICA_CLIENTE.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		modificaCliente.click();
		
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.CRMPage.MODIFICA_CLIENTE_NOME.getWebLocator())),20);
		
		String nome,cognome;
		nome=Utility.getAlphaNumericString(10);
		cognome=Utility.getAlphaNumericString(10);
		
		DatiPersonaliBean bean=new DatiPersonaliBean();
		
		bean.setNomeCognome(nome+" "+cognome);
		
		String indirizzo=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.INDIRIZZO_CLIENTE.getWebLocator())).getText().trim();
		String data=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.DATA_DI_NASCITA_CLIENTE.getWebLocator())).getText().trim().replaceAll("-", ".");
		//String nucleo=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.NUCLEO_CLIENTE.getWebLocator())).getText().trim();
		
		bean.setIndirizzo(indirizzo);
		bean.setNucleo("2");
		//bean.setDataNascita(data);
		bean.setNazionalita("Argentina");
		
		modNome=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.MODIFICA_CLIENTE_NOME.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		modNome.clear();
		modNome.sendKeys(nome);
		
		modCognome=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.MODIFICA_CLIENTE_COGNOME.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		modCognome.clear();
		modCognome.sendKeys(cognome);
		
		modAnno=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.MODIFICA_CLIENTE_ANNO_NASCITA.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		
		Select annoSelect = new Select(modAnno);
		
		Random random=new Random();
		String anno=this.years[random.nextInt(years.length)];
		
		String[] D=data.split("\\.");
		String currentYear=D[2];
		
		while(currentYear.equals(anno))
		{
			anno=this.years[random.nextInt(years.length)];
		}
		
		annoSelect.selectByVisibleText(anno);
		
		D[2]=anno;
		
		data=String.join(".", D);
		
		bean.setDataNascita(data);
		
		operatore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.MODIFICA_CLIENTE_OPERATORE.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		operatore.clear();
		operatore.sendKeys("PRISMA");
	
		
		DinamicData.getIstance().set("DATI_PERSONALI", bean);
		
		return this;
	}

	public CRMPageManager saveUpdateCustomerInfo() 
	{
		modificaClienteSalva=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.MODIFICA_CLIENTE_SAVE.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		modificaClienteSalva.click();
		
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.CRMPage.CONFIRM_OK_POPUP.getWebLocator())),20);
		
		confirmPopup=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.CONFIRM_OK_POPUP.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		confirmPopup.click();
		
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.CRMPage.OPERATION_OK_MESSAGE.getWebLocator())),20);
		
		confirmPopup=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.CRMPage.CONFIRM_OK_POPUP.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		confirmPopup.click();
		
		return this;
	}
	
}
