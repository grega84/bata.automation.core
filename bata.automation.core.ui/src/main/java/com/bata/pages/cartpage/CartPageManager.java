package com.bata.pages.cartpage;

import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.Locators;
import com.bata.pages.AbstractPageManager;

import bean.datatable.PaymentBean;
import bean.datatable.SearchItemBean;
import test.automation.core.UIUtils;

public class CartPageManager extends AbstractPageManager 
{
	protected WebElement title=null;
	protected WebElement instock=null;
	protected WebElement arrowup=null;
	protected WebElement arrowdown=null;
	protected WebElement quantity=null;
	protected WebElement removeItem=null;
	protected WebElement titleCoupon=null;
	protected WebElement inputCoupon=null;
	protected WebElement applyButton=null;
	protected WebElement procedicheckout=null;
	protected WebElement cartList;
	protected WebElement removeButton;
	protected WebElement homeLink;
	protected WebElement errorMessage;
	protected WebElement increaseQuantity;
	
	protected CartPageManager(WebDriver driver, Properties languages) 
	{
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static CartPageManager with(WebDriver driver, Properties languages)
	{
		return new CartPageManager(driver, languages);
	}

	public CartPageManager isLoaded()
	{
		title=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Cart.TITLE_CART.getWebLocator())),40);
		//instock=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Cart.IN_STOCK.getWebLocator())),40);
		//arrowup=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Cart.ARROW_UP.getWebLocator())),40);
		//arrowdown=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Cart.ARROW_DOWN.getWebLocator())),40);
		//quantity=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Cart.QUANTITY.getWebLocator())),40);
		//cartList=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Cart.CART_TABLE.getWebLocator())),40);
		
		Assert.assertTrue(title!=null);
		//Assert.assertTrue(instock!=null);
		//Assert.assertTrue(arrowup!=null);
		//Assert.assertTrue(arrowdown!=null);
		//Assert.assertTrue(quantity!=null);
		//Assert.assertTrue(cartList != null);
		
		return this;
	}
	
	public CartPageManager upQuantity(int pos) 
	{
		arrowup=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Cart.ARROW_UP.getWebLocator().replace("#", ""+(pos+1))));
		try {Thread.sleep(500);} catch(Exception err) {}
		Assert.assertTrue(arrowup!=null);
		arrowup.click();
		return this;
	}
	
	public CartPageManager downQuantity(int pos) {
		arrowdown=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Cart.ARROW_DOWN.getWebLocator().replace("#", ""+(pos+1))));
		try {Thread.sleep(500);} catch(Exception err) {}
		Assert.assertTrue(arrowdown!=null);
		arrowdown.click();
		return this;
	}
	
	public CartPageManager removeArticle() {
		removeItem=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Cart.REMOVE_ITEM.getWebLocator())),40);
		Assert.assertTrue(removeItem!=null);
		removeItem.click();
		return this;
	}
	
	public CartPageManager addCoupon(String coupon) {
		titleCoupon=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Cart.TITLE_COUPON.getWebLocator())),40);
		inputCoupon=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Cart.INPUT_COUPON.getWebLocator())),40);
		applyButton=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Cart.APPLY_BUTTON.getWebLocator())),40);
		Assert.assertTrue(titleCoupon!=null);
		Assert.assertTrue(inputCoupon!=null);
		Assert.assertTrue(applyButton!=null);
		
		if(!StringUtils.isBlank(coupon))
			inputCoupon.sendKeys(coupon);
		
		applyButton.click();
		return this;
	}
	
	public CartPageManager clickOnCheckout() 
	{
		procedicheckout=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Cart.PROCEDE_TO_CHECKOUT.getWebLocator())),40);
		Assert.assertTrue(procedicheckout!=null);
		procedicheckout=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Cart.PROCEDE_TO_CHECKOUT.getWebLocator()));
		procedicheckout.click();
		try {Thread.sleep(5000);}catch(Exception err) {}
		return this;
	}
	
	public CartPageManager checkCart(List<SearchItemBean> elements)
	{
		cartList=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Cart.CART_TABLE.getWebLocator())),40);
		Assert.assertTrue(cartList != null);
		
		//scorro la lista e controllo che il carrello contenga gli items selezionati
		findElement(elements);
		
		return this;
	}

	protected void findElement(List<SearchItemBean> elements) 
	{
		for(SearchItemBean element : elements)
		{
			boolean find=false;
			List<WebElement> items=driver.findElements(By.xpath(Locators.Cart.CART_ELEMENT.getWebLocator()));
			
			for(int i=0;i<items.size();i++)
			{
				
				String name=items.get(i).findElement(By.xpath(Locators.Cart.CART_ELEMENT_TITLE.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				String size=items.get(i).findElement(By.xpath(Locators.Cart.CART_ELEMENT_SIZE.getWebLocator().replace("#", ""+(i+1)))).getText().toLowerCase();
				String price=items.get(i).findElement(By.xpath(Locators.Cart.CART_ELEMENT_PRICE.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				String articolo=items.get(i).findElement(By.xpath(Locators.Cart.CART_ELEMENT_ID.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				String brand=items.get(i).findElement(By.xpath(Locators.Cart.CART_ELEMENT_BRAND.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				String color=items.get(i).findElement(By.xpath(Locators.Cart.CART_ELEMENT_COLOR.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				String quantity=items.get(i).findElement(By.xpath(Locators.Cart.QUANTITY.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				
				
				if(name.equals(element.getName().toLowerCase()) && 
				   price.equals(element.getPrice().toLowerCase()) && 
				   size.contains(element.getSize().toLowerCase()) //&&
				   //articolo.equals(element.getNumItem().toLowerCase()) &&
				   //brand.equals(element.getBrand().toLowerCase()) &&
				   //color.equals(element.getColor().toLowerCase())
				   )
				{
					find=true;
					element.setQuantity(quantity);
					break;
				}
			}
			
			//Assert.assertTrue("controllo presenza item "+element.getName()+" - "+element.getSize()+" - "+element.getPrice(),find == true);
		}
	}

	public CartPageManager checkCartIsEmpty() 
	{
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Cart.EMPTY_CART.getWebLocator())),15);
		return this;
	}
	
	public CartPageManager clearCart()
	{
		boolean find=true;
		
		//clicco su x fino alla pulizia del carrello
		int numOfItems=driver.findElements(By.xpath(Locators.Cart.REMOVE_ITEM.getWebLocator())).size();
		
		for(int i=0;i<numOfItems;i++)
		{
			try
			{
				this.removeButton=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Cart.REMOVE_ITEM.getWebLocator())));
				this.removeButton.click();
				
				Thread.sleep(1000);
			}
			catch(Exception err)
			{
				
			}
			
		}
		
		this.homeLink=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Homepage.LOGO.getWebLocator()));
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Assert.assertTrue(this.homeLink != null);
		
		this.homeLink.click();
		
		return this;
	}
	
	public CartPageManager checkCouponErrorMessage()
	{
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		errorMessage=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Cart.COUPON_ERROR_MESSAGE.getWebLocator()));
		Assert.assertTrue(errorMessage != null);
		
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String actual=errorMessage.getText().trim();
		String expected=language.getProperty("couponErrorMessage");
		
		Assert.assertTrue("controllo errore coupon errato; attuale:"+actual+", atteso:"+expected,actual.contains(expected));
		
		return this;
	}
	
	public CartPageManager increaseQuantity(String quantity2, SearchItemBean element)
	{
		List<WebElement> items=driver.findElements(By.xpath(Locators.Cart.CART_ELEMENT.getWebLocator()));
		
		int pos=findElement(items,element);
		
		//ricavo la quantità attuale
		String quantity=items.get(pos).findElement(By.xpath(Locators.Cart.QUANTITY.getWebLocator().replace("#", ""+(pos+1)))).getText().trim();
		
		//ricavo di quanti click devo incrementare
		int tot=Math.abs(Integer.parseInt(quantity) - Integer.parseInt(quantity2));
		
		for(int i=0;i<tot;i++)
		{
			this.upQuantity(i);
			try {Thread.sleep(1000);} catch(Exception err) {}
		}
		
		return this;
	}

	protected int findElement(List<WebElement> items, SearchItemBean element) 
	{
		//ricerco l'elemento nel cart
		boolean find=false;
		int pos=-1;
		
		for(int i=0;i<items.size();i++)
		{
			pos=i;
			
			String name=items.get(i).findElement(By.xpath(Locators.Cart.CART_ELEMENT_TITLE.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
			String size=items.get(i).findElement(By.xpath(Locators.Cart.CART_ELEMENT_SIZE.getWebLocator().replace("#", ""+(i+1)))).getText().toLowerCase();
			String price=items.get(i).findElement(By.xpath(Locators.Cart.CART_ELEMENT_PRICE.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
			String articolo=items.get(i).findElement(By.xpath(Locators.Cart.CART_ELEMENT_ID.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
			String brand=items.get(i).findElement(By.xpath(Locators.Cart.CART_ELEMENT_BRAND.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
			String color=items.get(i).findElement(By.xpath(Locators.Cart.CART_ELEMENT_COLOR.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
			String quantity=items.get(i).findElement(By.xpath(Locators.Cart.QUANTITY.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
			
			
			if(name.equals(element.getName().toLowerCase()) && 
			   price.equals(element.getPrice().toLowerCase()) && 
			   size.contains(element.getSize().toLowerCase())
			   )
			{
				find=true;
				//element.setQuantity(quantity);
				break;
			}
		}
		
		Assert.assertTrue("controllo presenza item "+element.getName()+" - "+element.getSize()+" - "+element.getPrice(),find == true);
		
		return pos;
	}
	
	public CartPageManager gotoHome()
	{
		UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Homepage.LOGO.getWebLocator())).click();
		return this;
	}

	public void clickSuCheckOutRegistered() {
		// TODO Auto-generated method stub
		
	}
}
