package com.bata.pages.cartpage;

import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.bata.pagepattern.Locators;

import bean.datatable.SearchItemBean;

public class CartPageManagerIOS extends CartPageManager {

	protected CartPageManagerIOS(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static CartPageManagerIOS with(WebDriver driver, Properties languages)
	{
		return new CartPageManagerIOS(driver, languages);
	}
	
	protected void findElement(List<SearchItemBean> elements) 
	{
		for(SearchItemBean element : elements)
		{
			boolean find=false;
			List<WebElement> items=driver.findElements(By.xpath(Locators.Cart.CART_ELEMENT.getWebLocator()));
			
			for(int i=0;i<items.size();i++)
			{
				
				String name=driver.findElement(By.xpath(Locators.Cart.CART_ELEMENT_TITLE.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				String size=driver.findElement(By.xpath(Locators.Cart.CART_ELEMENT_SIZE.getWebLocator().replace("#", ""+(i+1)))).getText().toLowerCase();
				String price=driver.findElement(By.xpath(Locators.Cart.CART_ELEMENT_PRICE.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				String articolo=driver.findElement(By.xpath(Locators.Cart.CART_ELEMENT_ID.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				String brand=driver.findElement(By.xpath(Locators.Cart.CART_ELEMENT_BRAND.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				String color=driver.findElement(By.xpath(Locators.Cart.CART_ELEMENT_COLOR.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				String quantity=driver.findElement(By.xpath(Locators.Cart.QUANTITY.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				
				
				if(name.equals(element.getName().toLowerCase()) && 
				   price.equals(element.getPrice().toLowerCase()) && 
				   size.contains(element.getSize().toLowerCase()) &&
				   articolo.equals(element.getNumItem().toLowerCase()) &&
				   brand.equals(element.getBrand().toLowerCase()) &&
				   color.equals(element.getColor().toLowerCase()))
				{
					find=true;
					element.setQuantity(quantity);
					break;
				}
			}
			
			Assert.assertTrue("controllo presenza item "+element.getName()+" - "+element.getSize()+" - "+element.getPrice(),find == true);
		}
	}

}
