package com.bata.pages.cartpage;

import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.pagepattern.Locators;
import com.bata.pages.benvenutocheckout.BenvenutoCheckoutPage;
import com.bata.pages.homepage.HomePage;

import bean.datatable.PaymentBean;
import bean.datatable.ProductBean;
import bean.datatable.SearchItemBean;
import bean.datatable.ShippingBean;
import common.WaitManager;
import test.automation.core.UIUtils;

public class CartPage extends LoadableComponent<CartPage> {
	
	private CartPageManager gesture;
	private int DRIVER;
	
	protected CartPage(WebDriver driver,Properties language) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		case Locators.IOS_SAFARI:
			gesture=CartPageManagerIOS.with(driver, language);
			break;
		default:
			gesture=CartPageManager.with(driver, language);
		}
	}
	
	public static CartPage from(WebDriver driver,Properties language)
	{
		return new CartPage(driver, language).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}
	
	public CartPage upQuantity(int pos) 
	{
		gesture.upQuantity(pos);
		return this;
	}
	
	public CartPage downQuantity(int pos) 
	{
		gesture.downQuantity(pos);
		return this;
	}
	
	public CartPage removeArticle() 
	{
		gesture.removeArticle();
		return this;
	}
	
	public CartPage addCoupon(PaymentBean b) 
	{
		gesture.addCoupon(b.getCoupon());
		return this;
	}
	
	public CartPage procediAlCheckoutRegisterd() 
	{
		gesture.clickOnCheckout();
		return this;
	}
	
	public BenvenutoCheckoutPage procediAlCheckoutGuest() 
	{
		gesture.clickOnCheckout();
		return BenvenutoCheckoutPage.from(gesture.getDriver(),gesture.getLanguages());
	}

	public CartPage checkCart(List<SearchItemBean> elements) 
	{
		gesture.checkCart(elements);
		return this;
	}

	public CartPage addCoupon(ShippingBean b) 
	{
		gesture.addCoupon(b.getCoupon());
		return this;
	}

	public boolean isEmpty() 
	{
		try
		{
			gesture.checkCartIsEmpty();
			return true;
		}
		catch(Exception err)
		{
			return false;
		}
	}

	public HomePage clearCart() 
	{
		gesture.clearCart();
		
		WaitManager.get().waitLongTime();
		
		return HomePage.from(gesture.getDriver(), gesture.getLanguages());
	}

//	public CartPage setLanguage(Properties language) 
//	{
//		this.language=language;
//	}

	public CartPage checkCouponErrorMessage(Properties language) 
	{
		gesture.checkCouponErrorMessage();
		return this;
	}

	public CartPage increaseItemQuantity(String quantity2, SearchItemBean element) 
	{
		gesture.increaseQuantity(quantity2, element);
		return this;
	}

	public HomePage goToHome(Properties language) 
	{
		gesture.gotoHome();
		return HomePage.from(gesture.getDriver(), gesture.getLanguages());
	}

	public CartPage clickSuCheckOutRegistered() 
	{
		gesture.clickOnCheckout();
		return this;
	}

	
	
}
