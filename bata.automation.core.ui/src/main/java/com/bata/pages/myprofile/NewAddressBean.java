package com.bata.pages.myprofile;

public class NewAddressBean 
{
	private String name;
	private String surname;
	private String address;
	private CityProvinceCap city;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public CityProvinceCap getCity() {
		return city;
	}
	public void setCity(CityProvinceCap city) {
		this.city = city;
	}
}
