package com.bata.pages.myprofile;

public class CityProvinceCap 
{
	private String cap;
	private String city;
	private String province;
	
	public CityProvinceCap(String cap, String city, String province) {
		super();
		this.cap = cap;
		this.city = city;
		this.province = province;
	}
	public String getCap() {
		return cap;
	}
	public void setCap(String cap) {
		this.cap = cap;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}

}
