package com.bata.pages.myprofile;

import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.login.LoginPage;

import bean.datatable.CredentialBean;
import bean.datatable.DatiDiContattoBean;
import bean.datatable.DatiPersonaliBean;
import test.automation.core.UIUtils;

public class MyProfilePage extends LoadableComponent<MyProfilePage> 
{
	
	private int DRIVER;
	private MyProfilePageManager gesture;
	
	protected MyProfilePage(WebDriver driver, Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		default:
			gesture=MyProfilePageManager.with(driver, languages);
		}
	}
	
	public static MyProfilePage from(WebDriver driver, Properties languages)
	{
		return new MyProfilePage(driver, languages).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}

	public MyProfilePage modificaDatiContatto(DatiDiContattoBean datiDiContattoBean) 
	{
		gesture.checkAndRetrieveDatiContatto()
		.clickModificaDatiContatto()
		.insertDatiContatto(datiDiContattoBean)
		.submitDatiContatto();
		return this;
	}

	public MyProfilePage checkModificaDatiContatto() 
	{
		gesture.checkDatiContattoAfterModify();
		return this;
	}

	public MyProfilePage changePassword(CredentialBean credentialBean) 
	{
		gesture.clickModificaPassword()
		.modificaPassword(credentialBean)		
		.submitModificaPassword();
		return this;
	}

	public LoginPage logout() 
	{
		gesture.logout();
		
		return LoginPage.from(gesture.getDriver(), gesture.getLanguages());
	}

	public MyProfilePage changePersonalData(Properties language) 
	{
		gesture.retrieveDatiPersonali()
		.modifyDatiPersonali()
		.submitModificaDatiPersonali();
		return this;
	}
	
	public MyProfilePage checkModificaDatiPersonali() 
	{
		gesture.checkDatiPersonaliAfterModify();
		return this;
	}

	public MyProfilePage addNuovoIndirizzo() 
	{
		gesture.clickAddNewAddress()
		.addNewAddress()
		.submitNewAddress();
		return this;
	}

	public MyProfilePage checkNewAddressAdded() 
	{
		gesture.checkNewAddressAfterInserting();
		return this;
	}

	public MyProfilePage completeMandatoryFields(Properties language) 
	{
		gesture.completeMandatoryFields();
		return this;
	}
}
