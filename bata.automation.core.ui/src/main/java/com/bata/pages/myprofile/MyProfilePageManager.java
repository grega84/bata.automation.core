package com.bata.pages.myprofile;

import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.AbstractPageManager;

import bean.datatable.CredentialBean;
import bean.datatable.DatiDiContattoBean;
import bean.datatable.DatiPersonaliBean;
import common.CommonFields;
import test.automation.core.UIUtils;

public class MyProfilePageManager extends AbstractPageManager 
{
	protected Object datiContattoHeader;
	protected WebElement datiContattoEMail;
	protected WebElement datiContattoPhone;
	protected WebElement modificaDatiContatto;
	protected WebElement modificaPassword;
	protected WebElement datiPersonaliHeader;
	protected WebElement nomeText;
	protected WebElement indirizzoText;
	protected WebElement dataNascitaText;
	protected WebElement nazionalitaField;
	protected WebElement nucleoText;
	protected WebElement modificaITuoiDati;
	protected WebElement minori14Field;
	protected WebElement altriIndirizziHeader;
	protected WebElement aggiungiIndirizzo;
	protected WebElement emailTxtField;
	protected WebElement phoneTxtField;
	protected WebElement confirmPassword;
	protected WebElement datiContattoSubmitButton;
	protected WebElement modpswdPasswordAttuale;
	protected WebElement modpswdPasswordNuova;
	protected WebElement modpswdPasswordConferma;
	protected WebElement modpswdSubmit;
	protected WebElement profileMenu;
	protected WebElement logout;
	
	protected String[] days=new String[] {
			"01",
			"02",
			"04",
			"05",
			"10",
			"11",
			"12",
			"15",
			"18",
			"03",
	};
	
	protected String[] years=new String[] {
		"1999",
		"1987",
		"1954",
		"1985",
		"1990",
		"1997",
		"1984",
	};
	
	protected String[] nazionality=new String[] {
			"Italia",
			"Algeria",
			"Argentina",
			"Australia",
			"Anguilla",
	};
	
	protected HashMap<Integer,CityProvinceCap> citta=new HashMap<Integer, CityProvinceCap>() {{
	    put(0, new CityProvinceCap("74028", "Taranto", "Taranto"));
	    put(1, new CityProvinceCap("80100", "Napoli", "Napoli"));
	    put(2, new CityProvinceCap("73100", "Lecce", "Lecce"));
	    put(3, new CityProvinceCap("84059", "Camerota", "Salerno"));
	    put(4, new CityProvinceCap("80078", "Pozzuoli", "Napoli"));
	    put(5, new CityProvinceCap("80077", "Ischia", "Napoli"));
	}};
	
	protected WebElement nameField;
	protected WebElement surnameField;
	protected WebElement addressField;
	protected WebElement capField;
	protected WebElement cityField;
	protected WebElement province;
	protected WebElement dayField;
	protected WebElement monthField;
	protected WebElement yearField;
	protected WebElement consensi;
	protected WebElement nazionalityField;
	protected WebElement nucleoField;
	protected WebElement submitModificaPersonali;
	protected WebElement mrRadio;
	protected WebElement submitAddAddress;
	protected WebElement numeroBambini;
	protected WebElement categoria;
	protected WebElement acquistatoOnline;
	
	protected MyProfilePageManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static MyProfilePageManager with(WebDriver driver, Properties languages)
	{
		return new MyProfilePageManager(driver, languages);
	}
	
	public MyProfilePageManager isLoaded()
	{
		datiContattoHeader=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.DATI_CONTATTO_HEADER.getWebLocator(), new Entry("datiContattoHeader",language.getProperty("datiContattoHeader"))))), 60);
		return this;
		/*
		datiContattoEMail=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MyProfilePage.DATI_CONTATTO_EMAIL.getWebLocator())));
		datiContattoPhone=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MyProfilePage.DATI_CONTATTO_PHONE.getWebLocator())));
		modificaDatiContatto=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MyProfilePage.MODIFICA_DATI_CONTATTO.getWebLocator())));
		modificaPassword=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.MODIFICA_PASSWORD.getWebLocator(), new Entry("modificaPassword",language.getProperty("modificaPassword"))))), 60);
		datiPersonaliHeader=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.DATI_PERSONALI_HEADER.getWebLocator(), new Entry("datiPersonaliHeader",language.getProperty("datiPersonaliHeader"))))), 60);
		nomeText=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.NOME_TEXT.getWebLocator(), new Entry("nameField",language.getProperty("nameField"))))), 60);
		indirizzoText=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.INDIRIZZO_TEXT.getWebLocator(), new Entry("addressField",language.getProperty("addressField"))))), 60);
		dataNascitaText=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.DATA_NASCITA_TEXT.getWebLocator(), new Entry("dataNascitaField",language.getProperty("dataNascitaField"))))), 60);
		nazionalitaField=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.NAZIONALITA_TEXT.getWebLocator(), new Entry("nazionalitaField",language.getProperty("nazionalitaField"))))), 60);
		nucleoText=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.NUCLEO_FAMILIARE_TEXT.getWebLocator(), new Entry("nucleoFamiliareField",language.getProperty("nucleoFamiliareField"))))), 60);
		minori14Field=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.MINORI_14_TEXT.getWebLocator(), new Entry("minori14Field",language.getProperty("minori14Field"))))), 60);
		modificaITuoiDati=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MyProfilePage.MODIFICA_I_TUOI_DATI.getWebLocator())));
		altriIndirizziHeader=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.ALTRI_INDIRIZZI_HEADER.getWebLocator(), new Entry("altriIndirizziHeader",language.getProperty("altriIndirizziHeader"))))), 60);
		aggiungiIndirizzo=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MyProfilePage.AGGIUNGI_INDIRIZZO.getWebLocator())));
		
		Assert.assertTrue(datiContattoHeader != null);
		Assert.assertTrue(datiContattoEMail != null);
		Assert.assertTrue(datiContattoPhone != null);
		Assert.assertTrue(modificaDatiContatto != null);
		Assert.assertTrue(modificaPassword != null);
		Assert.assertTrue(datiPersonaliHeader != null);
		Assert.assertTrue(nomeText != null);
		Assert.assertTrue(indirizzoText != null);
		Assert.assertTrue(dataNascitaText != null);
		Assert.assertTrue(nazionalitaField != null);
		Assert.assertTrue(nucleoText != null);
		Assert.assertTrue(minori14Field != null);
		Assert.assertTrue(modificaITuoiDati != null);
		Assert.assertTrue(altriIndirizziHeader != null);
		Assert.assertTrue(aggiungiIndirizzo != null);
		*/
	}
	
	public MyProfilePageManager checkAndRetrieveDatiContatto()
	{
		//ricavo i dati attuali dalla interfaccia
		datiContattoEMail=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.DATI_CONTATTO_EMAIL.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		String email=datiContattoEMail.getText().trim();
		
		Assert.assertTrue("controllo email valorizzata; attuale:"+email,!email.isEmpty());
		
		datiContattoPhone=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.DATI_CONTATTO_PHONE.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		String phone=datiContattoPhone.getText().trim();
		
		Assert.assertTrue("controllo numero di telefono valorizzato; attuale:"+phone,!phone.isEmpty());
		
		//memorizzo i dati attualmente visualizzati
		DatiDiContattoBean bean=new DatiDiContattoBean();
		
		bean.setEmail(email);
		bean.setPhone(phone);
		
		DinamicData.getIstance().set(CommonFields.DATI_DI_CONTATTO, bean);
		
		return this;
	}
	
	public MyProfilePageManager clickModificaDatiContatto()
	{
		//clicco modifica
		modificaDatiContatto=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.MODIFICA_DATI_CONTATTO.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		modificaDatiContatto.click();
		
		return this;
				
	}
	
	public MyProfilePageManager insertDatiContatto(DatiDiContattoBean datiDiContattoBean)
	{
		//inserisco i dati e sottometto la form
		String[] mail=((String) DinamicData.getIstance().get(CommonFields.DATI_DI_CONTATTO)).split("@");
		emailTxtField=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.MODIFICA_DATI_CONTATTO_EMAIL_TXTFIELD.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		emailTxtField.clear();
		emailTxtField.sendKeys(mail[0]+Utility.getAlphaNumericString(5)+"@"+mail[1]);
		
		phoneTxtField=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.MODIFICA_DATI_CONTATTO_PHONE_TXTFIELD.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		phoneTxtField.clear();
		phoneTxtField.sendKeys("393"+""+Utility.generateRandomDigits(9));
		
		confirmPassword=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.MODIFICA_DATI_CONTATTO_PASSWORD_TXTFIELD.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		confirmPassword.sendKeys(datiDiContattoBean.getSubmitPassword());
		
		return this;
	}
	
	public MyProfilePageManager submitDatiContatto()
	{
		datiContattoSubmitButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.MODIFICA_DATI_CONTATTO_SUBMIT_BUTTON.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		datiContattoSubmitButton.click();
		
		try {Thread.sleep(3000);} catch(Exception err) {}
		
		return this;
	}
	
	public MyProfilePageManager checkDatiContattoAfterModify()
	{
		//ricavo i dati attuali dalla interfaccia
		datiContattoEMail=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.DATI_CONTATTO_EMAIL.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		String email=datiContattoEMail.getText().trim();
		
		Assert.assertTrue("controllo email valorizzata; attuale:"+email,!email.isEmpty());
		
		datiContattoPhone=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.DATI_CONTATTO_PHONE.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		String phone=datiContattoPhone.getText().trim();
		
		Assert.assertTrue("controllo numero di telefono valorizzato; attuale:"+phone,!phone.isEmpty());
		
		//memorizzo i dati attualmente visualizzati
		DatiDiContattoBean bean=(DatiDiContattoBean) DinamicData.getIstance().get(CommonFields.DATI_DI_CONTATTO);
		Assert.assertTrue(bean != null);
		
		Assert.assertTrue("controllo email modificata correttamete; attuale:"+email+", atteso:"+bean.getEmail(), !email.equals(bean.getEmail()));
		Assert.assertTrue("controllo phone modificata correttamete; attuale:"+phone+", atteso:"+bean.getPhone(), !phone.equals(bean.getPhone()));
		
		return this;
	}
	
	public MyProfilePageManager clickModificaPassword()
	{
		try {Thread.sleep(3000);} catch(Exception err) {}
		modificaPassword=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.MODIFICA_PASSWORD.getWebLocator(), new Entry("modificaPassword",language.getProperty("modificaPassword")))));
		try {Thread.sleep(1000);} catch(Exception err) {}
		modificaPassword.click();
		return this;
	}
	
	public MyProfilePageManager modificaPassword(CredentialBean credentialBean)
	{
		//inserisco la nuova password
		modpswdPasswordAttuale=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.MODIFICA_PSWD_PASSWORD_ATTUALE.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		modpswdPasswordAttuale.sendKeys(credentialBean.getPassword());
		
		modpswdPasswordNuova=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.MODIFICA_PSWD_PASSWORD_NUOVA.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		modpswdPasswordNuova.sendKeys(credentialBean.getNewPassword());
		
		modpswdPasswordConferma=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.MODIFICA_PSWD_PASSWORD_CONFERMA.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		modpswdPasswordConferma.sendKeys(credentialBean.getNewPassword());
		
		return this;
	}
	
	public MyProfilePageManager submitModificaPassword()
	{
		modpswdSubmit=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.MODIFICA_PSWD_SUBMIT_BUTTON.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		modpswdSubmit.click();

		try {Thread.sleep(3000);} catch(Exception err) {}
		
		return this;
	}
	
	public MyProfilePageManager logout()
	{
		profileMenu=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.PROFILE_LINK.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		profileMenu.click();
		try {Thread.sleep(1000);} catch(Exception err) {}
		
		logout=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.LOGOUT_BUTTON.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		logout.click();
		
		return this;
	}
	
	public MyProfilePageManager retrieveDatiPersonali()
	{
		modificaITuoiDati=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.MODIFICA_I_TUOI_DATI.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		
		nomeText=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.NOME_TEXT.getWebLocator(), new Entry("nameField",language.getProperty("nameField"))))), 60);
		indirizzoText=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.INDIRIZZO_TEXT.getWebLocator(), new Entry("addressField",language.getProperty("addressField"))))), 60);
		dataNascitaText=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.DATA_NASCITA_TEXT.getWebLocator(), new Entry("dataNascitaField",language.getProperty("dataNascitaField"))))), 60);
		
		
		//ricavo i dati correnti
		DatiPersonaliBean bean=new DatiPersonaliBean();
		
		String nameSurname=nomeText.getText().trim();
		String indirizzo=indirizzoText.getText().trim();
		String data=dataNascitaText.getText().trim();
		
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("ita") || !DinamicData.getIstance().get(CommonFields.COUNTRY).equals("sk"))
		{
			nazionalitaField=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.NAZIONALITA_TEXT.getWebLocator(), new Entry("nazionalitaField",language.getProperty("nazionalitaField"))))), 60);
			nucleoText=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.NUCLEO_FAMILIARE_TEXT.getWebLocator(), new Entry("nucleoFamiliareField",language.getProperty("nucleoFamiliareField"))))), 60);
			minori14Field=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.MINORI_14_TEXT.getWebLocator(), new Entry("minori14Field",language.getProperty("minori14Field"))))), 60);
			
			String nucleo=nucleoText.getText().trim();
			String minori=minori14Field.getText().trim();
			String nazionalita=nazionalitaField.getText().trim();
			
			bean.setNazionalita(nazionalita);
			bean.setNucleo(nucleo);
			bean.setDataNascita(data);
		}
		
		bean.setNomeCognome(nameSurname);
		bean.setIndirizzo(indirizzo);
		
		
		DinamicData.getIstance().set(CommonFields.MODIFICA_DATI_PERSONALI, bean);
		
		modificaITuoiDati.click();
		
		try {Thread.sleep(3000);} catch(Exception err) {}
		
		return this;
	}
	
	public MyProfilePageManager modifyDatiPersonali()
	{
		//ricavo i dati correnti
		DatiPersonaliBean bean=(DatiPersonaliBean) DinamicData.getIstance().get(CommonFields.MODIFICA_DATI_PERSONALI);
		
		String nameSurname=bean.getNomeCognome();
		String indirizzo=bean.getIndirizzo();
		String data=bean.getDataNascita();
		String nucleo=bean.getNucleo();
		//String minori=minori14Field.getText().trim();
		String nazionalita=bean.getNazionalita();
		
		
		String name=Utility.getAlphaNumericString(10);
		String surname=Utility.getAlphaNumericString(10);
		String ind=Utility.getAlphaNumericString(10)+" n."+Utility.generateRandomDigits(2);
		
		nameField=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.NAME_FIELD.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		nameField.clear();
		nameField.sendKeys(name);
		
		surnameField=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.SURNAME_FIELD.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		surnameField.clear();
		surnameField.sendKeys(surname);
		
		addressField=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.ADDRESS_FIELD.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		addressField.clear();
		addressField.sendKeys(ind);
		
		//bean.setNomeCognome(name+" "+surname);
		//bean.setIndirizzo(ind);
		
		//ricavo un cap che non sia uguale a quello corrente
		Random random=new Random();
		String cap=indirizzo.split("\n")[1].split(",")[0].trim();
		
		String actCap=cap;
		CityProvinceCap city = null;
		
		while(actCap.equals(cap))
		{
			int i=random.nextInt(this.citta.size());
			city=this.citta.get(i);
			
			if(city != null)
				actCap=city.getCap();
		}
		
		Assert.assertTrue(city != null);
		
		ind+="\n"+city.getCap()+", "+city.getCity();
		//bean.setIndirizzo(ind);
		
		capField=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.CAP_FIELD.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		capField.clear();
		capField.sendKeys(city.getCap());
		
		cityField=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.CITY_FIELD.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		cityField.clear();
		cityField.sendKeys(city.getCity());
		
		province=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.PROVINCE_FIELD.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		
		Select provinceSelect = new Select(province);
		provinceSelect.selectByVisibleText(city.getProvince());
		
		String[] mesi=language.getProperty("mesi").split(";");
		
		//creo una nuova data di nascita random
		String giorno,mese,anno;
		
		giorno=this.days[random.nextInt(days.length)];
		mese=mesi[random.nextInt(mesi.length)];
		anno=this.years[random.nextInt(years.length)];
		
		dayField=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.DAY_OF_BORN.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		
		Select dayFieldSelect = new Select(dayField);
		dayFieldSelect.selectByVisibleText(giorno);
		
		monthField=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.MONTH_OF_BORN.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		
		Select monthFieldSelect = new Select(monthField);
		monthFieldSelect.selectByVisibleText(mese.split("=")[0]);
		
		yearField=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.YEAR_OF_BORN.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		
		Select yearFieldSelect = new Select(yearField);
		yearFieldSelect.selectByVisibleText(anno);
		
		//bean.setDataNascita(giorno+"."+mese.split("=")[1]+"."+anno);
		
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("ita") || !DinamicData.getIstance().get(CommonFields.COUNTRY).equals("sk"))
		{
			//clicco sui consensi
			consensi=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.CONSENSI.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			consensi.click();
			
			//seleziono una nazionalità non uguale a quella settata
			String naz=nazionalita;
			
			while(naz.equals(nazionalita))
			{
				naz=this.nazionality[random.nextInt(nazionality.length)];
			}
			
			nazionalityField=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.NAZIONALITY_FIELD.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			
			Select nazionalityFieldSelect = new Select(nazionalityField);
			nazionalityFieldSelect.selectByVisibleText(naz);
			
			//bean.setNazionalita(naz);
			
			//seleziono membri della famiglia
			if(nucleo.contains("1"))
			{
				nucleoField=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.NUMBERS_FAMILY_2.getWebLocator()));
				try {Thread.sleep(1000);} catch(Exception err) {}
				nucleoField.click();
				
				//bean.setNucleo(DatiPersonaliBean.MEMBRI_2);
			}
			else
			{
				nucleoField=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.NUMBERS_FAILY_1.getWebLocator()));
				try {Thread.sleep(1000);} catch(Exception err) {}
				nucleoField.click();
				
				//bean.setNucleo(DatiPersonaliBean.MEMBRI_1);
			}
		}
		
		return this;
	}
	
	public MyProfilePageManager submitModificaDatiPersonali()
	{
		//clicco su submit
		submitModificaPersonali=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.MODIFICA_DATI_PERSONALI_SUBMIT.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		submitModificaPersonali.click();
		
		try {Thread.sleep(3000);} catch(Exception err) {}
		
		DatiPersonaliBean bean=(DatiPersonaliBean) DinamicData.getIstance().get(CommonFields.MODIFICA_DATI_PERSONALI);
		
		DinamicData.getIstance().set(CommonFields.DATI_PERSONALI, bean);
		
		return this;
				
	}
	
	public MyProfilePageManager checkDatiPersonaliAfterModify()
	{
		DatiPersonaliBean bean=(DatiPersonaliBean) DinamicData.getIstance().get(CommonFields.DATI_PERSONALI);
		Assert.assertTrue(bean != null);
		
		String nameSurname="";
		String indirizzo="";
		String data="";
		String nucleo="";
		String minori="";
		String nazionalita="";
		
		//ricavo i dati
		nomeText=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.NOME_TEXT.getWebLocator(), new Entry("nameField",language.getProperty("nameField")))));
		try {Thread.sleep(1000);} catch(Exception err) {}
		nameSurname=nomeText.getText().trim();
		
		indirizzoText=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.INDIRIZZO_TEXT.getWebLocator(), new Entry("addressField",language.getProperty("addressField")))));
		try {Thread.sleep(1000);} catch(Exception err) {}
		indirizzo=indirizzoText.getText().trim();
		
		dataNascitaText=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.DATA_NASCITA_TEXT.getWebLocator(), new Entry("dataNascitaField",language.getProperty("dataNascitaField")))));
		try {Thread.sleep(1000);} catch(Exception err) {}
		data=dataNascitaText.getText().trim();
		
		//controllo valori
		Assert.assertTrue("controllo nome congnome dopo modifica; precedente:"+bean.getNomeCognome()+", attuale:"+nameSurname,!bean.getNomeCognome().equals(nameSurname));
		Assert.assertTrue("controllo indrizzo dopo modifica; precedente:"+bean.getIndirizzo()+", attuale:"+indirizzo,!bean.getIndirizzo().equals(indirizzo));
		Assert.assertTrue("controllo data nascita dopo modifica; precedente:"+bean.getDataNascita()+", attuale:"+data,!bean.getDataNascita().equals(data));
		
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("ita") || !DinamicData.getIstance().get(CommonFields.COUNTRY).equals("sk"))
		{
			nazionalitaField=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.NAZIONALITA_TEXT.getWebLocator(), new Entry("nazionalitaField",language.getProperty("nazionalitaField")))));
			try {Thread.sleep(1000);} catch(Exception err) {}
			nazionalita=nazionalitaField.getText().trim();
			
			nucleoText=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.NUCLEO_FAMILIARE_TEXT.getWebLocator(), new Entry("nucleoFamiliareField",language.getProperty("nucleoFamiliareField")))));
			try {Thread.sleep(1000);} catch(Exception err) {}
			nucleo=nucleoText.getText().trim();
			
			minori14Field=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.MyProfilePage.MINORI_14_TEXT.getWebLocator(), new Entry("minori14Field",language.getProperty("minori14Field")))));
			try {Thread.sleep(1000);} catch(Exception err) {}
			minori=minori14Field.getText().trim();
			
			Assert.assertTrue("controllo nazionalita dopo modifica; precedente:"+bean.getNazionalita()+", attuale:"+nazionalita,!bean.getNazionalita().equals(nazionalita));
			Assert.assertTrue("controllo nucleo familiare dopo modifica; precedente:"+bean.getNucleo()+", attuale:"+nucleo,!bean.getNucleo().equals(nucleo));
			Assert.assertTrue("controllo minori 14 anni dopo modifica",!minori.isEmpty());
			
		}
		
		return this;
	}
	
	public MyProfilePageManager clickAddNewAddress()
	{
		aggiungiIndirizzo=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.AGGIUNGI_INDIRIZZO.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		aggiungiIndirizzo.click();
		
		try {Thread.sleep(5000);} catch(Exception err) {}
		
		return this;
	}
	
	public MyProfilePageManager addNewAddress()
	{
		mrRadio=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.MR_RADIO.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		mrRadio.click();
		
		String name,surname,address;
		
		name=Utility.getAlphaNumericString(5);
		surname=Utility.getAlphaNumericString(5);
		address=Utility.getAlphaNumericString(10)+"n."+Utility.generateRandomDigits(2);
		
		Random random=new Random();
		
		CityProvinceCap city=this.citta.get(random.nextInt(this.citta.size()));
		
		NewAddressBean bean=new NewAddressBean();
		
		bean.setName(name);
		bean.setSurname(surname);
		bean.setAddress(address);
		bean.setCity(city);
		
		DinamicData.getIstance().set(CommonFields.NEW_ADDRESS_FIELDS, bean);
		
		nameField=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.NAME_NEW_ADDRESS_FIELD.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		nameField.clear();
		nameField.sendKeys(name);
		
		surnameField=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.SURNAME_NEW_ADDRESS_FIELD.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		surnameField.clear();
		surnameField.sendKeys(surname);
		
		addressField=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.ADDRESS_NEW_ADDRESS_FIELD.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		addressField.clear();
		addressField.sendKeys(address);
		
		capField=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.CAP_NEW_ADDRESS_FIELD.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		capField.clear();
		capField.sendKeys(city.getCap());
		
		cityField=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.CITY_NEW_ADDRESS_FIELD.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		cityField.clear();
		cityField.sendKeys(city.getCity());
		
		province=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.PROVINCE_NEW_ADDRESS_FIELD.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		
		Select provinceSelect = new Select(province);
		provinceSelect.selectByVisibleText(city.getProvince());
		
		return this;
	}
	
	public MyProfilePageManager submitNewAddress()
	{
		NewAddressBean bean=(com.bata.pages.myprofile.NewAddressBean) DinamicData.getIstance().get(CommonFields.NEW_ADDRESS_FIELDS);
		String name=bean.getName();
		String surname=bean.getSurname();
		String address=bean.getAddress();
		CityProvinceCap city=bean.getCity();
		
		submitAddAddress=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.SUBMIT_NEW_ADDRESS_BUTTON.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		submitAddAddress.click();
		
		String indirizzo=name+" "+surname+", "+address+", "+city.getCap()+", "+city.getCity();
		
		DinamicData.getIstance().set(CommonFields.NEW_ADDRESS, indirizzo);
		
		try {Thread.sleep(5000);} catch(Exception err) {}
		
		return this;
		
	}
	
	public MyProfilePageManager checkNewAddressAfterInserting()
	{
		String indirizzo=(String) DinamicData.getIstance().get(CommonFields.NEW_ADDRESS);
		Assert.assertTrue(indirizzo != null);
		
		aggiungiIndirizzo=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.AGGIUNGI_INDIRIZZO.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		
		List<WebElement> indirizzi=driver.findElements(By.xpath(Locators.MyProfilePage.ALTRI_INDIRIZZI_ELEMENT.getWebLocator()));
		
		boolean find=false;
		
		for(int i=0;i<indirizzi.size();i++)
		{
			UIUtils.ui().scrollToElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.ALTRI_INDIRIZZI_ELEMENT_NAME.getWebLocator().replace("#", ""+(i+1))));
			try {Thread.sleep(1000);} catch(Exception err) {}
			String value=driver.findElement(By.xpath(Locators.MyProfilePage.ALTRI_INDIRIZZI_ELEMENT_NAME.getWebLocator().replace("#", ""+(i+1)))).getText().trim();
			
			if(value.equals(indirizzo))
			{
				find=true;
				WebElement elimina=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.ALTRI_INDIRIZZI_ELEMENT_ELIMINA.getWebLocator().replace("#", ""+(i+1))));
				try {Thread.sleep(3000);} catch(Exception err) {}
				elimina.click();
				
				WebElement confirm=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.ALTRI_INDIRIZZI_CONFIRM_DELETE.getWebLocator()));
				try {Thread.sleep(1000);} catch(Exception err) {}
				confirm.click();
				
				break;
			}
		}
		
		Assert.assertTrue("controllo indirizzo aggiunto:"+indirizzo,find);
		
		return this;
	}
	
	public MyProfilePageManager completeMandatoryFields()
	{
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("ita") || !DinamicData.getIstance().get(CommonFields.COUNTRY).equals("sk"))
		{
			//clicco sui consensi
			consensi=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.CONSENSI.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			consensi.click();
			
			nazionalityField=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.NAZIONALITY_FIELD.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			
			Select nazionalityFieldSelect = new Select(nazionalityField);
			nazionalityFieldSelect.selectByVisibleText("Italia");
			
			nucleoField=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.NUMBERS_FAMILY_2.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			nucleoField.click();
			
			numeroBambini=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.NUMERO_BAMBINI.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			numeroBambini.click();
			
			categoria=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.CATEGORIA_BAMBINI.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			categoria.click();
			
			acquistatoOnline=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.ACQUISTI_ONLINE_SI.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			acquistatoOnline.click();
			
			//attendo comparsa popup
			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MyProfilePage.HEADER_POPUP.getWebLocator())), 60);
			
			//controllo validità popup
			WebElement header=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.HEADER_POPUP.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			String head=header.getText().trim();
			
			Assert.assertTrue("controllo popup header 15 punti loyalty; attuale:"+head+", atteso:"+language.getProperty("headerPopup"),head.equals(language.getProperty("headerPopup")));
			
			WebElement message=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.CONGRATULAZIONI_MESSAGE.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			String mess=message.getText().trim();
			
			Assert.assertTrue("controllo messaggio header 15 punti loyalty; attuale:"+mess+", atteso:"+language.getProperty("congratulazioniMessage"),mess.equals(language.getProperty("congratulazioniMessage")));
			
			
			WebElement closePopup=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyProfilePage.CLOSE_CONGRAT_POPUP.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			closePopup.click();
		}
		
		return this;
	}
}
