package com.bata.pages.findinstore;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Locators;

import bean.datatable.FindAStoreProductBean;
import bean.datatable.ShippingBean;
import bean.datatable.StoreSearchBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class FindInStoreProductPage extends LoadableComponent<FindInStoreProductPage> 
{
	private FindInStoreProductPageManager gesture;
	private int DRIVER;
	
	
	protected FindInStoreProductPage(WebDriver driver,Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		default:
			gesture=FindInStoreProductPageManager.with(driver, languages);
		}
	}
	
	public static FindInStoreProductPage from(WebDriver driver,Properties language)
	{
		return new FindInStoreProductPage(driver, language).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}

	public FindInStoreProductPage search(FindAStoreProductBean bean) 
	{
		gesture.search(bean);
		return this;
	}

	public FindInStoreProductPage checkList(FindAStoreProductBean bean, Properties language) 
	{
		gesture.checkList(bean);
		return this;
	}

	public FindInStoreProductPage clickLoadMore() 
	{
		gesture.clickLoadMore();
		return this;
	}

	public FindInStoreProductPage checkListAfterClickMore(String criteria) 
	{
		gesture.checkListAfterClickMore(criteria);
		return this;
	}
	
	
}
