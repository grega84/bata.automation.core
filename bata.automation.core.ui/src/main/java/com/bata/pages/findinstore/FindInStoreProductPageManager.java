package com.bata.pages.findinstore;

import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Locators;
import com.bata.pages.AbstractPageManager;

import bean.datatable.FindAStoreProductBean;
import common.CommonFields;
import test.automation.core.UIUtils;

public class FindInStoreProductPageManager extends AbstractPageManager 
{
	protected static final int ADDRESS=0;
	protected static final int TITLE = 1;
	
	protected WebElement header;
	protected WebElement geoIp;
	protected WebElement searchBar;
	protected WebElement searchButton;
	protected WebElement close;
	protected WebElement storeList;
	protected WebElement error;
	protected WebElement findMore;
	protected boolean findMorePresent;
	
	
	protected FindInStoreProductPageManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static FindInStoreProductPageManager with(WebDriver driver, Properties languages)
	{
		return new FindInStoreProductPageManager(driver, languages);
	}
	
	public FindInStoreProductPageManager isLoaded()
	{
		header=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.FindInStoreProductPage.HEADER.getWebLocator())), 60);
		geoIp=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.FindInStoreProductPage.GEOIP_BUTTON.getWebLocator())), 60);
		searchBar=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.FindInStoreProductPage.SEARCH_BAR.getWebLocator())), 60);
		searchButton=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.FindInStoreProductPage.FIND_BUTTON.getWebLocator())), 60);
		close=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.FindInStoreProductPage.CLOSE_BUTTON.getWebLocator())), 60);
		
		Assert.assertTrue(header != null);
		Assert.assertTrue(geoIp != null);
		Assert.assertTrue(searchBar != null);
		Assert.assertTrue(searchButton != null);
		Assert.assertTrue(close != null);
		
		return this;
	}
	
	public FindInStoreProductPageManager search(FindAStoreProductBean bean)
	{
		switch(bean.getCriteria())
		{
		case FindAStoreProductBean.GEOIP:
			clickGeoIP();
			break;
		case FindAStoreProductBean.ADDRESS:
		case FindAStoreProductBean.CITY:
		case FindAStoreProductBean.WRONG:
			searchStore(bean.getKey());
			break;
		case FindAStoreProductBean.POSTALCODE:
			searchStore(bean.getKey().split(";")[0]);
			break;
		}
		
		try {Thread.sleep(2000);} catch(Exception err) {}
		
		return this;
	}

	public FindInStoreProductPageManager searchStore(String store) 
	{
		searchBar=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.FindInStoreProductPage.SEARCH_BAR.getWebLocator()));
		try {Thread.sleep(500);} catch(Exception err) {}
		searchBar.clear();
		searchBar.sendKeys(store);
		searchButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.FindInStoreProductPage.FIND_BUTTON.getWebLocator()));
		try {Thread.sleep(500);} catch(Exception err) {}
		searchButton.click();
		
		return this;
	}

	public FindInStoreProductPageManager clickGeoIP() 
	{
		geoIp=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.FindInStoreProductPage.GEOIP_BUTTON.getWebLocator()));
		try {Thread.sleep(500);} catch(Exception err) {}
		geoIp.click();
		return this;
	}
	
	public FindInStoreProductPageManager checkList(FindAStoreProductBean bean) 
	{
		switch(bean.getCriteria())
		{
		case FindAStoreProductBean.GEOIP:
			storeList=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.FindInStoreProductPage.STORE_LIST.getWebLocator())));
			Assert.assertTrue(storeList != null);
			int size=driver.findElements(By.xpath(Locators.FindInStoreProductPage.STORE_LIST.getWebLocator())).size();
			
			DinamicData.getIstance().set(CommonFields.STORE_FOUND, size);
			break;
		case FindAStoreProductBean.WRONG:
			error=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.FindInStoreProductPage.NOT_FOUND_MESSAGE.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			
			String actual=error.getText().toLowerCase();
			String expected=language.getProperty("storeNotFoundMessage").toLowerCase();
			
			Assert.assertTrue("controllo messaggio di errore store not found;attuale:"+actual+", atteso:"+expected,actual.equals(expected));
			break;
		case FindAStoreProductBean.ADDRESS:
			checkList(ADDRESS,bean.getKey().toLowerCase());
			break;
		case FindAStoreProductBean.CITY:
			checkList(TITLE,bean.getKey().toLowerCase());
			break;
		case FindAStoreProductBean.POSTALCODE:
			checkList(TITLE,bean.getKey().split(";")[1].toLowerCase());
			break;
		}
		
		return this;
	}

	protected FindInStoreProductPageManager checkList(int criteria, String key) 
	{
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.FindInStoreProductPage.STORE_LIST.getWebLocator())));
		List<WebElement> list=driver.findElements(By.xpath(Locators.FindInStoreProductPage.STORE_LIST.getWebLocator()));
		
		int size=list.size();
		
		DinamicData.getIstance().set(CommonFields.STORE_FOUND, size);
		
		for(int i=0;i<size;i++)
		{
			WebElement store=driver.findElement(By.xpath(Locators.FindInStoreProductPage.STORE_ELEMENT.getWebLocator().replace("#", ""+(i+1))));
			
			String actual="";
			
			switch(criteria)
			{
			case TITLE:
				actual=driver.findElement(By.xpath(Locators.FindInStoreProductPage.STORE_ELEMENT_TITLE.getWebLocator().replace("#", ""+(i+1)))).getText().toLowerCase();
				break;
			case ADDRESS:
				actual=driver.findElement(By.xpath(Locators.FindInStoreProductPage.STORE_ELEMENT_ADDRESS.getWebLocator().replace("#", ""+(i+1)))).getText().toLowerCase();
				break;
			}
			
			//Assert.assertTrue("controllo presenza store in base alla chiave di ricerca '"+key+"'; store:"+actual,actual.contains(key));
		}
		
		return this;
	}

	public FindInStoreProductPageManager clickLoadMore() 
	{
		try
		{
			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.FindInStoreProductPage.LOAD_MORE_BUTTON.getWebLocator())));
			findMore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.FindInStoreProductPage.LOAD_MORE_BUTTON.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			
			findMore.click();
			
			this.findMorePresent=true;
		}
		catch(Exception err)
		{
			System.err.println("il pulsante mostra altro non presente dopo la ricerca degli store");
			this.findMorePresent=false;
		}
		
		return this;
	}

	public FindInStoreProductPageManager checkListAfterClickMore(String criteria) 
	{
		if(!criteria.equals(FindAStoreProductBean.WRONG) && this.findMorePresent)
		{
			int size=(int) DinamicData.getIstance().get(CommonFields.STORE_FOUND);
			int actualSize=driver.findElements(By.xpath(Locators.FindInStoreProductPage.STORE_LIST.getWebLocator())).size();
			
			Assert.assertTrue("controllo numero elementi dopo click su load more;precedente:"+size+", dopo:"+actualSize,actualSize > size);
		}
		
		return this;
	}
}
