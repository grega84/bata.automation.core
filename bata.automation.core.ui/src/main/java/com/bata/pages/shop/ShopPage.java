package com.bata.pages.shop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Locators;

import bean.datatable.ShippingBean;
import bean.datatable.StoreSearchBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class ShopPage extends LoadableComponent<ShopPage> {
	
	private int DRIVER;
	private ShopPageManager gesture;
	
	protected ShopPage(WebDriver driver,Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		default:
			gesture=ShopPageManager.with(driver, languages);
		}
	}
	
	public static ShopPage from(WebDriver driver, Properties languages)
	{
		return new ShopPage(driver, languages).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}
	
	public ShopPage searchStoreByMap(StoreSearchBean storeSearchBean) 
	{
		gesture.insertSearchKey(storeSearchBean)
		.searchStoreByMap();
		return this;
	}
	
	public ShopPage searchStoreByList(StoreSearchBean storeSearchBean) 
	{
		gesture.insertSearchKey(storeSearchBean)
		.searchStoreByList();
		return this;
	}
	
	public ShopPage clickFranchisingFilter() 
	{
		gesture.clickFranchisingButton();
		return this;
	}
	
	public ShopPage clickDirettoFilter() 
	{
		gesture.clickFlitroDiretto();
		return this;
	}
	
	public ShopPage openAndCheckDetailsStoreDirettoByMap() 
	{
		gesture.openAndCheckDetailsStoreDirettoByMap();
		return this;
	}
	
	
	public ShopPage openAndCheckDetailsStoreFranchaiseByMap() 
	{
		gesture.openAndCheckDetailsStoreFranchaiseByMap();
		return this;
	}
	
	public ShopPage openAndCheckDetailsStoreByList() 
	{
		gesture.openAndCheckDetailsStoreByList();
		return this;
	}
}
