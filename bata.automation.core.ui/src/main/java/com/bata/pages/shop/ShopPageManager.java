package com.bata.pages.shop;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.Locators;
import com.bata.pages.AbstractPageManager;

import bean.datatable.StoreSearchBean;
import io.appium.java_client.AppiumDriver;
import test.automation.core.UIUtils;

public class ShopPageManager extends AbstractPageManager 
{
	protected WebElement titleShop=null;
	protected WebElement locateMe=null;
	protected WebElement searchButton=null;
	protected WebElement inputSearch=null;
	protected WebElement mappa=null;
	protected WebElement lista=null;
	protected WebElement filtroDiretto=null;
	protected WebElement filtroFranchising=null;
	protected WebElement franchising=null;
	protected WebElement diretto=null;
	protected WebElement titleDetailStore=null;
	protected WebElement kmStore=null;
	protected WebElement addressStore1=null;
	protected WebElement addressStore2=null;
	protected WebElement addressStore3=null;
	protected WebElement visualizzaStore=null;

	protected ShopPageManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static ShopPageManager with(WebDriver driver, Properties languages)
	{
		return new ShopPageManager(driver, languages);
	}
	
	public ShopPageManager isLoaded()
	{
		titleShop=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Shops.TITLESHOP.getWebLocator())),80);
		inputSearch=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.INPUT.getWebLocator()));
		searchButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.BUTTONSEARCH.getWebLocator()));
		Assert.assertTrue(titleShop!=null);
		Assert.assertTrue(inputSearch!=null);
		Assert.assertTrue(searchButton!=null);
		
		return this;
	}
	
	public ShopPageManager insertSearchKey(StoreSearchBean storeSearchBean)
	{
		inputSearch=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.INPUT.getWebLocator()));
		inputSearch.sendKeys(storeSearchBean.getSearchField());
		
		try {((AppiumDriver<?>)driver).hideKeyboard();} catch(Exception err) {}
		
		searchButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.BUTTONSEARCH.getWebLocator()));
		searchButton.click();
		
		return this;
	}
	
	public ShopPageManager searchStoreByMap()
	{
		
		mappa=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.BUTTONSHOWONTHEMAP.getWebLocator()));
		mappa.click();
		
		return this;
	}
	
	public ShopPageManager searchStoreByList()
	{
		lista=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.BUTTONSHOWLIST.getWebLocator()));
		lista.click();
		
		return this;
	}
	
	public ShopPageManager clickFranchisingButton()
	{
		filtroFranchising=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.FILTER_FRANCHISING.getWebLocator()));
		filtroFranchising.click();
		
		return this;
	}

	public ShopPageManager clickFlitroDiretto()
	{
		filtroDiretto=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.FILTER_DIRETTO.getWebLocator()));
		filtroDiretto.click();
		
		return this;
	}
	
	public ShopPageManager openAndCheckDetailsStoreDirettoByMap() 
	{
		diretto=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.LOGO_DIRETTO.getWebLocator()));
		diretto.click();
		titleDetailStore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.TITLE_DETAILS_STORE.getWebLocator()));
		kmStore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.KM_DETAILS_STORE.getWebLocator()));
		addressStore1=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.ADDRESS_DETAILS_1.getWebLocator()));
		addressStore2=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.ADDRESS_DETAILS_2.getWebLocator()));
		addressStore3=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.ADDRESS_DETAILS_3.getWebLocator()));
		visualizzaStore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.VISUALIZZA_NEGOZIO.getWebLocator()));
		
		return this;
	}
	
	public ShopPageManager openAndCheckDetailsStoreFranchaiseByMap() 
	{
		franchising=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.LOGO_FRANCHISING.getWebLocator()));
		franchising.click();
		titleDetailStore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.TITLE_DETAILS_STORE.getWebLocator()));
		kmStore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.KM_DETAILS_STORE.getWebLocator()));
		addressStore1=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.ADDRESS_DETAILS_1.getWebLocator()));
		addressStore2=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.ADDRESS_DETAILS_2.getWebLocator()));
		addressStore3=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.ADDRESS_DETAILS_3.getWebLocator()));
		visualizzaStore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.VISUALIZZA_NEGOZIO.getWebLocator()));
		return this;
	}
	
	public ShopPageManager openAndCheckDetailsStoreByList() 
	{
		titleDetailStore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.TITLE_DETAILS_STORE_LIST.getWebLocator()));
	//	kmStore=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.KM_DETAILS_STORE_LIST.getWebLocator()));
		addressStore1=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.ADDRESS_DETAILS_1_LIST.getWebLocator()));
		addressStore2=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Shops.ADDRESS_DETAILS_2_LIST.getWebLocator()));
		return this;
	}
}
