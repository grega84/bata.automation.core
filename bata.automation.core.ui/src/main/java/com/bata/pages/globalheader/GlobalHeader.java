package com.bata.pages.globalheader;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.contactpage.ContactPage;

import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class GlobalHeader extends LoadableComponent<GlobalHeader> 
{
	
	private int DRIVER;
	private GlobalHeaderManager gesture;
	
	protected GlobalHeader(WebDriver driver, Properties language) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		default:
			gesture=GlobalHeaderManager.with(driver, language);
		}
	}
	
	public static GlobalHeader from(WebDriver driver, Properties languages)
	{
		return new GlobalHeader(driver, languages).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}
	
	public ContactPage goToContactPage() 
	{
		gesture.gotoContactPage();
		return ContactPage.from(gesture.getDriver(),gesture.getLanguages());
	}
	
}
