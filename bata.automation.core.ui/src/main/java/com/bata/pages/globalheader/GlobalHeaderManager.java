package com.bata.pages.globalheader;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.AbstractPageManager;

import test.automation.core.UIUtils;

public class GlobalHeaderManager extends AbstractPageManager 
{
	protected WebElement customer=null;
	protected WebElement shop=null;
	protected WebElement contacts=null;
	protected WebElement newsletter=null;
	protected WebElement bataclub=null;

	protected GlobalHeaderManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static GlobalHeaderManager with(WebDriver driver, Properties languages)
	{
		return new GlobalHeaderManager(driver, languages);
	}
	
	public GlobalHeaderManager isLoaded()
	{
		customer=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Homepage.CUSTOMER_SERVICE.getWebLocator())),40);
		shop=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Homepage.SHOPS.getWebLocator())),10);
		contacts=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.Homepage.CONTACTS.getWebLocator(), new Entry("contacts",language.getProperty("contacts"))))),10);
		newsletter=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.Homepage.NEWSLETTER.getWebLocator(), new Entry("newsletters",language.getProperty("newsletter"))))),10);
		bataclub=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.Homepage.BATACLUB.getWebLocator(), new Entry("bataclub",language.getProperty("bataclub"))))),10);
		Assert.assertTrue(customer!=null);
		Assert.assertTrue(shop!=null);
		Assert.assertTrue(contacts!=null);
		Assert.assertTrue(newsletter!=null);
		Assert.assertTrue(bataclub!=null);
		
		return this;
	}
	
	public GlobalHeaderManager gotoContactPage()
	{
		 contacts=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.GlobalHeader.CONTACTS.getWebLocator()));
		 contacts.click();
		 
		 return this;
	}
}
