package com.bata.pages.gmail;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.time.Duration;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.email_template.EmailTemplate;
import com.bata.email_template.EmailTemplateFactory;
import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.AbstractPageManager;
import com.bata.pages.guerillamail.GuerillaMailPageManager;

import bean.datatable.EmailTemplateBean;
import common.CommonFields;
import common.WaitManager;
import test.automation.core.SystemClipboard;
import test.automation.core.UIUtils;

public class GmailMailPageManager extends AbstractPageManager 
{
	private static final String EMAIL = "automation.test.prisma@gmail.com";
	private static final String PASSWORD = "Alice.it0$";
	private WebElement email;
	private WebElement goNext;
	private WebElement password;
	private WebElement login;
	private WebElement mail;
	private WebElement riepilogoOrdineLink;

	protected GmailMailPageManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static GmailMailPageManager with(WebDriver driver, Properties languages)
	{
		return new GmailMailPageManager(driver, languages);
	}

	public GmailMailPageManager isLoaded()
	{
		//email=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.GmailMailPage.USERNAME.getWebLocator())),40);
		//Assert.assertTrue(email!=null);
		
		return this;
	}
	
	public GmailMailPageManager checkMail(String tmpMail,String subject)
	{
		//login(null,null);
		try {Thread.sleep(2000);} catch (InterruptedException e) {}
		
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.GmailMailPage.MAIL.getWebLocator(), new Entry("subject",subject)))), 300);
		
		//Assert.assertTrue("Email non arrivata!",false);
		
		return this;
	}
	
	public GmailMailPageManager login(String mail,String passwd) 
	{
		email=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.GmailMailPage.USERNAME.getWebLocator())), 40);
		email.clear();
		email.sendKeys(StringUtils.isBlank(mail) ? EMAIL : mail);
		
		goNext=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.GmailMailPage.GO_NEXT.getWebLocator())), 40);
		goNext.click();
		
		password=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.GmailMailPage.PASSWORD.getWebLocator())), 40);
		password.clear();
		password.sendKeys(StringUtils.isBlank(passwd) ? PASSWORD : passwd);
		
		login=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.GmailMailPage.LOGIN.getWebLocator())), 40);
		login.click();
		
		return this;
	}

	public GmailMailPageManager checkSummaryMail(String subjectMail, EmailTemplateBean bean, String country)
	{
		//apro la mail
		this.mail=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.GmailMailPage.MAIL.getWebLocator(), new Entry("subject",subjectMail))));
		Assert.assertTrue(mail != null);
		try {Thread.sleep(2000);} catch (InterruptedException e) {}
		this.mail=driver.findElement(By.xpath(Utility.replacePlaceHolders(Locators.GmailMailPage.MAIL.getWebLocator(), new Entry("subject",subjectMail))));
		this.mail.click();
		
		try {Thread.sleep(2000);} catch (InterruptedException e) {}
		
		LoadableComponent<?> template=((LoadableComponent<?>) EmailTemplateFactory.getEmailTemplate(driver, country)).get();
		
		((EmailTemplate)template).checkMail(bean);
		
		return this;
	}
	
	public GmailMailPageManager clickRiepilogoOrdine()
	{
		String winHandle=driver.getWindowHandle();
		
		this.riepilogoOrdineLink=UIUtils.ui().scrollAndReturnElementLocatedBy(driver,By.xpath(Locators.EmailTemplate.EMAIL_RIEPILOGO_ORDINE_LINK.getWebLocator()));
		this.riepilogoOrdineLink.click();
		
		WaitManager.get().waitLongTime();
		
		//TODO:pezza a ipercolori multi jet come la television
		/*
		 * ricavo il link puntato dall'ancor e lo modifico per farlo puntare a dev in
		 * quanto è presente un bug sul link di riepilogo ordine
		 */
		Set<String> wins=driver.getWindowHandles();
		
		for(String s : wins)
		{
			driver.switchTo().window(s);
			
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(driver.getCurrentUrl().contains("https://www.bata."))
			{
				break;
			}
		}
		
		try {
			Thread.sleep(300);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String url=driver.getCurrentUrl().replace("https://www.bata.", "https://dev-cc.bata.");
		driver.get(url);
		//### FINE PEZZA ###
		
		return this;
	}
	
	public GmailMailPageManager checkOrderIsTrackedRegistered(String orderId,long maxTimeOut)
	{
		//attendo max 5 min
		long max=maxTimeOut;
		
		while(max >= 0)
		{
			try 
			{
				UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PurchasesAndOrdersPage.ORDER_LIST_ELEMENT_ID.getWebLocator().replace("#", "1"))), 1);
				String order=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PurchasesAndOrdersPage.ORDER_LIST_ELEMENT_ID.getWebLocator().replace("#", "1"))).getText().trim();
				Assert.assertTrue(order.equals(orderId));
				
				UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PurchasesAndOrdersPage.ORDER_LIST_ELEMENT_DETAILS_BUTTON.getWebLocator().replace("#", "1"))).click();
				
				break;
			}
			catch(Exception err) 
			{
				
				driver.navigate().refresh();
				
				try {
					Thread.sleep(Duration.ofSeconds(10).toMillis());
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				max -= Duration.ofSeconds(10).toMillis();
			}
		}
		
		return this;
	}
	
	public GmailMailPageManager checkOrderIsTrackedGuest(long maxTimeOut)
	{
		//attendo max 5 min
		long max=maxTimeOut;
		
		while(max >= 0)
		{
			try 
			{
				UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.TrackAnOderDetailsPage.HEADER.getWebLocator())), 1);
				break;
			}
			catch(Exception err) 
			{
				
				driver.navigate().refresh();
				
				try {
					Thread.sleep(TimeUnit.SECONDS.toMillis(10));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				max -=TimeUnit.SECONDS.toMillis(10);
			}
		}
		
		return this;
	}

	public GmailMailPageManager clickOnRegisterLink() 
	{
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("ita"))
			return this;
		
		String win=driver.getWindowHandle();
		String url="";
		
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("cz"))
		{
			WebElement link=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath("//a[contains(text(),'LENSTVÍ V NOVÉM BA')]"));
			try {Thread.sleep(2000);} catch (InterruptedException e) {}
			link=driver.findElement(By.xpath("//a[contains(text(),'LENSTVÍ V NOVÉM BA')]"));
			url=link.getAttribute("href");
			link.click();
		}
		else if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("sk"))
		{
			WebElement link=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath("//a[contains(text(),'LENSTVO V NOVOM BA')]"));
			try {Thread.sleep(2000);} catch (InterruptedException e) {}
			link=driver.findElement(By.xpath("//a[contains(text(),'LENSTVO V NOVOM BA')]"));
			url=link.getAttribute("href");
			link.click();
		}
		else if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("pl"))
		{
			WebElement link=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath("//a[contains(text(),'POTWIERDZI')]"));
			try {Thread.sleep(2000);} catch (InterruptedException e) {}
			link=driver.findElement(By.xpath("//a[contains(text(),'POTWIERDZI')]"));
			url=link.getAttribute("href");
			link.click();
		}
		
		String[] auth=((String) DinamicData.getIstance().get(CommonFields.AUTHLOGIN)).split(":");
		
		try
		{
			try {Thread.sleep(5000);} catch (InterruptedException e) {}
			
			SystemClipboard.copyAndPaste(auth[0]);
			Robot robot = new Robot();
			
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
			
			Thread.sleep(1000);
			
			SystemClipboard.copyAndPaste(auth[1]);
			
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			
			Thread.sleep(1000);
			
			robot.keyPress(KeyEvent.VK_F5);
			robot.keyRelease(KeyEvent.VK_F5);
			
			Utility.waitNewWindowAndGoToIt(driver, win);
			
			Thread.sleep(1000);
			
			robot.keyPress(KeyEvent.VK_F5);
			robot.keyRelease(KeyEvent.VK_F5);
		}
		catch(Exception err) {}
		
		
		
		
		
		
		return this;
	}

	public GmailMailPageManager openMail(String subjectMail) 
	{
		this.mail=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.GmailMailPage.MAIL.getWebLocator(), new Entry("subject",subjectMail))));
		Assert.assertTrue(mail != null);
		try {Thread.sleep(2000);} catch (InterruptedException e) {}
		this.mail=driver.findElement(By.xpath(Utility.replacePlaceHolders(Locators.GmailMailPage.MAIL.getWebLocator(), new Entry("subject",subjectMail))));
		this.mail.click();
		
		return this;
	}
}
