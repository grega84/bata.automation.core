package com.bata.pages.paymentstore;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.AbstractPageManager;
import com.bata.pages.payments.PaymentsManager;

import bean.datatable.ShippingBean;
import common.CommonFields;
import test.automation.core.UIUtils;

public class PaymentShipToStoreManager extends PaymentsManager 
{
	
	protected WebElement radioMr;
	protected WebElement inputName;
	protected WebElement inputSurname;
	protected WebElement inputAddress;
	protected WebElement inputCap;
	protected WebElement inputCity;
	protected WebElement province;

	protected PaymentShipToStoreManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static PaymentShipToStoreManager with(WebDriver driver, Properties languages)
	{
		return new PaymentShipToStoreManager(driver, languages);
	}
	
	public PaymentShipToStoreManager isLoaded()
	{
		logo=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PaymentPage.LOGO.getWebLocator())),40);
		Assert.assertTrue(logo!=null);
		billingAddress=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.PaymentPage.BILLING_ADDRESS.getWebLocator(), new Entry("addressFatt",language.getProperty("addressFatt")))));
		
		return this;
	}
	
	public PaymentShipToStoreManager insertBillingDataGuest(ShippingBean shippingBean)
	{
		radioMr=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.MRRADIOBUTTON.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		radioMr.click();
		inputName=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.INPUT_NAME.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		inputName.sendKeys(shippingBean.getName());
		inputSurname=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.INPUT_SURNAME.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		inputSurname.sendKeys(shippingBean.getSurname());
		inputAddress=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.INPUT_ADDRESS.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		inputAddress.sendKeys(shippingBean.getAddress());
		inputCap=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.INPUT_CAP.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		inputCap.sendKeys(shippingBean.getCap());
		inputCity=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.INPUT_CITY.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		inputCity.sendKeys(shippingBean.getCity());
		
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("ita"))
		{
			province=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.INPUT_PROVINCE.getWebLocator()));
			Select provinceSelect = new Select(province);
			provinceSelect.selectByVisibleText(shippingBean.getProvince());
			try {Thread.sleep(200);}catch(Exception err) {}
		}
		
		return this;
	}
}
