package com.bata.pages.paymentstore;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Locators;
import com.bata.pages.payments.PaymentsManager;

import bean.datatable.PaymentBean;
import bean.datatable.ShippingBean;
import common.CommonFields;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;

public class PaymentShipToStoreManagerIOS extends PaymentShipToStoreManager {

	protected PaymentShipToStoreManagerIOS(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static PaymentShipToStoreManagerIOS with(WebDriver driver, Properties languages)
	{
		return new PaymentShipToStoreManagerIOS(driver, languages);
	}
	
	public PaymentShipToStoreManagerIOS insertBillingDataGuest(ShippingBean shippingBean)
	{
		radioMr=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.MRRADIOBUTTON.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		radioMr.click();
		inputName=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.INPUT_NAME.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		inputName.sendKeys(shippingBean.getName());
		inputSurname=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.INPUT_SURNAME.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		inputSurname.sendKeys(shippingBean.getSurname());
		inputAddress=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.INPUT_ADDRESS.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		inputAddress.sendKeys(shippingBean.getAddress());
		inputCap=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.INPUT_CAP.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		inputCap.sendKeys(shippingBean.getCap());
		inputCity=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.INPUT_CITY.getWebLocator()));
		try {Thread.sleep(200);}catch(Exception err) {}
		inputCity.sendKeys(shippingBean.getCity());
		
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("ita"))
		{
			province=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.INPUT_PROVINCE.getWebLocator()));
			province.click();
			
			((AppiumDriver)driver).context("NATIVE_APP");
			
			UIUtils.ui().mobile().selectElementFromIOSPickerWeel((MobileDriver) driver, "//*[@class='UIAPicker']//*[@class='UIAView']", "//*[@class='UIAPicker']/*[@class='UIAPickerWheel']", shippingBean.getProvince());
			
			((AppiumDriver)driver).context("WEBVIEW_1");
			try {Thread.sleep(200);}catch(Exception err) {}
		}
		
		return this;
	}
	
	public PaymentShipToStoreManagerIOS selectTc()
	{
		UIUtils.ui().safari().scrollToElement(driver, Locators.PaymentPage.CHECKBOX_INFORMATIVA.getWebLocator());
		UIUtils.ui().safari().click(driver, Locators.PaymentPage.CHECKBOX_INFORMATIVA.getWebLocator());
		
		UIUtils.ui().safari().scrollToElement(driver, Locators.PaymentPage.CHECKBOX_CONDITIONS.getWebLocator());
		UIUtils.ui().safari().click(driver, Locators.PaymentPage.CHECKBOX_CONDITIONS.getWebLocator());
		return this;
	}
	
	public PaymentShipToStoreManagerIOS insertCreditCardData(PaymentBean paymentBean)
	{
		if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("ita"))
		{
			cardOwner=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.OWNER_CARD.getWebLocator()));
			cardOwner.clear();
			cardOwner.sendKeys(paymentBean.getOwnerCard());
			cardNumber=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.NUMBER_CARD.getWebLocator()));
			cardNumber.clear();
			cardNumber.sendKeys(paymentBean.getCardNumber());
			
			monthExpired=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.MONTH_EXPIRED.getWebLocator()));
			monthExpired.click();
			
			((AppiumDriver)driver).context("NATIVE_APP");
			
			UIUtils.ui().mobile().selectElementFromIOSPickerWeel((MobileDriver) driver, "//*[@class='UIAPicker']//*[@class='UIAView']", "//*[@class='UIAPicker']/*[@class='UIAPickerWheel']", paymentBean.getMonthExpiredDate());
			
			((AppiumDriver)driver).context("WEBVIEW_1");
			
			yearExpired=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.YEAR_EXPIRED.getWebLocator()));
			yearExpired.click();
			
			((AppiumDriver)driver).context("NATIVE_APP");
			
			UIUtils.ui().mobile().selectElementFromIOSPickerWeel((MobileDriver) driver, "//*[@class='UIAPicker']//*[@class='UIAView']", "//*[@class='UIAPicker']/*[@class='UIAPickerWheel']", paymentBean.getYearExpiredDate());
			
			((AppiumDriver)driver).context("WEBVIEW_1");
			
			
			cvv=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.CVV.getWebLocator()));
			cvv.clear();
			cvv.sendKeys(paymentBean.getCvv());
			price=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaymentPage.PRICE.getWebLocator()));
			
			
		}
		else if(DinamicData.getIstance().get(CommonFields.COUNTRY).equals("cz"))
		{
			try {Thread.sleep(5000);} catch (InterruptedException e) {}
		}
		
		return this;
	}

}
