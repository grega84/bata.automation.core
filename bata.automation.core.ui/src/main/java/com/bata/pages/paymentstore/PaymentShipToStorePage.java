package com.bata.pages.paymentstore;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.PayWith;
import com.bata.pages.gpwebpay.GPWebPayPage;
import com.bata.pages.paypalpayment.PaypalPaymentPage;
import com.bata.pages.riepilogo.RiepilogoPage;

import bean.datatable.CreditCardErrorCheckBean;
import bean.datatable.PaymentBean;
import bean.datatable.SearchItemBean;
import bean.datatable.ShippingBean;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import test.automation.core.UIUtils;

public class PaymentShipToStorePage extends LoadableComponent<PaymentShipToStorePage> implements PayWith {
	
	
	private int DRIVER;
	private PaymentShipToStoreManager gesture;
	
	protected PaymentShipToStorePage(WebDriver driver, Properties language) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		case Locators.ANDROID_CHROME:
			gesture=PaymentShipToStoreManagerAndroid.with(driver, language);
			break;
		case Locators.IOS_SAFARI:
			gesture=PaymentShipToStoreManagerIOS.with(driver, language);
			break;
		default:
			gesture=PaymentShipToStoreManager.with(driver, language);
		}
	}
	
	public static PaymentShipToStorePage from(WebDriver driver, Properties languages)
	{
		return new PaymentShipToStorePage(driver, languages).get();
	}

	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}
	
	public PaymentShipToStorePage insertBillingDataGuestUser(ShippingBean shippingBean) 
	{
		gesture.insertBillingDataGuest(shippingBean);
		return this;
	}
	
	public PaymentShipToStorePage applyCoupon(PaymentBean paymentBean) 
	{
		gesture.applyCupon(paymentBean);
		return this;
	}
	
	public RiepilogoPage payWithCreditCard(PaymentBean paymentBean) 
	{
		gesture.selectCreditCard()
		.insertCreditCardData(paymentBean)
		.selectTc()
		.submitCreditCardPayment();
		
		return RiepilogoPage.from(gesture.getDriver(),gesture.getLanguages());
	}

	public PaypalPaymentPage payWithPayPal(PaymentBean paymentBean)  
	{
		gesture.selectPayPal()
		.selectTc()
		.submitPayPalPayment();
		
		return PaypalPaymentPage.from(gesture.getDriver(),gesture.getLanguages());
	}

	@Override
	public RiepilogoPage payWithCod(PaymentBean paymentBean) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PaymentShipToStorePage payWithCreditCardWithoutTS(PaymentBean paymentBean) 
	{
		gesture.selectCreditCard()
		.insertCreditCardData(paymentBean)
		.submitCreditCardPayment();
		return this;
	}

	@Override
	public PaymentShipToStorePage payWithPayPalWithoutTS(PaymentBean b) 
	{
		gesture.selectPayPal()
		.submitPayPalPayment();
		return this;
	}

	@Override
	public PaymentShipToStorePage checkTsErrorMessages(Properties language) 
	{
		gesture.checkTsErrors();
		return this;
	}
	
	@Override
	public PaymentShipToStorePage selectCreditCardPaymentMethod() 
	{
		gesture.selectCreditCard();
		return this;
	}

	@Override
	public PaymentShipToStorePage selectPaypalPaymentMethod() 
	{
		gesture.selectPayPal();
		return this;
	}
	
	@Override
	public PaymentShipToStorePage checkCreditCardErrorMessage(CreditCardErrorCheckBean error) 
	{
		gesture.selectCreditCard()
		.insertCreditCardData(error)
		.selectTc()
		.submitCreditCardPayment()
		.checkCreditCardErrors(error);
		return this;
	}

	@Override
	public PaymentShipToStorePage checkCODIsntDisplayed() 
	{
		gesture.checkCodIsntVisible();
		return this;
	}

	@Override
	public GPWebPayPage payWithWebPay(PaymentBean paymentBean) 
	{
		gesture.selectCreditCard()
		.insertCreditCardData(paymentBean)
		.selectTc()
		.submitCreditCardPayment();
		
		return GPWebPayPage.from(gesture.getDriver(),gesture.getLanguages());
	}
	
}
