package com.bata.pages.registration;

import java.nio.charset.Charset;
import java.util.Properties;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;

import bean.datatable.RegistrationBean;
import test.automation.core.UIUtils;

public class RegistrationPage extends LoadableComponent<RegistrationPage> {
	
	
	private int DRIVER;
	private RegistrationPageManager gesture;


	protected RegistrationPage(WebDriver driver, Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		case Locators.DESKTOP_IE:
			gesture=RegistrationPageManagerIE.with(driver, languages);
			break;
		case Locators.IOS_SAFARI:
			gesture=RegistrationPageManagerIOS.with(driver, languages);
			break;
		default:
			gesture=RegistrationPageManager.with(driver, languages);
		}
	}
	
	public static RegistrationPage from(WebDriver driver, Properties languages)
	{
		return new RegistrationPage(driver, languages).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}
	
	public RegistrationPage insertDataRegistration(RegistrationBean registrationBean, String country) 
	{
		gesture.insertRegistrationData(registrationBean, country)
		.acceptTc()
		.submit();
		return this;
	}

	public RegistrationPage checkErrorMessage(RegistrationBean registrationBean) 
	{
		gesture.checkErrorMessages(registrationBean);
		return this;
	}

}
