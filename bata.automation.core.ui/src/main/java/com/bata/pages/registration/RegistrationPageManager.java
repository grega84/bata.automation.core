package com.bata.pages.registration;

import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.AbstractPageManager;
import com.bata.pages.gmail.GmailMailPage;

import bean.datatable.RegistrationBean;
import common.CommonFields;
import test.automation.core.UIUtils;

public class RegistrationPageManager extends AbstractPageManager 
{
	protected static final String EMAIL_ALREADY_EXISTS = "email-already-exists";
	protected static final String INVALID_PHONE = "invalid-phone-number";
	protected static final String TC_NOT_ACCEPTED = "tc-not-accepted";
	protected WebElement emailInput=null;
	protected WebElement passwordInput=null;
	protected WebElement logo=null;
	protected WebElement title=null;
	protected WebElement confirmPassword=null;
	protected WebElement name=null;
	protected WebElement surname=null;
	protected WebElement address=null;
	protected WebElement genderMrs=null;
	protected WebElement genderMr=null;
	protected WebElement cap=null;
	protected WebElement city=null;
	protected WebElement province=null;
	protected WebElement phone=null;
	protected WebElement dayBorn=null;
	protected WebElement yearBorn=null;
	protected WebElement monthBorn=null;
	protected WebElement privacy=null;
	protected WebElement marketing=null;
	protected WebElement profilazione=null;
	protected WebElement registratiButton=null;
	protected WebElement mailExistsError;
	protected WebElement invalidPhoneError;
	protected WebElement privacyError;
	protected WebElement marketingError;
	protected WebElement profilError;

	protected RegistrationPageManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static RegistrationPageManager with(WebDriver driver, Properties languages)
	{
		return new RegistrationPageManager(driver, languages);
	}
	
	public RegistrationPageManager isLoaded()
	{
		//logo=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Registration.LOGO.getWebLocator())),40);
		title=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Registration.TITLE.getWebLocator())),40);
		//Assert.assertTrue(logo!=null);
		Assert.assertTrue(title!=null);
		
		return this;
	}
	
	public RegistrationPageManager insertRegistrationData(RegistrationBean registrationBean, String country)
	{
		//String tmpMail = registrationBean.getName()+registrationBean.getSurname()+Math.random()+"@sharklasers.com";
		String tmpMail="";
		
		if(StringUtils.isBlank(registrationBean.getEmail()))
		{
			//tmpMail = registrationBean.getName()+registrationBean.getSurname()+Utility.getAlphaNumericString(5)+Utility.generateRandomDigits(5)+"@sharklasers.com";
			tmpMail = GmailMailPage.DEFAULT_MAIL+"+"+registrationBean.getName()+registrationBean.getSurname()+Utility.generateRandomDigits(8)+"@gmail.com";
		}
		else
		{
			tmpMail=registrationBean.getEmail();
		}
		
		DinamicData.getIstance().set(CommonFields.TEMP_MAIL, tmpMail);
		
		emailInput=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTEMAIL.getWebLocator()));
		try {Thread.sleep(500);} catch(Exception err) {}
		emailInput.sendKeys(tmpMail);
		
		passwordInput=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTPASSWORD.getWebLocator()));
		try {Thread.sleep(500);} catch(Exception err) {}
		passwordInput.sendKeys(registrationBean.getPassword());
		
		confirmPassword=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTCONFIRMPASSWORD.getWebLocator()));
		try {Thread.sleep(500);} catch(Exception err) {}
		confirmPassword.sendKeys(registrationBean.getConfirmPassword());
		//genderMrs=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.RADIOBUTTONMRS.getWebLocator()));
		
		genderMr=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.RADIOBUTTONMR.getWebLocator()));
		try {Thread.sleep(500);} catch(Exception err) {}
		genderMr.click();
		
		name=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTNAME.getWebLocator()));
		try {Thread.sleep(500);} catch(Exception err) {}
		name.sendKeys(registrationBean.getName());
		
		surname=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTSURNAME.getWebLocator()));
		try {Thread.sleep(500);} catch(Exception err) {}
		surname.sendKeys(registrationBean.getSurname());
		
		if(country.equals("ita") || country.equals("pl"))
		{
			address=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTADDRESS1.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			address.sendKeys(registrationBean.getAddress());
			
			cap=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTCAP.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			cap.sendKeys(registrationBean.getCap());
			
			city=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTCITY.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			city.sendKeys(registrationBean.getCity());
			
			if(country.equals("ita"))
			{
				province=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.SELECTPROVINCE.getWebLocator()));
				try {Thread.sleep(500);} catch(Exception err) {}
				Select provinceSelect = new Select(province);
				provinceSelect.selectByVisibleText(registrationBean.getProvince());
			}
			
			phone=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTPHONE.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			phone.sendKeys(StringUtils.isBlank(registrationBean.getPhone()) ? Utility.generateRandomDigits(10) : registrationBean.getPhone());
			
			dayBorn=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.SELECTDAY.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			Select dayBornSelect = new Select(dayBorn);
			dayBornSelect.selectByVisibleText(registrationBean.getDayBorn());
			
			monthBorn=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.SELECTMONHT.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			Select monthBornSelect = new Select(monthBorn);
			monthBornSelect.selectByVisibleText(registrationBean.getMonthBorn());

			yearBorn=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.SELECTYEAR.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			Select yearBornSelect = new Select(yearBorn);
			yearBornSelect.selectByVisibleText(registrationBean.getYearBorn());

		}
		
		return this;
	}
	
	public RegistrationPageManager acceptTc()
	{
		if(DinamicData.getIstance().get(CommonFields.TC_NOT_ACCEPTED) == null)
		{
			privacy=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.RADIOBUTTONTERMSCONDITIONSAGREE.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			privacy.click();
			
			marketing=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.RADIOBUTTONMARKETINGAGREE.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			marketing.click();
			
			profilazione=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.RADIOBUTTONPROFILINGAGREE.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			profilazione.click();
		}
		
		return this;
	}
	
	public RegistrationPageManager submit()
	{
		registratiButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.BUTTONSIGNIN.getWebLocator()));
		try {Thread.sleep(500);} catch(Exception err) {}
		registratiButton.click();
		return this;
	}
	
	public RegistrationPageManager checkErrorMessages(RegistrationBean registrationBean)
	{
		UIUtils.ui().scroll(driver, UIUtils.SCROLL_DIRECTION.UP, 1000);
		try {Thread.sleep(1000);} catch(Exception err) {}
		
		switch(registrationBean.getField())
		{
		case EMAIL_ALREADY_EXISTS:
			mailExistsError=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.EMAIL_ALREADY_EXISTS_ERROR.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			String mailError=mailExistsError.getText().trim().toLowerCase();
			Assert.assertTrue("controllo errore mail esistente; attuale:"+mailError+", atteso:"+language.getProperty("mailExistsError"),mailError.contains(language.getProperty("mailExistsError").toLowerCase()));
			break;
		case INVALID_PHONE:
			invalidPhoneError=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INVALID_PHONE_NUMBER_ERROR.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			break;
		case TC_NOT_ACCEPTED:
			privacyError=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.PRIVACY_ERROR.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			
			break;
		}
		
		return this;
	}
}
