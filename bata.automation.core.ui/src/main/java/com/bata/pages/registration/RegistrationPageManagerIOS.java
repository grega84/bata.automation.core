package com.bata.pages.registration;

import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.gmail.GmailMailPage;

import bean.datatable.RegistrationBean;
import common.CommonFields;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;

public class RegistrationPageManagerIOS extends RegistrationPageManager {

	public RegistrationPageManagerIOS(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static RegistrationPageManagerIOS with(WebDriver driver, Properties languages)
	{
		return new RegistrationPageManagerIOS(driver, languages);
	}
	
	public RegistrationPageManager insertRegistrationData(RegistrationBean registrationBean, String country)
	{
		//String tmpMail = registrationBean.getName()+registrationBean.getSurname()+Math.random()+"@sharklasers.com";
		String tmpMail="";
		
		if(StringUtils.isBlank(registrationBean.getEmail()))
		{
			//tmpMail = registrationBean.getName()+registrationBean.getSurname()+Utility.getAlphaNumericString(5)+Utility.generateRandomDigits(5)+"@sharklasers.com";
			tmpMail = GmailMailPage.DEFAULT_MAIL+"+"+registrationBean.getName()+registrationBean.getSurname()+Utility.generateRandomDigits(8)+"@gmail.com";
		}
		else
		{
			tmpMail=registrationBean.getEmail();
		}
		
		DinamicData.getIstance().set(CommonFields.TEMP_MAIL, tmpMail);
		
		emailInput=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTEMAIL.getWebLocator()));
		try {Thread.sleep(500);} catch(Exception err) {}
		emailInput.sendKeys(tmpMail);
		
		passwordInput=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTPASSWORD.getWebLocator()));
		try {Thread.sleep(500);} catch(Exception err) {}
		passwordInput.sendKeys(registrationBean.getPassword());
		
		confirmPassword=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTCONFIRMPASSWORD.getWebLocator()));
		try {Thread.sleep(500);} catch(Exception err) {}
		confirmPassword.sendKeys(registrationBean.getConfirmPassword());
		//genderMrs=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.RADIOBUTTONMRS.getWebLocator()));
		
		genderMr=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.RADIOBUTTONMR.getWebLocator()));
		try {Thread.sleep(500);} catch(Exception err) {}
		genderMr.click();
		
		name=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTNAME.getWebLocator()));
		try {Thread.sleep(500);} catch(Exception err) {}
		name.sendKeys(registrationBean.getName());
		
		surname=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTSURNAME.getWebLocator()));
		try {Thread.sleep(500);} catch(Exception err) {}
		surname.sendKeys(registrationBean.getSurname());
		
		if(country.equals("ita"))
		{
			address=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTADDRESS1.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			address.sendKeys(registrationBean.getAddress());
			
			cap=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTCAP.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			cap.sendKeys(registrationBean.getCap());
			
			city=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTCITY.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			city.sendKeys(registrationBean.getCity());
			
			province=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.SELECTPROVINCE.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			province.click();
			
			((AppiumDriver)driver).context("NATIVE_APP");
			
			UIUtils.ui().mobile().selectElementFromIOSPickerWeel((MobileDriver) driver, "//*[@class='UIAPicker']//*[@class='UIAView']", "//*[@class='UIAPicker']/*[@class='UIAPickerWheel']", registrationBean.getProvince());
			
			((AppiumDriver)driver).context("WEBVIEW_1");
			
			phone=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTPHONE.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			phone.sendKeys(StringUtils.isBlank(registrationBean.getPhone()) ? "+39"+Utility.generateRandomDigits(10) : registrationBean.getPhone());
			
			dayBorn=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.SELECTDAY.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			dayBorn.click();
			
			((AppiumDriver)driver).context("NATIVE_APP");
			
			UIUtils.ui().mobile().selectElementFromIOSPickerWeel((MobileDriver) driver, "//*[@class='UIAPicker']//*[@class='UIAView']", "//*[@class='UIAPicker']/*[@class='UIAPickerWheel']", registrationBean.getDayBorn());
			
			((AppiumDriver)driver).context("WEBVIEW_1");
			
			monthBorn=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.SELECTMONHT.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			monthBorn.click();
			
			((AppiumDriver)driver).context("NATIVE_APP");
			
			UIUtils.ui().mobile().selectElementFromIOSPickerWeel((MobileDriver) driver, "//*[@class='UIAPicker']//*[@class='UIAView']", "//*[@class='UIAPicker']/*[@class='UIAPickerWheel']", registrationBean.getMonthBorn());
			
			((AppiumDriver)driver).context("WEBVIEW_1");

			yearBorn=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.SELECTYEAR.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			yearBorn.click();
			
			((AppiumDriver)driver).context("NATIVE_APP");
			
			UIUtils.ui().mobile().selectElementFromIOSPickerWeel((MobileDriver) driver, "//*[@class='UIAPicker']//*[@class='UIAView']", "//*[@class='UIAPicker']/*[@class='UIAPickerWheel']", registrationBean.getYearBorn());
			
			((AppiumDriver)driver).context("WEBVIEW_1");

		}
		
		return this;
	}
}
