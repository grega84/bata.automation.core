package com.bata.pages.registration;

import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.gmail.GmailMailPage;

import bean.datatable.RegistrationBean;
import common.CommonFields;
import test.automation.core.SystemClipboard;
import test.automation.core.UIUtils;

public class RegistrationPageManagerIE extends RegistrationPageManager {

	public RegistrationPageManagerIE(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static RegistrationPageManagerIE with(WebDriver driver, Properties languages)
	{
		return new RegistrationPageManagerIE(driver, languages);
	}
	
	public RegistrationPageManagerIE checkErrorMessages(RegistrationBean registrationBean)
	{
		UIUtils.ui().scroll(driver, UIUtils.SCROLL_DIRECTION.UP, 1000);
		try {Thread.sleep(1000);} catch(Exception err) {}
		
		switch(registrationBean.getField())
		{
		case EMAIL_ALREADY_EXISTS:
			mailExistsError=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.EMAIL_ALREADY_EXISTS_ERROR.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			String mailError=mailExistsError.getText().trim().toLowerCase();
			Assert.assertTrue("controllo errore mail esistente; attuale:"+mailError+", atteso:"+language.getProperty("mailExistsError"),mailError.contains(language.getProperty("mailExistsError").toLowerCase()));
			break;
		case INVALID_PHONE:
			invalidPhoneError=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INVALID_PHONE_NUMBER_ERROR.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			break;
		case TC_NOT_ACCEPTED:
			privacyError=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.PRIVACY_ERROR.getWebLocator()));
			try {Thread.sleep(1000);} catch(Exception err) {}
			
			break;
		}
		
		return this;
	}
	
	public RegistrationPageManagerIE insertRegistrationData(RegistrationBean registrationBean, String country)
	{
		//String tmpMail = registrationBean.getName()+registrationBean.getSurname()+Math.random()+"@sharklasers.com";
		String tmpMail="";
		
		if(StringUtils.isBlank(registrationBean.getEmail()))
		{
			//tmpMail = registrationBean.getName()+registrationBean.getSurname()+Utility.getAlphaNumericString(5)+Utility.generateRandomDigits(5)+"@sharklasers.com";
			tmpMail = GmailMailPage.DEFAULT_MAIL+"+"+registrationBean.getName()+registrationBean.getSurname()+Utility.generateRandomDigits(8)+"@gmail.com";
		}
		else
		{
			tmpMail=registrationBean.getEmail();
		}
		
		DinamicData.getIstance().set(CommonFields.TEMP_MAIL, tmpMail);
		
		emailInput=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTEMAIL.getWebLocator()));
		try {Thread.sleep(500);} catch(Exception err) {}
		emailInput.click();
		try {SystemClipboard.copyAndPaste(tmpMail);} catch(Exception err) {}
		
		passwordInput=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTPASSWORD.getWebLocator()));
		try {Thread.sleep(500);} catch(Exception err) {}
		passwordInput.click();
		try {SystemClipboard.copyAndPaste(registrationBean.getPassword());} catch(Exception err) {}
		
		confirmPassword=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTCONFIRMPASSWORD.getWebLocator()));
		try {Thread.sleep(500);} catch(Exception err) {}
		confirmPassword.click();
		try {SystemClipboard.copyAndPaste(registrationBean.getConfirmPassword());} catch(Exception err) {}
		
		//genderMrs=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.RADIOBUTTONMRS.getWebLocator()));
		
		genderMr=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.RADIOBUTTONMR.getWebLocator()));
		try {Thread.sleep(500);} catch(Exception err) {}
		genderMr.click();
		
		name=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTNAME.getWebLocator()));
		try {Thread.sleep(500);} catch(Exception err) {}
		name.click();
		try {SystemClipboard.copyAndPaste(registrationBean.getName());} catch(Exception err) {}
		
		surname=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTSURNAME.getWebLocator()));
		try {Thread.sleep(500);} catch(Exception err) {}
		surname.click();
		try {SystemClipboard.copyAndPaste(registrationBean.getSurname());} catch(Exception err) {}
		
		if(country.equals("ita"))
		{
			address=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTADDRESS1.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			address.click();
			try {SystemClipboard.copyAndPaste(registrationBean.getAddress());} catch(Exception err) {}
			
			
			cap=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTCAP.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			cap.click();
			try {SystemClipboard.copyAndPaste(registrationBean.getCap());} catch(Exception err) {}
			
			
			city=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTCITY.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			city.click();
			try {SystemClipboard.copyAndPaste(registrationBean.getCity());} catch(Exception err) {}
			
			
			province=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.SELECTPROVINCE.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			Select provinceSelect = new Select(province);
			provinceSelect.selectByVisibleText(registrationBean.getProvince());
			
			phone=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.INPUTPHONE.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			
			if(DinamicData.getIstance().get(CommonFields.INVALID_PHONE_ERROR_CHECK) == null)
				phone.sendKeys(StringUtils.isBlank(registrationBean.getPhone()) ? Utility.generateRandomDigits(10) : registrationBean.getPhone());
			
			
			dayBorn=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.SELECTDAY.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			Select dayBornSelect = new Select(dayBorn);
			dayBornSelect.selectByVisibleText(registrationBean.getDayBorn());
			
			monthBorn=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.SELECTMONHT.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			Select monthBornSelect = new Select(monthBorn);
			monthBornSelect.selectByVisibleText(registrationBean.getMonthBorn());

			yearBorn=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Registration.SELECTYEAR.getWebLocator()));
			try {Thread.sleep(500);} catch(Exception err) {}
			Select yearBornSelect = new Select(yearBorn);
			yearBornSelect.selectByVisibleText(registrationBean.getYearBorn());

		}
		
		return this;
	}
}
