package com.bata.pages.riepilogo;

import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.AbstractPageManager;

import bean.datatable.PaymentBean;
import bean.datatable.RiepilogoBean;
import bean.datatable.SearchItemBean;
import bean.datatable.StoreBean;
import common.CommonFields;
import test.automation.core.UIUtils;

public class RiepilogoPageManager extends AbstractPageManager 
{
	protected static final String CREDIT_CARD_START = "carta di credito";
	protected static final String PAYPAL = "pay pal";
	protected static final Object COD = "pagamento alla consegna";
	protected static final Object COD_CZ = "zaplatit pomocí cash on delivery";
	protected static final String WEBPAY_CZ = "platba kartou";
	protected WebElement logo=null;
	protected WebElement confirmTitleMessage=null;
	protected WebElement orderNumber=null;
	protected WebElement price=null;
	protected WebElement consegna;
	protected WebElement pagamento;
	protected WebElement datiContatto;
	protected WebElement indirizzoFatt;
	protected WebElement indirizzoCons;
	protected WebElement subTotale;
	protected WebElement consegnaHeader;
	protected WebElement pagamentoHeader;
	protected WebElement datiContattoHeader;
	protected WebElement indirizzoFattHeader;
	protected WebElement indirizzoConsHeader;
	protected WebElement ritiroNegozio;
	protected WebElement confirmMailText;

	protected RiepilogoPageManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static RiepilogoPageManager with(WebDriver driver, Properties languages)
	{
		return new RiepilogoPageManager(driver, languages);
	}
	
	public RiepilogoPageManager isLoaded()
	{
		logo=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.RiepilogoPage.LOGO.getWebLocator())),80);
		Assert.assertTrue(logo!=null);
		
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.RiepilogoPage.CONFIRM_TITLE_MESSAGE.getWebLocator())),60);
		
		confirmTitleMessage=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.RiepilogoPage.CONFIRM_TITLE_MESSAGE.getWebLocator()));
		Assert.assertTrue(confirmTitleMessage!=null);

		orderNumber=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.RiepilogoPage.ORDER_NUMBER.getWebLocator()));
		Assert.assertTrue(orderNumber!=null);

		price=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.RiepilogoPage.PRICE.getWebLocator()));
		Assert.assertTrue(price!=null);

		return this;
	}
	
	public RiepilogoPageManager checkItems(PaymentBean b, String paymentType, String country)
	{
		UIUtils.ui().scrollToElementLocatedBy(driver, By.xpath(Locators.RiepilogoPage.ORDER_DETAIL_HEADER.getWebLocator()));
		
		StoreBean store=(StoreBean) DinamicData.getIstance().get(CommonFields.SELECTED_STORE);
		
		//controllo che la lista prodotti ordinati presenti tutti gli item selezionati
		List<SearchItemBean> elements=(List<SearchItemBean>) DinamicData.getIstance().get(CommonFields.SELECTED_ITEMS);
		Assert.assertTrue(elements != null && !elements.isEmpty());
		
		for(SearchItemBean element : elements)
		{
			boolean find=false;
			
			List<WebElement> items=driver.findElements(By.xpath(Locators.RiepilogoPage.MINICART_ELEMENTS.getWebLocator()));
			
			for(int i=0;i<items.size();i++)
			{
				WebElement item=items.get(i);
				Utility.scrollToWebElement(driver, item);
				
				String title=item.findElement(By.xpath(Locators.RiepilogoPage.MINICART_ELEMENT_TITLE.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				String id=item.findElement(By.xpath(Locators.RiepilogoPage.MINICART_ELEMENT_ID.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				String brand=item.findElement(By.xpath(Locators.RiepilogoPage.MINICART_ELEMENT_BRAND.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				String color=item.findElement(By.xpath(Locators.RiepilogoPage.MINICART_ELEMENT_COLOR.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				String price=item.findElement(By.xpath(Locators.RiepilogoPage.MINICART_ELEMENT_PRICE.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				String quantity=item.findElement(By.xpath(Locators.RiepilogoPage.MINICART_ELEMENT_QUANTITY.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				String totalPrice=item.findElement(By.xpath(Locators.RiepilogoPage.MINICART_ELEMENT_TOTAL_PRICE.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				
				if(title.equals(element.getName()) &&
				   id.equals(element.getNumItem()) &&
				   brand.equals(element.getBrand()) &&
				   color.equals(element.getColor()) &&
				   price.equals(element.getPrice()) &&
				   quantity.equals(element.getQuantity()))
				{
					find=true;
					
					//controllo che il totale mostrato sia uguale alla quantità * prezzo unitario
					double tot=Utility.round((Utility.parseCurrency(price, country) * Double.parseDouble(quantity)), 2);
					double totP=Utility.round(Utility.parseCurrency(totalPrice, country), 2);
					
					Assert.assertTrue("controllo totale prodotto "+element.getName()+" - "+element.getSize()+": attuale="+totP+", atteso="+tot,totP == tot);
					
					element.setTotalPrice(totalPrice);
					
					break;
				}
			}
			
			//Assert.assertTrue("controllo presenza item "+element.getName()+" - "+element.getSize()+" - "+element.getPrice(),find == true);
		}
		
		return this;
	}
	
	public RiepilogoPageManager checkSubtotale(PaymentBean b, String paymentType, String country)
	{
		//memorizzo il subtotale che andra poi in confronto con quello mostrato
		List<WebElement> items=driver.findElements(By.xpath(Locators.RiepilogoPage.MINICART_ELEMENTS.getWebLocator()));
		
		double subtotale=0;
		
		for(int i=0;i<items.size();i++)
		{
			WebElement item=items.get(i);
			Utility.scrollToWebElement(driver, item);
			
			String totalPrice=item.findElement(By.xpath(Locators.RiepilogoPage.MINICART_ELEMENT_TOTAL_PRICE.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
			
			double tot=Utility.round(Utility.parseCurrency(totalPrice, country), 2);
			subtotale += tot;
		}
		
		DinamicData.getIstance().set(CommonFields.RIEPILOGO_SUBTOTALE, subtotale);
		
		orderNumber=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.RiepilogoPage.ORDER_NUMBER.getWebLocator()));
		Assert.assertTrue(orderNumber!=null);
		Assert.assertTrue("controllo numero ordine",!orderNumber.getText().trim().isEmpty());
		
		DinamicData.getIstance().set(CommonFields.ORDER_ID, orderNumber.getText().trim());
		
		//controllo dati di fatturazione
		this.consegna=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.RiepilogoPage.ORDER_SHIPPING.getWebLocator()));
		Assert.assertTrue(consegna != null);
		
		Assert.assertTrue("controllo modalita di consegna; attuale:"+this.consegna.getText().trim(),!this.consegna.getText().trim().isEmpty());
		
		//controllo header
		consegnaHeader=driver.findElement(By.xpath(Locators.RiepilogoPage.ORDER_DETAILS_DELIVERY_HEADER.getWebLocator()));
		Assert.assertTrue(consegnaHeader != null);
		
		String header=consegnaHeader.getText().trim().toLowerCase();
		String headerExp=language.getProperty("modalitaConsegna").toLowerCase();
		
		Assert.assertTrue("controllo header 'modalita di consegna' attuale:"+header+", atteso:"+headerExp,header.equals(headerExp));
		return this;	
	}
	
	public RiepilogoPageManager checkMetodoDiPagamento(PaymentBean b, String paymentType, String country)
	{
		//controllo metodo di pagamento
		this.pagamento=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.RiepilogoPage.ORDER_PAYMENT.getWebLocator()));
		Assert.assertTrue(pagamento != null);
		String cdc=this.pagamento.getText().trim().toLowerCase();
		
		switch(paymentType)
		{
		case "paypal":
			Assert.assertTrue("controllo presenza stringa Paga con Pay Pal; attuale:"+cdc,cdc.equals(PAYPAL));
			break;
		case "card":
			if(country.equals("cz"))
			{
				Assert.assertTrue("controllo presenza stringa "+WEBPAY_CZ+"; attuale:"+cdc,cdc.toLowerCase().contains(WEBPAY_CZ.toLowerCase()));
			}
			else if(country.equals("ita"))
			{
				String last4=b.getCardNumber().split(" ")[3];
				Assert.assertTrue("controllo presenza stringa Carta di credito xxxx ************yyyy; attuale:"+cdc,cdc.startsWith(CREDIT_CARD_START) && cdc.endsWith(last4));
			}
			break;
		case "cod":
			if(country.equals("cz"))
				Assert.assertTrue("controllo presenza stringa Paga con Pagamento alla consegna; attuale:"+cdc,cdc.equals(COD_CZ));
			else if(country.equals("ita"))
				Assert.assertTrue("controllo presenza stringa Paga con Pagamento alla consegna; attuale:"+cdc,cdc.equals(COD));
			break;
		}
		
		return this;
	}
	
	public RiepilogoPageManager checkDatiContatto(PaymentBean b, String paymentType, String country)
	{
		StoreBean store=(StoreBean) DinamicData.getIstance().get(CommonFields.SELECTED_STORE);
		
		//controllo header
		pagamentoHeader=driver.findElement(By.xpath(Locators.RiepilogoPage.ORDER_DETAILS_PAYMENTS_HEADER.getWebLocator()));
		Assert.assertTrue(pagamentoHeader != null);
		
		String header=pagamentoHeader.getText().trim().toLowerCase();
		String headerExp=language.getProperty("metodoPagamento").toLowerCase();
		
		Assert.assertTrue("controllo header 'modalita di pagamento' attuale:"+header+", atteso:"+headerExp,header.equals(headerExp));
		
		
		//controllo dati di contatto
		this.datiContatto=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.RiepilogoPage.ORDER_OWNER.getWebLocator()));
		Assert.assertTrue(datiContatto != null);
		
		String dati=datiContatto.getText().trim().toLowerCase();
		
		//controllo header
		datiContattoHeader=driver.findElement(By.xpath(Locators.RiepilogoPage.ORDER_DETAILS_OWNER_OR_STORE_HEADER.getWebLocator()));
		Assert.assertTrue(datiContattoHeader != null);
		
		header=datiContattoHeader.getText().trim().toLowerCase();
		
		switch(b.getHomeOrStore().toLowerCase())
		{
		case "home":
			headerExp=language.getProperty("ownerHeader");
			Assert.assertTrue("controllo stringa dati di contatto; attuale:"+dati,
					dati.contains(b.getEmailAddress()) && dati.contains(b.getPhone()));
			Assert.assertTrue("controllo header 'dati di contatto' attuale:"+header+", atteso:"+headerExp,header.equals(headerExp.toLowerCase()));
			break;
		case "store":
			
			Assert.assertTrue(store != null);
			Assert.assertTrue("controllo presenza nome store",header.equals(store.getName()));
			Assert.assertTrue("controllo dati store; attuale:"+dati+", atteso:"+store.getDetails()+" "+store.getPhone(),
					dati.contains(store.getDetails()) && 
					dati.contains(store.getPhone()) && 
					dati.contains(store.getStoreCap()) &&
					dati.contains(store.getStoreCity()));
			break;
		}
		
		return this;
	}
	
	public RiepilogoPageManager checkIndirizzi(PaymentBean b, String paymentType, String country)
	{
		//controllo indirizzo di fatturazione
		this.indirizzoFatt=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.RiepilogoPage.ORDER_ADDRESS_BILLING.getWebLocator()));
		Assert.assertTrue(indirizzoFatt != null);
		
		String indF=indirizzoFatt.getText().trim();
		
		Assert.assertTrue("controllo stringa indirizzo di fatturazione; attuale:"+indF,
				indF.contains(b.getName()) &&
				indF.contains(b.getSurname()) &&
				indF.contains(b.getAddress()) &&
				indF.contains(b.getCity()) &&
				indF.contains(b.getCap()));
		
		
		//controllo header
		indirizzoFattHeader=driver.findElement(By.xpath(Locators.RiepilogoPage.ORDER_DETAILS_ADDRESS_INVOICE_HEADER.getWebLocator()));
		Assert.assertTrue(indirizzoFattHeader != null);
		
		String header=indirizzoFattHeader.getText().trim().toLowerCase();
		String headerExp=language.getProperty("addressFatt").toLowerCase();
		
		Assert.assertTrue("controllo header 'indirizzo di fatturazione' attuale:"+header+", atteso:"+headerExp,header.equals(headerExp));
		
		
		//controllo indirizzo di fatturazione
		this.indirizzoCons=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.RiepilogoPage.ORDER_ADDRESS_DELIVERY.getWebLocator()));
		Assert.assertTrue(indirizzoCons != null);
		
		String indC=indirizzoCons.getText().trim();
		
		//controllo header
		indirizzoConsHeader=driver.findElement(By.xpath(Locators.RiepilogoPage.ORDER_DETAILS_ADDRESS_OR_DELIVERY_HEADER.getWebLocator()));
		Assert.assertTrue(indirizzoConsHeader != null);
		
		header=indirizzoConsHeader.getText().trim().toLowerCase();
		
		switch(b.getHomeOrStore().toLowerCase())
		{
		case "home":
			Assert.assertTrue("controllo stringa indirizzo di consegna; attuale:"+indC,
					indC.contains(b.getName()) &&
					indC.contains(b.getSurname()) &&
					indC.contains(b.getAddress()) &&
					indC.contains(b.getCity()) &&
					indC.contains(b.getCap()));
			Assert.assertTrue("controllo header 'indirizzo di consegna' attuale:"+header+", atteso:"+language.getProperty("consegna").toLowerCase(),header.equals(language.getProperty("consegna").toLowerCase()));
			break;
		case "store":
			Assert.assertTrue("controllo stringa 'dati di contatto per il ritiro'; attuale:"+indC,
					indC.contains(b.getName()) &&
					indC.contains(b.getSurname()) &&
					indC.contains(b.getPhone()));
			String exp=language.getProperty("ritiro").toLowerCase();
			Assert.assertTrue("controllo header 'dati di contatto per ritiro'",header.equals(exp));
			break;
		}
		
		return this;
	}
	
	public RiepilogoPageManager checkFooter(PaymentBean b, String paymentType, String country)
	{
		//controllo subtotale
		this.subTotale=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.RiepilogoPage.ORDER_SUB_TOTAL.getWebLocator(), new Entry("subtotale",language.getProperty("subtotale")))));
		Assert.assertTrue(this.subTotale != null);
		
		double subtotale=(double) DinamicData.getIstance().get(CommonFields.RIEPILOGO_SUBTOTALE);
		
		double actualSubTot=Utility.round(Utility.parseCurrency(subTotale.getText().trim(), country), 2);
		
		Assert.assertTrue("controllo subtotale; attuale:"+actualSubTot+", atteso:"+subtotale,
				subtotale == actualSubTot);
		
		price=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.RiepilogoPage.PRICE.getWebLocator()));
		Assert.assertTrue(price!=null);
		
		Assert.assertTrue("controllo presenza totale:",!price.getText().trim().isEmpty());
		
		switch(b.getHomeOrStore().toLowerCase())
		{
		case "store":
			//controllo prezzo di ritiro
			ritiroNegozio=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.RiepilogoPage.ORDER_STORE_PRICE.getWebLocator(), new Entry("ritiroNegozio",language.getProperty("ritiroNegozio")))));
			Assert.assertTrue(this.ritiroNegozio != null);
			
			Assert.assertTrue("controllo presenza prezzo uguale a "+language.getProperty("ritiroNegozioPrezzo"),!ritiroNegozio.getText().trim().isEmpty());
			
			//controllo riceverai una mail ...
			confirmMailText=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.RiepilogoPage.ORDER_FROM_STORE_CONFIRM_MAIL.getWebLocator()));
			Assert.assertTrue(this.confirmMailText != null);
			
			Assert.assertTrue("controllo messaggio 'riceverai una mail di conferma...'",confirmMailText.getText().trim().toLowerCase().equals(language.getProperty("confirmMailInfo").toLowerCase()));
			break;
		}
		
		return this;
	}

	public RiepilogoPageManager saveRiepilogo(PaymentBean b, String paymentType, String country)
	{
		StoreBean store=(StoreBean) DinamicData.getIstance().get(CommonFields.SELECTED_STORE);
		
		
		//procedo con la creazione del bean per il controllo tramite mail
		RiepilogoBean bean=new RiepilogoBean();//4
		
		bean.setOrderNumber(orderNumber.getText().trim());
		bean.setItems((List<SearchItemBean>) DinamicData.getIstance().get(CommonFields.SELECTED_ITEMS));
		bean.setBillingAddress(this.indirizzoFatt.getText().trim());
		bean.setDeliveryAddress(this.indirizzoCons.getText().trim());
		bean.setOwner(this.datiContatto.getText().trim());
		
		switch(paymentType)
		{
		case "paypal":
			bean.setPaymentMethod("Paypal");
			break;
		case "card":
			bean.setPaymentMethod("Carta di credito");
			break;
		case "cod":
			bean.setPaymentMethod("Contrassegno");
			break;
		}
		
		//bean.setPaymentMethod((paymentType) ? "Paypal" : "Carta di credito");
		bean.setShipping(this.consegna.getText().trim());
		bean.setSubTotal(this.subTotale.getText().trim());
		bean.setTotal(price.getText().trim());
		bean.setCity(b.getCity());
		bean.setCap(b.getCap());
		bean.setPhone(b.getPhone());
		bean.setName(b.getName());
		bean.setSurname(b.getSurname());
		bean.setHomeOrStore(b.getHomeOrStore());
		bean.setStore(store);
		
		switch(b.getHomeOrStore().toLowerCase())
		{
		case "store":
			ritiroNegozio=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.RiepilogoPage.ORDER_STORE_PRICE.getWebLocator(), new Entry("ritiroNegozio",language.getProperty("ritiroNegozio")))));
			bean.setRitiroNegozioPrezzo(ritiroNegozio.getText().trim());
			
			confirmMailText=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.RiepilogoPage.ORDER_FROM_STORE_CONFIRM_MAIL.getWebLocator()));
			bean.setConfirmMailText(confirmMailText.getText().trim().toLowerCase());
			break;
		}
		
		DinamicData.getIstance().set("RECAP_ORDER", bean);
		
		return this;
	}
}
