package com.bata.pages.riepilogo;

import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;

import bean.datatable.PaymentBean;
import bean.datatable.SearchItemBean;
import bean.datatable.StoreBean;
import common.CommonFields;
import test.automation.core.UIUtils;

public class RiepilogoPageManagerIOS extends RiepilogoPageManager {

	protected RiepilogoPageManagerIOS(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static RiepilogoPageManagerIOS with(WebDriver driver, Properties languages)
	{
		return new RiepilogoPageManagerIOS(driver, languages);
	}

	public RiepilogoPageManagerIOS isLoaded()
	{
		UIUtils.ui().safari().waitForCondition(driver, UIUtils.ui().safari().visibilityOfElement(driver, Locators.RiepilogoPage.LOGO.getWebLocator()), 80);
		
		UIUtils.ui().safari().waitForCondition(driver, UIUtils.ui().safari().visibilityOfElement(driver, Locators.RiepilogoPage.CONFIRM_TITLE_MESSAGE.getWebLocator()), 60);
		
		confirmTitleMessage=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.RiepilogoPage.CONFIRM_TITLE_MESSAGE.getWebLocator()));
		Assert.assertTrue(confirmTitleMessage!=null);

		orderNumber=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.RiepilogoPage.ORDER_NUMBER.getWebLocator()));
		Assert.assertTrue(orderNumber!=null);

		price=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.RiepilogoPage.PRICE.getWebLocator()));
		Assert.assertTrue(price!=null);

		return this;
	}
	
	public RiepilogoPageManager checkItems(PaymentBean b, String paymentType, String country)
	{
		UIUtils.ui().scrollToElementLocatedBy(driver, By.xpath(Locators.RiepilogoPage.ORDER_DETAIL_HEADER.getWebLocator()));
		
		StoreBean store=(StoreBean) DinamicData.getIstance().get(CommonFields.SELECTED_STORE);
		
		//controllo che la lista prodotti ordinati presenti tutti gli item selezionati
		List<SearchItemBean> elements=(List<SearchItemBean>) DinamicData.getIstance().get(CommonFields.SELECTED_ITEMS);
		Assert.assertTrue(elements != null && !elements.isEmpty());
		
		for(SearchItemBean element : elements)
		{
			boolean find=false;
			
			List<WebElement> items=driver.findElements(By.xpath(Locators.RiepilogoPage.MINICART_ELEMENTS.getWebLocator()));
			
			for(int i=0;i<items.size();i++)
			{
				WebElement item=items.get(i);
				Utility.scrollToWebElement(driver, item);
				
				String title=driver.findElement(By.xpath(Locators.RiepilogoPage.MINICART_ELEMENT_TITLE.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				String id=driver.findElement(By.xpath(Locators.RiepilogoPage.MINICART_ELEMENT_ID.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				String brand=driver.findElement(By.xpath(Locators.RiepilogoPage.MINICART_ELEMENT_BRAND.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				String color=driver.findElement(By.xpath(Locators.RiepilogoPage.MINICART_ELEMENT_COLOR.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				String price=driver.findElement(By.xpath(Locators.RiepilogoPage.MINICART_ELEMENT_PRICE.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				String quantity=driver.findElement(By.xpath(Locators.RiepilogoPage.MINICART_ELEMENT_QUANTITY.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				String totalPrice=driver.findElement(By.xpath(Locators.RiepilogoPage.MINICART_ELEMENT_TOTAL_PRICE.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
				
				if(title.equals(element.getName()) &&
				   id.equals(element.getNumItem()) &&
				   brand.equals(element.getBrand()) &&
				   color.equals(element.getColor()) &&
				   price.equals(element.getPrice()) &&
				   quantity.equals(element.getQuantity()))
				{
					find=true;
					
					//controllo che il totale mostrato sia uguale alla quantità * prezzo unitario
					double tot=Utility.round((Utility.parseCurrency(price, country) * Double.parseDouble(quantity)), 2);
					double totP=Utility.round(Utility.parseCurrency(totalPrice, country), 2);
					
					Assert.assertTrue("controllo totale prodotto "+element.getName()+" - "+element.getSize()+": attuale="+totP+", atteso="+tot,totP == tot);
					
					element.setTotalPrice(totalPrice);
					
					break;
				}
			}
			
			Assert.assertTrue("controllo presenza item "+element.getName()+" - "+element.getSize()+" - "+element.getPrice(),find == true);
		}
		
		return this;
	}
	
	public RiepilogoPageManager checkSubtotale(PaymentBean b, String paymentType, String country)
	{
		//memorizzo il subtotale che andra poi in confronto con quello mostrato
		List<WebElement> items=driver.findElements(By.xpath(Locators.RiepilogoPage.MINICART_ELEMENTS.getWebLocator()));
		
		double subtotale=0;
		
		for(int i=0;i<items.size();i++)
		{
			WebElement item=items.get(i);
			Utility.scrollToWebElement(driver, item);
			
			String totalPrice=driver.findElement(By.xpath(Locators.RiepilogoPage.MINICART_ELEMENT_TOTAL_PRICE.getWebLocator().replace("#", ""+(i+1)))).getText().trim().toLowerCase();
			
			double tot=Utility.round(Utility.parseCurrency(totalPrice, country), 2);
			subtotale += tot;
		}
		
		DinamicData.getIstance().set("RIEPILOGO_SUBTOTALE", subtotale);
		
		orderNumber=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.RiepilogoPage.ORDER_NUMBER.getWebLocator()));
		Assert.assertTrue(orderNumber!=null);
		Assert.assertTrue("controllo numero ordine",!orderNumber.getText().trim().isEmpty());
		
		//controllo dati di fatturazione
		this.consegna=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.RiepilogoPage.ORDER_SHIPPING.getWebLocator()));
		Assert.assertTrue(consegna != null);
		
		Assert.assertTrue("controllo modalita di consegna; attuale:"+this.consegna.getText().trim(),!this.consegna.getText().trim().isEmpty());
		
		//controllo header
		consegnaHeader=driver.findElement(By.xpath(Locators.RiepilogoPage.ORDER_DETAILS_DELIVERY_HEADER.getWebLocator()));
		Assert.assertTrue(consegnaHeader != null);
		
		String header=consegnaHeader.getText().trim().toLowerCase();
		String headerExp=language.getProperty("modalitaConsegna").toLowerCase();
		
		Assert.assertTrue("controllo header 'modalita di consegna' attuale:"+header+", atteso:"+headerExp,header.equals(headerExp));
		return this;	
	}
}
