package com.bata.pages.riepilogo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import bean.datatable.PaymentBean;
import bean.datatable.RiepilogoBean;
import bean.datatable.SearchItemBean;
import bean.datatable.ShippingBean;
import bean.datatable.StoreBean;

import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;

import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class RiepilogoPage extends LoadableComponent<RiepilogoPage> {
	
	private int DRIVER;
	private RiepilogoPageManager gesture;

	protected RiepilogoPage(WebDriver driver,Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		case Locators.IOS_SAFARI:
			gesture=RiepilogoPageManagerIOS.with(driver, languages);
			break;
		default:
			gesture=RiepilogoPageManager.with(driver, languages);
		}
	}
	
	public static RiepilogoPage from(WebDriver driver, Properties languages)
	{
		return new RiepilogoPage(driver, languages).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}

	public RiepilogoPage checkRiepilogo(PaymentBean b, String paymentType, String country, Properties language) 
	{
		gesture.checkItems(b, paymentType, country)
		.checkMetodoDiPagamento(b, paymentType, country)
		.checkDatiContatto(b, paymentType, country)
		.checkIndirizzi(b, paymentType, country)
		.checkSubtotale(b, paymentType, country)
		.checkFooter(b, paymentType, country)
		.saveRiepilogo(b, paymentType, country);
		return this;
	}
	
	
	
	
}
