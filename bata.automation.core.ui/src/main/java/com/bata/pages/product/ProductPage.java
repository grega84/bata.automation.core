package com.bata.pages.product;

import java.util.Properties;

import javax.swing.text.Utilities;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Sleeper;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.cartpage.CartPage;
import com.bata.pages.findinstore.FindInStoreProductPage;
import com.bata.pages.homepage.HomePage;
import com.bata.pages.minicart.MiniCartPage;
import com.bata.pages.wishlist.WishlistPage;

import bean.datatable.ProductBean;
import bean.datatable.SearchItemBean;
import io.appium.java_client.MobileDriver;
import io.confluent.common.utils.Time;
import kafka.utils.threadsafe;
import test.automation.core.UIUtils;

public class ProductPage extends LoadableComponent<ProductPage> {
	
	private int DRIVER;
	private ProductPageManager gesture;
	
	protected ProductPage(WebDriver driver, Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		case Locators.IOS_SAFARI:
			gesture=ProductPageManagerIOS.with(driver, languages);
		default:
			gesture=ProductPageManager.with(driver, languages);
		}
	}
	
	public static ProductPage from(WebDriver driver, Properties languages)
	{
		return new ProductPage(driver, languages).get();
	}

	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}

	public ProductPage selectDetailOfItemAndAddToCart() throws InterruptedException 
	{
		gesture.getProductMetaData()
		.addToCart();		
		//.clickOnCartIcon();
		return this;
	}
	
	public ProductPage checkMiniCart() 
	{
		MiniCartPage.from(gesture.getDriver(),gesture.getLanguages())
		.checkPresenceOfAddedElement();
		return this;
	}
		
	public ProductPage addToWishlistRegistered() throws InterruptedException 
	{
		gesture.clickOnFirstAvailableSize()
		.clickOnWishList()
		.checkPrecenceOfRemoveWishList();
		return this;
	}
	
	
	public ProductPage addToWishlistGuest() throws InterruptedException 
	{
		gesture.clickOnFirstAvailableSize()
		.clickOnWishList();
		return this;
	}
	
	public WishlistPage clickOnWishlistIcon() 
	{
		gesture.clickOnWishListIcon();
		return WishlistPage.from(gesture.getDriver(),gesture.getLanguages());
	}
	
	public CartPage goToCartPage() 
	{
		gesture.gotoCartPage();
		return CartPage.from(gesture.getDriver(),gesture.getLanguages());
	}

	public ProductPage checkItemPage() 
	{
		gesture.checkPage();
		return this;
	}

	public ProductPage closeMiniCart() 
	{
		gesture.closeMiniCart();
		return this;
	}

	public HomePage gotoHomePage() 
	{
		gesture.gotoHome();
		return HomePage.from(gesture.getDriver(),gesture.getLanguages());
	}

	public ProductPage selectSize(ProductBean productBean) 
	{
		gesture.clickOnSize(productBean.getSize());
		return this;
	}

	public FindInStoreProductPage clickFindAStore() 
	{
		gesture.clickOnFindInStore();
		
		return FindInStoreProductPage.from(gesture.getDriver(),gesture.getLanguages());
	}
}
