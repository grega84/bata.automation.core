package com.bata.pages.product;

import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.AbstractPageManager;

import bean.datatable.SearchItemBean;
import common.CommonFields;
import common.WaitManager;
import test.automation.core.UIUtils;

public class ProductPageManager extends AbstractPageManager 
{
	protected WebElement title=null;
	protected WebElement brand=null;
	protected WebElement sizeproduct=null;
	protected WebElement addtocart=null;
	protected WebElement addtowish=null;
	protected WebElement carticon=null;
	protected WebElement title_mini_cart=null;
	protected WebElement showcart=null;
	protected WebElement procede_checkout=null;
	protected WebElement price=null;
	protected WebElement productName;
	protected WebElement productPrice;
	protected WebElement closeMiniCart;
	protected WebElement bataLogo;
	protected WebElement colorProduct;
	protected WebElement idProduct;
	protected WebElement brandProduct;
	protected WebElement errorWishlist;
	protected WebElement itemAddedToWish;
	protected WebElement iconWish;
	protected WebElement inStoreButton;
	protected WebElement listsize;

	protected ProductPageManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static ProductPageManager with(WebDriver driver, Properties languages)
	{
		return new ProductPageManager(driver, languages);
	}
	
	public ProductPageManager isLoaded()
	{
		title=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ProductPage.TITLE_PRODUCT.getWebLocator())),40);
		brand=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ProductPage.BRAND_PRODUCT.getWebLocator())),10);
		sizeproduct=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ProductPage.SIZE_PRODUCT.getWebLocator())),10);
		Assert.assertTrue(title!=null);
		Assert.assertTrue(brand!=null);
		Assert.assertTrue(sizeproduct!=null);
		
		return this;
	}
	
	public ProductPageManager getProductMetaData()
	{
		//memorizzo la taglia
		SearchItemBean bean=(SearchItemBean) DinamicData.getIstance().get(CommonFields.SELECTED_ITEM);
		
		listsize=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ProductPage.LIST_SIZE.getWebLocator()));
		
		sizeproduct=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ProductPage.SIZE_PRODUCT.getWebLocator())),10);	
		//sizeproduct=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ProductPage.SIZE_PRODUCT.getWebLocator()));

		Assert.assertTrue(sizeproduct!=null);
		bean.setSize(sizeproduct.getText().trim());
		
		sizeproduct.click();
		try {Thread.sleep(3000);} catch(Exception err) {}
		
		colorProduct=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ProductPage.COLOR_PRODUCT.getWebLocator()));
		Assert.assertTrue(colorProduct!=null);
		bean.setColor(colorProduct.getText().trim().toLowerCase());
		
		idProduct=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ProductPage.ID_PRODUCT.getWebLocator()));
		Assert.assertTrue(idProduct!=null);
		bean.setNumItem(idProduct.getText().trim().toLowerCase());
		
		brandProduct=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ProductPage.BRAND_PRODUCT_INFO.getWebLocator()));
		Assert.assertTrue(brandProduct!=null);
		bean.setBrand(brandProduct.getText().trim().toLowerCase());
		
		DinamicData.getIstance().set(CommonFields.SELECTED_ITEM, bean);
		
		return this;
	}
	
	public ProductPageManager addToCart()
	{
		addtocart=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ProductPage.ADD_TO_CART.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		Assert.assertTrue(addtocart!=null);
		addtocart.click();
		
		try {Thread.sleep(1000);} catch(Exception err) {}
		
		return this;
	}
	
	public ProductPageManager clickOnCartIcon()
	{
		try {Thread.sleep(5000);} catch(Exception err) {}
		UIUtils.ui().scrollToElementLocatedBy(driver, By.xpath(Locators.Homepage.LOGO.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		carticon=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ProductPage.MINI_CART.getWebLocator())),10);	
		carticon.click();
		
		return this;
	}
	
	public ProductPageManager clickOnFirstAvailableSize()
	{
		WaitManager.get().waitShortTime();
		sizeproduct=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ProductPage.SIZE_PRODUCT.getWebLocator()));
		Assert.assertTrue(sizeproduct!=null);
		sizeproduct.click();
		try {Thread.sleep(3000);} catch(Exception err) {}
		
		return this;
	}
	
	public ProductPageManager clickOnSize(String size)
	{
		if(!StringUtils.isBlank(size))
			sizeproduct=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.ProductPage.SIZE_PRODUCT_BY_VALUE.getWebLocator(), new Entry("size",size))));
		else
			sizeproduct=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ProductPage.SIZE_PRODUCT.getWebLocator()));
		
		try {Thread.sleep(500);} catch(Exception err) {}
		sizeproduct.click();
		
		return this;
	}
	
	public ProductPageManager clickOnWishList()
	{
		addtowish=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ProductPage.ADD_TO_WISHLIST.getWebLocator()));
		Assert.assertTrue(addtowish!=null);
		addtowish.click();
		try {Thread.sleep(2000);} catch(Exception err) {}
		
		return this;
	}
	
	public ProductPageManager checkPrecenceOfRemoveWishList()
	{
		try {
			itemAddedToWish=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ProductPage.REMOVE_TO_WISHLIST.getWebLocator())),20);	

		} catch (Exception e) {
			addtowish.click();
			try {Thread.sleep(2000);} catch(Exception err) {}
		}
		itemAddedToWish=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ProductPage.REMOVE_TO_WISHLIST.getWebLocator())),20);	
		Assert.assertTrue(itemAddedToWish!=null);
		
		return this;
	}
	
	public ProductPageManager clickOnWishListIcon()
	{
		iconWish=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ProductPage.WISHLIST_ICON.getWebLocator()));
		iconWish.click();
		
		return this;
	}
	
	public ProductPageManager gotoCartPage()
	{
		showcart=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MiniCartProductPage.SHOW_CART_BUTTON.getWebLocator())),20);
		Assert.assertTrue(showcart!=null);
		showcart.click();
		
		return this;
	}
	
	public ProductPageManager checkPage()
	{
		this.productName=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MiniCartProductPage.PRODUCT_NAME.getWebLocator())),20);
		Assert.assertTrue(productName!=null);
		this.productPrice=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MiniCartProductPage.PRODUCT_PRICE.getWebLocator())),20);
		Assert.assertTrue(productPrice!=null);
		
		SearchItemBean bean=(SearchItemBean) DinamicData.getIstance().get(CommonFields.SELECTED_ITEM);
		
		String name=productName.getText().trim().toLowerCase();
		String price=productPrice.getText().trim().toLowerCase();
		
		Assert.assertTrue("controllo nome prodotto selezionato: attuale="+name+", atteso="+bean.getName(),name.equals(bean.getName()));
		Assert.assertTrue("controllo prezzo prodotto selezionato: attuale="+price+", atteso="+bean.getPrice(),price.equals(bean.getPrice()));
		
		return this;
	}
	
	public ProductPageManager closeMiniCart()
	{
		this.closeMiniCart=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MiniCartProductPage.CLOSE_MINI_CART.getWebLocator())));
		Assert.assertTrue(this.closeMiniCart != null);
		
		this.closeMiniCart.click();
		
		return this;
	}
	
	public ProductPageManager gotoHome()
	{
		this.bataLogo=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Homepage.LOGO.getWebLocator())));
		Assert.assertTrue(this.bataLogo != null);
		
		this.bataLogo.click();
		
		return this;
	}
	
	public ProductPageManager clickOnFindInStore()
	{
		UIUtils.ui().scrollToElementLocatedBy(driver, By.xpath(Locators.ProductPage.ADD_TO_WISHLIST.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		inStoreButton=driver.findElement(By.xpath(Locators.ProductPage.IN_STORE_BUTTON.getWebLocator()));
		inStoreButton.click();
		
		return this;
	}
}
