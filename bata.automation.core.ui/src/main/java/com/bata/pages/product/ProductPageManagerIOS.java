package com.bata.pages.product;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.Locators;

import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;

public class ProductPageManagerIOS extends ProductPageManager {

	public ProductPageManagerIOS(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static ProductPageManagerIOS with(WebDriver driver, Properties languages)
	{
		return new ProductPageManagerIOS(driver, languages);
	}
	
	public ProductPageManagerIOS isLoaded()
	{
		try {Thread.sleep(3000);} catch(Exception err) {}
		UIUtils.ui().mobile().swipe((MobileDriver) driver, UIUtils.SCROLL_DIRECTION.DOWN, 1000);
		try {Thread.sleep(1000);} catch(Exception err) {}
		title=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ProductPage.TITLE_PRODUCT.getWebLocator())),40);
		brand=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ProductPage.BRAND_PRODUCT.getWebLocator())),10);
		sizeproduct=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ProductPage.SIZE_PRODUCT.getWebLocator())),10);
		Assert.assertTrue(title!=null);
		Assert.assertTrue(brand!=null);
		Assert.assertTrue(sizeproduct!=null);
		
		return this;
	}
}
