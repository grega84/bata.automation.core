package com.bata.pages.purchaseandoders;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.Locators;
import com.bata.pages.AbstractPageManager;

import bean.datatable.TrackOrderBean;
import test.automation.core.UIUtils;

public class PurchasesAndOrdersManager extends AbstractPageManager {

	public PurchasesAndOrdersManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static PurchasesAndOrdersManager with(WebDriver driver, Properties languages)
	{
		return new PurchasesAndOrdersManager(driver, languages);
	}

	public PurchasesAndOrdersManager gotoTrackAnOrderPage(TrackOrderBean bean)
	{
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PurchasesAndOrdersPage.ORDER_LIST.getWebLocator())), 60);
		
		//controllo liste ordini alla ricerca del desiderato
		int max=driver.findElements(By.xpath(Locators.PurchasesAndOrdersPage.ORDER_LIST_ELEMENT.getWebLocator())).size();
		
		boolean find=false;
		
		for(int i=0;i<max;i++)
		{
			WebElement headerElement=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PurchasesAndOrdersPage.ORDER_LIST_ELEMENT_HEADER.getWebLocator().replace("#", ""+(i+1))));
			String header=headerElement.getText().trim();
			
			WebElement oderIdElement=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PurchasesAndOrdersPage.ORDER_LIST_ELEMENT_ID.getWebLocator().replace("#", ""+(i+1))));
			String orderId=oderIdElement.getText().trim();
			
			WebElement statusElement=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PurchasesAndOrdersPage.ORDER_LIST_ELEMENT_STATUS.getWebLocator().replace("#", ""+(i+1))));
			String status=statusElement.getText().trim();
			
			if(header.equals(bean.getOrderDetailHeader()) && 
			   orderId.equals(bean.getOrderId()) && 
			   status.equals(bean.getOrderStatus()))
			{
				find=true;
				UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PurchasesAndOrdersPage.ORDER_LIST_ELEMENT_DETAILS_BUTTON.getWebLocator().replace("#", ""+(i+1)))).click();
				break;
			}
		}
		
		Assert.assertTrue("controllo presenza ordine "+bean.getOrderId()+" - "+bean.getOrderDetailHeader()+" - "+bean.getOrderStatus(),find);
		
		return this;
	}
}
