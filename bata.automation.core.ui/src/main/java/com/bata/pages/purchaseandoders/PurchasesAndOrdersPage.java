package com.bata.pages.purchaseandoders;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import com.bata.pagepattern.Locators;
import com.bata.pages.trackanorder.TrackAnOrderDetailsPage;

import bean.datatable.CredentialBean;
import bean.datatable.TrackOrderBean;
import common.WaitManager;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class PurchasesAndOrdersPage extends LoadableComponent<PurchasesAndOrdersPage> {
	
	private int DRIVER;
	private PurchasesAndOrdersManager gesture;
	

	protected PurchasesAndOrdersPage(WebDriver driver,Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		default:
			gesture=PurchasesAndOrdersManager.with(driver, languages);
		}
	}
	
	public static PurchasesAndOrdersPage from(WebDriver driver, Properties languages)
	{
		return new PurchasesAndOrdersPage(driver, languages).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		
	}

	public TrackAnOrderDetailsPage gotoTrackOrderDetails(TrackOrderBean bean,Properties language) 
	{
		gesture.gotoTrackAnOrderPage(bean);
		WaitManager.get().waitMediumTime();
		return TrackAnOrderDetailsPage.from(gesture.getDriver(),gesture.getLanguages());
	}
}
