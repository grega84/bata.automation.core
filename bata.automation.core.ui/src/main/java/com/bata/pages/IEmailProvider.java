package com.bata.pages;

import java.util.Properties;

import com.bata.pages.purchaseandoders.PurchasesAndOrdersPage;
import com.bata.pages.trackanorder.TrackAnOrderDetailsPage;
import com.bata.pages.welcomeuser.WelcomeUserPage;

import bean.datatable.EmailTemplateBean;

public interface IEmailProvider<T> 
{
	public T checkEmail(String tmpMail,String subject);
	
	public T openMail(String subject);
	
	public WelcomeUserPage clickOnRegisterLink();
	
	public T login(String user,String passwd);

	public T checkSummaryMail(String subjectMail, EmailTemplateBean bean, String country) ;
	
	public PurchasesAndOrdersPage clickRiepilogoOrdineRegisteredUser(Properties language, String orderId,long maxTimeOut);

	public TrackAnOrderDetailsPage clickRiepilogoOrdine(Properties language,long maxTimeOut) ;
}
