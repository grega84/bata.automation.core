package com.bata.pages.guerillamail;

import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.email_template.EmailTemplate;
import com.bata.email_template.EmailTemplateFactory;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.IEmailProvider;
import com.bata.pages.purchaseandoders.PurchasesAndOrdersPage;
import com.bata.pages.trackanorder.TrackAnOrderDetailsPage;
import com.bata.pages.welcomeuser.WelcomeUserPage;

import bean.datatable.ContactBean;
import bean.datatable.EmailTemplateBean;
import bean.datatable.RiepilogoBean;
import test.automation.core.UIUtils;

public class GuerrillaMailPage extends LoadableComponent<GuerrillaMailPage> implements IEmailProvider<GuerrillaMailPage>
{
	private int DRIVER;
	private GuerillaMailPageManager gesture;

	protected GuerrillaMailPage(WebDriver driver,Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		case Locators.IOS_SAFARI:
			gesture=GuerillaMailPageManagerIOS.with(driver, languages);
			break;
		default:
			gesture=GuerillaMailPageManager.with(driver, languages);
		}
	}
	
	public static GuerrillaMailPage from(WebDriver driver, Properties languages)
	{
		return new GuerrillaMailPage(driver, languages).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}
	
	public GuerrillaMailPage checkEmail(String tmpMail,String subject) 
	{
		gesture.checkMail(tmpMail, subject);
		return this;
	}

	public GuerrillaMailPage checkSummaryMail(String subjectMail, EmailTemplateBean bean, String country) 
	{
		gesture.checkSummaryMail(subjectMail, bean, country);
		return this;
	}
	
	public GuerrillaMailPage login(String user,String pswd)
	{
		gesture.login(user, pswd);
		return this;
	}
	
	public PurchasesAndOrdersPage clickRiepilogoOrdineRegisteredUser(Properties language, String orderId,long maxTimeOut)
	{
		gesture.clickRiepilogoOrdine()
		.checkOrderIsTrackedRegistered(orderId, maxTimeOut);
		
		return PurchasesAndOrdersPage.from(gesture.getDriver(),gesture.getLanguages());
	}

	public TrackAnOrderDetailsPage clickRiepilogoOrdine(Properties language,long maxTimeOut) 
	{
		gesture.clickRiepilogoOrdine()
		.checkOrderIsTrackedGuest(maxTimeOut);

		return TrackAnOrderDetailsPage.from(gesture.getDriver(),gesture.getLanguages());
	}

	@Override
	public WelcomeUserPage clickOnRegisterLink() 
	{
		// TODO Auto-generated method stub
		return WelcomeUserPage.from(gesture.getDriver(), gesture.getLanguages());
	}

	@Override
	public GuerrillaMailPage openMail(String subject) 
	{
		// TODO Auto-generated method stub
		return this;
	}
}
