package com.bata.pages.guerillamail;

import java.time.Duration;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.email_template.EmailTemplate;
import com.bata.email_template.EmailTemplateFactory;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.AbstractPageManager;

import bean.datatable.EmailTemplateBean;
import test.automation.core.UIUtils;

public class GuerillaMailPageManager extends AbstractPageManager 
{
	protected WebElement title=null;
	protected WebElement email=null;
	protected WebElement alias=null;
	protected WebElement oggettoMail=null;
	protected WebElement inputEmail=null;
	protected WebElement regolaButton;
	protected WebElement mail;
	protected WebElement riepilogoOrdineLink;

	protected GuerillaMailPageManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static GuerillaMailPageManager with(WebDriver driver, Properties languages)
	{
		return new GuerillaMailPageManager(driver, languages);
	}

	public GuerillaMailPageManager isLoaded()
	{
		title=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.GuerillaMailPage.LOGO.getWebLocator())),40);
		email=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.GuerillaMailPage.MAIL.getWebLocator())),40);
		alias=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.GuerillaMailPage.ALIAS.getWebLocator())),40);
		Assert.assertTrue(title!=null);
		Assert.assertTrue(email!=null);
		Assert.assertTrue(alias!=null);
		
		return this;
	}
	
	public GuerillaMailPageManager checkMail(String tmpMail,String subject)
	{
		
		UIUtils.ui().scrollToElementLocatedBy(driver, By.xpath("//div[@class='main-panel']"));
		
		String subjectXpath=Utility.replacePlaceHolders(Locators.GuerillaMailPage.SUBJECT_MAIL_2.getWebLocator(), new Entry("subject",subject));
		
		//controllo per 2 min fino alla ricezione della mail
		for(int i=0; i<12; i++) {
			try {
				UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(subjectXpath)), 10);
				return this;
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
		Assert.assertTrue("Email non arrivata!",false);
		
		return this;
	}
	
	public GuerillaMailPageManager login(String user,String passwd)
	{
		//setto la mail a tmpMail
		alias=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.GuerillaMailPage.ALIAS.getWebLocator())),40);
		alias.click();
		email=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.GuerillaMailPage.MAIL.getWebLocator()));
		email.click();
		inputEmail=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.GuerillaMailPage.INPUT_NEW_MAIL.getWebLocator()));
		inputEmail.clear();
		inputEmail.sendKeys(user);
		regolaButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.GuerillaMailPage.REGOLA_BUTTON.getWebLocator()));
		regolaButton.click();
		
		return this;
	}
	
	public GuerillaMailPageManager checkSummaryMail(String subjectMail, EmailTemplateBean bean, String country)
	{
		//apro la mail
		this.mail=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.GuerillaMailPage.SUBJECT_MAIL_2.getWebLocator(), new Entry("subject",subjectMail))));
		Assert.assertTrue(mail != null);
		
		this.mail.click();
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		LoadableComponent<?> template=((LoadableComponent<?>) EmailTemplateFactory.getEmailTemplate(driver, country)).get();
		
		((EmailTemplate)template).checkMail(bean);
		
		return this;
	}
	
	public GuerillaMailPageManager clickRiepilogoOrdine()
	{
		String winHandle=driver.getWindowHandle();
		
		this.riepilogoOrdineLink=UIUtils.ui().scrollAndReturnElementLocatedBy(driver,By.xpath(Locators.EmailTemplate.EMAIL_RIEPILOGO_ORDINE_LINK.getWebLocator()));
		this.riepilogoOrdineLink.click();
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//TODO:pezza a ipercolori multi jet come la television
		/*
		 * ricavo il link puntato dall'ancor e lo modifico per farlo puntare a dev in
		 * quanto è presente un bug sul link di riepilogo ordine
		 */
		Set<String> wins=driver.getWindowHandles();
		
		for(String s : wins)
		{
			driver.switchTo().window(s);
			
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(driver.getCurrentUrl().contains("https://www.bata."))
			{
				break;
			}
		}
		
		try {
			Thread.sleep(300);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String url=driver.getCurrentUrl().replace("https://www.bata.", "https://dev-cc.bata.");
		driver.get(url);
		//### FINE PEZZA ###
		
		return this;
	}
	
	public GuerillaMailPageManager checkOrderIsTrackedRegistered(String orderId,long maxTimeOut)
	{
		//attendo max 5 min
		long max=maxTimeOut;
		
		while(max >= 0)
		{
			try 
			{
				UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PurchasesAndOrdersPage.ORDER_LIST_ELEMENT_ID.getWebLocator().replace("#", "1"))), 1);
				String order=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PurchasesAndOrdersPage.ORDER_LIST_ELEMENT_ID.getWebLocator().replace("#", "1"))).getText().trim();
				Assert.assertTrue(order.equals(orderId));
				
				UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PurchasesAndOrdersPage.ORDER_LIST_ELEMENT_DETAILS_BUTTON.getWebLocator().replace("#", "1"))).click();
				
				break;
			}
			catch(Exception err) 
			{
				
				driver.navigate().refresh();
				
				try {
					Thread.sleep(Duration.ofSeconds(10).toMillis());
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				max -= Duration.ofSeconds(10).toMillis();
			}
		}
		
		return this;
	}
	
	public GuerillaMailPageManager checkOrderIsTrackedGuest(long maxTimeOut)
	{
		//attendo max 5 min
		long max=maxTimeOut;
		
		while(max >= 0)
		{
			try 
			{
				UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.TrackAnOderDetailsPage.HEADER.getWebLocator())), 1);
				break;
			}
			catch(Exception err) 
			{
				
				driver.navigate().refresh();
				
				try {
					Thread.sleep(TimeUnit.SECONDS.toMillis(10));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				max -=TimeUnit.SECONDS.toMillis(10);
			}
		}
		
		return this;
	}
}
