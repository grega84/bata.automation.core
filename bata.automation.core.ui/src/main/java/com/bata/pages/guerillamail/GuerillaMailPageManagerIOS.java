package com.bata.pages.guerillamail;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.Locators;

import test.automation.core.UIUtils;

public class GuerillaMailPageManagerIOS extends GuerillaMailPageManager {

	protected GuerillaMailPageManagerIOS(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static GuerillaMailPageManagerIOS with(WebDriver driver, Properties languages)
	{
		return new GuerillaMailPageManagerIOS(driver, languages);
	}

	public GuerillaMailPageManagerIOS isLoaded()
	{
		/*
		UIUtils.ui().safari().waitForCondition(driver, UIUtils.ui().safari().visibilityOfElement(driver, Locators.GuerillaMailPage.LOGO.getWebLocator()),40);
		UIUtils.ui().safari().waitForCondition(driver, UIUtils.ui().safari().visibilityOfElement(driver, Locators.GuerillaMailPage.MAIL.getWebLocator()),40);
		UIUtils.ui().safari().waitForCondition(driver, UIUtils.ui().safari().visibilityOfElement(driver, Locators.GuerillaMailPage.ALIAS.getWebLocator()),40);
		*/
		return this;
	}
}
