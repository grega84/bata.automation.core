package com.bata.pages.homepage;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import test.automation.core.UIUtils;

public class HomePageManagerIOS extends HomePageManagerAndroid {

	protected HomePageManagerIOS(WebDriver driver, Properties languages) 
	{
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static HomePageManagerIOS with(WebDriver driver, Properties languages)
	{
		return new HomePageManagerIOS(driver, languages);
	}
	
	public HomePageManager openMiniCart()
	{
		this.miniCartButton=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(locators.ICONBAG.getWebLocator())));
		Assert.assertTrue(miniCartButton != null);
		
		this.miniCartButton.click();
		
		try {Thread.sleep(2000);} catch(Exception err) {}
		
		UIUtils.ui().safari().click(driver, "//a[@class='b-minicart__view-bag b-btn_third']");
		
		
		return this;
	}

}
