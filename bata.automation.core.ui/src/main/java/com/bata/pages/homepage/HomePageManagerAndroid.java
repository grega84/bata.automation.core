package com.bata.pages.homepage;

import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;

import bean.datatable.ProductBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;

public class HomePageManagerAndroid extends HomePageManager 
{

	protected HomePageManagerAndroid(WebDriver driver, Properties languages) 
	{
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static HomePageManagerAndroid with(WebDriver driver, Properties languages)
	{
		return new HomePageManagerAndroid(driver, languages);
	}

	public HomePageManagerAndroid isLoaded()
	{
		logo=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Homepage.LOGO.getWebLocator())),40);
		Assert.assertTrue(logo!=null);
		
		Utility.closeEditPageMenu(driver);
		return this;
	}
	
	public HomePageManagerAndroid goToShopPage() 
	{
		shop=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Homepage.SHOPS.getAndroidLocator())),40);
		shop.click();
		return this;
	}
	
	public HomePageManagerAndroid openSearchBar()
	{
		this.searchIcon=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Homepage.SEARCH_ICON.getAndroidLocator())),40);
		this.searchIcon.click();
		
		searchBar=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Homepage.SEARCH_BAR.getWebLocator())),40);
		Assert.assertTrue(searchBar!=null);
		searchBar.click();
		
		return this;
	}
	
	public HomePageManagerAndroid searchItem(ProductBean productBean)
	{
		productName=productBean.getProductName();
		color=productBean.getColor();
		gender=productBean.getGender();
		material=productBean.getMaterial();
		category=productBean.getCategory();
		bataID=productBean.getBataID();
		//searchfield=productName;
		
		if (!StringUtils.isBlank(productName)) {
			searchfield+=""+productName;
		}		
		
		if (!StringUtils.isBlank(bataID)) {
			searchfield+=" "+bataID;
		}
		
		if (!StringUtils.isBlank(color)) {
			searchfield+=" "+color;
		}
		if (!StringUtils.isBlank(gender)) {
			searchfield+=" "+gender;
		}
		if (!StringUtils.isBlank(material)) {
			searchfield+=" "+material;
		}
		if (!StringUtils.isBlank(category)) {
			searchfield+=" "+category;
		}
		searchBar.clear();
		searchBar.sendKeys(searchfield);
		try {((AppiumDriver)driver).hideKeyboard();}catch(Exception err) {}
		
		return this;
	}
	
	public HomePageManagerAndroid countItemsPreviewSearched()
	{
		item1=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PreviewItemsSearched.PRODUCT1.getWebLocator())),40);
		item2=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PreviewItemsSearched.PRODUCT2.getWebLocator())),40);
		item3=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PreviewItemsSearched.PRODUCT3.getWebLocator())),40);
		Assert.assertTrue(item1!=null);
		Assert.assertTrue(item2!=null);
		Assert.assertTrue(item3!=null);
		
		return this;
	}
	
	public HomePageManagerAndroid gotoWishList()
	{
		//clicco su menu e poi su wishlist
		WebElement menu=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Homepage.MOBILE_LEFT_MENU.getWebLocator()));
		menu.click();
		WebElement iconWish= UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Homepage.MOBILE_WISHLIST_ICON.getWebLocator())),20);
		iconWish.click();
				
		return this;
	}
	
	public HomePageManagerAndroid gotoTrackAnOrder()
	{
		UIUtils.ui().scrollToElementLocatedBy(driver, By.xpath("(//div[contains(@class,'ui-accordion-header')])[1]"));
		try {Thread.sleep(1000);}catch(Exception err) {}
		driver.findElement(By.xpath("(//div[contains(@class,'ui-accordion-header')])[1]")).click();
		try {Thread.sleep(1000);}catch(Exception err) {}
		this.trackAnOrder=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.Homepage.TRACK_AN_ORDER_FOOTER_LINK.getWebLocator(), new Entry("trackAnOrder",language.getProperty("trackAnOrder")))));
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.trackAnOrder.click();
		
		return this;
	}
	
	public HomePageManagerAndroid clickOnFirstPopularSearchedItem()
	{
		((AppiumDriver)driver).context("NATIVE_APP");
		UIUtils.ui().mobile().swipe((MobileDriver) driver,UIUtils.SCROLL_DIRECTION.DOWN, 1000);
		((AppiumDriver)driver).context("CHROMIUM");
		
		try {Thread.sleep(1000);} catch (InterruptedException e) {}
		
		popularFirstItem=driver.findElement(By.xpath(Utility.replacePlaceHolders(Locators.Homepage.POPULAR_SEARCH_FIRST.getWebLocator(), new Entry("popularSearchHeader",language.getProperty("popularSearchHeader")))));
		popularFirstItem.click();
		return this;
	}
	
	public HomePageManager gotoSendContactPage()
	{
		menuLeft=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Homepage.MOBILE_LEFT_MENU.getWebLocator()));
		menuLeft.click();
		try {Thread.sleep(1000);} catch (InterruptedException e) {}
		
		//System.out.println(((AppiumDriver) driver).getCapabilities().getCapability("udid"));
		
		contacts=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.Homepage.CONTACTS.getAndroidLocator(), new Entry("contacts",language.getProperty("contacts")))));
	
		try {Thread.sleep(1000);} catch (InterruptedException e) {}
		
		contacts.click();
		
		return this;
	}
}
