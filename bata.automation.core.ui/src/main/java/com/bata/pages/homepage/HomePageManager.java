package com.bata.pages.homepage;

import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.AbstractPageManager;
import com.bata.pages.contactpage.ContactPage;

import bean.datatable.ProductBean;
import bean.datatable.SearchItemBean;
import bean.datatable.StoreSearchBean;
import common.WaitManager;
import test.automation.core.UIUtils;

public class HomePageManager extends AbstractPageManager
{
	protected Locators.Homepage locators;
	
	protected WebElement logo=null;
	protected WebElement myaccount=null;
	protected WebElement contacts=null;
	protected WebElement shop=null;
	protected WebElement searchBar=null;
	protected WebElement suggestion=null;
	protected String productName=null;
	protected String color=null;
	protected String category=null;
	protected String gender=null;
	protected String material=null;
	protected String searchfield="";
	protected WebElement showall=null;
	protected WebElement categoryName=null;
	protected WebElement item1=null;
	protected WebElement item2=null;
	protected WebElement item3=null;
	protected WebElement item4=null;
	protected WebElement item5=null;
	protected WebElement item6=null;
	protected WebElement searchIcon=null;
	protected WebElement miniCartButton;
	protected String bataID;
	protected String categoryNameSearched;
	protected String brand;
	protected WebElement iconWish;
	protected WebElement trackAnOrder;
	protected WebElement suggestionLink;
	protected WebElement popularFirstItem;
	protected WebElement categoryFirstItem;
	protected WebElement showcart;
	protected WebElement menuLeft;
	
	protected HomePageManager(WebDriver driver, Properties languages) 
	{
		super(driver, languages);
	}
	
	public static HomePageManager with(WebDriver driver, Properties languages)
	{
		return new HomePageManager(driver, languages);
	}
	
	public HomePageManager isLoaded()
	{
		logo=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(locators.LOGO.getWebLocator())),40);
		Assert.assertTrue(logo!=null);
		
		return this;
	}
	
	public HomePageManager gotoLoginPage()
	{
		myaccount=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(locators.MYACCOUNT.getWebLocator())),40);
		Assert.assertTrue(myaccount!=null);
		myaccount.click();
		return this;
	}
	
	public HomePageManager goToMyContactPage() 
	{
		contacts=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(locators.CONTACTS.getWebLocator(), new Entry("contacts",language.getProperty("contacts"))))),40);
		Assert.assertTrue(contacts!=null);
		contacts.click();
		return this;
	}
	
	public HomePageManager goToShopPage() 
	{
		shop=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(locators.SHOPS.getWebLocator())),40);
		Assert.assertTrue(shop!=null);
		shop.click();
		return this;
	}
	
	public HomePageManager openSearchBar()
	{
		this.searchIcon=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(locators.SEARCH_ICON.getWebLocator())),40);
		this.searchIcon.click();
		searchBar=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(locators.SEARCH_BAR.getWebLocator())),40);
		Assert.assertTrue(searchBar!=null);
		searchBar.click();
		WaitManager.get().waitMediumTime();
		return this;
	}
	
	public HomePageManager searchItem(ProductBean productBean)
	{
		productName=productBean.getProductName();
		color=productBean.getColor();
		gender=productBean.getGender();
		material=productBean.getMaterial();
		category=productBean.getCategory();
		bataID=productBean.getBataID();
		//searchfield=productName;
		
		if (!StringUtils.isBlank(productName)) {
			searchfield+=""+productName;
		}		
		
		if (!StringUtils.isBlank(bataID)) {
			searchfield+=" "+bataID;
		}
		
		if (!StringUtils.isBlank(color)) {
			searchfield+=" "+color;
		}
		if (!StringUtils.isBlank(gender)) {
			searchfield+=" "+gender;
		}
		if (!StringUtils.isBlank(material)) {
			searchfield+=" "+material;
		}
		if (!StringUtils.isBlank(category)) {
			searchfield+=" "+category;
		}
		if (!StringUtils.isBlank(productBean.getBrand())) {
			searchfield+=" "+productBean.getBrand();
		}
		if (!StringUtils.isBlank(productBean.getCategoryName())) {
			searchfield+=" "+productBean.getCategoryName();
		}
		
		System.out.println("searchField:"+searchfield);
		
		searchBar.clear();
		searchBar.sendKeys(searchfield);
		
		return this;
	}
	
	public HomePageManager clickShowAll()
	{
		//showall=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PreviewItemsSearched.SHOWALL.getWebLocator())),40);
		UIUtils.ui().scrollToElementLocatedBy(driver, By.xpath(Locators.PreviewItemsSearched.SHOWALL.getWebLocator()));
		try {
			
			Thread.sleep(1000);
			
		} 
		catch(Exception err) {
			
		}

		showall=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PreviewItemsSearched.SHOWALL.getWebLocator())),40);
		Assert.assertTrue(showall!=null);
		showall.click();
		
		return this;
	}
	
	public HomePageManager clickOnSearchCategoryItem(String category)
	{
		WaitManager.get().waitMediumTime();
		categoryName=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(Locators.PreviewItemsSearched.CATEGORY_FILTER1.getWebLocator(), new Entry("category", category))));
		categoryName.click();
		return this;
	}
	
	public HomePageManager countItemsPreviewSearched()
	{
		item1=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PreviewItemsSearched.PRODUCT1.getWebLocator())),40);
		
		try {
			item2=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PreviewItemsSearched.PRODUCT2.getWebLocator())),3);
			item3=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PreviewItemsSearched.PRODUCT3.getWebLocator())),3);
			
			item4=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PreviewItemsSearched.PRODUCT4.getWebLocator())),3);
			item5=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PreviewItemsSearched.PRODUCT5.getWebLocator())),3);
			item6=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PreviewItemsSearched.PRODUCT6.getWebLocator())),3);
			Assert.assertTrue(item4!=null);
			Assert.assertTrue(item5!=null);
			Assert.assertTrue(item6!=null);
			Assert.assertTrue(item1!=null);
			Assert.assertTrue(item2!=null);
			Assert.assertTrue(item3!=null);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return this;
	}
	
	public HomePageManager selectFirstItemSearched()
	{
		//prelevo i campi nome e prezzo dall'item
		item1=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PreviewItemsSearched.PRODUCT1.getWebLocator())),10);
		
		WebElement name=item1.findElement(By.xpath(Locators.PreviewItemsSearched.PRODUCT_NAME.getWebLocator()));
		WebElement price=item1.findElement(By.xpath(Locators.PreviewItemsSearched.PRODUCT_PRICE.getWebLocator()));
		
		SearchItemBean bean=new SearchItemBean();
		bean.setName(name.getText().trim().toLowerCase());
		bean.setPrice(price.getText().trim().toLowerCase());
		
		DinamicData.getIstance().set("SELECTED_ITEM", bean);
		
		//clicco sull'item
		item1.click();
		
		return this;
	}
	
	public HomePageManager closeCookie()
	{
		try
		{
			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(locators.CLOSE_COOKIE_BUTTON.getWebLocator())),3);
			driver.findElement(By.xpath(locators.CLOSE_COOKIE_BUTTON.getWebLocator())).click();
		}
		catch(Exception err)
		{
			
		}
		
		try
		{
			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@class='b-marketing-popup__btn ui-button ui-corner-all ui-widget']")), 5);
			driver.findElement(By.xpath("//*[@class='b-marketing-popup__btn ui-button ui-corner-all ui-widget']")).click();
		}
		catch(Exception err)
		{
			
		}
		
		return this;
	}
	
	public HomePageManager openMiniCart()
	{
		this.miniCartButton=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(locators.ICONBAG.getWebLocator())));
		Assert.assertTrue(miniCartButton != null);
		
		this.miniCartButton.click();
		
		
		return this;
	}
	
	public HomePageManager searchStore(StoreSearchBean storeSearchBean)
	{
		searchBar.sendKeys(storeSearchBean.getSearchField());
		searchIcon=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(locators.ICON_SEARCH_BAR.getWebLocator())),40);
		searchIcon.click();
		
		return this;
	}
	
	public HomePageManager gotoWishList()
	{
		iconWish=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.ProductPage.WISHLIST_ICON.getWebLocator()));
		iconWish.click();
		
		return this;
	}
	
	public HomePageManager gotoTrackAnOrder()
	{
		this.trackAnOrder=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(locators.TRACK_AN_ORDER_FOOTER_LINK.getWebLocator(), new Entry("trackAnOrder",language.getProperty("trackAnOrder")))));
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.trackAnOrder.click();
		
		return this;
	}
	
	public HomePageManager checkNoMoreElementFindTextDisplayed() 
	{
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(locators.NO_MORE_ELEMENT_TEXT.getWebLocator(), new Entry("noMoreElementText",language.getProperty("noMoreElementText"))))));
		
		return this;
	}

	public HomePageManager checkPopularSearchSection() 
	{
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(locators.POPULAR_SEARCH_DIV.getWebLocator(), new Entry("popularSearchHeader",language.getProperty("popularSearchHeader"))))));
		return this;
	}

	public HomePageManager checkSuggetionSearchLink() 
	{
		suggestionLink=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(locators.SUGGESTION_SEARCH_LINK.getWebLocator())));
		Assert.assertTrue(suggestionLink != null);
		return this;
	}
	
	public HomePageManager clickOnSuggestionLinkSearch()
	{
		suggestionLink=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(locators.SUGGESTION_SEARCH_LINK.getWebLocator())));
		suggestionLink.click();
		return this;
	}
	
	public HomePageManager clickOnFirstPopularSearchedItem()
	{
		popularFirstItem=UIUtils.ui().scrollAndReturnElementLocatedBy(driver,By.xpath(Utility.replacePlaceHolders(locators.POPULAR_SEARCH_FIRST.getWebLocator(), new Entry("popularSearchHeader",language.getProperty("popularSearchHeader")))));
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		popularFirstItem.click();
		
		return this;
	}
	
	public HomePageManager checkCategorySection() 
	{
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(locators.CATEGORY_SEARCH_DIV.getWebLocator(), new Entry("categorySearchHeader",language.getProperty("categorySearchHeader"))))));
		return this;
	}

	public HomePageManager clickOnFirstCategorySearchElement() 
	{
		categoryFirstItem=UIUtils.ui().scrollAndReturnElementLocatedBy(driver,By.xpath(Utility.replacePlaceHolders(locators.CATEGORY_SEARCH_DIV.getWebLocator(), new Entry("categorySearchHeader",language.getProperty("categorySearchHeader")))));
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		categoryFirstItem.click();
		
		return this;
	}
	
	public HomePageManager gotoSendContactPage()
	{
		contacts=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Utility.replacePlaceHolders(locators.CONTACTS.getWebLocator(), new Entry("contacts",language.getProperty("contacts")))));
		
		try {Thread.sleep(1000);} catch (InterruptedException e) {}
		
		contacts.click();
		
		return this;
	}
}
