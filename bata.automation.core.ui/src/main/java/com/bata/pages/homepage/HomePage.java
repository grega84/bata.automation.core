package com.bata.pages.homepage;

import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.cartpage.CartPage;
import com.bata.pages.contactpage.ContactPage;
import com.bata.pages.globalheader.GlobalHeader;
import com.bata.pages.login.LoginPage;
import com.bata.pages.minicart.MiniCartPage;
import com.bata.pages.product.ProductPage;
import com.bata.pages.purchaseandoders.PurchasesAndOrdersPage;
import com.bata.pages.seach.SearchingPage;
import com.bata.pages.seach.SearchingPageFiltered;
import com.bata.pages.shop.ShopPage;
import com.bata.pages.trackanorder.TrackAnOrderPage;
import com.bata.pages.wishlist.WishlistPage;

import bean.datatable.ProductBean;
import bean.datatable.SearchItemBean;
import bean.datatable.StoreSearchBean;
import common.WaitManager;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class HomePage extends LoadableComponent<HomePage> 
{
	private HomePageManager gesture;
	private int DRIVER;
	
	protected HomePage(WebDriver driver, Properties language) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		
		switch(DRIVER)
		{
		case Locators.ANDROID_CHROME:
			gesture=HomePageManagerAndroid.with(driver, language);
			break;
		case Locators.IOS_SAFARI:
			gesture=HomePageManagerIOS.with(driver, language);
			break;
		default:
			gesture=HomePageManager.with(driver, language);
		}
	}
	
	public static HomePage from(WebDriver driver, Properties languages)
	{
		return new HomePage(driver, languages).get();
	}

	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}
	
	/*go to my account guest*/
	public LoginPage goToLoginPage() 
	{
		gesture.gotoLoginPage();
		return LoginPage.from(gesture.getDriver(),gesture.getLanguages());
	}
	
	/*go to my account registered*/
	
	public ContactPage goToMyContactPage() 
	{
		gesture.goToMyContactPage();
		return ContactPage.from(gesture.getDriver(),gesture.getLanguages());
	}
	
	public ShopPage goToShopPage() 
	{
		gesture.goToShopPage();
		return ShopPage.from(gesture.getDriver(),gesture.getLanguages());
	}
	
	public HomePage searchProduct(ProductBean productBean) 
	{
		WaitManager.get().waitLongTime();
		gesture.openSearchBar()
		.searchItem(productBean);
		return this;
	}

	public SearchingPage showAll() 
	{
		gesture.clickShowAll();
		return SearchingPage.from(gesture.getDriver(),gesture.getLanguages());
	}

	public SearchingPageFiltered searchProductAndClickOnCategory(ProductBean productBean) 
	{
		gesture.openSearchBar()
		.searchItem(productBean)
		.clickOnSearchCategoryItem(productBean.getCategoryName());
		
		
		return SearchingPageFiltered.from(gesture.getDriver(),gesture.getLanguages());
	}

	public HomePage countItemsPreviewSearched() 
	{
		gesture.countItemsPreviewSearched();
		return this;
	}
	
	public ProductPage chooseItem() 
	{
		WaitManager.get().waitHighTime();
		gesture.selectFirstItemSearched();
		return ProductPage.from(gesture.getDriver(),gesture.getLanguages());
	}

	public HomePage closeCookieDiv() 
	{
		gesture.closeCookie();
		return this;
	}

	public MiniCartPage openMiniCart() 
	{
		gesture.openMiniCart();
		
		WaitManager.get().waitMediumTime();
		
		return MiniCartPage.from(gesture.getDriver(),gesture.getLanguages());
	}
	
	public HomePage searchNegozi(StoreSearchBean storeSearchBean) 
	{
		gesture.openSearchBar()
		.searchStore(storeSearchBean);
		return this;
	}

	public WishlistPage goToWishlistPage() 
	{
		gesture.gotoWishList();
		return WishlistPage.from(gesture.getDriver(),gesture.getLanguages());
	}

	
	public CartPage clickBagIcon() 
	{
		gesture.openMiniCart();
		
		try
		{
			MiniCartPage.from(gesture.getDriver(), gesture.getLanguages())
			.clickShowCart();
		}
		catch(Exception err)
		{
			
		}
		
		return CartPage.from(gesture.getDriver(), gesture.getLanguages());
	}
	
	public TrackAnOrderPage gotoTrackAnOrderPage()
	{
		gesture.gotoTrackAnOrder();
		return TrackAnOrderPage.from(gesture.getDriver(),gesture.getLanguages());
	}

	public PurchasesAndOrdersPage gotoPurchaseAndOrdersPage() 
	{
		gesture.gotoTrackAnOrder();
		
		return PurchasesAndOrdersPage.from(gesture.getDriver(),gesture.getLanguages());
	}

	
	public HomePage checkNoMoreElementFindTextDisplayed() 
	{
		gesture.checkNoMoreElementFindTextDisplayed();
		return this;
	}

	public HomePage checkPopularSearchSection() 
	{
		gesture.checkPopularSearchSection();
		return this;
	}

	public HomePage checkSuggetionSearchLink() 
	{
		gesture.checkSuggetionSearchLink();
		return this;
	}

	public HomePage clickOnSuggestionLinkSearch() 
	{
		gesture.clickOnSuggestionLinkSearch();
		return this;
	}

	public HomePage clickOnFirstPopularSearchElement() 
	{
		gesture.clickOnFirstPopularSearchedItem();
		return this;
	}

	public HomePage checkCategorySection() 
	{
		gesture.checkCategorySection();
		return this;
	}

	public HomePage clickOnFirstCategorySearchElement() 
	{
		gesture.clickOnFirstCategorySearchElement();
		return this;
	}

	public ContactPage gotoSendContactPage() 
	{
		gesture.gotoSendContactPage();
		
		return ContactPage.from(gesture.getDriver(),gesture.getLanguages());
	}

	public CartPage goToCartPage() 
	{
		gesture.openMiniCart();
		return CartPage.from(gesture.getDriver(), gesture.getLanguages()).get();
	}

	/*jianni*/
	
//	public CartPage goToCartPageForMobile() 
//	{
//		String typeOfTest = (String) DinamicData.getIstance().get("typeOfTest");
//		
//		if (!typeOfTest.equals("webTest"))
//			showcart=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MiniCartProductPage.SHOW_CART_BUTTON.getAndroidLocator())));
//		
//		showcart.click();
//		return new CartPage(driver).get();
//	}
}
