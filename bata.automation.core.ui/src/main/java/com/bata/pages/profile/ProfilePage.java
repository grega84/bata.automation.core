package com.bata.pages.profile;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.pagepattern.Locators;
import com.bata.pages.homepage.HomePage;
import com.bata.pages.myprofile.MyProfilePage;

import common.WaitManager;
import test.automation.core.UIUtils;

public class ProfilePage extends LoadableComponent<ProfilePage> {
	
	
	private int DRIVER;
	private ProfilePageManager gesture;
	
	protected ProfilePage(WebDriver driver,Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		default:
			gesture=ProfilePageManager.with(driver, languages);
		}
	}
	
	public static ProfilePage from(WebDriver driver, Properties languages)
	{
		return new ProfilePage(driver, languages).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error {
		gesture.isLoaded();
	}
	
	public HomePage goToHomePage() {
		gesture.gotoHomePage();
		return HomePage.from(gesture.getDriver(),gesture.getLanguages());
	}

	public MyProfilePage gotoMyProfile(Properties language) 
	{
		WaitManager.get().waitMediumTime();
		gesture.gotoProfilePage();
		return MyProfilePage.from(gesture.getDriver(),gesture.getLanguages());
	}
}
