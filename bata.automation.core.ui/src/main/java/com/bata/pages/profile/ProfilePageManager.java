package com.bata.pages.profile;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.Locators;
import com.bata.pages.AbstractPageManager;

import test.automation.core.UIUtils;

public class ProfilePageManager extends AbstractPageManager 
{
	protected WebElement logo=null;
	protected WebElement title=null;
	protected WebElement pointsandcouponsbutton=null;
	protected WebElement purchasesandordersbutton=null;
	protected WebElement myprofilebutton=null;

	protected ProfilePageManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static ProfilePageManager with(WebDriver driver, Properties languages)
	{
		return new ProfilePageManager(driver, languages);
	}
	
	public ProfilePageManager isLoaded()
	{
		title=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ProfilePage.TITLE.getWebLocator())),60);
		pointsandcouponsbutton=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ProfilePage.POINTSANDCOUPONSBUTTON.getWebLocator())),40);
		purchasesandordersbutton=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ProfilePage.PURCHASESANDORDERSBUTTON.getWebLocator())),40);
		myprofilebutton=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ProfilePage.MYPROFILEBUTTON.getWebLocator())),40);
		Assert.assertTrue(title!=null);
		Assert.assertTrue(pointsandcouponsbutton!=null);
		Assert.assertTrue(purchasesandordersbutton!=null);
		Assert.assertTrue(myprofilebutton!=null);
		
		return this;
	}
	
	public ProfilePageManager gotoHomePage()
	{
		logo=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ProfilePage.LOGO.getWebLocator())),50);
		logo.click();
		return this;
	}
	
	public ProfilePageManager gotoProfilePage()
	{
		myprofilebutton=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ProfilePage.MYPROFILEBUTTON.getWebLocator())),40);
		Assert.assertTrue(myprofilebutton!=null);
		myprofilebutton.click();
		return this;
	}
}
