package com.bata.pages;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

public abstract class AbstractPageManager 
{
	protected WebDriver driver=null;
	protected Properties language=null;
	
	public AbstractPageManager(WebDriver driver, Properties languages) 
	{
		super();
		this.driver = driver;
		this.language = languages;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public Properties getLanguages() {
		return language;
	}
	
	
}
