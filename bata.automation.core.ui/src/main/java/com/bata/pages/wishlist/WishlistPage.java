package com.bata.pages.wishlist;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.pagepattern.Locators;

import test.automation.core.UIUtils;

public class WishlistPage extends LoadableComponent<WishlistPage> {
	
	
	private int DRIVER;
	private WishListPageManager gesture;
	
	protected WishlistPage(WebDriver driver,Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		default:
			gesture=WishListPageManager.with(driver, languages);
		}
	}

	public static WishlistPage from(WebDriver driver, Properties languages)
	{
		return new WishlistPage(driver, languages).get();
	}

	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}
	
	public WishlistPage checkElementAddedToWishlist() throws InterruptedException 
	{
		gesture.checkElementAddedToWishlist();
		return this;
	}
	
	public WishlistPage removeItemAddedFromWishlistPage() {
		gesture.removeItemAddedFromWishlistPage();
		return this;
	}
	
	public WishlistPage removeItemAddedFromProductPage() {
		gesture.removeItemAddedFromProductPage();
		return this;
	}
	
	public WishlistPage checkWishlistEmpty() {
		gesture.checkWishlistEmpty();
		return this;
		}
	
	public WishlistPage addToCartItemFromWishlist() throws InterruptedException {
		gesture.addToCartItemFromWishlist();
		return this;
	}
	
	public WishlistPage copyUrl() throws InterruptedException {
		gesture.copyUrl();
		return this;
	}
}
