package com.bata.pages.wishlist;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.Locators;
import com.bata.pages.AbstractPageManager;

import test.automation.core.UIUtils;

public class WishListPageManager extends AbstractPageManager 
{
	protected WebElement logo=null;
	
	protected WebElement removeButton;
	protected WebElement title;
	protected WebElement detailsItem;
	protected WebElement homepageButton;
	protected WebElement nuoviArriviButton;
	protected WebElement removeFromWishlistButton;
	protected WebElement urlCopy;
	protected WebElement buyItem;

	protected WishListPageManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static WishListPageManager with(WebDriver driver, Properties languages)
	{
		return new WishListPageManager(driver, languages);
	}
	
	public WishListPageManager isLoaded()
	{
		logo=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MyWishlistPage.LOGO.getWebLocator())),60);
		title=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MyWishlistPage.TITLE.getWebLocator())),60);
		Assert.assertTrue(logo!=null);
		Assert.assertTrue(title!=null);
		
		return this;
	}
	
	public WishListPageManager checkElementAddedToWishlist()
	{
		detailsItem=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyWishlistPage.DETAILS_ITEM.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		buyItem=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyWishlistPage.ADD_TO_CART_FIRST_ITEM.getWebLocator()));
		removeFromWishlistButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyWishlistPage.REMOVE_BUTTON.getWebLocator()));
		Assert.assertTrue(detailsItem!=null);
		Assert.assertTrue(buyItem!=null);
		Assert.assertTrue(removeFromWishlistButton!=null);
		return this;
	}
	
	public WishListPageManager removeItemAddedFromWishlistPage() {
		removeFromWishlistButton=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MyWishlistPage.REMOVE_BUTTON.getWebLocator())),60);
		removeFromWishlistButton.click();
		return this;
	}
	
	public WishListPageManager removeItemAddedFromProductPage() {
		removeButton=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ProductPage.REMOVE_TO_WISHLIST.getWebLocator())),60);
		removeButton.click();
		return this;
	}
	
	public WishListPageManager checkWishlistEmpty() {
		homepageButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyWishlistPage.HOMEPAGE_BUTTON.getWebLocator()));
		nuoviArriviButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyWishlistPage.NUOVI_ARRIVI_BUTTON.getWebLocator()));
		return this;
	}
	
	public WishListPageManager addToCartItemFromWishlist()
	{
		buyItem=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.MyWishlistPage.ADD_TO_CART_FIRST_ITEM.getWebLocator()));
		try {Thread.sleep(1000);} catch(Exception err) {}
		buyItem.click();
		return this;
	}
	
	public WishListPageManager copyUrl()
	{
		try {Thread.sleep(1000);} catch(Exception err) {};
		urlCopy=UIUtils.ui().scrollAndReturnElementLocatedBy(driver,By.xpath (Locators.MyWishlistPage.URL_COPY.getWebLocator()));
		urlCopy.click();
		try {Thread.sleep(2000);} catch(Exception err) {}
		
		 //get copied string from clipboard
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		String paste="";
		try {
			paste = (String) clipboard.getContents(null).getTransferData(DataFlavor.stringFlavor);
		} catch (UnsupportedFlavorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//open in separate tab
		 JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("popup_window = window.open('"+ paste+ "');");
		try {Thread.sleep(3000);} catch(Exception err) {}
		js.executeScript("popup_window.close()");
		return this;
	}

}
