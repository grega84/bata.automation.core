package com.bata.pages.gpwebpay;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.AbstractPageManager;

import bean.datatable.PaymentBean;
import test.automation.core.UIUtils;

public class GPWebPayPageManager extends AbstractPageManager 
{
	protected WebElement cardInput;
	protected WebElement month;
	protected WebElement year;
	protected WebElement cvv;
	protected WebElement submit;
	
	protected GPWebPayPageManager(WebDriver driver, Properties languages) 
	{
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static GPWebPayPageManager with(WebDriver driver, Properties languages)
	{
		return new GPWebPayPageManager(driver, languages);
	}
	
	public GPWebPayPageManager isLoaded()
	{
		cardInput=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.GPWebPayPage.CARD.getWebLocator())), 60);
		month=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.GPWebPayPage.MONTH.getWebLocator())), 60);
		year=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.GPWebPayPage.YEAR.getWebLocator())), 60);
		cvv=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.GPWebPayPage.CVV.getWebLocator())), 60);
		submit=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.GPWebPayPage.SUBMIT.getWebLocator())), 60);
		
		
		Assert.assertTrue(cardInput != null);
		Assert.assertTrue(month != null);
		Assert.assertTrue(year != null);
		Assert.assertTrue(cvv != null);
		Assert.assertTrue(submit != null);
		
		return this;
	}
	
	public GPWebPayPageManager payWithCreditCard(PaymentBean bean)
	{
		cardInput=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.GPWebPayPage.CARD.getWebLocator()));
		try {Thread.sleep(1000);} catch (InterruptedException e) {}
		cardInput.sendKeys(bean.getCardNumber());
		
		month=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.GPWebPayPage.MONTH.getWebLocator()));
		String monthExpiredDate = bean.getMonthExpiredDate();
		try {Thread.sleep(1000);} catch (InterruptedException e) {}
		month.click();
		try {Thread.sleep(1000);} catch (InterruptedException e) {}
		
		WebElement monthElem=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.GPWebPayPage.MONTH_VALUE.getWebLocator(), new Entry("value",monthExpiredDate)))));
		
		monthElem.click();
		
		year=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.GPWebPayPage.YEAR.getWebLocator()));
		String yearExpiredDate = bean.getYearExpiredDate();
		try {Thread.sleep(1000);} catch (InterruptedException e) {}
		year.click();
		try {Thread.sleep(1000);} catch (InterruptedException e) {}
		
		WebElement yearElem=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.replacePlaceHolders(Locators.GPWebPayPage.YEAR_VALUE.getWebLocator(), new Entry("value",yearExpiredDate)))));
		
		yearElem.click();
		
		cvv=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.GPWebPayPage.CVV.getWebLocator()));
		try {Thread.sleep(1000);} catch (InterruptedException e) {}
		cvv.sendKeys(bean.getCvv());
		
		submit=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.GPWebPayPage.SUBMIT.getWebLocator()));
		try {Thread.sleep(1000);} catch (InterruptedException e) {}
		submit.click();
		
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.GPWebPayPage.SMS_SUBMIT.getWebLocator())), 60);
		
		submit=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.GPWebPayPage.SMS_SUBMIT.getWebLocator()));
		try {Thread.sleep(1000);} catch (InterruptedException e) {}
		submit.click();
		
		return this;
	}
}
