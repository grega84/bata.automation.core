package com.bata.pages.gpwebpay;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;

import bean.datatable.PaymentBean;
import io.appium.java_client.android.AndroidDriver;
import test.automation.core.UIUtils;

public class GPWebPayPageManagerAndroid extends GPWebPayPageManager {

	public GPWebPayPageManagerAndroid(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static GPWebPayPageManagerAndroid with(WebDriver driver, Properties languages)
	{
		return new GPWebPayPageManagerAndroid(driver, languages);
	}
	
	public GPWebPayPageManagerAndroid payWithCreditCard(PaymentBean bean)
	{
		super.payWithCreditCard(bean);
		
		((AndroidDriver) driver).context("NATIVE_APP");
		
		try
		{
			Thread.sleep(3000);
			((AndroidDriver) driver).findElementById("com.android.chrome:id/infobar_close_button").click();
		}
		catch(Exception err)
		{
			err.printStackTrace();
		}
		
		((AndroidDriver) driver).context("CHROMIUM");
		
		return this;
	}
}
