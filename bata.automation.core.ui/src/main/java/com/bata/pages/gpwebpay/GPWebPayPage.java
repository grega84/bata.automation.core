package com.bata.pages.gpwebpay;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Properties;
import java.util.Set;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;
import com.bata.pages.riepilogo.RiepilogoPage;

import bean.datatable.CredentialBean;
import bean.datatable.PaymentBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import test.automation.core.UIUtils;

public class GPWebPayPage extends LoadableComponent<GPWebPayPage> 
{
	
	private int DRIVER;
	private GPWebPayPageManager gesture;

	protected GPWebPayPage(WebDriver driver,Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		case Locators.ANDROID_CHROME:
			gesture=GPWebPayPageManagerAndroid.with(driver, languages);
		default:
			gesture=GPWebPayPageManager.with(driver, languages);
		}
	}
	
	public static GPWebPayPage from(WebDriver driver, Properties languages)
	{
		return new GPWebPayPage(driver, languages).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}
	
	public RiepilogoPage payWithCreditCard(PaymentBean bean)
	{
		gesture.payWithCreditCard(bean);
		
		return RiepilogoPage.from(gesture.getDriver(),gesture.getLanguages());
	}
}
