package com.bata.pages.paypalpayment;

import java.util.Properties;
import java.util.Set;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;

import com.bata.pagepattern.Locators;
import com.bata.pages.riepilogo.RiepilogoPage;

import bean.datatable.PaymentBean;
import test.automation.core.UIUtils;

public class PaypalPaymentRiepilogoPage extends LoadableComponent<PaypalPaymentRiepilogoPage> {
	
	private int DRIVER;
	private PaypalPaymentRiepilogoManager gesture;
	
	protected PaypalPaymentRiepilogoPage(WebDriver driver,Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		case Locators.IOS_SAFARI:
			gesture=PaypalPaymentRiepilogoManagerIOS.with(driver, languages);
			break;
		default:
			gesture=PaypalPaymentRiepilogoManager.with(driver, languages);
		}
	}
	
	public static PaypalPaymentRiepilogoPage from(WebDriver driver, Properties languages)
	{
		return new PaypalPaymentRiepilogoPage(driver, languages).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}
	
	public RiepilogoPage confirmRiepilogo() 
	{
		gesture.close();
		//driver.switchTo().frame(0);
		return RiepilogoPage.from(gesture.getDriver(),gesture.getLanguages());
	}

	
}
