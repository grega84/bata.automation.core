package com.bata.pages.paypalpayment;

import java.util.Properties;
import java.util.Set;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.Locators;

import test.automation.core.UIUtils;

public class PaypalPaymentRiepilogoManagerIOS extends PaypalPaymentRiepilogoManager {

	protected PaypalPaymentRiepilogoManagerIOS(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static PaypalPaymentRiepilogoManagerIOS with(WebDriver driver, Properties languages)
	{
		return new PaypalPaymentRiepilogoManagerIOS(driver, languages);
	}
	
	public PaypalPaymentRiepilogoManagerIOS isLoaded()
	{
		//logo=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PaypalPaymentRiepilogoPage.LOGO.getWebLocator())),40);
		//Assert.assertTrue(logo!=null);
		//UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PaypalPaymentRiepilogoPage.CONTINUE_BUTTON.getWebLocator())),40);
		UIUtils.ui().safari().waitForCondition(driver, UIUtils.ui().safari().visibilityOfElement(driver, Locators.PaypalPaymentRiepilogoPage.CONTINUE_BUTTON.getWebLocator()), 40);
		return this;
	}
	
	public PaypalPaymentRiepilogoManagerIOS close()
	{
		//continueButton=driver.findElement(By.xpath(Locators.PaypalPaymentRiepilogoPage.CONTINUE_BUTTON.getWebLocator()));
		UIUtils.ui().safari().click(driver, Locators.PaypalPaymentRiepilogoPage.CONTINUE_BUTTON.getWebLocator());
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//continueButton.click();
		String winHandler=driver.getWindowHandle();
		
		Set<String> wins=driver.getWindowHandles();
		
		for(String s : wins)
		{
			if(!s.equals(winHandler))
			{
				driver.switchTo().window(s);
				break;
			}
		}
		
		return this;
	}
}
