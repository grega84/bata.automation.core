package com.bata.pages.paypalpayment;

import java.util.Properties;
import java.util.Set;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.Locators;
import com.bata.pages.AbstractPageManager;

import common.WaitManager;
import test.automation.core.UIUtils;

public class PaypalPaymentRiepilogoManager extends AbstractPageManager 
{
	protected WebElement logo=null;
	protected WebElement continueButton=null;
	
	protected PaypalPaymentRiepilogoManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}
	
	public static PaypalPaymentRiepilogoManager with(WebDriver driver, Properties languages)
	{
		return new PaypalPaymentRiepilogoManager(driver, languages);
	}

	public PaypalPaymentRiepilogoManager isLoaded()
	{
		logo=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PaypalPaymentRiepilogoPage.LOGO.getWebLocator())),40);
		Assert.assertTrue(logo!=null);
		continueButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaypalPaymentRiepilogoPage.CONTINUE_BUTTON.getWebLocator()));
		WaitManager.get().waitLongTime();
		return this;
	}
	
	public PaypalPaymentRiepilogoManager close()
	{
		//continueButton=driver.findElement(By.xpath(Locators.PaypalPaymentRiepilogoPage.CONTINUE_BUTTON.getWebLocator()));
		try {
			UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaypalPaymentRiepilogoPage.ACCEPT_COOKIE.getWebLocator()))
			.click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		continueButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaypalPaymentRiepilogoPage.CONTINUE_BUTTON.getWebLocator()));
		Assert.assertTrue(continueButton != null);
		WaitManager.get().waitShortTime();
		continueButton.click();
		String winHandler=driver.getWindowHandle();
		
		Set<String> wins=driver.getWindowHandles();
		
		for(String s : wins)
		{
			if(!s.equals(winHandler))
			{
				driver.switchTo().window(s);
				break;
			}
		}
		
		return this;
	}
}
