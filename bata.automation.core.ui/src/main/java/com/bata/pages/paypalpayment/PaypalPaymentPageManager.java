package com.bata.pages.paypalpayment;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.Locators;
import com.bata.pages.AbstractPageManager;

import bean.datatable.PaymentBean;
import test.automation.core.UIUtils;

public class PaypalPaymentPageManager extends AbstractPageManager 
{
	protected WebElement logo=null;
	protected WebElement inputEmail=null;
	protected WebElement inputPassword=null;
	protected WebElement accediButton=null;
	protected WebElement errorMessage;

	protected PaypalPaymentPageManager(WebDriver driver, Properties languages) {
		super(driver, languages);
		// TODO Auto-generated constructor stub
	}

	public static PaypalPaymentPageManager with(WebDriver driver, Properties languages)
	{
		return new PaypalPaymentPageManager(driver, languages);
	}
	
	public PaypalPaymentPageManager isLoaded()
	{
		//logo=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PaypalPaymentPage.LOGO.getWebLocator())),35);
		//Assert.assertTrue(logo!=null);
		inputEmail=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaypalPaymentPage.INPUT_EMAIL.getWebLocator()));
		Assert.assertTrue(inputEmail!=null);
		inputPassword=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaypalPaymentPage.INPUT_PASSWORD.getWebLocator()));
		Assert.assertTrue(inputPassword!=null);
		accediButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaypalPaymentPage.ACCEDI_BUTTON.getWebLocator()));
		Assert.assertTrue(accediButton!=null);
		return this;
	}
	
	public PaypalPaymentPageManager insertCredential(PaymentBean paymentBean)
	{
		inputEmail=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PaypalPaymentPage.INPUT_EMAIL.getWebLocator())),60);
		Assert.assertTrue(inputEmail!=null);
		inputEmail.clear();
		inputEmail.sendKeys(paymentBean.getEmailPaypal());
		inputPassword=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaypalPaymentPage.INPUT_PASSWORD.getWebLocator()));
		Assert.assertTrue(inputPassword!=null);
		inputPassword.sendKeys(paymentBean.getPasswordPaypal());
		
		return this;
	}
	
	public PaypalPaymentPageManager login()
	{
		accediButton=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.PaypalPaymentPage.ACCEDI_BUTTON.getWebLocator()));
		Assert.assertTrue(accediButton!=null);
		accediButton.click();
		return this;
	}
	
	public PaypalPaymentPageManager checkLoginError()
	{
		errorMessage=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.PaypalPaymentPage.ERROR_MESSAGE.getWebLocator())));
		Assert.assertTrue(errorMessage != null);
		
		String actual=errorMessage.getText().trim();
		String expected=language.getProperty("wrongPaypalCredential");
		
		Assert.assertTrue("controllo messaggio di errore dopo credenziali paypal errate: attuale:"+actual+", atteso:"+expected,actual.equals(expected));
		
		return this;
	}
}
