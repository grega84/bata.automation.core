package com.bata.pages.paypalpayment;

import java.time.Duration;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import javax.swing.InputMap;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Locators;

import bean.datatable.PaymentBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import test.automation.core.UIUtils;

public class PaypalPaymentPage extends LoadableComponent<PaypalPaymentPage> {
	
	
	private int DRIVER;
	private PaypalPaymentPageManager gesture;

	
	protected PaypalPaymentPage(WebDriver driver,Properties languages) 
	{
		this.DRIVER=Locators.getDriverType(driver);
		switch(DRIVER)
		{
		default:
			gesture=PaypalPaymentPageManager.with(driver, languages);
		}
	}
	
	public static PaypalPaymentPage from(WebDriver driver, Properties languages)
	{
		return new PaypalPaymentPage(driver, languages).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(gesture.getDriver(), this);

	}

	@Override
	protected void isLoaded() throws Error 
	{
		gesture.isLoaded();
	}
	
	
	public PaypalPaymentRiepilogoPage insertCredential(PaymentBean paymentBean) 
	{
		gesture.insertCredential(paymentBean)
		.login();
		
		return new PaypalPaymentRiepilogoPage(gesture.getDriver(),gesture.getLanguages()).get();
	}

	public PaypalPaymentPage insertWrongCredential(PaymentBean paymentBean) 
	{
		gesture.insertCredential(paymentBean)
		.login();
		return this;
	}

	public PaypalPaymentPage checkErrorMessage(Properties language) 
	{
		gesture.checkLoginError();
		return this;
	}
	
}
