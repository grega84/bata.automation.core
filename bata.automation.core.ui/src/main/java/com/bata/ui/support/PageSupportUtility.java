package com.bata.ui.support;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.bata.pagepattern.Locators;
import com.bata.pages.wishlist.WishlistPage;

import io.appium.java_client.AppiumDriver;
import test.automation.core.UIUtils;

public class PageSupportUtility {
	
	public static WishlistPage clickOnWishlistIcon(WebDriver driver,Properties languages) 
	{
		if(driver instanceof AppiumDriver)
		{
			//clicco su menu e poi su wishlist
			WebElement menu=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.Homepage.MOBILE_LEFT_MENU.getWebLocator()));
			menu.click();
			WebElement iconWish= UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.Homepage.MOBILE_WISHLIST_ICON.getWebLocator())),20);
			iconWish.click();
		}
		else
		{
			WebElement iconWish= UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.ProductPage.WISHLIST_ICON.getWebLocator())),60);
			iconWish.click();
		}
		
		return WishlistPage.from(driver,languages);
	}
	
	
}
