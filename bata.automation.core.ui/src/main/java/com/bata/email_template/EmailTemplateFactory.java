package com.bata.email_template;

import org.openqa.selenium.WebDriver;

public class EmailTemplateFactory 
{
	public static EmailTemplate getEmailTemplate(WebDriver driver,String country)
	{
		switch(country.toLowerCase())
		{
		case "ita":
			return new EmailTemplateItaly(driver).get();
		case "send_request-ita":
			return new SendRequestEmailTemplateItaly(driver).get();
			default:
				return new EmailTemplateItaly(driver).get();
		}
	}
}
