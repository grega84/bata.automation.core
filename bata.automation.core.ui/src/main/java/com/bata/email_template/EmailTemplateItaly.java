package com.bata.email_template;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;

import bean.datatable.EmailTemplateBean;
import bean.datatable.RiepilogoBean;
import bean.datatable.SearchItemBean;
import common.CommonFields;
import test.automation.core.UIUtils;

public class EmailTemplateItaly extends LoadableComponent<EmailTemplateItaly> implements EmailTemplate
{
	private static final String EMAIL_WELCOME="ORDINE CONFERMATO\n" + 
			"\n" + 
			"Ciao ${name} ${surname},\n" + 
			"grazie per il tuo ordine su Bata.it.\n" + 
			"\n" + 
			"Riceverai una e-mail di conferma relativa alla disponibilità degli articoli con i dettagli per il tracking.\n" + 
			"\n" + 
			"Il Team Bata" + 
			"";
	private static final String EMAIL_RIEPILOGO_ORDINE="RIEPILOGO ORDINE";
	private static final String EMAIL_HELP_SUBSECTION="Hai bisogno di aiuto?\n" + 
			"Puoi contattarci al numero telefonico\n" + 
			"041 8620719 o usare il modulo di contatto." + 
			"";
	private static final String EMAIL_PRODUCT_LIST_HEADER="PRODOTTI ORDINATI";
	private static final String EMAIL_PRODUCT_ELEMENT_DETAILS="${productName}\n" + 
			"Articolo: ${productId}\n" + 
			"Brand: ${productBrand}\n" + 
			"Misura: ${productSize}\n" + 
			"Colore: ${productColor}\n" + 
			"Prezzo: ${productPrice}\n" + 
			"Quantità: ${productQuantity}" + 
			"";
	private static String EMAIL_PRODUCT_ELEMENT_TOTAL="${productElTot}";
	private static String EMAIL_PRODUCT_SUMMARY_SUBTOTAL_TEXT="Subtotale";
	private static String EMAIL_PRODUCT_SUMMARY_SUBTOTAL_VALUE="${productSubTotalPrice}";
	private static String EMAIL_PRODUCT_SUMMARY_DELIVERY_TEXT="Spedizione ${shipping}";
	private static String EMAIL_PRODUCT_SUMMARY_DELIVERY_VALUE="${productDeliveryValue}";
	private static String EMAIL_PRODUCT_SUMMARY_TOTAL_PRICE_TEXT="PREZZO TOTALE";
	private static String EMAIL_PRODUCT_SUMMARY_TOTAL_PRICE_VALUE="${totalPrice}";
	private static String EMAIL_FOOTER="Riceverai una mail di conferma quando il tuo ordine sarà pronto per il ritiro - da allora avrai a disposizione 10 giorni per prelevarlo.";
	private static String EMAIL_PRODUCT_BILLING_SUMMARY_ORDERNUMBER="NUMERO ORDINE\n" + 
			"${productId}" + 
			"";
	private static String EMAIL_PRODUCT_BILLING_SUMMARY_DATE="DATA ORDINE\n" + 
			"${date}" + 
			"";
	private static String EMAIL_PRODUCT_BILLING_SUMMARY_PAYMENT="PAGAMENTO\n" + 
			"${payment}" + 
			"";
	private static String EMAIL_PRODUCT_BILLING_SUMMARY_SHIPPING="SPEDIZIONE\n" + 
			"${shipping}" + 
			"";
	private static String EMAIL_PRODUCT_BILLING_SUMMARY_BILLINGADDRESS="INDIRIZZO\n" + 
			"SPEDIZIONE\n" + 
			"${nameSurname}\n" + 
			"${address}\n" + 
			"${city}, ${cap}, IT\n" + 
			//"+39${phone}" + 
			"";
	private static String EMAIL_PRODUCT_BILLING_SUMMARY_BILLINGADDRESS_STORE="INDIRIZZO\n" + 
			"SPEDIZIONE\n" + 
			"${nameSurname}\n" + 
			"${storeName}\n" + 
			"${storeCity}, ${storeCap}, IT\n" + 
			//"+39${phone}" + 
			"";
	private static String EMAIL_PRODUCT_BILLING_SUMMARY_DELIVERYADDRESS="INDIRIZZO\n" + 
			"FATTURAZIONE\n" + 
			"${nameSurname}\n" + 
			"${address}\n" + 
			"${city}, ${cap}, IT\n" + 
			//"+39${phone}" + 
			"";
	private static String EMAIL_PICKUP_STORE="NEGOZIO DI\n" + 
			"RITIRO\n" + 
			"${nameSurname}\n" + 
			"${storeName}\n" + 
			"${storeCity}, ${storeCap}, IT" + 
			"";
	
	private WebDriver driver;
	private WebElement mailBody;
	private WebElement welcomeHeader;
	private WebElement riepilogoHeader;
	private WebElement helpSection;
	private WebElement productList;
	private WebElement productSummary;
	private WebElement productBillingSummary;
	private WebElement productFooter;
	private WebElement productListHeader;
	private WebElement subTotalOrder;
	private WebElement subTotalOrderValue;
	private WebElement shipping;
	private WebElement totalOrderText;
	private WebElement totalOrderValue;
	private WebElement orderDetailsHeader;
	private WebElement orderNumber;
	private WebElement dateOrder;
	
	private SimpleDateFormat formatDate=new SimpleDateFormat("yyyy-MM-dd");
	private WebElement payments;
	private WebElement oderDelivery;
	private WebElement orderInvoice;
	private WebElement storeShipping;
	private WebElement emailFooter;

	public EmailTemplateItaly(WebDriver driver) {
		super();
		this.driver = driver;
	}

	@Override
	public void checkMail(EmailTemplateBean b) 
	{
		if(!DinamicData.getIstance().get(CommonFields.COUNTRY).equals("ita"))
			return;
		
		/*### INIZIO CONTROLLO MAIL ###*/
		RiepilogoBean body=(RiepilogoBean) b;
		welcomeHeader=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_WELCOME.getWebLocator()));
		Assert.assertTrue(welcomeHeader != null);
		
		String actual=welcomeHeader.getText().trim();
		String expected=Utility.replacePlaceHolders(this.EMAIL_WELCOME, new Entry("name",body.getName()),
																		new Entry("surname",body.getSurname()));
		
		Assert.assertTrue("controllo Welcome header\nattuale:\n"+actual+"\n\natteso:\n"+expected,actual.contains(expected));
		
		riepilogoHeader=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_RIEPILOGO_ORDINE.getWebLocator()));
		Assert.assertTrue(riepilogoHeader != null);
		
		actual=riepilogoHeader.getText().trim();
		expected=this.EMAIL_RIEPILOGO_ORDINE;
		
		Assert.assertTrue("controllo riepilogo button\nattuale:\n"+actual+"\n\natteso:\n"+expected,actual.contains(expected));
		
		helpSection=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_HELP_SUBSECTION.getWebLocator()));
		Assert.assertTrue(helpSection != null);
		
		actual=helpSection.getText().trim();
		expected=this.EMAIL_HELP_SUBSECTION;
		
		Assert.assertTrue("controllo sezione 'hai bisogno di aiuto?'\nattuale:\n"+actual+"\n\natteso:\n"+expected,actual.contains(expected));
		
		productListHeader=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_LIST_HEADER.getWebLocator()));
		Assert.assertTrue(productListHeader != null);
		
		actual=productListHeader.getText().trim();
		expected=this.EMAIL_PRODUCT_LIST_HEADER;
		
		Assert.assertTrue("controllo header 'Prodotti ordinati'\nattuale:\n"+actual+"\n\natteso:\n"+expected,actual.contains(expected));
		
		UIUtils.ui().scrollToElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_LIST.getWebLocator()));
		
		for(SearchItemBean item : body.getItems())
		{
			boolean find=false;
			
			String detailsExpected=Utility.replacePlaceHolders(this.EMAIL_PRODUCT_ELEMENT_DETAILS, new Entry("productName",item.getName()),
																								   new Entry("productId",item.getNumItem()),
																								   new Entry("productBrand",item.getBrand()),
																								   new Entry("productSize",item.getSize()),
																								   new Entry("productColor",item.getColor()),
																								   new Entry("productPrice",item.getPrice()),
																								   new Entry("productQuantity",item.getQuantity())).toLowerCase();

			List<WebElement> products=driver.findElements(By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_LIST.getWebLocator()));
			
			for(int i=0;i<products.size();i++)
			{
				String detailsActual=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_ELEMENT_DETAILS.getWebLocator().replace("__index__", ""+(i+1)))).getText().trim().toLowerCase();
				String totalPriceActual=driver.findElement(By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_ELEMENT_TOTAL.getWebLocator().replace("__index__", ""+(i+1)))).getText().trim();
				String totalPriceExpected=item.getTotalPrice();
				
				if(detailsActual.contains(detailsExpected) && totalPriceActual.contains(totalPriceExpected))
				{
					find=true;
					break;
				}
			}
			
			Assert.assertTrue("controllo dettaglio prodotto per\n"+detailsExpected,find);
		}
		
//		subTotalOrder=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_SUMMARY_SUBTOTAL_TEXT.getWebLocator()));
//		Assert.assertTrue(subTotalOrder != null);
//		
//		actual=subTotalOrder.getText().trim().toLowerCase();
//		expected=this.EMAIL_PRODUCT_SUMMARY_SUBTOTAL_TEXT.toLowerCase();
//		
//		Assert.assertTrue("controllo sezione 'Subtotale'\nattuale:\n"+actual+"\n\natteso:\n"+expected,actual.contains(expected));
//		
//		subTotalOrderValue=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_SUMMARY_SUBTOTAL_VALUE.getWebLocator()));
//		Assert.assertTrue(subTotalOrderValue != null);
//		
//		actual=subTotalOrderValue.getText().trim().toLowerCase();
//		expected=Utility.replacePlaceHolders(this.EMAIL_PRODUCT_SUMMARY_SUBTOTAL_VALUE, new Entry("productSubTotalPrice",body.getSubTotal())).toLowerCase();
//		
//		Assert.assertTrue("controllo valore 'Subtotale'\nattuale:\n"+actual+"\n\natteso:\n"+expected,actual.contains(expected));
//		
//		shipping=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_SUMMARY_DELIVERY_TEXT.getWebLocator()));
//		Assert.assertTrue(shipping != null);
		
//		actual=shipping.getText().trim().toLowerCase();
//		expected=Utility.replacePlaceHolders(this.EMAIL_PRODUCT_SUMMARY_DELIVERY_TEXT, new Entry("shipping",body.getShipping())).toLowerCase();
//		
//		Assert.assertTrue("controllo sezione 'Spedizione'\nattuale:\n"+actual+"\n\natteso:\n"+expected,actual.contains(expected));
		
		totalOrderText=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_SUMMARY_TOTAL_PRICE_TEXT.getWebLocator()));
		Assert.assertTrue(totalOrderText != null);
		
		actual=totalOrderText.getText().trim().toLowerCase();
		expected=Utility.replacePlaceHolders(this.EMAIL_PRODUCT_SUMMARY_TOTAL_PRICE_TEXT, new Entry("productSubTotalPrice",body.getSubTotal())).toLowerCase();
		
		Assert.assertTrue("controllo sezione 'Prezzo totale'\nattuale:\n"+actual+"\n\natteso:\n"+expected,actual.contains(expected));
		
		totalOrderValue=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_SUMMARY_TOTAL_PRICE_VALUE.getWebLocator()));
		Assert.assertTrue(totalOrderValue != null);
		
		actual=totalOrderValue.getText().trim().toLowerCase();
		expected=Utility.replacePlaceHolders(this.EMAIL_PRODUCT_SUMMARY_TOTAL_PRICE_VALUE, new Entry("totalPrice",body.getTotal())).toLowerCase();
		
		Assert.assertTrue("controllo valore 'Prezzo totale'\nattuale:\n"+actual+"\n\natteso:\n"+expected,actual.contains(expected));
		
		orderDetailsHeader=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_BILLING_SUMMARY_HEADER.getWebLocator()));
		Assert.assertTrue(orderDetailsHeader != null);
		
		actual=orderDetailsHeader.getText().trim().toLowerCase();
		expected=this.EMAIL_RIEPILOGO_ORDINE.toLowerCase();
		
		Assert.assertTrue("controllo sezione 'Riepilogo Ordine'\nattuale:\n"+actual+"\n\natteso:\n"+expected,actual.contains(expected));
		
		orderNumber=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_BILLING_SUMMARY_ORDERNUMBER.getWebLocator()));
		Assert.assertTrue(orderNumber != null);
		
		actual=orderNumber.getText().trim().toLowerCase();
		expected=Utility.replacePlaceHolders(this.EMAIL_PRODUCT_BILLING_SUMMARY_ORDERNUMBER, new Entry("productId",body.getOrderNumber())).toLowerCase().replace("numero ordine: ", "");
		
		Assert.assertTrue("controllo sezione 'Numero Ordine'\nattuale:\n"+actual+"\n\natteso:\n"+expected,actual.contains(expected));
	
		dateOrder=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_BILLING_SUMMARY_DATE.getWebLocator()));
		Assert.assertTrue(dateOrder != null);
		
		actual=dateOrder.getText().trim().toLowerCase();
		
		Calendar c=Calendar.getInstance();
		
		Date d=c.getTime();
		
		expected=Utility.replacePlaceHolders(this.EMAIL_PRODUCT_BILLING_SUMMARY_DATE, new Entry("date",formatDate.format(d))).toLowerCase();
		
		Assert.assertTrue("controllo sezione 'Data Ordine'\nattuale:\n"+actual+"\n\natteso:\n"+expected,actual.contains(expected));
		
		payments=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_BILLING_SUMMARY_PAYMENT.getWebLocator()));
		Assert.assertTrue(payments != null);
		
		actual=payments.getText().trim().toLowerCase();
		
		expected=Utility.replacePlaceHolders(this.EMAIL_PRODUCT_BILLING_SUMMARY_PAYMENT, new Entry("payment",body.getPaymentMethod())).toLowerCase();
		
		Assert.assertTrue("controllo sezione 'Pagamento'\nattuale:\n"+actual+"\n\natteso:\n"+expected,actual.contains(expected));
	
		shipping=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_BILLING_SUMMARY_SHIPPING.getWebLocator()));
		Assert.assertTrue(shipping != null);
		
		actual=shipping.getText().trim().toLowerCase();
		
		expected=Utility.replacePlaceHolders(this.EMAIL_PRODUCT_BILLING_SUMMARY_SHIPPING, new Entry("shipping",body.getShipping())).toLowerCase();
		
		Assert.assertTrue("controllo sezione 'Spedizione'\nattuale:\n"+actual+"\n\natteso:\n"+expected,actual.contains(expected));
	
		oderDelivery=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_BILLING_SUMMARY_DELIVERYADDRESS.getWebLocator()));
		Assert.assertTrue(oderDelivery != null);
		
		actual=oderDelivery.getText().trim().toLowerCase();
		String[] delivery=null;
		String city="";
		String cap="";
		
		switch(body.getHomeOrStore().toLowerCase())
		{
		case "home":
			delivery=body.getDeliveryAddress().split("\\n");
			
			break;
		case "store":
			delivery=body.getBillingAddress().split("\\n");
			break;
		}
		
		city=delivery[2].split(",")[0];
		cap=delivery[2].split(",")[1].trim().split(" ")[1];
		
		expected=Utility.replacePlaceHolders(this.EMAIL_PRODUCT_BILLING_SUMMARY_DELIVERYADDRESS, new Entry("nameSurname",delivery[0].trim()),
																								 new Entry("address",delivery[1].trim()),
																								 new Entry("city",city),
																								 new Entry("cap",cap)/*,
																								 new Entry("phone",body.getPhone())*/)
				.toLowerCase();
		
		Assert.assertTrue("controllo sezione 'Indirizzo Spedizione'\nattuale:\n"+actual+"\n\natteso:\n"+expected,actual.contains(expected) && actual.contains(body.getPhone()));
	
		orderInvoice=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_BILLING_SUMMARY_BILLINGADDRESS.getWebLocator()));
		Assert.assertTrue(orderInvoice != null);
		actual=orderInvoice.getText().trim().toLowerCase();
		
		switch(body.getHomeOrStore().toLowerCase())
		{
		case "home":
			delivery=body.getDeliveryAddress().split("\\n");
			city=delivery[2].split(",")[0];
			cap=delivery[2].split(",")[1].trim().split(" ")[1];
			
			expected=Utility.replacePlaceHolders(this.EMAIL_PRODUCT_BILLING_SUMMARY_BILLINGADDRESS, new Entry("nameSurname",delivery[0].trim()),
																									 new Entry("address",delivery[1].trim()),
																									 new Entry("city",city),
																									 new Entry("cap",cap)/*,
																									 new Entry("phone",body.getPhone())*/)
					.toLowerCase();
			
			Assert.assertTrue("controllo sezione 'Indirizzo fatturazione'\nattuale:\n"+actual+"\n\natteso:\n"+expected,actual.contains(expected) && actual.contains(body.getPhone()));
		
			break;
		case "store":
			delivery=body.getDeliveryAddress().split("\\n");
			expected=Utility.replacePlaceHolders(this.EMAIL_PRODUCT_BILLING_SUMMARY_BILLINGADDRESS_STORE, new Entry("nameSurname",delivery[0].trim()),
																		  new Entry("storeName",body.getStore().getDetails()),
																		  new Entry("storeCity",body.getStore().getStoreCity()),
																		  new Entry("storeCap",body.getStore().getStoreCap()))
					.toLowerCase();
			
			Assert.assertTrue("controllo sezione 'indirizzo di spedizione'\nattuale:\n"+actual+"\n\natteso:\n"+expected,actual.contains(expected));
			
			storeShipping=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_BILLING_SUMMARY_BILLINGADDRESS_STORE.getWebLocator()));
			Assert.assertTrue(storeShipping != null);
			
			actual=storeShipping.getText().trim().toLowerCase();
			expected=Utility.replacePlaceHolders(this.EMAIL_PICKUP_STORE, new Entry("nameSurname",delivery[0].trim()),
																										  new Entry("storeName",body.getStore().getDetails()),
																										  new Entry("storeCity",body.getStore().getStoreCity()),
																										  new Entry("storeCap",body.getStore().getStoreCap()))
					.toLowerCase();
			
			Assert.assertTrue("controllo sezione 'negozio di ritiro'\nattuale:\n"+actual+"\n\natteso:\n"+expected,actual.contains(expected));
			
//			emailFooter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_FOOTER.getWebLocator()));
//			Assert.assertTrue(emailFooter != null);
//			
//			actual=emailFooter.getText().trim().toLowerCase();
//			expected=this.EMAIL_FOOTER.toLowerCase();
//			
//			Assert.assertTrue("controllo sezione 'footer'\nattuale:\n"+actual+"\n\natteso:\n"+expected,actual.contains(expected));
//			
			break;
		}

		/*### FINE CONTROLLO MAIL ###*/
	}

	@Override
	protected void load() 
	{
		PageFactory.initElements(driver, this);
		
	}

	@Override
	protected void isLoaded() throws Error 
	{
		if(!DinamicData.getIstance().get(CommonFields.COUNTRY).equals("ita"))
			return;
		
//		mailBody=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_BODY.getWebLocator()));
//		Assert.assertTrue(mailBody != null);
		welcomeHeader=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_WELCOME.getWebLocator()));
		Assert.assertTrue(welcomeHeader != null);
		riepilogoHeader=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_RIEPILOGO_ORDINE.getWebLocator()));
		Assert.assertTrue(riepilogoHeader != null);
		helpSection=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_HELP_SUBSECTION.getWebLocator()));
		Assert.assertTrue(helpSection != null);
//		mailBody=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_BODY.getWebLocator()));
//		Assert.assertTrue(mailBody != null);
		productList=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_LIST.getWebLocator()));
		Assert.assertTrue(productList != null);
//		productSummary=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_SUMMARY.getWebLocator()));
//		Assert.assertTrue(productSummary != null);
//		productBillingSummary=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_BILLING_SUMMARY.getWebLocator()));
//		Assert.assertTrue(productBillingSummary != null);
//		productFooter=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.EmailTemplate.EMAIL_PRODUCT_FOOTER.getWebLocator()));
//		Assert.assertTrue(productFooter != null);
	}
	
}
