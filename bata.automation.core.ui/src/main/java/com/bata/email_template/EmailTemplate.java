package com.bata.email_template;

import bean.datatable.EmailTemplateBean;
import bean.datatable.RiepilogoBean;

public interface EmailTemplate 
{
	public void checkMail(EmailTemplateBean body);
}
