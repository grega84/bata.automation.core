package com.bata.email_template;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.bata.pagepattern.DinamicData;
import com.bata.pagepattern.Entry;
import com.bata.pagepattern.Locators;
import com.bata.pagepattern.Utility;

import bean.datatable.ContactBean;
import bean.datatable.EmailTemplateBean;
import bean.datatable.RiepilogoBean;
import bean.datatable.SearchItemBean;
import common.CommonFields;
import test.automation.core.UIUtils;

public class SendRequestEmailTemplateItaly extends LoadableComponent<SendRequestEmailTemplateItaly> implements EmailTemplate
{
	private static final String MAIL_WELCOME="Ciao ${name},";
	private static final String MAIL_DESCRIPTION="Abbiamo ricevuto il tuo messaggio, il nostro servizio clienti ti risponderà al più presto.";
	private static final String MAIL_DESCRIPTION_TNKS="Grazie per averci contattato.";
	private static final String MAIL_I_TUOI_DATI_HEADER="I tuoi dati:";
	private static final String MAIL_NAME_FIELD_TEXT="Nome:";
	private static final String MAIL_EMAIL_FIELD_TEXT="Email:";
	private static final String MAIL_PHONE_FIELD_TEXT="Telefono:";
	private static final String MAIL_REASON_FIELD_TEXT="Motivazione:";
	private static final String MAIL_MESSAGE_FIELD_TEXT="Messaggio:";
	private static final String MAIL_NAME_FIELD_VALUE="${name} ${surname}";
	private static final String MAIL_PHONE_FIELD_VALUE="+39${phone}";
	
	private WebDriver driver;
	private WebElement mailBody;
	private WebElement mailWelcome;
	private WebElement mailDescription;
	private WebElement mailThanks;
	private WebElement mailDatiHeader;
	private WebElement mailNameText;
	private WebElement mailNameValue;
	private WebElement mailEmailText;
	private WebElement mailEmailValue;
	private WebElement mailPhoneText;
	private WebElement mailPhoneValue;
	private WebElement mailReasonText;
	private WebElement mailReasonValue;
	private WebElement mailMessageText;
	private WebElement mailMessageValue;

	public SendRequestEmailTemplateItaly(WebDriver driver) {
		super();
		this.driver = driver;
	}

	@Override
	public void checkMail(EmailTemplateBean b) 
	{
		if(!DinamicData.getIstance().get(CommonFields.COUNTRY).equals("ita"))
			return;
		
		/*### INIZIO CONTROLLO MAIL ###*/
		ContactBean bean=(ContactBean) b;
		mailWelcome=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SendRequestMailTemplate.MAIL_WELCOME.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		String actual=mailWelcome.getText().trim();
		String expected=Utility.replacePlaceHolders(this.MAIL_WELCOME, new Entry("name",bean.getName()));
		
		Assert.assertTrue("controllo mail header; attuale:"+actual+"\natteso:"+expected,actual.contains(expected));

		mailDescription=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SendRequestMailTemplate.MAIL_DESCRIPTION.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		actual=mailDescription.getText().trim();
		expected=this.MAIL_DESCRIPTION;
		
		Assert.assertTrue("controllo mail description; attuale:"+actual+"\natteso:"+expected,actual.contains(expected));

		mailThanks=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SendRequestMailTemplate.MAIL_DESCRIPTION_TNKS.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		actual=mailThanks.getText().trim();
		expected=this.MAIL_DESCRIPTION_TNKS;
		
		Assert.assertTrue("controllo mail thanks; attuale:"+actual+"\natteso:"+expected,actual.contains(expected));

		mailDatiHeader=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SendRequestMailTemplate.MAIL_I_TUOI_DATI_HEADER.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		actual=mailDatiHeader.getText().trim();
		expected=this.MAIL_I_TUOI_DATI_HEADER;
		
		Assert.assertTrue("controllo mail dati header; attuale:"+actual+"\natteso:"+expected,actual.contains(expected));

		mailNameText=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SendRequestMailTemplate.MAIL_NAME_FIELD_TEXT.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		actual=mailNameText.getText().trim();
		expected=this.MAIL_NAME_FIELD_TEXT;
		
		Assert.assertTrue("controllo mail name text; attuale:"+actual+"\natteso:"+expected,actual.contains(expected));

		mailNameValue=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SendRequestMailTemplate.MAIL_NAME_FIELD_VALUE.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		actual=mailNameValue.getText().trim();
		expected=Utility.replacePlaceHolders(this.MAIL_NAME_FIELD_VALUE, new Entry("name",bean.getName()), new Entry("surname",bean.getSurname()));
		
		Assert.assertTrue("controllo mail name value; attuale:"+actual+"\natteso:"+expected,actual.contains(expected));

		mailEmailText=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SendRequestMailTemplate.MAIL_EMAIL_FIELD_TEXT.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		actual=mailEmailText.getText().trim();
		expected=this.MAIL_EMAIL_FIELD_TEXT;
		
		Assert.assertTrue("controllo mail email text; attuale:"+actual+"\natteso:"+expected,actual.contains(expected));

		mailEmailValue=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SendRequestMailTemplate.MAIL_EMAIL_FIELD_VALUE.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		actual=mailEmailValue.getText().trim();
		expected=bean.getEmail();
		
		Assert.assertTrue("controllo mail email value; attuale:"+actual+"\natteso:"+expected,actual.contains(expected));

		mailPhoneText=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SendRequestMailTemplate.MAIL_PHONE_FIELD_TEXT.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		actual=mailPhoneText.getText().trim();
		expected=this.MAIL_PHONE_FIELD_TEXT;
		
		Assert.assertTrue("controllo mail phone text; attuale:"+actual+"\natteso:"+expected,actual.contains(expected));

		mailPhoneValue=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SendRequestMailTemplate.MAIL_PHONE_FIELD_VALUE.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		actual=mailPhoneValue.getText().trim();
		expected=Utility.replacePlaceHolders(this.MAIL_PHONE_FIELD_VALUE, new Entry("phone",bean.getPhone()));
		
		Assert.assertTrue("controllo mail phone value; attuale:"+actual+"\natteso:"+expected,actual.contains(expected));

		mailReasonText=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SendRequestMailTemplate.MAIL_REASON_FIELD_TEXT.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		actual=mailReasonText.getText().trim();
		expected=this.MAIL_REASON_FIELD_TEXT;
		
		Assert.assertTrue("controllo mail reason text; attuale:"+actual+"\natteso:"+expected,actual.contains(expected));

		mailReasonValue=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SendRequestMailTemplate.MAIL_REASON_FIELD_VALUE.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		actual=mailReasonValue.getText().trim();
		expected=bean.getReason().toLowerCase();
		
		Assert.assertTrue("controllo mail reason value; attuale:"+actual+"\natteso:"+expected,actual.contains(expected));

		mailMessageText=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SendRequestMailTemplate.MAIL_MESSAGE_FIELD_TEXT.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		actual=mailMessageText.getText().trim();
		expected=this.MAIL_MESSAGE_FIELD_TEXT;
		
		Assert.assertTrue("controllo mail message text; attuale:"+actual+"\natteso:"+expected,actual.contains(expected));

		mailMessageValue=UIUtils.ui().scrollAndReturnElementLocatedBy(driver, By.xpath(Locators.SendRequestMailTemplate.MAIL_MESSAGE_FIELD_VALUE.getWebLocator()));
		try {Thread.sleep(500);}catch(Exception err) {}
		
		actual=mailMessageValue.getText().trim();
		expected=bean.getComment();
		
		Assert.assertTrue("controllo mail message value; attuale:"+actual+"\natteso:"+expected,actual.contains(expected));

		/*### FINE CONTROLLO MAIL ###*/
	}

	@Override
	protected void load() 
	{
		PageFactory.initElements(driver, this);
		
	}

	@Override
	protected void isLoaded() throws Error 
	{
		if(!DinamicData.getIstance().get(CommonFields.COUNTRY).equals("ita"))
			return;
		
		mailBody=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SendRequestMailTemplate.MAIL_BODY.getWebLocator())), 60);
		mailWelcome=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SendRequestMailTemplate.MAIL_WELCOME.getWebLocator())), 60);
		mailDescription=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SendRequestMailTemplate.MAIL_DESCRIPTION.getWebLocator())), 60);
		mailThanks=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SendRequestMailTemplate.MAIL_DESCRIPTION_TNKS.getWebLocator())), 60);
		mailDatiHeader=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SendRequestMailTemplate.MAIL_I_TUOI_DATI_HEADER.getWebLocator())), 60);
		mailNameText=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SendRequestMailTemplate.MAIL_NAME_FIELD_TEXT.getWebLocator())), 60);
		mailNameValue=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SendRequestMailTemplate.MAIL_NAME_FIELD_VALUE.getWebLocator())), 60);
		mailEmailText=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SendRequestMailTemplate.MAIL_EMAIL_FIELD_TEXT.getWebLocator())), 60);
		mailEmailValue=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SendRequestMailTemplate.MAIL_EMAIL_FIELD_VALUE.getWebLocator())), 60);
		mailPhoneText=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SendRequestMailTemplate.MAIL_PHONE_FIELD_TEXT.getWebLocator())), 60);
		mailPhoneValue=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SendRequestMailTemplate.MAIL_PHONE_FIELD_VALUE.getWebLocator())), 60);
		mailReasonText=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SendRequestMailTemplate.MAIL_REASON_FIELD_TEXT.getWebLocator())), 60);
		mailReasonValue=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SendRequestMailTemplate.MAIL_REASON_FIELD_VALUE.getWebLocator())), 60);
		mailMessageText=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SendRequestMailTemplate.MAIL_MESSAGE_FIELD_TEXT.getWebLocator())), 60);
		mailMessageValue=UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SendRequestMailTemplate.MAIL_MESSAGE_FIELD_VALUE.getWebLocator())), 60);
		
		Assert.assertTrue(mailBody != null);
		Assert.assertTrue(mailWelcome != null);
		Assert.assertTrue(mailDescription != null);
		Assert.assertTrue(mailThanks != null);
		Assert.assertTrue(mailDatiHeader != null);
		Assert.assertTrue(mailNameText != null);
		Assert.assertTrue(mailNameValue != null);
		Assert.assertTrue(mailEmailText != null);
		Assert.assertTrue(mailEmailValue != null);
		Assert.assertTrue(mailPhoneText != null);
		Assert.assertTrue(mailPhoneValue != null);
		Assert.assertTrue(mailReasonText != null);
		Assert.assertTrue(mailReasonValue != null);
		Assert.assertTrue(mailMessageText != null);
		Assert.assertTrue(mailMessageValue != null);
	}
	
}
