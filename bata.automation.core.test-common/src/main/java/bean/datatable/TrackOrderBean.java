package bean.datatable;

public class TrackOrderBean 
{
	 private String orderId;
	 private String emailAddress;
	 private String orderDetailHeader;
	 private String orderStatus;
	 private String orderNumber;
	 private String contactData;
	 private String deliveryAddress;
	 private String billingAddress;
	 private String payment;
	 private String totalPrice;
	 private String productName;
	 private String productBrand;
	 private String productId;
	 private String productSize;
	 private String productColor;
	 private String productQuantity;
	 private String productPrice;
	 private String withError;
	 private String trackErrorMessage;
	 private String registeredUser;
	 
	 
	public String getRegisteredUser() {
		return registeredUser;
	}
	public void setRegisteredUser(String registeredUser) {
		this.registeredUser = registeredUser;
	}
	public String getTrackErrorMessage() {
		return trackErrorMessage;
	}
	public void setTrackErrorMessage(String trackErrorMessage) {
		this.trackErrorMessage = trackErrorMessage;
	}
	public String getWithError() {
		return withError;
	}
	public void setWithError(String withError) {
		this.withError = withError;
	}
	public String getOrderDetailHeader() {
		return orderDetailHeader;
	}
	public void setOrderDetailHeader(String orderDetailHeader) {
		this.orderDetailHeader = orderDetailHeader;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getContactData() {
		return contactData;
	}
	public void setContactData(String contactData) {
		this.contactData = contactData;
	}
	public String getDeliveryAddress() {
		return deliveryAddress;
	}
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	public String getBillingAddress() {
		return billingAddress;
	}
	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}
	public String getPayment() {
		return payment;
	}
	public void setPayment(String payment) {
		this.payment = payment;
	}
	public String getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductBrand() {
		return productBrand;
	}
	public void setProductBrand(String productBrand) {
		this.productBrand = productBrand;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductSize() {
		return productSize;
	}
	public void setProductSize(String productSize) {
		this.productSize = productSize;
	}
	public String getProductColor() {
		return productColor;
	}
	public void setProductColor(String productColor) {
		this.productColor = productColor;
	}
	public String getProductQuantity() {
		return productQuantity;
	}
	public void setProductQuantity(String productQuantity) {
		this.productQuantity = productQuantity;
	}
	public String getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(String productPrice) {
		this.productPrice = productPrice;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
}
