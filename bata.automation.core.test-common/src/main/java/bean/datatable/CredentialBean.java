package bean.datatable;

public class CredentialBean {
private String password;
private String email;
private String country;
private String newPassword;
private String authUsername;
private String authPassword;


	public String getAuthUsername() {
	return authUsername;
}
public void setAuthUsername(String authUsername) {
	this.authUsername = authUsername;
}
public String getAuthPassword() {
	return authPassword;
}
public void setAuthPassword(String authPassword) {
	this.authPassword = authPassword;
}
	public String getNewPassword() {
	return newPassword;
}
public void setNewPassword(String newPassword) {
	this.newPassword = newPassword;
}
	public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	
}
