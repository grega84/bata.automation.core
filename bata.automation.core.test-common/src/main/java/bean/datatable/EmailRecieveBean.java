package bean.datatable;

public class EmailRecieveBean 
{
	private String emailAddress;
	private String emailSubject;
	private String homeOrStore;
	private String driverMailConfig;
	
	
	
	public String getDriverMailConfig() {
		return driverMailConfig;
	}
	public void setDriverMailConfig(String driverMailConfig) {
		this.driverMailConfig = driverMailConfig;
	}
	public String getHomeOrStore() {
		return homeOrStore;
	}
	public void setHomeOrStore(String homeOrStore) {
		this.homeOrStore = homeOrStore;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getEmailSubject() {
		return emailSubject;
	}
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}
	
	
}
