package bean.datatable;

public class FindAStoreProductBean 
{
	public static final String GEOIP="geoip";
	public static final String CITY="city";
	public static final String ADDRESS="address";
	public static final String POSTALCODE="postalcode";
	public static final String WRONG="wrong";
	
	private String criteria;
	private String key;
	
	public String getCriteria() {
		return criteria;
	}
	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	
	
}
