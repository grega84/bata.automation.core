package bean.datatable;

public class ProductBean {
private String productName;
private String gender;
private String color;
private String category;
private String material;
private String quantity;
private String size;
private String brand;
private String categoryName;
private String bataID;
private String multiItems;

public String getMultiItems() {
	return multiItems;
}
public void setMultiItems(String multiItems) {
	this.multiItems = multiItems;
}
//update
public String getProductName() {
	return productName;
}
public void setProductName(String productName) {
	this.productName = productName;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public String getColor() {
	return color;
}
public void setColor(String color) {
	this.color = color;
}
public String getCategory() {
	return category;
}
public void setCategory(String category) {
	this.category = category;
}
public String getMaterial() {
	return material;
}
public void setMaterial(String material) {
	this.material = material;
}
public String getQuantity() {
	return quantity;
}
public void setQuantity(String quantity) {
	this.quantity = quantity;
}
public String getSize() {
	return size;
}
public void setSize(String size) {
	this.size = size;
}
public String getBrand() {
	return brand;
}
public void setBrand(String brand) {
	this.brand = brand;
}
public String getCategoryName() {
	return categoryName;
}
public void setCategoryName(String categoryName) {
	this.categoryName = categoryName;
}
public String getBataID() {
	return bataID;
}
public void setBataID(String bataID) {
	this.bataID = bataID;
}



}
