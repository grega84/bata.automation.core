package bean.datatable;

public class CreditCardErrorCheckBean extends PaymentBean
{
	public static final String ALL_FIELD_EMPTY="all_field_empty";
	public static final String CARD_NUMBER_LESS_THAN_16="number_less_than_16";
	public static final String CARD_NUMBER_GREATER_THAN_16="number_greater_than_16";
	public static final String CARD_NUMBER_WITH_ALFANUMERIC="number_with_alpha";
	public static final String CARD_OWNER_WITH_ONE_CHAR="owner_with_one_char";
	public static final String CARD_OWNER_WITH_ONLY_DIGITS="owner_with_only_digits";
	public static final String CARD_CVV_LESS_THAN_3="cvv_less_than_3";
	public static final String CARD_CVV_ALPHANUMERIC="cvv_alphanumeric";
	
	private String errorCheck;
	private String value;
	private String errorMessage;
	
	
	public String getErrorCheck() {
		return errorCheck;
	}
	public void setErrorCheck(String errorCheck) {
		this.errorCheck = errorCheck;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
}
