package bean.datatable;

public class DatiDiContattoBean 
{
	private String email;
	private String phone;
	private String submitPassword;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getSubmitPassword() {
		return submitPassword;
	}
	public void setSubmitPassword(String submitPassword) {
		this.submitPassword = submitPassword;
	}
	
}
