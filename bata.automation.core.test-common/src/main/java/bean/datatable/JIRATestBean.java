package bean.datatable;

public class JIRATestBean 
{
	private String testIdJIRA;
	private String typeOfTest;
	private String driverConfig;
	private String driverMailConfig;

	public String getTestIdJIRA() {
		return testIdJIRA;
	}

	public void setTestIdJIRA(String testIdJIRA) {
		this.testIdJIRA = testIdJIRA;
	}
	
	public String getTypeOfTest() {
		return typeOfTest;
	}
	public void setTypeOfTest(String typeOfTest) {
		this.typeOfTest = typeOfTest;
	}

	public String getDriverConfig() {
		
		return driverConfig;
	}

	public String getDriverMailConfig() {
		
		return driverMailConfig;
	}

	public void setDriverConfig(String driverConfig) {
		this.driverConfig = driverConfig;
	}

	public void setDriverMailConfig(String driverMailConfig) {
		this.driverMailConfig = driverMailConfig;
	}
}
