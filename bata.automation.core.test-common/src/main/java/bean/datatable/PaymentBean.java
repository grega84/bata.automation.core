package bean.datatable;

public class PaymentBean {

	protected String voucher;
	protected String coupon;
	protected String paymentMethod;
	protected String paymentType;
	protected String cardNumber;
	protected String cvv;
	protected String monthExpiredDate;
	protected String yearExpiredDate;
	protected String ownerCard;
	protected String emailPaypal;
	protected String passwordPaypal;
	protected String dataStore;
	protected String emailAddress;
	protected String phone;
	protected String name;
	protected String surname; 
	protected String address; 
	protected String cap; 
	protected String city;
	protected String province; 
	protected String country;
	protected String homeOrStore;
	
	
	public String getHomeOrStore() {
		return homeOrStore;
	}
	public void setHomeOrStore(String homeOrStore) {
		this.homeOrStore = homeOrStore;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCap() {
		return cap;
	}
	public void setCap(String cap) {
		this.cap = cap;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getDataStore() {
		return dataStore;
	}
	public void setDataStore(String dataStore) {
		this.dataStore = dataStore;
	}
	public String getVoucher() {
		return voucher;
	}
	public void setVoucher(String voucher) {
		this.voucher = voucher;
	}
	public String getCoupon() {
		return coupon;
	}
	public void setCoupon(String coupon) {
		this.coupon = coupon;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getCvv() {
		return cvv;
	}
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	public String getMonthExpiredDate() {
		return monthExpiredDate;
	}
	public void setMonthExpiredDate(String monthExpiredDate) {
		this.monthExpiredDate = monthExpiredDate;
	}
	public String getYearExpiredDate() {
		return yearExpiredDate;
	}
	public void setYearExpiredDate(String yearExpiredDate) {
		this.yearExpiredDate = yearExpiredDate;
	}
	public String getOwnerCard() {
		return ownerCard;
	}
	public void setOwnerCard(String ownerCard) {
		this.ownerCard = ownerCard;
	}
	
	public String getEmailPaypal() {
		return emailPaypal;
	}
	public void setEmailPaypal(String emailPaypal) {
		this.emailPaypal = emailPaypal;
	}
	public String getPasswordPaypal() {
		return passwordPaypal;
	}
	public void setPasswordPaypal(String passwordPaypal) {
		this.passwordPaypal = passwordPaypal;
	}
	


}
