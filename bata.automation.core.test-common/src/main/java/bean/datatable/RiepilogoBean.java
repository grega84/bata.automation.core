package bean.datatable;

import java.util.List;

public class RiepilogoBean implements EmailTemplateBean
{
	private String orderNumber;
	private List<SearchItemBean> items;
	private String shipping;
	private String paymentMethod;
	private String owner;
	private String billingAddress;
	private String deliveryAddress;
	private String subTotal;
	private String total;
	private String name;
	private String surname;
	private String city;
	private String cap;
	private String phone;
	private String ritiroNegozioPrezzo;
	private String confirmMailText;
	private String homeOrStore;
	private StoreBean store;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public List<SearchItemBean> getItems() {
		return items;
	}
	public void setItems(List<SearchItemBean> items) {
		this.items = items;
	}
	public String getShipping() {
		return shipping;
	}
	public void setShipping(String shipping) {
		this.shipping = shipping;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getBillingAddress() {
		return billingAddress;
	}
	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}
	public String getDeliveryAddress() {
		return deliveryAddress;
	}
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	public String getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public void setCity(String city) 
	{
		this.city=city;
	}
	public void setCap(String cap) 
	{
		this.cap=cap;
	}
	public String getCity() {
		return city;
	}
	public String getCap() {
		return cap;
	}
	public void setPhone(String phone) 
	{
		this.phone=phone;
	}
	public String getPhone() {
		return phone;
	}
	public void setRitiroNegozioPrezzo(String price) 
	{
		this.ritiroNegozioPrezzo=price;
	}
	public String getRitiroNegozioPrezzo() {
		return ritiroNegozioPrezzo;
	}
	public void setConfirmMailText(String confirmText) 
	{
		this.confirmMailText=confirmText;
	}
	public String getConfirmMailText() {
		return confirmMailText;
	}
	public void setHomeOrStore(String homeOrStore) 
	{
		this.homeOrStore=homeOrStore;
	}
	public String getHomeOrStore() {
		return homeOrStore;
	}
	public void setStore(StoreBean store) 
	{
		this.store=store;
	}
	public StoreBean getStore() {
		return store;
	}
	
	
}
