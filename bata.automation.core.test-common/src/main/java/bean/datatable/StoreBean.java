package bean.datatable;

public class StoreBean 
{
	private String name;
	private String details;
	private String storeHours;
	private String phone;
	private String storeCap;
	private String storeCity;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getStoreHours() {
		return storeHours;
	}
	public void setStoreHours(String storeHours) {
		this.storeHours = storeHours;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getStoreCap() {
		return storeCap;
	}
	public void setStoreCap(String storeCap) {
		this.storeCap = storeCap;
	}
	public void setStoreCity(String storeCity) 
	{
		this.storeCity=storeCity;
	}
	public String getStoreCity() {
		return storeCity;
	}
	
	public boolean equals(Object o)
	{
		if(!(o instanceof StoreBean))
			return false;
		
		return  this.name.equals(((StoreBean)o).name) &&
				this.details.equals(((StoreBean)o).details) &&
				this.phone.equals(((StoreBean)o).phone) &&
				this.storeCap.equals(((StoreBean)o).storeCap) &&
				this.storeCity.equals(((StoreBean)o).storeCity) &&
				this.storeHours.equals(((StoreBean)o).storeHours);
	}
	
}
