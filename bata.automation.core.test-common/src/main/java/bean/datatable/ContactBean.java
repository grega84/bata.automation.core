package bean.datatable;

public class ContactBean implements EmailTemplateBean
{
private String reason;
private String country;
private String email;
private String phone; 
private String name; 
private String surname;  
private String comment;
private String emailSubject;


public String getEmailSubject() {
	return emailSubject;
}
public void setEmailSubject(String emailSubject) {
	this.emailSubject = emailSubject;
}
public String getReason() {
	return reason;
}
public void setReason(String reason) {
	this.reason = reason;
}
public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPhone() {
	return phone;
}
public void setPhone(String phone) {
	this.phone = phone;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getSurname() {
	return surname;
}
public void setSurname(String surname) {
	this.surname = surname;
}
public String getComment() {
	return comment;
}
public void setComment(String comment) {
	this.comment = comment;
}            


}
