package bean.datatable;

public class ShippingBean {

private String emailAddress;
private String phone;
private String name;
private String surname;
private String address;
private String cap;
private String city;
private String province;
private String country;
private String coupon;
private String voucher;
private String paymentMethod;
private String dataStore;
private String newDataStore;
private String registeredUser;



public String getRegisteredUser() {
	return registeredUser;
}
public void setRegisteredUser(String registeredUser) {
	this.registeredUser = registeredUser;
}
public String getNewDataStore() {
	return newDataStore;
}
public void setNewDataStore(String newDataStore) {
	this.newDataStore = newDataStore;
}
public String getDataStore() {
	return dataStore;
}
public void setDataStore(String dataStore) {
	this.dataStore = dataStore;
}
public String getCoupon() {
	return coupon;
}
public void setCoupon(String coupon) {
	this.coupon = coupon;
}
public String getVoucher() {
	return voucher;
}
public void setVoucher(String voucher) {
	this.voucher = voucher;
}
public String getPaymentMethod() {
	return paymentMethod;
}
public void setPaymentMethod(String paymentMethod) {
	this.paymentMethod = paymentMethod;
}
public String getEmailAddress() {
	return emailAddress;
}
public void setEmailAddress(String emailAddress) {
	this.emailAddress = emailAddress;
}
public String getPhone() {
	return phone;
}
public void setPhone(String phone) {
	this.phone = phone;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getSurname() {
	return surname;
}
public void setSurname(String surname) {
	this.surname = surname;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public String getCap() {
	return cap;
}
public void setCap(String cap) {
	this.cap = cap;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public String getProvince() {
	return province;
}
public void setProvince(String province) {
	this.province = province;
}
public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}


}
