package bean.datatable;


public class RegistrationBean {
	
	private String email;
	private String country;
	private String password;
	private String confirmPassword;
	private String name;
	private String surname;
	private String address;
	private String cap;
	private String city;
	private String province;
	private String phone;
	private String dayBorn;
	private String monthBorn;
	private String yearBorn;
	private String field;
	
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCap() {
		return cap;
	}
	public void setCap(String cap) {
		this.cap = cap;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getDayBorn() {
		return dayBorn;
	}
	public void setDayBorn(String dayBorn) {
		this.dayBorn = dayBorn;
	}
	public String getMonthBorn() {
		return monthBorn;
	}
	public void setMonthBorn(String monthBorn) {
		this.monthBorn = monthBorn;
	}
	public String getYearBorn() {
		return yearBorn;
	}
	public void setYearBorn(String yearBorn) {
		this.yearBorn = yearBorn;
	}
	
	

}
