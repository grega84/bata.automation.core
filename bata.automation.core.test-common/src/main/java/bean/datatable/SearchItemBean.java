package bean.datatable;

public class SearchItemBean 
{
	private String name;
	private String price;
	private String size;
	private String numItem;
	private String brand;
	private String color;
	private String quantity;
	private String totalPrice;
	private String bataID;
	

	public String getBataID() {
		return bataID;
	}
	public void setBataID(String bataID) {
		this.bataID = bataID;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getNumItem() {
		return numItem;
	}
	public void setNumItem(String numItem) {
		this.numItem = numItem;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public void setTotalPrice(String totalPrice) 
	{
		this.totalPrice=totalPrice;
	}
	public String getTotalPrice() {
		return totalPrice;
	}
	
	
}
