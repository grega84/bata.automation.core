package bean.datatable;

public class SearchFilterBean 
{
	private String filterKey;
	private String filterValue;
	private String orderFilter;
	
	public String getOrderFilter() {
		return orderFilter;
	}
	public void setOrderFilter(String orderFilter) {
		this.orderFilter = orderFilter;
	}
	public String getFilterKey() {
		return filterKey;
	}
	public void setFilterKey(String filterKey) {
		this.filterKey = filterKey;
	}
	public String getFilterValue() {
		return filterValue;
	}
	public void setFilterValue(String filterValue) {
		this.filterValue = filterValue;
	}
	
	
}
