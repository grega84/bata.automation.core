package common;

public class WaitManager {
	
	private static WaitManager singleton;
	
	int HIGH = 5000;
	int LONG = 3000;
	int MEDIUM = 2000;
	int SHORT = 1000;

	double mHigh;
	double mLong;
	double mMedium;
	double mShort;
	
	WaitManagerConfig propConf;
	
	private WaitManager() {
		propConf = new WaitManagerConfig();
		
		mHigh = propConf.getHighPropertie();
		mLong = propConf.getLongPropertie();
		mMedium = propConf.getMediumPropertie();
		mShort = propConf.getShortPropertie();
	}
	
	public static WaitManager get() {
		if (singleton==null) {
			singleton = new WaitManager();
		}
		return singleton;
	}
	
	public void waitHighTime() {
		int t = (int) (mHigh * HIGH);
		
		try {Thread.sleep(t);} catch (Exception err )  {}
	}
	
	public void waitLongTime() {
		int t = (int) (mLong * LONG);
		
		try {Thread.sleep(t);} catch (Exception err )  {}
	}
	
	public void waitMediumTime() {
		int t = (int) (mMedium * MEDIUM);
		
		try {Thread.sleep(t);} catch (Exception err )  {}
	}
	
	public void waitShortTime() {
		int t = (int) (mShort * SHORT);
		
		try {Thread.sleep(t);} catch (Exception err )  {}
	}
}
