package common;

import java.util.Properties;

import test.automation.core.UIUtils;

public class WaitManagerConfig {
		
	double mHigh;
	double mLong;
	double mMedium;
	double mShort;
	
	public WaitManagerConfig() {
		Properties properties = UIUtils.ui().getCurrentProperties();
		
		String sHigh = properties.getProperty("wait.high");
		String sLong = properties.getProperty("wait.long");
		String sMedium = properties.getProperty("wait.medium");
		String sShort = properties.getProperty("wait.short");
		
		mHigh = Double.parseDouble(sHigh);
		mLong = Double.parseDouble(sLong);
		mMedium = Double.parseDouble(sMedium);
		mShort = Double.parseDouble(sShort);
		
	}
	
	public double getHighPropertie() {
		return mHigh;
	}
	
	public double getLongPropertie() {
		return mLong;
	}

	public double getMediumPropertie() {
		return mMedium;
	}

	public double getShortPropertie() {
		return mShort;
	}

}

