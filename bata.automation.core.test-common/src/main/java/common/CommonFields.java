package common;

public class CommonFields 
{
	public static final String SELECTED_ITEM = "SELECTED_ITEM";
	public static final String TEMP_MAIL = "tmpMail";
	public static final String SELECTED_ITEMS = "SELECTED_ITEMS";
	public static final String RECAP_ORDER = "RECAP_ORDER";
	public static final String STORE_FOUND = "STORE_FOUND";
	public static final String NEW_ADDRESS = "NEW_ADDRESS";
	public static final String DATI_DI_CONTATTO = "DATI_DI_CONTATTO";
	public static final String COUNTRY = "COUNTRY";
	public static final String MODIFICA_DATI_PERSONALI = "MODIFICA_DATI_PERSONALI";
	public static final String DATI_PERSONALI = "DATI_PERSONALI";
	public static final String NEW_ADDRESS_FIELDS = "NEW_ADDRESS_FIELDS";
	public static final String TC_NOT_ACCEPTED = "tc-not-accepted";
	public static final String RIEPILOGO_SUBTOTALE = "RIEPILOGO_SUBTOTALE";
	public static final String SELECTED_STORE = "SELECTED_STORE";
	public static final String SELECTED_FILTER = "SELECTED_SEARCH_FILTER";
	public static final String ALL_PRICES = "ALL_PRICES";
	public static final String RANGE = "RANGE";
	public static final String EXPECTED_RESULTS = "EXPECTED_RESULTS";
	public static final String BEFORE_ORDERING = "BEFORE_ORDERING";
	public static final String AUTHLOGIN_IMAGE_FOUND = "AUTHLOGIN_IMAGE_FOUND";
	public static final String INVALID_PHONE_ERROR_CHECK = "INVALID_PHONE_ERROR_CHECK";
	public static final String DRIVER_CONFIG = "DRIVER_CONFIG";
	public static final String DRIVER_MAIL_CONFIG = "DRIVER_MAIL_CONFIG";
	public static final String TYPEOFTEST = "typeOfTest";
	public static final String AUTHLOGIN = "AUTHLOGIN";
	public static final String REGISTERED_USER = "REGISTERED_USER";
	public static final String ORDER_ID = "ORDER_ID";
	
}
